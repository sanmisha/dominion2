<?php //debug($documents->toArray()); exit; ?>
<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 7px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>Document List for <?= $project->project_with_client ?> as on <?= date('d/m/Y H:i a') ?></strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">
					<th width="7%">Sr No</th>
					<th widht="10%">Document</th>
                    <th width="7%">Version</th>
                    <th>Remark</th>
					<th width="15%">Updated by</th>
                    <th width="18%">Updated on</th>
				</tr>
			</thead>
			<tbody>
            <?php
                foreach($documents as $i => $document) {
            ?>
            <tr>
                <td><?= ($i+1) ?></td>
                <td><?= "{$document->drawing_no->drawing_no}/{$document->level->level}/{$document->building->building}" ?></td>
                <td><?= $document->version ?></td>
                <td><?= $document->remarks ?></td>
                <td><?= $document->uploaded_by->name ?></td>
                <td><?= $document->created ?></td>
            </tr>
			<?php } ?>
			</tbody>
		</table>
	</body>
</html>
