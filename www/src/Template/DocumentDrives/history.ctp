<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
</div>
<div class="box box-block bg-white">
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Document</th>
                            <th scope="col">Version</th>
							<th scope="col">Uploaded By</th>
							<th scope="col">Timestamp</th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($documents as $document): ?>
                        <tr>
                            <td><?= $this->Html->link($document->drawing_no->drawing_no ,'/files/documents/document/'.$document->document_dir.'/'.$document->document,['target'=>'_blank']) ?></td>
                            <td><span class="tag tag-info"><?= $document->version ?></span></td>
                            <td><span class="tag tag-info"><?= $document->uploaded_by->name ?></span></td>
                            <td><?= $document->created; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
