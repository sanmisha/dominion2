

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($document, ['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->hidden('project_id'); ?>
                <?php echo $this->Form->control('drawing_no_id'); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('level_id'); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('building_id'); ?>
			</div>
			<div class="col-md-3">
			    <label>Document Attachment</label>
				<?php
					echo $this->Form->control('document',['type'=>'file','label'=>false]);
					echo $this->Form->hidden('document_dir');
					if(!empty($document->document_dir)) {
						echo $this->Html->link($document->document,'/files/documents/document/'.$document->document_dir.'/'.$document->document,['target'=>'_blank']);
					}
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('remarks'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'projects']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

