<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('Documents', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit', $project_id)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('document')?></th>
                            <th scope="col"><?= $this->Paginator->sort('version')?></th>
							<th scope="col"><?= $this->Paginator->sort('uploaded_by') ?></th>
							<th scope="col"><?= $this->Paginator->sort('created', 'Timestamp') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($documentDrives as $document): ?>
                        <tr>
                            <td><?= $this->Html->link("{$document->drawing_no->drawing_no}/{$document->level->level}/{$document->building->building}" ,'/files/documents/document/'.$document->document_dir.'/'.$document->document,['target'=>'_blank']) ?></td>
                            <td><span class="tag tag-info"><?= $document->version ?></span></td>
                            <td><span class="tag tag-info"><?= $document->uploaded_by->name ?></span></td>
                            <td><?= $document->created; ?></td>
							<td class="actions">
                                <?= $this->Html->link('Revise',['controller'=>'DocumentDrives','action'=>'revise',$document->id],['class'=>'btn btn-success btn-sm']); ?>
                                <?= $this->Html->link('History',['controller'=>'DocumentDrives','action'=>'History',$document->id],['class'=>'btn btn-success btn-sm']); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
