<script>
	index = 0;
	index = <?php echo count($projectTypes['project_type_details'])+1; ?>;

	function add_row() {
		html = "<tr id='row_"+index+"'>";
			html += "<td><a href='javascript:void(0); remove_row("+index+");'><i class='fa fa-minus'></i></a></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='project-type-details-"+index+"-task-id' name='project_type_details["+index+"][task_id]' class='form-control'>";
				html += "<option value=''>(Choose One)</option>";
					<?php foreach($tasks as $id=>$task) { ?>
					html += "<option value='<?php echo $id;?>'><?php echo $task;?></option>";
					<?php } ?>
				html += "</select>";
			html += "</div></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='project-type-details-"+index+"-fees-type' name='project_type_details["+index+"][fees_type]' class='form-control'>";
				html += "<option value=''>(Choose One)</option>";
				html += "<option value='Percentage'>Percentage</option>";
				html += "<option value='Lumpsum'>Lumpsum</option>";
                html += "<option value='Lumpsum Retainer'>Lumpsum Retainer</option>";
				html += "</select>";
			html += "</div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-type-details-"+index+"-days-require'  class='form-control' name='project_type_details["+index+"][days_require]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-type-details-"+index+"-list-order'  class='form-control' name='project_type_details["+index+"][list_order]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-type-details-"+index+"-percentage'  class='form-control' name='project_type_details["+index+"][percentage]'></div></td>";
		html += "</tr>";
		$(html).appendTo("#tblDetails tbody");
		index++;
	}

	function remove_row(i) {
		$("#row_"+i).detach();
		index--;

	}
</script>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($projectTypes,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('project_type'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
	<div class="col-md-12">
		<div class="row">
			<legend><span>Project Tasks/Services Details</span></legend>
			<table class="table table-striped table-condense table-responsive" id="tblDetails">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th style="width:40%">Task</th>
						<th style="width:30%">Fees Type</th>
						<th style="width:10%">Days Require</th>
						<th style="width:10%">List Order</th>
						<th style="width:10%">Percentage / Fees</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(!empty($projectTypes['project_type_details'])) {
						foreach($projectTypes['project_type_details'] as $i=>$detail) {
				?>
					<tr id="row_<?php echo $i;?>">
						<td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
						<td>
							<?php
								echo $this->Form->input('project_type_details.'.$i.'.id',['type'=>'hidden']);
								echo $this->Form->control('project_type_details.'.$i.'.task_id',array('label'=>false,'options'=>$tasks,'empty'=>'(Choose One)'));
							?>
						</td>
						<td><?php echo $this->Form->control('project_type_details.'.$i.'.fees_type',['label'=>false,'options'=>['Percentage'=>'Percentage','Lumpsum'=>'Lumpsum','Lumpsum Retainer'=>'Lumpsum Retainer'],'empty'=>'(Choose One)']);?></td>
						<td><?php echo $this->Form->control('project_type_details.'.$i.'.days_require',['type'=>'text','label'=>false,'class'=>'form-control']);?></td>
						<td><?php echo $this->Form->control('project_type_details.'.$i.'.list_order',['type'=>'text','label'=>false,'class'=>'form-control']);?></td>
						<td><?php echo $this->Form->control('project_type_details.'.$i.'.percentage',['type'=>'text','label'=>false,'class'=>'form-control']);?></td>

					</tr>
				<?php  }} ?>
				</tbody>
				<tfoot>
					<tr>
						<td><a href="javascript:void(0); add_row();">+</a></td>
						<td colspan="5"></td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

