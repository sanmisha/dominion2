<script>
index = 0;
index = <?php echo count($invoiceReceipt['invoice_receipt_details'])+1; ?>

$(document).ready(function() {
    $("#payment-mode").change(function() {

        if ($("#payment-mode").val() == 'Cash') {
            $("#refno").hide();
            $("#tdate").hide();
            $("#chequeNo").hide();
            $("#chequeDate").hide();
            $("#bank").hide();
        } else if ($("#payment-mode").val() == 'Bank') {
            $("#refno").hide();
            $("#tdate").hide();
            $("#chequeNo").show();
            $("#chequeDate").show();
            $("#bank").show();

        } else if ($("#payment-mode").val() == 'Bank Transfer') {
            $("#refno").show();
            $("#tdate").show();
            $("#chequeNo").hide();
            $("#chequeDate").hide();
            $("#bank").hide();
        } else {
            $("#refno").hide();
            $("#tdate").hide();
            $("#chequeNo").hide();
            $("#chequeDate").hide();
            $("#bank").hide();
        }
    });
    $("#payment-mode").trigger('change');



});

$(document).ready(function() {
    calculate();
    $("#client-id").change(function() {
       $("#tblDetail tbody").empty();
       if($("#client-id").val()){
        $.ajax({
            type: "GET",
			url: "/Invoices/getPendingInvoices/"+$("#client-id").val(),
			dataType:"json"
        }).done(function(data) {
            data.invoices.forEach(function(row, index) {
                console.log(row);
                addRow(row, index);
            });
            console.log(data);
        });
       }
    });

    function addRow(row, i) {
        console.log("fff");
        html = "<tr>";
            html += "<td>";
            html += "<input type='hidden' id='invoice-receipt-details-" + i + "-invoice-id'  name='invoice_receipt_details[" + i + "][invoice_id]' value=" + row.id + ">";
            html += "<input type='hidden' id='invoice-receipt-details-" + i + "-invoice-no'  name='invoice_receipt_details[" + i + "][invoice][invoice_no]' value=" + row.invoice_no + ">";
            html += row.invoice_no;
            html += "</td>";
            html += "<td ><input type='hidden' id='invoice-receipt-details-" + i + "-invoice-invoice-date'  name='invoice_receipt_details[" + i + "][invoice][invoice_date]'  value=" + row.invoice_date + ">" + row.invoice_date + "  </td>";
            html += "<td align='right'><input type='hidden'  id='invoice-receipt-details-" + i + "-invoice-total-amount'  name='invoice_receipt_details[" + i + "][invoice][total_amount]' value=" + row.total_amount + "> &#8377 " + row.total_amount + " </td>";
            html += "<td align='right'><input type='hidden'  id='invoice-receipt-details-" + i + "-invoice-balanced-amount'  name='invoice_receipt_details[" + i + "][invoice][balance_amount]' value=" + row.balance_amount + "> &#8377 " + row.balance_amount + " </td>";
            html += "<td><input id='invoice-receipt-details-" + i + "-received-amount'  name='invoice_receipt_details[" + i + "][received_amount]' class='form-control calc' rel="+i+"></td>";
            html += "<td><input id='invoice-receipt-details-" + i + "-tds'  name='invoice_receipt_details[" + i + "][tds]' class='form-control calc' rel="+i+"></td>";
            html += "<td><input id='invoice-receipt-details-" + i + "-total-received-amount'  name='invoice_receipt_details[" + i + "][total_received_amount]' class='form-control amt' readonly='readonly'></td>";
        html += "</tr>";
        $(html).appendTo("#tblDetail");
        $(".calc").on("blur", calculate);
        i++;
    }
    $(".calc").on("blur", calculate);

});

function calculate() {
    rel = $(this).attr('rel');

    receivedAmt = parseFloat($("#invoice-receipt-details-" + rel + "-received-amount").val());
    tdsAmt = parseFloat($("#invoice-receipt-details-" + rel + "-tds").val());

    receivedAmt = isNaN(receivedAmt) ? 0 : receivedAmt;
    tdsAmt = isNaN(tdsAmt) ? 0 : tdsAmt;

    amount = receivedAmt + tdsAmt;

    $("#invoice-receipt-details-" + rel + "-total-received-amount").val(amount);
    calculateTotal();
}


function calculateTotal() {
    totalAmt = 0;
    $(".amt").each(function() {
        amount = parseFloat($(this).val());
        totalAmt += isNaN(amount) ? 0 : amount;
    });

    $("#amount").val(totalAmt);
}



</script>


<h4><?= $title; ?></h4>
<?= $this->Form->create($invoiceReceipt, ['type'=>'file', 'novalidate'=>true]) ?>
<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-4">
            <?= $this->Form->hidden('id') ?>
            <?= $this->Form->control('receipt_no', [ 'readonly'=>true]) ?>
        </div>
        <div class="col-md-4">
            <?php echo $this->Form->control('receipt_date', ['type' => 'text', 'class'=>'mydatepicker']);?>
        </div>
        <div class="col-md-4">
            <?php
            if($invoiceReceipt->isNew()) {
                echo $this->N->select2('client_id',['options'=>$clients ]);
			} else {
				echo $this->Form->hidden('client_id');
				echo $this->Form->control('client',['readonly'=>true, 'value'=>$invoiceReceipt->client->client_name, 'type'=>'text' ]);
			}
			?>
        </div>
    </div>

</div>

<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-12">

            <legend><span> Invoice Details</span></legend>
            <table class="table " id="tblDetail">
                <thead>
                    <tr>
                        <th scope="col">Invoice No</th>
                        <th scope="col">Invoice Date</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Balanced Amount</th>
                        <th scope="col">Received Amount</th>
                        <th scope="col">TDS </th>
                        <th scope="col">Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                <tbody>
                    <?php
                        foreach($invoiceReceipt['invoice_receipt_details'] as $i=>$detail) {
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.id'); ?>
                            <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.invoice_id');?>
                            <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.invoice.invoice_no');?>
                            <?=  $detail->invoice['invoice_no'] ?>
                        </td>
                        <td>
                             <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.invoice.invoice_date');?>
                             <?= $detail->invoice['invoice_date'] ?>
                        </td>
                        <td align="right">
                            <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.invoice.total_amount', ['label'=>false, 'readonly'=>true]);?>
                            <?=  $this->Number->currency($detail->invoice['total_amount']) ?>
                        </td>
                        <td align="right">
                            <?= $this->Form->hidden('invoice_receipt_details.'.$i.'.invoice.balanced_amount', ['label'=>false, 'readonly'=>true]);?>
                            <?=  $this->Number->currency($detail->invoice['balance_amount']) ?>
                        </td>
                        <td> <?php echo $this->Form->control('invoice_receipt_details.'.$i.'.received_amount',['type'=>'text', 'class'=>'form-control calc', 'rel'=>$i, 'label'=>false]); ?></td>
                        <td> <?php echo $this->Form->control('invoice_receipt_details.'.$i.'.tds',['type'=>'text', 'class'=>'form-control calc', 'rel'=>$i, 'label'=>false]);?></td>
                        <td> <?php echo $this->Form->control('invoice_receipt_details.'.$i.'.total_received_amount',['type'=>'text', 'class'=>'form-control amt', 'rel'=>$i, 'readonly'=>true,'label'=>false]); ?></td>
                    </tr>
                    <?php } ?>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" align="right"><b>Total Amount:</b></td>
                        <td width="20%" align="right">
                            <?php echo $this->Form->control('amount', ['id'=>'amount','label'=>false, 'onblur'=>' return calculate()', 'readonly'=>true]); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-3" id="paymentMode">
            <?php echo $this->N->select2('payment_mode',['options'=>['Cash'=> 'Cash', 'Bank'=> 'Bank', 'Bank Transfer'=> 'Bank Transfer' ],'type'=>'select']); ?>
        </div>
        <div class="col-md-6" id="refno" style="display:none;">
            <?php echo $this->Form->control('transaction_no'); ?>
        </div>
        <div class="col-md-3" id="tdate" style="display:none;">
            <?php echo $this->Form->control('transaction_date',  ['type' => 'text', 'class'=>'mydatepicker']); ?>
        </div>
        <div class="col-md-3" id="chequeNo" style="display:none;">
            <?php echo $this->Form->control('cheque_no'); ?>
        </div>
        <div class="col-md-3" id="chequeDate" style="display:none;">
            <?php echo $this->Form->control('cheque_date',  ['type' => 'text', 'class'=>'mydatepicker']); ?>
        </div>
        <div class="col-md-3" id="bank" style="display:none;">
            <?php echo $this->Form->control('bank_name'); ?>
        </div>
    </div>
</div>

<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-4">


        </div>


    </div></br>


    <div class="box box-block bg-white">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <?= $this->N->cancelLink(['action'=>'index']); ?>
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end(); ?>
