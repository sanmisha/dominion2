<div class="box box-block bg-white">

    <div class="clearfix mb-1">
	    <h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('invoice_receipts', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
        <div class="float-xs-right">
            <?= $this->N->addLink(array('action'=>'edit')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <th scope="col"> <?= $this->Paginator->sort('receipt_no') ?></th>
                        <th scope="col"> <?= $this->Paginator->sort('receipt_dtae') ?></th>
                        <th scope="col"> <?= $this->Paginator->sort('client.client_name', 'Client') ?></th>
                        <th scope="col"> <?= $this->Paginator->sort('amount') ?></th>
                        <th scope="col"> <?= $this->Paginator->sort('Action') ?></th>
                    </thead>
                    <tbody>
                        <?php foreach($invoiceReceipts as $receipt){ ?>
                        <tr>
                            <td> <?= $receipt->receipt_no ?></td>
                            <td> <?= $receipt->receipt_date ?></td>
                            <td> <?= $receipt->client->client_name ?></td>
                            <td><div class="pull-right"><?= $this->Number->currency($receipt->amount) ?></div></td>
                            <td>
                                <?= $this->N->editLink(['action'=>'edit', $receipt->id]) ?>
                                <?= $this->Html->link('Print', ['action'=>'print', $receipt->id ],['class'=>'btn btn-info btn-sm', 'target'=>'_blank']) ?>
                                <?= $this->N->deleteLink(['action'=>'delete', $receipt->id]) ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
