<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;

			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #4D4D4D;
				padding: 5px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}
			.tr{
				border: none;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3.5cm;
			}
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<img src="https://dominion.kashelkardesigns.com/img/letterhead.png" style="width:100%" />

    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
            <div style="text-align:center">
                410,411-412, Globe Arcade, Near Ganesh Mandir, MIDC Residential Zone, Dombivli East 421 203<br />
                <strong>Website:</strong> www.kashelkardesings.com | <strong>Email:</strong> info@archkdpl.com | <strong>Phone:</strong> +91 9920951588 / 0251-2447744<br />
                <strong>CIN: U74999MH2020PTC339226</strong>
            </div>
    	</htmlpagefooter>

		<br>
		<table class="tr">
			<tr class="tr">
				<td class="tr">
                    To,<br />
					<strong><?= $invoiceReceipt->client->client_name ?></strong><br />
                    <?= $invoiceReceipt->client->office_address_line_1 ?><br />
                    <?= $invoiceReceipt->client->office_address_line_2 ?><br />
                    <?= $invoiceReceipt->client->office_city ?> - <?= $invoiceReceipt->client->office_pincode ?><br /><br />


                    <strong>
                        GSTIN: <?= $invoiceReceipt->client->gst_no ?><br />
                        State Code: <?= substr($invoiceReceipt->client->gst_no, 0, 2) ?><br />
                        PAN: <?= $invoiceReceipt->client->pan_no ?><br />

                    </strong>
                    Email: <?= $invoiceReceipt->client->email ?><br />
                    Mobile: <?= $invoiceReceipt->client->mobile ?><br />
				</td>
			</tr>
		</table>

		<h1 align="center">  Receipts </h1>
		<table class="tr">
			<tr class="tr">
				<td width="50%" class="tr"> Received with thanks from :- <strong><?= $invoiceReceipt->client->client_name ?></strong> </td>
				<td width="50%" align="right" class="tr">
					<p>Receipt No. : <?= $invoiceReceipt->receipt_no ?> </p>
					<p>Receipt Date. : <?= $invoiceReceipt->receipt_date ?> </p>
				</td>
			</tr>
			<tr class="tr">
				<td colspan="2" class="tr" width="100%">
					<p>By :  <strong><?= $invoiceReceipt->payment_mode ?></strong> </p>
					<?php if($invoiceReceipt->payment_mode == 'Bank'){ ?>
					<p> <span> Cheque No. :  <strong> <?= $invoiceReceipt->cheque_no ?></strong> </span>  &emsp;  <span> Dated :  <?= $invoiceReceipt->cheque_date ?> </span> &emsp; <span align="right"> Drawee Bank : <?= $invoiceReceipt->bank_name  ?> </span> </p>
					<?php } else if($invoiceReceipt->payment_mode == 'Bank Transfer') { ?>
					<p>Trasaction No. : <?= $invoiceReceipt->transaction_no ?> &emsp; &emsp; &emsp; Dated : <?= $invoiceReceipt->transaction_date  ?>   </p>
					<?php }  ?>
				</td>
			</tr>


		</table>
		<br>
		<h3>  In full / part payment of the following invoice/s </h3>
		<br>
		<table>
			<thead>
				<tr  class="color">
					<th colspan="6"> Particuler </th>
					<th rowspan="2"> Amount </th>
				</tr>
				<tr  class="color">
					<th> Invoice No </th>
					<th> Invoice Date </th>
					<th> Project Code</th>
					<th> Invoice Amount </th>
					<th> Received Amount</th>
					<th> TDS </th>

				</tr>
			</thead>
			<tbody>
				<?php foreach($invoiceReceipt->invoice_receipt_details as $i=>$receipt){ ?>
					<tr>
						<td><?=  $receipt->invoice->invoice_no ?></td>
						<td> <?= $receipt->invoice->invoice_date ?></td>
						<td><?= (!empty($receipt->invoice->project_id)) ?  $receipt->invoice->project->project_no : '' ?></td>
						<td align="right"> <?=  $this->Number->currency( $receipt->invoice->total_amount)  ?></td>
						<td align="right"> <?=  $this->Number->currency( $receipt->received_amount) ?></td>
						<td align="right">  <?= $this->Number->currency( $receipt->tds) ?> </td>
						<td align="right"><?= $this->Number->currency( $receipt->total_received_amount)  ?></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr  class="color">
					<td colspan="6" align="right"><strong>  Total:- </strong> </td>
					<td> <strong> <?=  $this->Number->currency($invoiceReceipt->amount) ?> </strong></td>
				</tr>
				<tr  class="color">
					<td colspan="3" align="right"> <strong> Amount In Words</strong></td>
					<td colspan="4" align="right"> <strong> <?=  $invoiceReceipt->amount_in_words ?></strong></td>
				</tr>
				<tr  class="color">
					<td colspan="3" class="tr">
						<br> <br> <br> <br>
						<p>E. & O.E.<p>
						<p> <strong> Cheque subject to realisation </strong></p>
					</td>
					<td colspan="4" align="right" class="tr">
						<br> <br> <br> <br>
						<p> <strong> For Kashelkar Designs Pvt. Ltd. </strong> </p>
						<p> &ensp; Authorised Signature</p>
					</td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>
