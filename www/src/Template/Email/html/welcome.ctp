<?php
	$url = $this->Url->build([
		'controller' => 'Account',
		'action' => 'reset_password',
		$user['activation_key'],
		$user['email'],
	], true);
?>
<p>Welcome <?= $user['name'];?></p>
<p>Please <a href='<?= $url;?>'>click here</a> to Register.<br /> <?= $url;?></p>
