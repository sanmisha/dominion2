<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($user,['type'=>'file','novalidate'=>true]) ?> 
	<fieldset>
		<legend><?= __('Stake Holder Details') ?></legend>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('stakeholder.contractor'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.gst_no',['label'=>'GST No']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.pan_no',['label'=>'PAN No']); ?>
			</div>
		</div>
		<legend><?= __('Address Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.address_line_1'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.city'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.address_line_2'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.pincode'); ?>
			</div>
		</div>
		<legend><?= __('Office Address  Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.office_address_line_1'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.office_city'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.office_address_line_2'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.office_pincode'); ?>
			</div>	
		</div>	
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.landline'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.email'); ?>
			</div>
		</div>		
		<legend><?= __('Contact Person Details') ?></legend>		
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.contact_person_1'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.email_1'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.mobile_1'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('stakeholder.mobile_2'); ?>
			</div>
		</div>
		
		
	
		
		<legend><?= __('Login Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('email'); ?>
			</div>
			<?php if($user->isNew()) { ?>
			<div class="col-md-6">
				<?php echo $this->Form->control('password'); ?>
			</div>
			<?php }	?>			
		</div>
		
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

