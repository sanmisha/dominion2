

<?= $this->Form->create($followup,['type'=>'file','novalidate'=>true]) ?>
<div class="box box-block bg-white">
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<strong>Quoation No:</strong><?php echo $projects['quotation_no']; ?>
			</div>
			<div class="col-md-4">
				<strong>Quoation Date:</strong> <?php echo $projects['quotation_date']; ?>
			</div>
			<div class="col-md-4">
				<strong>Client Name:</strong> <?php echo $projects['client']['client_name']; ?>
			</div>
		</div>
	</fieldset><br>
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
   <fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->hidden('project_id',['value'=>$ProjectId]); ?>
				<?php echo $this->N->datepicker('followup_date'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('project_status',['options'=>$projectStatus,'empty'=>true,'value'=>$projects['status']]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('remarks'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['controller'=>'Projects','action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>
<?=$this->Form->end() ?>




<div class="box box-block bg-white">
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('followup_date')?></th>
							<th scope="col"><?= $this->Paginator->sort('remarks')?></th>
							<th scope="col"><?= $this->Paginator->sort('project_status')?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($followupList as $followup): //debug($followup);exit; ?>
                        <tr>
                            <td><?= $followup->followup_date?></td>
							<td><?= $followup->remarks?></td>
							<td><?= $followup->project_status?></td>
						    <td class="actions">
                                <?= $this->N->editLink(['controller' => 'Followups','action' => 'edit', $followup->id , $followup->project_id]); ?>
								<?= $this->N->deleteLink(['controller' => 'Followups','action' => 'delete', $followup->id , $followup->project_id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
