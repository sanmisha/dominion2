<?php
    use Cake\Core\Configure;
?>
<script>	
    indexs = 0;
    indexs = <?php echo count($meetingReport['meeting_report_images'])+1; ?>;
    k = 0;
    k = <?php echo count($meetingReport['meeting_report_stakeholders'])+1; ?>;
    j = 0;
    j = <?php echo count($meetingReport['meeting_report_visitors'])+1; ?>;

    function addRows(row, k) {
        html = "<tr id='rowss_"+k+"'>";
            html += "<td><a href='javascript:void(0); removeRows("+k+");'><i class='fa fa-minus'></i></a></td>";
            html += "<td><div class='form-group'>";
            html += "<td><input type='hidden' id='meeting-report-stakeholders-" + k + "-stakeholder-id'  name='meeting_report_stakeholders[" + k + "][stakeholder_id]' value=" + row.stakeholder_id + ">"+ row.stakeholder.contractor+"</td>";          
            html += "</div></td>";
        html += "</tr>";
        $(html).appendTo("#stakeholdersData tbody");       
        k++;
    
    } 
	

	
    function projectChange(i){
        var id = $("#project-id").val();       
        $.getJSON('/Projects/getStakeholders/' + id, function(data){
            console.log(data.projects.project_stakeholders)
            data.projects.project_stakeholders.forEach(function(row, index) {
                    addRows(row, index);
            });

        });
	} 



    function add_rows() {
		html = "<tr id='rowws_"+j+"'>";
			html += "<td><a href='javascript:void(0); remove_rows("+j+");'><i class='fa fa-minus'></i></a></td>";			
			html += "<td><div class='form-group'><input type='text' id='meeting-report-visitors-"+j+"-name'  class='form-control qt' name='meeting_report_visitors["+j+"][name]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='meeting-report-visitors-"+j+"-email'  class='form-control rt' name='meeting_report_visitors["+j+"][email]'></div></td>";		
		html += "</tr>";
		$(html).appendTo("#visitorsData tbody");
		j++;		
	}
    function addRow(){       
        html ="<tr id='rows_"+indexs+"'>";
            html += "<td><a href='javascript:void(0); removeRow("+indexs+");'><i class='fa fa-minus'></i></a></td>";          
            html += "<td><input type='file' id='meeting-report-images-"+indexs+"-image' name='meeting_report_images["+indexs+"][image]' class='form-control'></td>";
            html += "<td><input type='hidden' id='meeting-report-images-"+indexs+"-image-dir' name='meeting_report_images["+indexs+"][image.dir]' class='form-control'></td>";
          
         html += "</tr>";
        html2 ="<tr id='second_rows_"+indexs+"'>";
            html2 +="<td>&nbsp; </td>";
            html2 +="<td colspan='12'><input id='meeting-report-images-"+indexs+"-remarks' name='meeting_report_images["+indexs+"][remarks]' class='form-control'  placeholder='Remarks'></td>";
        html2 += "</tr>";
        $(html).appendTo("#tblDetail tbody");
        $(html2).appendTo("#tblDetail tbody");
        indexs++;
    }  
	
    function removeRow(i) {
		$("#rows_"+i).detach();
		$("#second_rows_"+i).detach();
		indexs--;
	}
    function removeRows(k) {
        $("#rowss_"+k).detach();
        k--;		
    }
    
	function remove_rows(j) {
		$("#rowws_"+j).detach();
		j--;	
	}
   
</script>
<?= $this->Form->create($meetingReport,['type'=>'file','novalidate'=>true]) ?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('meeting_no', ['readonly'=>true]); ?>
			</div>
			<div class="col-md-4">

				<?php echo $this->N->datepicker('meeting_date',['required'=>true]); ?>
			</div>
			<div class="col-md-4">
                <?php if(empty($meetingReport->id)){
                   echo  $this->N->select2('meeting_time', ['options'=>Configure::read('time'), 'empty'=>true]);
                } else{
                    echo $this->Form->control('meeting_time', [ 'type'=>'text', 'value'=>$meetingReport->meeting_time->format('g:i a'),'readonly'=>true]);
                } ?>
        	</div>

		</div>
		<div class="row">
            <div class="col-md-4">
				<?php echo $this->N->select2('employee_id', ['options'=>$employees , 'empty'=>'---']); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->N->select2('project_id', ['options'=>$projects , 'empty'=>'---', 'onChange'=>'return projectChange()']); ?>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('report'); ?>
			</div>
		</div>
		</fieldset><br>
</div>
<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-12">
            <legend><span>Meeting Reports Images</span></legend>
            <table class="table  table-hover" id="tblDetail">
                <thead>
                    <tr>
                        <th>&nbsp;</th>                       
                        <th>Image</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($meetingReport['meeting_report_images'])) {
                        foreach($meetingReport['meeting_report_images'] as $i=>$image) {
                ?>
                    <tr id="rows_<?php echo $i;?>">
                        <td><a href='javascript:void(0); removeRow("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                        <td>
                            <?php
                                echo $this->Form->input('meeting_report_images.'.$i.'.id',['type'=>'hidden']);                    
                                                        
                                if(!empty($image->id)){
                                    echo $this->Form->hidden('meeting_report_images.'.$i.'.image',['value'=>$image->image]);
                                    echo $this->Html->image('/files/meetingReportimages/image/'.$image->image_dir.'/'.$image->image, array('width'=>'150px', 'hieght'=>'150px'));
                                } else {
                                    echo $this->Form->control('meeting_report_images.'.$i.'.image',['value'=>$image->image, 'type'=>'file', 'label'=>false]);
                                }

                            ?>
                        </td>
                        <td><?php echo $this->Form->hidden('meeting_report_images.'.$i.'.image_dir',[ 'value'=>$image->image_dir]);?></td>
                    </tr>
                    <tr id="second_rows_<?php echo $i;?>">
                        <td>&nbsp;</td>
                        <td colspan="3"><?php echo $this->Form->control('meeting_report_images.'.$i.'.remarks', ['type'=>'text', 'label'=>false, 'class'=>'span12' , 'value'=>$image->remarks, 'placeholder'=>'Remarks']);?></td>
                    </tr>

                <?php  }} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><a href="javascript:void(0); addRow();">+</a></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <legend><span> Stake Holders </span></legend>
		<table class="table table-striped table-condense table-responsive" id="stakeholdersData">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th style="">Stake Holders </th>					
				</tr>
			</thead>
			<tbody>
			<?php
				if(!empty($meetingReport['meeting_report_stakeholders'])) {
					foreach($meetingReport['meeting_report_stakeholders'] as $i=>$stakeholder) {
			?>
			<tr id="rowss_<?php echo $i;?>">
				<td><a href='javascript:void(0); removeRows("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
				<td>
					<?php
						echo $this->Form->input('meeting_report_stakeholders.'.$i.'.id',['type'=>'hidden']);
						echo $this->N->select2('meeting_report_stakeholders.'.$i.'.stakeholder_id',array('label'=>false,'options'=>$stakeholders,'empty'=>'(Choose One)'));
					?>
				</td>			
			</tr>
			<?php  }} ?>
			</tbody>			
		</table>	
        </div>		        
	
        <div class="col-md-6">
        <legend><span> Visitors Details</span></legend>
        <table class="table table-striped table-condense table-responsive" id="visitorsData">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Name</th>
                    <th>Email</th>
                  
                </tr>
            </thead>
            <tbody>
            <?php
                if(!empty($meetingReport['meeting_report_visitors'])) {
                    foreach($meetingReport['meeting_report_visitors'] as $i=>$visitor) {
            ?>
                <tr id="rowws_<?php echo $i;?>">
                    <td><a href='javascript:void(0); remove_rows("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                    <td>
                        <?php
                            echo $this->Form->input('meeting_report_visitors.'.$i.'.id',['type'=>'hidden']);
                            echo $this->Form->control('meeting_report_visitors.'.$i.'.name',array('label'=>false));
                        ?>
                    </td>
                    <td><?php echo $this->Form->control('meeting_report_visitors.'.$i.'.email',['label'=>false,]);?></td>
                </tr>
            <?php  }} ?>
            </tbody>
            <tfoot>
                <tr>
                    <td><a href="javascript:void(0); add_rows();">+</a></td>
                    <td colspan="2"></td>
                </tr>              
            </tfoot>
        </table> 
        </div>            
    </div>
    <fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>

<?= $this->Form->end() ?>

