

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>		
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>    
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('meetingReports', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});			
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('meeting_no')?></th>	
							<th scope="col"><?= $this->Paginator->sort('meeting_date')?></th>	
							<th scope="col"><?= $this->Paginator->sort('meeting_time')?></th>						
							<th scope="col"><?= $this->Paginator->sort('employee_id')?></th>
							<th scope="col"><?= $this->Paginator->sort('project_id')?></th>
                            <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($meetingReports as $meetingReport): ?>
                        <tr>
                            <td><?= $meetingReport->meeting_no?></td>
							<td><?= $meetingReport->meeting_date?></td>
							<td><?= $meetingReport->meeting_time->format('h:i a') ?></td>
							<td><?= $meetingReport->employee->name?></td>
							<td><?= $meetingReport->project->project_with_client?></td>
                            <td class="actions">
                                <?= $this->N->editLink(['action' => 'edit', $meetingReport->id]); ?>
								<?= $this->Html->link('Print', '/files' .  DS . 'meeting_report' . DS . 'meeting_report-' . $meetingReport->meeting_no . '.pdf' ,['class'=>'btn btn-info btn-sm', 'target'=>'_blank']); ?>
								<?= $this->N->deleteLink(['action' => 'delete', $meetingReport->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>	
<?php echo $this->element('Neptune.pagination'); ?>
