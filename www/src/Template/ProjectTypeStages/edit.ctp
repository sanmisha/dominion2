<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($leadType,['type'=>'file','novalidate'=>true]) ?> 
    <fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('project_type_stage'); ?>
			</div>
		</div>
		<div class="col-md-6">
				<?php echo $this->Form->control('list_order'); ?>
			</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('days_require'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('fees_in_percentages'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('description'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('services'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

