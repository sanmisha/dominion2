<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($handbook,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('department_id',['options'=>$department,'empty'=>'---']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('designation_id',['options'=>$designation,'empty'=>'---']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-8">
				<?php echo $this->Form->control('topic'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('list_order'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('content', ['class'=>'summernote', 'rows'=>12]); ?>
			</div>
		</div>
        <div class="row">
            <div class="col-md-3">
			    <label>Attachment 1</label>
				<?php
					echo $this->Form->control('attachment_1',['type'=>'file','label'=>false]);
					echo $this->Form->hidden('attachment_1_dir');
					if(!empty($handbook->attachment_1_dir)) {
						echo $this->Html->link($handbook->attachment_1,'/files/handbooks/attachment_1/'.$handbook->attachment_1_dir.'/'.$handbook->attachment_1,['target'=>'_blank']);
					}
				?>
			</div>
            <div class="col-md-3">
			    <label>Attachment 2</label>
				<?php
					echo $this->Form->control('attachment_2',['type'=>'file','label'=>false]);
					echo $this->Form->hidden('attachment_2_dir');
					if(!empty($handbook->attachment_2_dir)) {
						echo $this->Html->link($handbook->attachment_2,'/files/handbooks/attachment_2/'.$handbook->attachment_2_dir.'/'.$handbook->attachment_2,['target'=>'_blank']);
					}
				?>
			</div>
        </div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

