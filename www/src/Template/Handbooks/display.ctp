<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>		
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>    
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('handbooks', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});			
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('topic')?></th>
                            <th scope="col"><?= $this->Paginator->sort('department_id')?></th>
							<th scope="col"><?= $this->Paginator->sort('designation_id')?></th>
							<th scope="col"><?= $this->Paginator->sort('employee_id')?></th>
                            <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($handbooks as $handbook): //debug($handbook);exit; ?>
                        <tr>
							<td><?= $handbook->topic?></td>
							<?php if(!empty($handbook['department']['department'])){ ?>
							<td><?= $handbook->department->department?></td>
							<?php }else{ ?>
							<td></td>
							<?php } ?>
							<?php if(!empty($handbook['designation']['designation'])){ ?>
							<td><?= $handbook->designation->designation?></td>
							<?php }else{ ?>
							<td></td>
							<?php } ?>
							<?php if(!empty($handbook['employee']['name'])){ ?>
							<td><?= $handbook->employee->name?></td>
							<?php }else{ ?>
							<td></td>
							<?php } ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>	
<?php echo $this->element('Neptune.pagination'); ?>
