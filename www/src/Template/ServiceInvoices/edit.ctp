<script>

	index = 0;
	index = <?php echo count($serviceInvoice['service_invoice_details'])+1; ?>


	$(document).ready(function(){
		$("#client-id").on("change", function(){
			$("#project-id").empty();
				$.ajax({
					type: "GET",
					url: "/Projects/getProjects/"+$("#client-id").val(),
					dataType:"json"
				}).done(function(data) {
					$('#project-id ')
						.append($("<option></option>")
							.attr("value",'')
							.text('---'));
					$.each(data.projects, function(key, value) {
					$('#project-id ')
						.append($("<option></option>")
								.attr("value",value.id)
								.text(value.project_with_client));


					});
					<?php if(!empty($serviceInvoice->project_id)) { ?>
						$('#project-id').val("<?php echo $serviceInvoice['project']['id']; ?>");
					<?php } ?>
				});
			});
			$("#client-id").trigger("change");

		$(".calc").blur(function(){
			amountCalculate();
		});
		$("#service-invoice-details-"+index+"-task-id").change(function(){
			taskChange();
		})
	});

	function taskChange(i){
        var id = $("#service-invoice-details-"+i+"-task-id").val();
        $.getJSON('/tasks/get/' + id, function(data){
            $("#service-invoice-details-"+i+"-service").val(data.service);
            $("#service-invoice-details-"+i+"-description").val(data.description);
			console.log(data);
        });
	}


	function add_row() {
		html = "<tr id='row_"+index+"'>";
			html += "<td> <a href='javascript:void(0); remove_row("+index+");'><i class='fa fa-minus'></i></a></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='service-invoice-details-"+index+"-task-id' name='service_invoice_details["+index+"][task_id]' class='form-control' onChange='return taskChange("+index+")' >";
				html += "<option value=''>(Choose One)</option>";
					<?php foreach($tasks as $id=>$task) { ?>
					    html += "<option value='<?php echo $id;?>'><?php echo $task;?></option>";
					<?php } ?>
				html += "</select>";
			html += "</div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-fees'  class='form-control' name='service_invoice_details["+index+"][fees]' onblur='return amountCalculate("+index+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-discount'  class='form-control ' name='service_invoice_details["+index+"][discount]' onblur='return amountCalculate("+index+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-dis-amt'  class='form-control ' name='service_invoice_details["+index+"][dis_amt]' onblur='return amountCalculate("+index+")' readonly></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-cgst-rate'  class='form-control' name='service_invoice_details["+index+"][cgst_rate]' onblur='return amountCalculate("+index+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-cgst-amount'  class='form-control ' name='service_invoice_details["+index+"][cgst_amount]' onblur='return amountCalculate("+index+")' readonly></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-sgst-rate'  class='form-control' name='service_invoice_details["+index+"][sgst_rate]' onblur='return amountCalculate("+index+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-sgst-amount'  class='form-control ' name='service_invoice_details["+index+"][sgst_amount]' onblur='return amountCalculate("+index+")' readonly></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-igst-rate'  class='form-control' name='service_invoice_details["+index+"][igst_rate]' onblur='return amountCalculate("+index+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-igst-amount'  class='form-control ' name='service_invoice_details["+index+"][igst_amount]' onblur='return amountCalculate("+index+")' readonly></div></td>";
			html += "<td><div class='form-group'><input type='text' id='service-invoice-details-"+index+"-amount'  class='form-control amt' name='service_invoice_details["+index+"][amount]' onblur='return amountCalculate("+index+")' readonly></div></td>";

		html += "</tr>";
		html2 = "<tr id='second_row_"+index+"'>";
			html2 += "<td>&nbsp;</td>";
			html2 += "<td colspan='12'><input type='text' id='service-invoice-details-"+index+"-description'  class='form-control' name='service_invoice_details["+index+"][description]' placeholder='Description'></td>";
		html2 += "</tr>";
		$(html).appendTo("#tblDetails tbody");
		$(html2).appendTo("#tblDetails tbody");
		//$("#service-invoice-details-"+index+"-description").val(description);

		index++;
		$(".amt").blur(calculate);
		calculate();

	}


	function amountCalculate(i) {


		var discountAmount = 0;
		var finalAmount=0;
		var cgstAmount= 0;


		discountAmount = $("#service-invoice-details-"+i+"-fees").val() * $("#service-invoice-details-"+i+"-discount").val()/100;
		$("#service-invoice-details-"+i+"-dis-amt").val(discountAmount.toFixed(2));



		finalAmount = $("#service-invoice-details-"+i+"-fees").val() - discountAmount;

		grossAmount = finalAmount;


		cgstAmount = grossAmount * $("#service-invoice-details-"+i+"-cgst-rate").val()/100;
        console.log($("#service-invoice-details-"+i+"-cgst-rate").val());
		$("#service-invoice-details-"+i+"-cgst-amount").val(cgstAmount.toFixed(2));
		console.log(cgstAmount);

		sgstAmount= grossAmount * $("#service-invoice-details-"+i+"-sgst-rate").val()/100;
		$("#service-invoice-details-"+i+"-sgst-amount").val(sgstAmount.toFixed(2));

		igstAmount= grossAmount * $("#service-invoice-details-"+i+"-igst-rate").val()/100;
		$("#service-invoice-details-"+i+"-igst-amount").val(igstAmount.toFixed(2));

        finalAmount = finalAmount + cgstAmount;
		finalAmount = finalAmount + sgstAmount;
		finalAmount = finalAmount + igstAmount;

		$("#service-invoice-details-"+i+"-amount").val(finalAmount.toFixed(2));
		//$("#service-invoice-details-"+i+"-final-amount").val(finalAmount.toFixed(2));

		calculate();
	}

	function calculate() {

		var total=0;
		var amount=0;

		$(".amt").each(function(){
			if(!isNaN($(this).val()) && $(this).val()!="")  {
				amount += parseFloat($(this).val());
				total += parseFloat($(this).val());
			}
		});

		$("#amount").val(amount.toFixed(2));
		$("#total-amount").val(total.toFixed(2));
	}

	function remove_row(i) {
		$("#row_"+i).detach();
		$("#second_row_"+i).detach();
		calculate();
		i--;
	}
</script>




<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <?= $this->Form->create($serviceInvoice,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('invoice_no',['readonly'=>true]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->datepicker('invoice_date',['required'=>true]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->select2('client_id', ['options'=>$clients , 'empty'=>'---']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->select2('project_id', [ 'empty'=>'---']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row stage">
            <div class="col-md-12">
                <legend><span>Invoice Details</span></legend>
                <table class="table table-striped table-condense table-responsive" id="tblDetails">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th style="width:10%">Service</th>
                            <th style="width:8%">Fees</th>
                            <th style="width:6%">Discount</th>
                            <th style="width:8%">Discount Amount</th>
                            <th style="width:8%"> CGST Rate</th>
                            <th style="width:10%">CGST Amount</th>
                            <th style="width:8%">SGST Rate</th>
                            <th style="width:10%">SGST Amount</th>
                            <th style="width:8%">IGST Rate</th>
                            <th style="width:10%">IGST Amount</th>
                            <th style="width:15%">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if(!empty($serviceInvoice['service_invoice_details'])) {
                            foreach($serviceInvoice['service_invoice_details'] as $i=>$detail) {
                    ?>
                        <tr id="row_<?php echo $i;?>">
                        <td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                            <td>
                                <?php
                                    echo $this->Form->input('service_invoice_details.'.$i.'.id',['type'=>'hidden']);
									echo $this->Form->control('service_invoice_details.'.$i.'.task_id', array('label'=>false,'options'=>$tasks,'empty'=>'(Choose One)','value'=>$detail['task_id'], 'onChange'=>'return taskChange('.$i.')'));
                                ?>
                            </td>

                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.fees',['type'=>'text','label'=>false,'class'=>'form-control' ,'value'=>$detail['fees'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.discount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['discount']]);?></td>
							<td><?php echo $this->Form->control('service_invoice_details.'.$i.'.dis_amt',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['dis_amt'],'onblur'=>'return amountCalculate('.$i.')', 'readonly'=>true]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.cgst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['cgst_rate']]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.cgst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['cgst_amount'],'onblur'=>'return amountCalculate('.$i.')', 'readonly'=>true]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.sgst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['sgst_rate']]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.sgst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['sgst_amount'],'onblur'=>'return amountCalculate('.$i.')', 'readonly'=>true]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.igst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['igst_rate']]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.igst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['igst_amount'],'onblur'=>'return amountCalculate('.$i.')', 'readonly'=>true]);?></td>
                            <td><?php echo $this->Form->control('service_invoice_details.'.$i.'.amount',['type'=>'text','label'=>false,'class'=>'form-control amt', 'readonly'=>true ,'value'=>$detail['amount'],'onblur'=>'return amountCalculate('.$i.')', 'readonly'=>true]);?></td>

                        </tr>
						<tr id="second_row_<?php echo $i;?>">
							<td>&nbsp;</td>
							<td colspan="12"><?php echo $this->Form->control('service_invoice_details.'.$i.'.description',['type'=>'text','label'=>false,'class'=>'span12' ,'value'=>$detail['description'],'placeholder'=>'Description']);?></td>
						</tr>

                    <?php  }} ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><a href="javascript:void(0); add_row();">+</a></td>
                            <td colspan="5"></td>
                        </tr>
                        <tr>
                            <td colspan="10" align="right"></td>
                            <td align="right"><strong>Total Amount</strong></td>
                            <td align="right">
                                <?php echo $this->Form->control('total_amount',array('readonly'=>true,'label'=>false,'class'=>'calc')); ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
    <?= $this->Form->end() ?>
</div>

