<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('DailyTasks', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('project_id','Project No')?></th>
							<th scope="col"><?= $this->Paginator->sort('activity_id')?></th>
                            <th scope="col"><?= $this->Paginator->sort('description')?></th>
							<th scope="col"><?= $this->Paginator->sort('assigned_to','Assigned To') ?></th>
							<th scope="col"><?= $this->Paginator->sort('due_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('duration') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('done_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('status') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dailyTasks as $dailyTask): ?>
                        <tr>
                            <td><?= $dailyTask->project ? $dailyTask->project->project_no . ' ' . $dailyTask->project->client->client_name : '---'; ?></td>
							<td><?= $dailyTask->activity->activity?></td>
                            <td><?= $dailyTask->description?></td>
                            <td><span class="tag tag-info"><?= $dailyTask->assigned_employee->name?></span></td>
                            <td><?= $dailyTask->due_date; ?></td>
                            <td><?= $dailyTask->duration; ?></td>
							<td><?= $dailyTask->done_date; ?></td>
                            <td>
                                <?php
                                    if($dailyTask->status == 'Open') {
                                        echo "<span class='tag tag-success'>Open</span>";
                                    } elseif($dailyTask->status == 'In Progress') {
                                        echo "<span class='tag tag-warning'>In Progress</span>";
                                    } else {
                                        echo "<span class='tag tag-danger'>Done</span>";
                                    }
                                ?>
                            </td>
							<td class="actions">
								<?php
                                    if($dailyTask->status == 'Open') {
                                        echo $this->Html->link('Progress',['controller'=>'DailyTasks','action'=>'progress', $dailyTask->id], ['class'=>'btn btn-primary btn-sm']);
                                    }
                                ?>
                                <?= $this->N->editLink(['action' => 'edit', $dailyTask->id]); ?>
                                <?php //$this->N->deleteLink(['action' => 'delete', $dailyTask->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
