

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($dailyTask,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('project',['type'=>'text', 'readonly'=>'true', 'value'=> $dailyTask->project ? $dailyTask->project->project_no : '---']); ?>
			</div>
			<div class="col-md-9">
                <?php echo $this->Form->control('activity',['type'=>'text', 'readonly'=>'true', 'value'=>$dailyTask->activity->activity]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('description', ['type'=>'textarea']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
                <?php echo $this->Form->control('assigned_to_employee',['type'=>'text', 'label'=>'Assigned To', 'readonly'=>'true', 'value'=>$dailyTask->assigned_employee->name]); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('due_date', ['type'=>'text', 'readonly'=>'true']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('duration', ['options'=>array_combine(range(0,12,0.5), range(0,12,0.5))]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('status', ['options'=>['In Progress'=>'In Progress', 'Done'=>'Done']]); ?>
			</div>
        </div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>

