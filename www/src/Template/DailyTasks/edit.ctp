

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($dailyTask,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->N->select2('project_id',['empty'=>'=== No Project Linked ===']); ?>
			</div>
			<div class="col-md-9">
                <?php echo $this->N->select2('activity_id',['empty'=>false]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('description', ['type'=>'textarea']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->control('assigned_to',['options'=>$employees, 'empty'=>'---']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->datepicker('due_date', ['required'=>true]); ?>
			</div>
        </div>
        <?php if(!$dailyTask->isNew()) { ?>
        <div class="row">
            <div class="col-md-3">
				<?php echo $this->N->datepicker('done_date', ['required'=>true]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('duration', ['options'=>range(0,12,0.5)]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('status', ['options'=>['Open'=>'Open', 'In Progress'=>'In Progress', 'Done'=>'Done']]); ?>
			</div>
		</div>
        <?php } ?>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
                <?php if($dailyTask->isNew()) { ?>
				<button class="btn btn-primary">Save</button>
                <?php } ?>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

