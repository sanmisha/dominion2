<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="box box-block bg-white">
			<div class="row">
				<div class="col-md-8">
					<h4><?php echo $title_for_layout; ?></h4>	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="box box-block bg-white">
	<legend>Task</legend>						
		 <div class="row">                                           
			<div class="col-sm-4">
				<strong>Project No:</strong> <?php echo $projectTasks['project']['project_no'];	?>
			</div>
			<div class="col-sm-4">
				<strong>Project Date:</strong> <?php echo $projectTasks['project']['project_date'];	?>
			</div>
		</div><br>
        <div class="row">                                           
			<div class="col-sm-4">
				<strong>Fees Type:</strong> <?php echo $projectTasks['fees_type'];	?>
			</div>
			<div class="col-sm-4">
				<strong>Days Require:</strong> <?php echo $projectTasks['days_require'];	?>
			</div>
		</div><br>
        <div class="row">                                           
			<div class="col-sm-4">
				<strong>List Order:</strong> <?php echo $projectTasks['list_order'];	?>
			</div>
			<div class="col-sm-4">
				<strong>Percentage:</strong> <?php echo $projectTasks['percentage'];	?>
			</div>
            <div class="col-sm-4">
				<strong>Fees:</strong> <?php echo $projectTasks['fees'];	?>
			</div>
		</div><br>
		<div class="row">                                           
			<div class="col-sm-4">
				<strong>Service:</strong> <?php echo $projectTasks['service'];	?>
			</div>
			<div class="col-sm-4">
				<strong>Description:</strong> <?php echo $projectTasks['description'];	?>
			</div>
			
		</div><br>
		
	</div>
<?php echo $this->Form->create('projectTasks',array('type'=>'file','noValidate'=>true)); ?>
	<div class="box box-block bg-white">
		 <div class="row">                                           
			<div class="col-sm-4">
				<?php
                //debug($employees->toArray());exit;
					echo $this->Form->hidden('id');
					echo $this->Form->control('task_id',array('value'=>$TaskId,'type'=>'hidden'));
				    echo $this->Form->control('assigned_to',['options'=>$employees,'empty'=>'---']); 
				?>
			</div>
			<div class="col-sm-4">
				<?php echo $this->N->datepicker('target_date'); ?>
           </div>
		</div>
	</div>
	<div class="box box-block bg-white">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
                    <?= $this->N->cancelLink(['action'=>'index']); ?>
                    <button class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>		
	</div>
<?php echo $this->Form->end(); ?>
