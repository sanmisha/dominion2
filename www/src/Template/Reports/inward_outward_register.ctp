<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create(null,['type'=>'file','novalidate'=>true, 'target'=>'_blank']) ?>
    <fieldset>
	<div class="row">
		<div class="col-md-3">
            <?php echo $this->N->datepicker('from_date',['required'=>true]); ?>
		</div>
		<div class="col-md-3">
            <?php echo $this->N->datepicker('to_date',['required'=>true]); ?>
		</div>
		<div class="col-md-3">
            <?php echo $this->N->select2('assigned_to',['options'=>$employees, 'empty'=>'---']); ?>
		</div>
		<div class="col-md-3">
            <?php echo $this->Form->control('status', ['options'=>['Inward'=>'Inward', 'Outward'=>'Outward', 'Assigned'=>'Assigned', 'Archive'=>'Archive'], 'empty'=>'---']); ?>
		</div>
		<div class="col-md-3">
            <?php echo $this->Form->control('export',['options'=>['pdf'=>'PDF','csv'=>'CSV']]); ?>
		</div>
	</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary">Submit</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

