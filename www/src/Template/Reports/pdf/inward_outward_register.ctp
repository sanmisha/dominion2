<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 7px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>Inward Outward Register</strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">
					<th width="10%">Register No</th>
					<th>Subject</th>
                    <th>Description</th>
                    <th width="10%">Assigned To</th>
                    <th width="12%">Assigned On</th>
                    <th width="12%">Timestamp</th>
                    <th width="15%">File No</th>
                    <th width="10%">Status</th>
                    <th width="10%">Handler</th>
				</tr>
			</thead>
			<tbody>
            <?php
                foreach($inwardOutwards as $inwardOutward) {
            ?>
            <tr>
                <td><?= $inwardOutward->register_no ?></td>
                <td><?= $inwardOutward->subject ?></td>
                <td><?= $inwardOutward->description ?></td>
                <td><?= $inwardOutward->assigned_employee ? $inwardOutward->assigned_employee->name : ''?></td>
                <td><?= $inwardOutward->assigned_on ?></td>
                <td><?= $inwardOutward->created ?></td>
                <td><?= $inwardOutward->location ? $inwardOutward->location->location : ''; ?> - <?= $inwardOutward->file_no ? $inwardOutward->file_no->file_no : ''; ?></td>
                <td><?= $inwardOutward->status ?></td>
                <td><?= $inwardOutward->handler ?> - <?= $inwardOutward->handler_id ?></td>
            </tr>
			<?php } ?>
			</tbody>
		</table>
	</body>
</html>
