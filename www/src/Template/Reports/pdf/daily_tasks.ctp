<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 7px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>Daily Tasks Report</strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">
					<th width="10%">Due Date</th>
					<th widht="20%">Project</th>
                    <th width="10%">Activity</th>
                    <th>Description</th>
					<th width="10%">Assigned To</th>
                    <th width="10%">Done Date</th>
                    <th width="10%">Duration</th>
                    <th width="10%">Status</th>
				</tr>
			</thead>
			<tbody>
            <?php
                foreach($dailyTasks as $dailyTask) {
            ?>
            <tr>
                <td><?= $dailyTask->due_date ?></td>
                <td><?= $dailyTask->project ? $dailyTask->project->project_no . ' ' . $dailyTask->project->client->client_name : '---'; ?></td>
                <td><?= $dailyTask->activity->activity ?></td>
                <td><?= $dailyTask->description ?></td>
                <td><?= $dailyTask->assigned_employee->name ?></td>
                <td><?= $dailyTask->done_date ?></td>
                <td><?= $dailyTask->duration ?></td>
                <td><?= $dailyTask->status ?></td>
            </tr>
			<?php } ?>
			</tbody>
		</table>
	</body>
</html>
