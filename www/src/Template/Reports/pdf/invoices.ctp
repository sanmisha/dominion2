<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 7px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>Invoices Report</strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">
					<th>#</th>				
					<th width="10%">Date</th>
					<th width="10%">Project No</th>
					<th>Client</th>
                    <th>Task</th>
                    <th width="10%">Before Tax</th>
                    <th width="10%">GST</th>
                    <th width="10%">Invoice Amount</th>
					<th width="10%">Received Amount</th>
					<th width="10%">Balanced Amount</th>
					<th>Cancel</th>
				</tr>
			</thead>
			<tbody>
            <?php
                foreach($invoices as $invoice) {
					$class = $invoice->is_cancel ? "cancel" : "";
            ?>
            <tr class="<?=$class?>">				
                <td><?= $invoice->invoice_no ?></td>
                <td><?= $invoice->invoice_date ?></td>
                <td><?= $invoice->project->project_no?></td>
                <td><?= $invoice->project->client->client_name?></td>
                <td><?= $invoice->invoice_details[0]->task->task?></td>
				<td align="right"><?= $this->Number->currency($invoice->amount_before_tax,'INR') ?></td>
                <td align="right"><?= $this->Number->currency($invoice->total_amount - $invoice->amount_before_tax,'INR') ?></td>
                <td align="right"><?= $this->Number->currency($invoice->total_amount,'INR') ?> </td>
				<td align="right"><?= $this->Number->currency($invoice->received_amount,'INR') ?></td>
				<td align="right"><?= $this->Number->currency($invoice->balance_amount,'INR') ?></td>
				<td><?= ($invoice->is_cancel== true) ? 'Yes' : 'No' ?></td>
            </tr>
			<?php
			 	$total_amount_before_tax += $invoice->amount_before_tax;
				$total_gst += $invoice->total_amount - $invoice->amount_before_tax;
				$all_total_amount += $invoice->total_amount;
				$total_received_amount += $invoice->received_amount;
				$total_balance_amount += $invoice->balance_amount;
			 } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5" align="right"><strong> Total :-</strong></td>
					<td align="right"> <?= $this->Number->currency( $total_amount_before_tax,'INR') ?></td>
					<td align="right"> <?= $this->Number->currency( $total_gst,'INR') ?></td>
					<td align="right"> <?= $this->Number->currency( $all_total_amount,'INR') ?></td>
					<td align="right"><?= $this->Number->currency( $total_received_amount,'INR') ?></td>
					<td align="right"><?= $this->Number->currency( $total_balance_amount,'INR') ?></td>
					
				</tr>
			</tfoot>
		</table>
	</body>
</html>
