<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 7px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>Invoice Receipts Report</strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">	
                    <th>#</th>					
					<th width="10%">Date</th>				
                    <th width="10%">Payment Mode</th>       
                    <th width="12%">Amount</th>                   	

				</tr>
			</thead>
			<tbody>
            <?php 
              $client = '';            
             foreach($invoiceReceipts as $invoice) {  ?>           
            <?php
                
                if($client != $invoice->client->client_name){  ?>              
               <tr> <td colspan="4"><?= $invoice->client->client_name?></td></tr>
                
            <?php   $client = $invoice->client->client_name; } ?>           
            
            <tr>               
                <td><?= $invoice->receipt_no ?></td>
                <td><?= $invoice->receipt_date ?></td> 
				<td><?= $invoice->payment_mode ?></td>  
                <td align="right"><?= $this->Number->currency($invoice->amount) ?></strong></td>                           
            </tr>             
          
			<?php $total += $invoice->amount; } ?>
			</tbody>
			<tfoot>
                <tr>
                    <td colspan="3" align="right"><strong> Total Amount</strong></td>
                    <td align="right"><strong><?= $this->Number->currency($total) ?></strong></td>    
                </tr>
				
			</tfoot>
		</table>
	</body>
</html>
