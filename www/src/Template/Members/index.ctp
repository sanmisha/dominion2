<div class="row">
    <div class="col-md-8">
        <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('name',['placeholder'=>'Name','label'=>false]); ?>		
        <?php echo $this->Form->input('search',['placeholder'=>'Mobile/Email/Pincode','label'=>false]); ?>		
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Form->end(); ?>    
        <script>
            $(document).ready(function(){
                $("#SearchForm").one("submit", function(e) {
                    e.preventDefault();
                    $.post('/admin/members', $('#SearchForm').serialize()).done(function(data){
                        $("#ajaxDiv").empty().append(data);					
                    });;
                });
            });
	    </script>
    </div>
    <div class="col-md-4">
        <div class="pull-right">
            <?= $this->N->add(array('action'=>'edit')); ?>
        </div>
    </div>
</div><br>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pincode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_count') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_limit') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wallet_balance') ?></th>
                
                <th scope="col" class="actions" width="15%"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($members as $member):  ?>
            <tr>
				<td><?= $member['user']['name'] ?></td>
				<td><?= $member['user']['email'] ?></td>
				<td><?= $member['user']['mobile'] ?></td>
				<td><?= $member['pincode'] ?></td>
                <td><div class="pull-right"><?= $member->order_count ?></div></td>				
                <td><div class="pull-right"><?= $this->Number->currency($member->order_value) ?></div></td>				
                <td><div class="pull-right"><?= $this->Number->currency($member['credit_limit']) ?></div></td>				
                <td><div class="pull-right"><?= $this->Number->currency($member['wallet_balance']) ?></div></td>				
				<td class="actions">
                    <?= $this->Html->link('Login',['controller'=>'Members','action'=>'sudo',$member->id],['class'=>'btn btn-warning btn-sm', 'target'=>'_blank']);?>
                    <?= $this->Html->link('Wallet',['controller'=>'Wallets','action'=>'index',$member->id],['class'=>'btn btn-info btn-sm']);?>
                    <?= $this->N->edit(['action' => 'edit', $member->id]); ?>
                    <?= $this->N->delete(['action' => 'delete', $member->id]); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
