<script>
	$(document).ready(function(){
		$("#member-country-id").on("change", function(){
			$("#member-state-id").empty();
			if($("#member-country-id").val()!="") {
				$.ajax({
					type: "GET",
					url: "/admin/Members/getStates/"+$("#member-country-id").val(),
					dataType:"json"
				}).done(function(data) {
					$.each(data.states, function(key, value) {
					$('#member-state-id')
						.append($("<option></option>")
									.attr("value",key)
									.text(value)); 
						
					});
					<?php if(!empty($user->member->state_id)) { ?>
						$('#member-state-id').val("<?php echo $user->member->state_id; ?>");
					<?php } ?>
				});
			}
		});
		$("#member-country-id").trigger("change");
	});
</script>


<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($user,['type'=>'file','novalidate'=>true]) ?> 
    <fieldset>
		<?php if(empty($user['id'])){ ?>
		<div class="row">
		   <div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('member.first_name'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.last_name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('mobile'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('email'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('member.pincode'); ?>
			</div>
		</div><br>
		<div class="row">
			<strong><h4>Billing Details</h4></strong><br>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.billing_address_1',['value'=>$user['member']['billing_address_1']]); ?>
				<?php echo $this->Form->control('member.billing_address_2',['value'=>$user['member']['billing_address_2']]); ?>
				<?php echo $this->Form->control('member.gstin',['value'=>$user['member']['gstin'],'label'=>'GSTIN']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.country_id',['options'=>$countries,'empty'=>true]); ?>
				<?php echo $this->Form->control('member.state_id',['empty'=>true]); ?>
				<?php echo $this->Form->control('member.city'); ?>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('password'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.credit_limit'); ?>
			</div>
		</div>
		<?php } else{ ?>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('member.first_name'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.last_name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('mobile'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('email'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('member.pincode'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.credit_limit'); ?>
			</div>
		</div><br>
		<div class="row">
		<strong><h4>Billing Details</h4></strong><br>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.gstin',['value'=>$user['member']['gstin'],'label'=>'GSTIN']); ?>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('member.billing_address_1',['value'=>$user['member']['billing_address_1']]); ?>
				<?php echo $this->Form->control('member.billing_address_2',['value'=>$user['member']['billing_address_2']]); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('member.country_id',['options'=>$countries,'empty'=>true]); ?>
				<?php echo $this->Form->control('member.state_id',['empty'=>true]); ?>
				<?php echo $this->Form->control('member.city'); ?>
			</div>
		</div><br>
		<?php }	?>
		</div><br>
     </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
	<?= $this->Form->end() ?>
</div>
