

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>		
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>    
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('siteVisit', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});			
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('visit_no')?></th>	
							<th scope="col"><?= $this->Paginator->sort('visit_date')?></th>	
							<th scope="col"><?= $this->Paginator->sort('visit_time')?></th>						
							<th scope="col"><?= $this->Paginator->sort('employee_id')?></th>
							<th scope="col"><?= $this->Paginator->sort('project_id')?></th>
                            <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($siteVisit as $sitevisit): ?>
                        <tr>
                            <td><?= $sitevisit->visit_no?></td>
							<td><?= $sitevisit->visit_date?></td>
							<td><?= $sitevisit->visit_time->format('h:i a') ?></td>
							<td><?= $sitevisit->employee->name?></td>
							<td><?= $sitevisit->project->project_with_client?></td>
                            <td class="actions">
                                <?= $this->N->editLink(['action' => 'edit', $sitevisit->id]); ?>
								<?= $this->Html->link('Print', '/files' .  DS . 'site_visits' . DS . 'site_visit-' . $sitevisit->visit_no . '.pdf' ,['class'=>'btn btn-info btn-sm', 'target'=>'_blank']); ?>
								<?= $this->N->deleteLink(['action' => 'delete', $sitevisit->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>	
<?php echo $this->element('Neptune.pagination'); ?>
