<?php
    use Cake\Core\Configure;
?>
<script>
	index = 0;  
	index = <?php echo count($siteVisit['site_visit_details'])+1; ?>;
    indexs = 0;
    indexs = <?php echo count($siteVisit['site_visit_images'])+1; ?>;
    k = 0;
    k = <?php echo count($siteVisit['site_visit_stakeholders'])+1; ?>;
    j = 0;
    j = <?php echo count($siteVisit['site_visit_visitors'])+1; ?>;

    function addRows(row, k) {
        html = "<tr id='rowss_"+k+"'>";
            html += "<td><a href='javascript:void(0); removeRows("+k+");'><i class='fa fa-minus'></i></a></td>";
            html += "<td><div class='form-group'>";
            html += "<td><input type='hidden' id='site-visit-stakeholders-" + k + "-stakeholder-id'  name='site_visit_stakeholders[" + k + "][stakeholder_id]' value=" + row.stakeholder_id + ">"+ row.stakeholder.contractor+"</td>";          
            html += "</div></td>";
        html += "</tr>";
        $(html).appendTo("#stakeholdersData tbody");       
        k++;
    
    } 
	

	function sectionChange(i){
        var id = $("#add-check-list-section-id").val();
        $("#add-check-list-item-id").empty();
        $.getJSON('/CheckListSections/get/' + id, function(data){
			items=data.check_list_items;
            $("#add-check-list-item-id")
				.append($("<option></option>")
				.attr("value",'')
                .text("- - -"));
			$.each(items, function(key, value) {
			$("#add-check-list-item-id")
				.append($("<option></option>")
				.attr("value",value.id)
				.text(value.item ));
		   });

        });
	}
    function projectChange(i){
        var id = $("#project-id").val();       
        $.getJSON('/Projects/getStakeholders/' + id, function(data){
            console.log(data.projects.project_stakeholders)
            data.projects.project_stakeholders.forEach(function(row, index) {
                    addRows(row, index);
            });

        });
	}
   


	function add_row() {
        data = $('#add-check-list-section-id').val();
        data_item = $('#add-check-list-item-id').val();
        section = $('#add-check-list-section-id').select2('data');
        item = $("#add-check-list-item-id").select2('data');
        remarks = $("#add-remarks").val();
        if(data == "" || data_item == ""){
            alert("this fields require");
            return false;
        } else {
            html = "<tr id='row_"+index+"'>";
                html += "<td> <a href='javascript:void(0); remove_row("+index+");'><i class='fa fa-minus'></i></a></td>";
                section.forEach(function(row, i){
                    html += "<td><input  type='hidden' id='site-visit-details-"+index+"-check-list-section-id' name='site_visit_details["+index+"][check_list_section_id]' value="+row.id+">"+row.text+"</td>";
                });
                item.forEach(function(col, i){
                    html += "<td><input type='hidden' id='site-visit-details-"+index+"-check-list-item-id'  name='site_visit_details["+index+"][check_list_item_id]'  value="+col.id+">"+col.text+"</td>";

                });
                if($('#drawing').is(':checked')){
                    html += "<td><td><div class='form-group'><input type='hidden' id='site-visit-details-"+index+"-as-per-drawing'   name='site_visit_details["+index+"][as_per_drawing]' value='true' ></div> Ok</td> </td>";
                } else {
                    html += "<td> <td><div class='form-group'><input type='hidden' id='site-visit-details-"+index+"-as-per-drawing'   name='site_visit_details["+index+"][as_per_drawing]' value='false'></div> Not Ok</td> </td>";
                }

            html += "</tr>";
            html2 = "<tr id='second_row_"+index+"'>";
                html2 += "<td>&nbsp;</td>";
                html2 += "<td colspan='12'><input  type='hidden' id='site-visit-details-"+index+"-remarks'  class='form-control' name='site_visit_details["+index+"][remarks]' placeholder='Remarks' value="+remarks+">"+remarks+"</td>";
            html2 += "</tr>";
            $(html).appendTo("#tblDetails tbody");
            $(html2).appendTo("#tblDetails tbody");
            index++;
            $("#add-check-list-item-id").empty();
            $("#add-check-list-item-id")
				.append($("<option></option>")
				.attr("value",'')
                .text("- - -"));
            $("#add-as-per-drawing").empty();
            $("#add-remarks").val('');
        }
	}
    function add_rows() {
		html = "<tr id='rowws_"+j+"'>";
			html += "<td><a href='javascript:void(0); remove_rows("+j+");'><i class='fa fa-minus'></i></a></td>";			
			html += "<td><div class='form-group'><input type='text' id='site-visit-visitors-"+j+"-name'  class='form-control qt' name='site_visit_visitors["+j+"][name]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='site-visit-visitors-"+j+"-email'  class='form-control rt' name='site_visit_visitors["+j+"][email]'></div></td>";		
		html += "</tr>";
		$(html).appendTo("#visitorsData tbody");
		j++;		
	}
    function addRow(){       
        html ="<tr id='rows_"+indexs+"'>";
            html += "<td><a href='javascript:void(0); removeRow("+indexs+");'><i class='fa fa-minus'></i></a></td>";
            html += "<td><input id='site-visit-images-"+indexs+"-caption' name='site_visit_images["+indexs+"][caption]' class='form-control'></td>";
            html += "<td><input type='file' id='site-visit-images-"+indexs+"-image' name='site_visit_images["+indexs+"][image]' class='form-control'></td>";
            html += "<td><input type='hidden' id='site-visit-images-"+indexs+"-image-dir' name='site_visit_images["+indexs+"][image.dir]' class='form-control'></td>";
          
         html += "</tr>";
        html2 ="<tr id='second_rows_"+indexs+"'>";
            html2 +="<td>&nbsp; </td>";
            html2 +="<td colspan='12'><input id='site-visit-images-"+indexs+"-remarks' name='site_visit_images["+indexs+"][remarks]' class='form-control'  placeholder='Remarks'></td>";
        html2 += "</tr>";
        $(html).appendTo("#tblDetail tbody");
        $(html2).appendTo("#tblDetail tbody");
        indexs++;
    }  

	function remove_row(i) {
		$("#row_"+i).detach();
		$("#second_row_"+i).detach();
		index--;
	}
    function removeRow(i) {
		$("#rows_"+i).detach();
		$("#second_rows_"+i).detach();
		indexs--;
	}
    function removeRows(k) {
        $("#rowss_"+k).detach();
        k--;		
    }
    
	function remove_rows(j) {
		$("#rowws_"+j).detach();
		j--;	
	}
   
</script>
<?= $this->Form->create($siteVisit,['type'=>'file','novalidate'=>true]) ?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('visit_no', ['readonly'=>true]); ?>
			</div>
			<div class="col-md-4">

				<?php echo $this->N->datepicker('visit_date',['required'=>true]); ?>
			</div>
			<div class="col-md-4">
                <?php if(empty($siteVisit->id)){
                   echo  $this->N->select2('visit_time', ['options'=>Configure::read('time'), 'empty'=>true]);
                } else{
                    echo $this->Form->control('visit_time', [ 'type'=>'text', 'value'=>$siteVisit->visit_time->format('g:i a'),'readonly'=>true]);
                } ?>
        	</div>

		</div>
		<div class="row">
            <div class="col-md-4">
				<?php echo $this->N->select2('employee_id', ['options'=>$employees , 'empty'=>'---']); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->N->select2('project_id', ['options'=>$project , 'empty'=>'---', 'onChange'=>'return projectChange()']); ?>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('report'); ?>
			</div>
		</div>
		</fieldset><br>
</div>
<div class="box box-block bg-white">
    <div class="row">
        <div class="col-md-4">
            <?php echo $this->N->select2('add.check_list_section_id', array('label'=>'Section', 'value'=>$checkListSections, 'onChange'=>'return sectionChange()')); ?>
        </div>
        <div class="col-md-4">
            <?php echo $this->N->select2('add.check_list_item_id', array('label'=>"Item")); ?>
        </div>
        <div class="col-md-4">
            <br>
            <span>As Per Drawing</span> &nbsp;
            <?php echo $this->Form->checkbox('add.as_per_drawing',[ 'id'=>'drawing']);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php echo $this->Form->control('add.remarks',['type'=>'text', 'label'=>false,  'placeholder'=>'Remarks']);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <button type="button" id="add" class="btn btn-primary add" onclick="add_row()">Add</button>
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-md-12">
		    <legend><span>Site Visits Details</span></legend>
            <table class="table  table-hover" id="tblDetails">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Check Lists Section</th>
                        <th>Check Lists Items</th>
                        <th>Drawing</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($siteVisit['site_visit_details'])) {
                        foreach($siteVisit['site_visit_details'] as $i=>$detail) {
                ?>
                    <tr id="row_<?php echo $i;?>">
                        <td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                         <td>
                            <?php
                                echo $this->Form->input('site_visit_details.'.$i.'.id',['type'=>'hidden']);
                                echo $this->Form->hidden('site_visit_details.'.$i.'.check_list_section_id', array('label'=>false, 'value'=>$detail['check_list_section_id']));
                            ?>
                            <?=  (!empty($detail->id)) ? $detail->check_list_section->section : $detail->check_list_section_id ?>
                        </td>
                        <td><?php echo $this->Form->hidden('site_visit_details.'.$i.'.check_list_item_id', array('label'=>false, 'value'=>$detail['check_list_item_id'])); ?>
                        <?= (!empty($detail->id)) ? $detail->check_list_item->item : $detail->check_list_item_id?></td>
                        <td>
                            <?php echo $this->Form->hidden('site_visit_details.'.$i.'.as_per_drawing',['label'=>false, 'value'=>$detail['as_per_drawing']]);?>
                            <?= ($detail->as_per_drawing == true) ? 'Ok' : 'Not Ok' ?>
                        </td>
                    </tr>
                    <tr id="second_row_<?php echo $i;?>">
                        <td>&nbsp;</td>
                        <td colspan="12">
                            <?php echo $this->Form->hidden('site_visit_details.'.$i.'.remarks',['type'=>'text', 'label'=>false, 'class'=>'span12' , 'value'=>$detail['remarks'], 'placeholder'=>'Remarks']);?>
                            <?= $detail->remarks ?>
                        </td>
                    </tr>

                <?php  }} ?>
                </tbody>
            </table>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            <legend><span>Site Visits Images</span></legend>
            <table class="table  table-hover" id="tblDetail">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Caption</th>
                        <th>Image</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($siteVisit['site_visit_images'])) {
                        foreach($siteVisit['site_visit_images'] as $i=>$image) {
                ?>
                    <tr id="rows_<?php echo $i;?>">
                        <td><a href='javascript:void(0); removeRow("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                        <td>
                            <?php
                                echo $this->Form->input('site_visit_images.'.$i.'.id',['type'=>'hidden']);
                                echo $this->Form->control('site_visit_images.'.$i.'.caption', array('label'=>false, 'value'=>$image['caption']));
                            ?>
                        </td>
                        <td>
                            <?php                               
                            if(!empty($image->id)){
                                echo $this->Form->hidden('site_visit_images.'.$i.'.image',['value'=>$image->image]);
                                echo $this->Html->image('/files/sitevisitimages/image/'.$image->image_dir.'/'.$image->image, array('width'=>'150px', 'hieght'=>'150px'));
                            } else {
                                echo $this->Form->control('site_visit_images.'.$i.'.image',['value'=>$image->image, 'type'=>'file', 'label'=>false]);
                            }

                        ?>
                        </td>
                        <td><?php echo $this->Form->hidden('site_visit_images.'.$i.'.image_dir',[ 'value'=>$image->image_dir]);?></td>
                    </tr>
                    <tr id="second_rows_<?php echo $i;?>">
                        <td>&nbsp;</td>
                        <td colspan="3"><?php echo $this->Form->control('site_visit_images.'.$i.'.remarks', ['type'=>'text', 'label'=>false, 'class'=>'span12' , 'value'=>$image->remarks, 'placeholder'=>'Remarks']);?></td>
                    </tr>

                <?php  }} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><a href="javascript:void(0); addRow();">+</a></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <legend><span> Stake Holders </span></legend>
		<table class="table table-striped table-condense table-responsive" id="stakeholdersData">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th style="">Stake Holders </th>					
				</tr>
			</thead>
			<tbody>
			<?php
				if(!empty($siteVisit['site_visit_stakeholders'])) {
					foreach($siteVisit['site_visit_stakeholders'] as $i=>$stakeholder) {
			?>
			<tr id="rowss_<?php echo $i;?>">
				<td><a href='javascript:void(0); removeRows("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
				<td>
					<?php
						echo $this->Form->input('site_visit_stakeholders.'.$i.'.id',['type'=>'hidden']);
						echo $this->N->select2('site_visit_stakeholders.'.$i.'.stakeholder_id',array('label'=>false,'options'=>$stakeholders,'empty'=>'(Choose One)'));
					?>
				</td>			
			</tr>
			<?php  }} ?>
			</tbody>			
		</table>	
        </div>		        
	
        <div class="col-md-6">
        <legend><span>Site Visit Visitors Details</span></legend>
        <table class="table table-striped table-condense table-responsive" id="visitorsData">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Name</th>
                    <th>Email</th>
                  
                </tr>
            </thead>
            <tbody>
            <?php
                if(!empty($siteVisit['site_visit_visitors'])) {
                    foreach($siteVisit['site_visit_visitors'] as $i=>$visitor) {
            ?>
                <tr id="rowws_<?php echo $i;?>">
                    <td><a href='javascript:void(0); remove_rows("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                    <td>
                        <?php
                            echo $this->Form->input('site_visit_visitors.'.$i.'.id',['type'=>'hidden']);
                            echo $this->Form->control('site_visit_visitors.'.$i.'.name',array('label'=>false));
                        ?>
                    </td>
                    <td><?php echo $this->Form->control('site_visit_visitors.'.$i.'.email',['label'=>false,]);?></td>
                </tr>
            <?php  }} ?>
            </tbody>
            <tfoot>
                <tr>
                    <td><a href="javascript:void(0); add_rows();">+</a></td>
                    <td colspan="2"></td>
                </tr>              
            </tfoot>
        </table> 
        </div>            
    </div>
    <fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>

<?= $this->Form->end() ?>

