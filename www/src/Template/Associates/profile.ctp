<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>

<?= $this->Form->create($associate,['type'=>'file','novalidate'=>true]) ?> 
<div class="box box-block bg-white">
		<fieldset>
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->Form->hidden('id'); ?>
					<?php echo $this->Form->hidden('parent_id'); ?>
					<?php echo $this->Form->control('associate_name', ['readonly'=>true]); ?>
				</div>
			</div>
		</fieldset>
		
		<fieldset>
		<legend><?= __('Address Details') ?></legend>	
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('address_line_1'); ?>
					<?php echo $this->Form->control('address_line_2'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->control('state'); ?>
					<?php echo $this->Form->control('city'); ?>
					<?php echo $this->Form->control('pincode'); ?>
				</div>
			</div>
		</fieldset><br>


		<fieldset>
			<legend><?= __('Bank Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('bank'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->control('branch'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('account_no'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->control('ifsc_code', ['label'=>'IFSC Code']); ?>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend><?= __('Other Details') ?></legend>
			<div class="row">
				<div class="col-md-3">
					<?php echo $this->Form->control('pan_no'); ?>
				</div>		
				<div class="col-md-3">
					<?php echo $this->Form->control('aadhar'); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->control('gstin', ['label'=>'GSTIN']); ?>
				</div>
			</div>		
		</fieldset><br>
		<fieldset>
			<legend><?= __('Login Details') ?></legend>
			<div class="row">
				<div class="col-md-3">
					<?php 
						echo $this->Form->control('teams.0.id'); 
						echo $this->Form->control('teams.0.user.id'); 
						echo $this->Form->control('teams.0.user.username', ['readonly'=>true]); 
					?>
				</div>		
				<div class="col-md-3">
					<?php echo $this->Form->control('teams.0.user.mobile'); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->control('teams.0.user.email'); ?>
				</div>			
			</div>
		</fieldset><br>		
	</div>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary">Update</button>
			</div>
		</div>	
	</fieldset>	
<?= $this->Form->end() ?>
