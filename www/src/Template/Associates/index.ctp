<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
	<?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>		
	<?php echo $this->Form->input('referral',['placeholder'=>'Referral','label'=>false]); ?>
	<?php echo $this->Form->input('active',['options'=>['Active'=>'Active', 'Non Active'=>'Non Active'],'label'=>false, 'empty'=>'All']); ?>		
	<?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
	<?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
	<?php echo $this->Form->end(); ?>    
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('associates', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					//$("#ajaxDiv").html(data);
					NProgress.done();
				});;
			});			
		});
	</script>
</div>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit', 0)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
                        <tr>
                        	<th scope="col"><?= $this->Paginator->sort('associate_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('referral.associate_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('associate_type') ?></th>
							<th scope="col"><?= $this->Paginator->sort('joining_date') ?></th>
							<th scope="col"><?= $this->Paginator->sort('active') ?></th>
							<th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
                        </tr>
					</thead>
					<tbody>
                        <?php foreach ($associates as $associate) { ?>
                            <tr>
                                <td><?= $associate->associate_name ?></td>
								<td><?= $associate->referral->associate_name ?></td>
                                <td><?= $associate->associate_type ?></td>
                                <td><?= $associate->joining_date ?></td>
								<td>
									<?php if($associate->active){?>
										<span class="tag tag-pill tag-success">Active</span>
									<?php } ?>
								</td>
                                <td class="actions pull-right">
									<?php 
										if(!$associate->active) {
											echo $this->Html->link('Activate',['action'=>'activate',$associate->id],['class'=>'btn btn-primary btn-sm']); 
										}
									?>
                                    <?= $this->N->editLink(['action' => 'edit', $associate->id]); ?>
                                    <?= $this->N->deleteLink(['action' => 'delete', $associate->id]); ?>
                                </td>
                            </tr>
                        <?php } ?>
					</tbody>
				</table>
			</div>
       </div>
	</div>
</div>	
<?php echo $this->element('Neptune.pagination'); ?>
