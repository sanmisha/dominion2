<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
	use Cake\Core\Configure;
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($associate,['type'=>'file','novalidate'=>true]) ?> 
    <fieldset>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->hidden('parent_id'); ?>
				<?php echo $this->Form->control('associate_name', ['readonly'=>true]); ?>
			</div>
		</div>
	</fieldset>
	<fieldset>
	<fieldset>
		<legend><?= __('Other Details') ?></legend>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->control('associate_type',['options'=>Configure::read("associateTypes")]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->datepicker('joining_date',['type'=>'date','label'=>'Joining Date', 'required'=>true]); ?>
			</div>	
			<div class="col-md-3">
				<br />
				<?php //echo $this->Form->control('active'); ?>
			</div>			
		</div>
	</fieldset>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Activate</button>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>
