<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
	use Cake\Core\Configure;
?>
<script>
	var cost = <?=json_encode($cost)?>;
	$(document).ready(function(){
		$("#associate-type").on("change", function(){
			console.log(cost[$(this).val()]);
			$("#cost").val(cost[$(this).val()]);
			$("#cost-label").text('Rs. ' + cost[$(this).val()] + '/-');			
		});

		$("#i-agree").on("click", function(){
			$("#save").attr("disabled", !$(this).prop("checked"));
		});

		$("#pay-later").on("click", function(){
			$("#save").html("Pay Now");
			if($(this).prop("checked")) {
				$("#save").html("Save");
			}
		});
		
		$("#i-agree").prop("checked", false);
		$("#pay-later").prop("checked", false);
		$("#associate-type").trigger("change");
	});
</script>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?> to the <?= $side ?></h5>
	</div>
    <?= $this->Form->create($associate,['type'=>'file','novalidate'=>true]) ?> 
    <fieldset>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->hidden('parent_id'); ?>
				<?php echo $this->Form->control('associate_name'); ?>
			</div>
		</div>
	</fieldset>
	<fieldset>
	<legend><?= __('Address Details') ?></legend>	
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('address_line_1'); ?>
				<?php echo $this->Form->control('address_line_2'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('state'); ?>
				<?php echo $this->Form->control('city'); ?>
				<?php echo $this->Form->control('pincode'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Bank Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('bank'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('branch'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('account_no'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('ifsc_code', ['label'=>'IFSC Code']); ?>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= __('Other Details') ?></legend>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->control('associate_type',['options'=>Configure::read("associateTypes")]); ?>
			</div>
			<div class="col-md-3">
				<br /><br />
				<h5 id="cost-label">-</h5>
				<?php echo $this->Form->control('cost',['label'=>false, 'type'=>'hidden']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->control('pan_no'); ?>
			</div>		
			<div class="col-md-3">
				<?php echo $this->Form->control('aadhar'); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('gstin', ['label'=>'GSTIN']); ?>
			</div>
			<?php if(!$associate->isNew()) { ?>
			<div class="col-md-3">
				<?php echo $this->Form->control('commission'); ?>
			</div>
			<?php } ?>
		</div>		
	</fieldset><br>	
	<?php if($associate->isNew()) { ?>
		<fieldset>
			<legend><?= __('Login Details') ?></legend>
			<div class="row">
				<div class="col-md-3">
					<?php echo $this->Form->control('username'); ?>
				</div>		
				<div class="col-md-3">
					<?php echo $this->Form->control('mobile'); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->control('email'); ?>
				</div>			
				<div class="col-md-3">
					<?php echo $this->Form->control('password'); ?>
				</div>			
			</div>
		</fieldset><br>	
	<?php } else { ?>
	<fieldset>
		<legend>User</legend>	
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">Name</th>
								<th scope="col">Username</th>
								<th scope="col">Email</th>
								<th scope="col">Mobile</th>
								<th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($associate->teams as $team) { //debug($subscription);exit; ?>
							<tr>
								<td><?= $team->user->name ?></td>
								<td><?= $team->user->username ?></td>
								<td><?= $team->user->email ?></td>
								<td><?= $team->user->mobile ?></td>
								<td class="actions">
									<?= $this->Html->link('Login',['action'=>'sudo',$team->user->id],['class'=>'btn btn-primary btn-sm']); ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>			
	</fieldset>
	<?php } ?>
	<fieldset>
		<div class="row ">
			<div class="col-md-4">
				<?php echo $this->Form->control('i_agree',['type'=>'checkbox', 'label'=>'&nbsp;I Agree <a href="/terms-and-conditions" target="_blank">Terms & Conditions</a>', 'escape'=>false]); ?>			
			</div>
			<div class="col-md-4">
				<?php 					
					if($associate->isNew()) {						
						echo $this->Form->control('pay_later',['type'=>'checkbox', 'label'=>'&nbsp;Pay Later', 'escape'=>false]); 
						$button = "Pay Now";
					} else {
						echo $this->Form->hidden('pay_later',['value'=>true]); 
						$button = "Save";
					}
				?>			
			</div>			
			<div class="col-md-4 pull-right">
				<div class="pull-right">
					<?= $this->N->cancelLink(['action'=>'index']); ?>
					<button class="btn btn-primary" id="save" disabled><?=$button?></button>
				</div>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>
