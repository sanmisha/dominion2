<?php
    $this->Html->css('orgchart', ['block' => true]);
    $this->Html->script('orgchart', ['block' => true]);

    if(!function_exists('getChildren')) {
        function getChildren($child) {
            if(isset($child->children) && !empty($child->children)) {
                echo "<ul>";
                foreach($child->children as $child) {
                    echo "<li id='".$child->id."'>";
                    echo $child->associate_name;
                    if($child->active) {
                        echo "&nbsp;<strong>(A)</strong>";
                    }
                    echo "<br />" . $child->associate_type;
                    getChildren($child);
                    echo "</li>";
                }
                echo "</ul>";            
            } else {
                return;
            }
        }
    }
 ?>
<script>
$(document).ready(function () {
    // create a tree
    $("#tree-data").jOrgChart({
        chartElement: $("#tree-view"), 
        nodeClicked: nodeClicked
    });
    
    // lighting a node in the selection
    function nodeClicked(node, type) {
        if(node.attr("id")!='root') {
            node = node || $(this);
            btn = $("#addBtn");
            //btn.attr('href', '/associates/edit/0/' + node.attr("id"));
            $('.jOrgChart .selected').removeClass('selected');
            node.addClass('selected');
        } else {
            //btn.attr('href', '#');
        }
    }
});
</script>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<!--
        <div class="float-xs-right">
            <a href="/cp/associates/edit/0/right" class="btn btn-outline-primary pull-right" id="addBtn"><span class="fa fa-plus"></span> Right</a>
            <a href="/cp/associates/edit/0/left" class="btn btn-outline-primary pull-right" id="addBtn"><span class="fa fa-plus"></span> Left</a>
		</div>
        -->
	</div>
	<div class="row">
        <div class="col-md-12" style="overflow: scroll;">
            <ul id="tree-data" style="display:none">
                <li id="root">
                    <?php
                        // Print Parent
                        echo $associate->associate_name;

                        echo "<ul>";
                        foreach($children as $child) {
                            echo "<li id='".$child->id."'>";
                            echo $child->associate_name;
                            if($child->active) {
                                echo "&nbsp;<strong>(A)</strong>";
                            }
                            echo "<br />" . $child->associate_type;
                            getChildren($child);
                            echo "</li>";
                        }
                        echo "</ul>";
                    ?>
                </li>
            </ul>
            <div id="tree-view"></div>		        
        </div>
    </div>
</div>