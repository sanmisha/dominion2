<h1>Contact Us</h1>
<h3>Office Address:</h3>
<p>
    <strong>My Dominion 2</strong><br />
    An Initiative by Yogeshwari Enterprises<br />
    Unit No. 303, Bhoomi Velocity,<br />
    Road No. 23, Wagle Estate,<br />
    Thane West, Mumbai - 400604<br />
    Contact Person: Mr. Lince Vergis
</p><br />
<h3>Customer Care</h3>
<p>
    <strong>Ms. Jayashree</strong><br />
    <a class="play-icon popup-with-zoom-anim" href="mailto:support@myedirectory.com">
        <i class="fa fa-envelope"></i>&nbsp;support@myedirectory.com
    </a><br />
    <a class="play-icon popup-with-zoom-anim" href="#">
        <i class="fa fa-phone-square"></i>&nbsp;+91 93212 81973
    </a>    
</p>
