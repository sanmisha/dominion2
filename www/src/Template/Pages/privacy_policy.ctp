<h1>Privacy Policy</h1>
<p>This Website Privacy Policy describes how Yogeshwari Enterprises (hereinafter &ldquo;myedirectory&rdquo;) uses personal&nbsp;data collected or received from visitors (&ldquo;Visitors&rdquo;) of this website (&ldquo;Website&rdquo;). It also describes how we use personal data collected when you register as a Listed Member (&ldquo;Customer&rdquo;) or register as Business Associate&nbsp;(&ldquo;BA&rdquo;). It describes how we collect or receive your personal data, the types of personal data we process, how&nbsp;we use, share and protect these data, how long we retain these data, your rights with respect to the&nbsp;processing of your personal data, and how you can contact us about our privacy practices.&nbsp;</p>
<h3>How We Collect or Receive Personal Data from You When you Visit the Website</h3>
<p>Myedirectory collects personal data from you through your use of the Website.</p>
<p>You may choose to submit your personal data and other information to us through various communication&nbsp;channels provided on the &ldquo;Contact Us&rdquo; webpage of the Website, for example, Myedirectory e-mail addresses,<br />telephone numbers or online forms.</p>
<p>Where we ask you to provide us with your personal data, we will indicate whether providing the personal data&nbsp;is a statutory or contractual requirement, or a requirement necessary to enter into and/or facilitating a<br />contract, as well as whether you are obliged to provide the personal data and of the possible consequences of&nbsp;a failure to provide the personal data.</p>
<p>We do not enable third parties to place cookies on your device. As long as you do not log on to our services,&nbsp;your data is anonymous and we cannot link any provided information to you.</p>
<h3>The Types of Personal Data We Collect From Visitors</h3>
<p>Through the communications channels made available on the Website&rsquo;s &ldquo;Contact Us&rdquo; webpage, we collect&nbsp;your contact details, such as your name, email address, address and telephone number. You may also<br />voluntarily provide other data that is related to you in connection with your inquiries or comments. We&nbsp;encourage you, however, to provide no more personal data relating to you than is necessary in order for us to<br />provide an appropriate response to your inquiries or comments.</p>
<h3>How We Collect or Receive Personal Data from You When You Are Registering as a Customer</h3>
<p>Myedirectory collects and receives personal data from its Customers in various ways. We collect personal data&nbsp;from you when you register as a Customer on our websites. You may also choose to submit your personal data&nbsp;and other information to us through various communication channels provided on the &ldquo;Contact Us&rdquo; webpage&nbsp;of our websites, for example, Myedirectory e-mail addresses, telephone numbers or online forms.</p>
<p>We also receive personal data of our Customers from the BAs with whom the concerned Customers have an&nbsp;existing relationship (such as when Customers have bought Myedirectory products from the BA).&nbsp;</p>
<h3>The Types of Personal Data We Collect From Customers</h3>
<p>If you are registering as a Customer directly with Myedirectory, we collect only Contact information (eg.,&nbsp;name, postal or e-mail address, fax number and phone number); and Login Information incl. Username,&nbsp;Connect ID and Password.&nbsp;</p>
<p>In addition, when you communicate with us through various communications channels, we collect your&nbsp;contact details, such as your name, email address, address and telephone number. You may also voluntarily&nbsp;provide other data that is related to you in connection with your inquiries or comments. We encourage you,&nbsp;however, to provide no more personal data relating to you than is necessary in order for us to provide an&nbsp;&nbsp;appropriate response to your inquiries or comments.</p>
<p>We may collect other types of personal data in exceptional circumstances only. If we do need to process&nbsp;additional types of personal data about you, we will make sure that you are informed about the processing of<br />your personal data and that there is a valid legal basis for doing so (e.g.&nbsp;your consent).&nbsp;</p>

<h3>How We Process the Personal Data We Collect From Customers</h3>
<p>We only process your personal data to the extent it is necessary (a) for the communication purpose; (b) to&nbsp;comply with our legal obligations; or (c) for the purpose of our legitimate interests, including operating,<br />evaluating and improving our services; preventing and protecting us and others against fraud, unauthorized&nbsp;transactions, claims and other liabilities; and ensuring compliance with company policies and industry<br />standards. For companies like Myedirectory that have online business operations, processing customer&rsquo;s&nbsp;personal data for internal administrative purposes is typically also considered a legitimate interest. As required<br />under law, we have carefully balanced our legitimate business interests against your data protection rights.</p>
<p>In particular, we process the personal data we obtain about you to:</p>
<ul>
<li>Manage your registration as a Customer;</li>
<li>Create and manage your online account;</li>
<li>Address your comments or inquiries</li>
<li>Process your orders of products or services;</li>
<li>Communicate with you;</li>
<li>Bring you in contact with your BAs (including providing your name, address, telephone number or e-mail address, purchases and purchasing history to your BA);</li>
<li>Operate, evaluate and improve our services and facilitate your use of our websites;</li>
<li>Perform accounting, auditing, billing and collection activities; and</li>
<li>Comply with applicable legal requirements, industry standards and our policies.</li>
</ul>
<h3>How We Process the Personal Data We Collect From Business Associates</h3>
<p>We only process your personal data to the extent it is necessary (a) for the communication purpose; (b) to&nbsp;comply with our legal obligations; or (c) for the purpose of our legitimate interests, including operating,<br />evaluating and improving our services; preventing and protecting us and others against fraud, unauthorized&nbsp;transactions, claims and other liabilities; and ensuring compliance with company policies and industry<br />standards. For companies like Myedirectory that have online business operations, processing customer&rsquo;s&nbsp;personal data for internal administrative purposes is typically also considered a legitimate interest. As required<br />under law, we have carefully balanced our legitimate business interests against your data protection rights.&nbsp;</p>
<p>In particular, we process the personal data we obtain about you to:</p>
<ul>
<li>Manage your registration as a Customer;</li>
<li>Create and manage your online account;</li>
<li>Address your comments or inquiries</li>
<li>Process your orders of products or services;</li>
<li>Communicate with you;</li>
<li>Bring you in contact with your BAs (including providing your name, address, telephone number or e-mail address to your BA);</li>
<li>Operate, evaluate and improve our services and facilitate your use of our websites;</li>
<li>Perform accounting, auditing, billing and monitoring activities;</li>
<li>Comply with applicable legal requirements, industry standards and our policies; and</li>
<li>Credit the payout due to your Bank Account for BA</li>
</ul>

<h3>How We Share Personal Data</h3>
<p>Myedirectory does not sell, rent or trade your personal data. Myedirectory does share your personal data<br />with:</p>
<ol>
<li>Entities within the Myedirectory group to whom it is reasonably necessary or desirable for&nbsp;Myedirectory to disclose personal data;</li>
<li>Myedirectory Business Associates, to allow communication regarding product advice, ordering advice,&nbsp;product information for the products that you order from Myedirectory and the Myedirectory&nbsp;Business Opportunity;</li>
<li>Processors who provide us with Infrastructure- or Platform- or Software-as-a-Service solutions,&nbsp;software development services, information system maintenance services, record management&nbsp;services or marketing services; and</li>
<li>Government authorities or other third parties, if required by law or reasonably necessary to protect&nbsp;the rights, property and safety of others or ourselves.</li>
</ol>
<h3>How We Protect Personal Data</h3>
<p>We maintain appropriate technical and organizational security safeguards designed to protect the personal&nbsp;data we process against accidental, unlawful or unauthorized destruction, loss, alteration, access, disclosure or&nbsp;use.</p>
<h3>How Long We Retain Personal Data</h3>
<p>We store personal data only for as long as necessary to fulfil the purposes for which we collect the data (see&nbsp;above under &ldquo;How We Use the Personal Data We Collect&rdquo;), except if required otherwise by law.&nbsp;</p>
<p>Links to Other Websites</p>
<p>The Website may contain links to websites maintained by third parties, whose information and privacy&nbsp;practices are different than ours. We are not responsible or liable for the information or privacy practices<br />employed by such third parties. We encourage you to read the privacy statements of all third party websites&nbsp;before using such websites or submitting any personal data or any other information on or through such<br />websites.</p>
<h3>Updates to the Website Privacy Policy</h3>
<p>Myedirectory reserves the right to change this Website Privacy Policy at any time. If we change this Website&nbsp;Privacy Policy we will update the effective date and such changes will be effective upon posting. We encourage&nbsp;you to review the Website Privacy Policy regularly.</p>
<h3>Your Rights</h3>
<p>Your rights under applicable law may include&nbsp;access&nbsp;to the personal data we process about you, the right to&nbsp;have such personal data&nbsp;corrected&nbsp;or&nbsp;erased, the right to&nbsp;restrict the processing&nbsp;of your personal data, as well&nbsp;as the right to&nbsp;data portability. Where we have obtained your consent for the processing of your personal&nbsp;data, you have the right to&nbsp;withdraw your consent&nbsp;at any time. This will not affect the lawfulness of the<br />processing that has happened based on your consent prior to the withdrawal. To exercise these rights, you&nbsp;should make a written request using our contact details set out below. If you are not satisfied with the way we&nbsp;handle your request, you may lodge a complaint with a supervisory authority.&nbsp;</p>
<p>You also have the right to object, at any time, to the processing of your personal data which is based on&nbsp;Myedirectory&rsquo;s legitimate interests. Where we process your personal data for direct marketing purposes, you<br />have the right to object at any time to such processing, including for profiling purposes to the extent that it is&nbsp;related to direct marketing. If you object to processing for direct marketing purposes, we will no longer<br />process your personal data for such purposes.</p>
<h3>How to Contact Us</h3>
<p>If you have any comments or inquiries about this Website Privacy Policy, if you would like to update information we have about you, or to exercise your rights, you may contact us or write to us care@Myedirectory.com.</p>