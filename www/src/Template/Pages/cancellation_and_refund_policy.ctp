<h1>Cancellation / Refund / Upgradation Policy</h1>
<h3>Policy for Business Associates:</h3>
<p>Myedirectory is having a unique plan for payout. As per the plan, once any new Business Associate&nbsp;joins the family, system does all the calculations immediately and update is available on the&nbsp;dashboard of all concerned Associates. Not only calculation of payout, but the up-gradation of&nbsp;qualifying ID took place immediately. On next working day, payout is also released to all those who&nbsp;qualify as per plan. At the same time as mentioned in company policy, before joining as Business&nbsp;Associate, the visitor had obtained all information &ndash; terms and conditions, product, business plan&nbsp;etc. and then joined.</p>
<p>Once the ID is active in the system, it therefore can neither be cancelled nor upgraded. Upgradation is possible only as per business plan or at the time of renewal.</p>
<p>Amount paid towards activation of ID as Business Associate is not refunded in any circumstance.</p>
<h3>Policy for Listed Members:</h3>
<p>Company&rsquo;s product i.e. Business Listing Entries are sold to Listed Members (LM) only by Business&nbsp;Associates. As they represent the company, we took the ownership of the same. If the issue does not<br />resolve at Associate level, proposed listed member can get in touch with us.</p>
<p>For us our Customer is always first. Rest assured; we will ensure that proposed Listed Member will&nbsp;get amount refunded for unused entry.</p>
<p>Once the entry is used, company will not refund the amount.</p>