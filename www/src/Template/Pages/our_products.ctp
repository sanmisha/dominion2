<h1>Our Products</h1>
<p>Company has made available an online directory which is available to everyone on
myedirectory.com. Company is offering Product in the form of an <strong>“Entry”</strong> in this myedirectory where
you can get your business registered whether it is of Product or Service.</p>
<p><strong>Duration:</strong> One year from the date of listing..</p>
<p><strong>Charges:</strong> MRP Rs. 2999/- including applicable GST per listing</p>
<p><strong>Benefits to Listed Members:</strong></p>
<ul>
<li>Get your business listed in the Dominion 2!</li>
<li>Create online presence where you will get
    <ul>
        <li>Dedicated Page</li>
        <li>Full Size Banner</li>
        <li>Space for Product / Service Information</li>
        <li>Space for Social Media Links and integration with Facebook, Twiter, Whatsapp, Linked In etc as per your business need</li>
        <li>Space for Google Maps</li>
    </ul>
</li>
<li>Set custom search criteria for own business</li>
<li>Increase the possibility of growth by generating more leads</li>
<li>For Discount offers contact our associate today!!</li>
</ul>
<h3>How to get your business listed:</h3>
<p>Company did not sell these entries directly. Online Listing Entries are available through our associate
members only. One, who has referred this opportunity to you, can support you. Else, contact us and
provide your details. We will ensure that our nearest associate will get in touch with you.</p>
<h3>Step by Step guidelines on how to get your business registered on myedirectory:</h3>
<ol>
<li>Approach to the person referred this opportunity to you</li>
<li>If you come to know from any other source, contact us and will ensure that nearest business&nbsp;associate will get in touch with you</li>
<li>The product is available for MRP of Rs. 2999/- including applicable GST and valid for two&nbsp;years from the date of listing</li>
<li>Don&rsquo;t forget to ask for any ongoing offer for discount!</li>
<li>On payment, the associate will share you link to upload your business details</li>
<li>After upload, the admin will check the same and within maximum 48 Hours, your business will get listed in mye-directoy.com for everyone</li>
<li>Online presence of your business is now available and ready to provide you with more lead.</li>
</ol>
<h3>For Business Associates:</h3>
<p>Three offer packs are available for Associates as below containing multiple Business Listing Entries.
On completion of KYC formalities and purchase one of the offer packs, you can become Business
Associate.</p>
<table class="table">
<tbody>
<tr>
    <th rowspan="2">Pack consisting Business Listing of</th>
    <th colspan="3">Amount in INR (Including GST @ 18%)&nbsp;</th>
</tr>
<tr>
    <th>MRP</th>
    <th>Offer Price</th>
    <th>Pre-Launch Price</th>
</tr>
<tr>
    <td>4 Entries</td>
    <td>11,996/-</td>
    <td>10,000/-</td>
    <td>10,000/-</td>
</tr>
<tr>
    <td>10 Entries</td>
    <td>29,990/-</td>
    <td>25,000/-</td>
    <td>25,000/-</td>
</tr>
<tr>
    <td>20 Entries</td>
    <td>59,980/-</td>
    <td>45,000/-</td>
    <td>40,000/-</td>
</tr>
</tbody>
</table>
<p>Company accept only online payment. Cash or cheques are not accepted.</p>
<p>After one year, Business Associate will have an option to renew his association</p>
<p><strong>We dot charge any kind of membership or registration fees for associates. By purchasing of any offer pack one can become Business Associate.</strong></p>