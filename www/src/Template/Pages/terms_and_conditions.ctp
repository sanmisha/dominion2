<h1>Terms & Conditions</h1>
<p>Myedirectory is an online platform created by a unique and innovative way to support each and
every one who is associated with it to become Atma Nirbhar and support others to become Atma
Nirbhar. For any person to become part of our family and become a Listed Member or Business
Associate has to follow the following TERMS &amp; CONDITIONS. By registering with Myedirectory, you
are accepting the following <strong>TERMS &amp; CONDITIONS:</strong></p>
<h3>GENERAL TERMS:</h3>
<ul>
    <li>
        You, on Registering with Myedirectory, have clearly agreed that you understand that there are
        TERMS &amp; CONDITIONS to be followed in Myedirectory. You also hereby agree that you have
        Understood &amp; Read All the TERMS &amp; CONDITIONS &amp; POLICIES of Myedirectory, and after being
        yourself fully convinced and satisfied, you have decided to be part of this Myedirectory yourself,
        without anybody’s Force or Request.
    </li>
    <li>
        To be eligible to use our system services and access the site, you must (1) be at least 18 years of
        age, ( 2 ) be registered with us (to the extend required) and not have been previously restricted,
        suspended or terminated by our system; and ( 3 ) not be using another associate’s account
        without his / her permission.        
    </li>
    <li>
        The Terms of use requires the use of Arbitration on an individual basis to resolve whatever
        major or minor disputes, rather than judiciary trails or class actions, which may limit the
        remedies to resolve the issues, if any.        
    </li>
    <li>
        All the LMs &amp; BAs are responsible to comply and follow all the local rules, laws &amp; regulations, of
        the India, regarding their use of this site and its services.        
    </li>
</ul>

<h3>RIGHTS OF MYEDIRECTORY:</h3>
<ul>
    <li>
        Our System, reserves the right fully &amp; solely, to add or, to modify, or to delete/remove, any
        features or functionalities, make any changes in how this system works, manage Associates, or
        any other system related matters, within the Myedirectory community.        
    </li>
    <li>
        Myedirectory reserves the rights, fully and solely, to add, or change, or modify, remove or delete
        any clause or terms of use, at any given point of time, without giving any intimation or giving
        notice to any of its community members.        
    </li>
    <li>
        Our System reserves the rights, fully and solely, without any LM’s or BA’s permission, to
        collaborate, or to enter into any agreement with any third party, or to make any tie ups with any
        products or services, in future also, or to remove or close any tie ups or partnerships with any
        person, or individual, or group, or any company. Myedirectory only has the rights to decide it.        
    </li>
    <li>
        Myedirectory reserves the rights, fully and solely, to add or remove, or modify or change, any of
        the payment gateways or payment processors as desired by Myedirectory System only.        
    </li>
</ul>

<h3>VIOLATION &amp; TERMINATION:</h3>
<ul>
    <li>
        Myedirectory system reserves the rights, fully and solely, to Terminate, or remove, or block, or
        delete, or modify the details of any member/s of this Community, without any prior permission
        or Intimations or notice.
    </li>
    <li>
        Any member, who is found to be guilty, and is in breach of this agreement and terms &amp;
        conditions, or otherwise found to be spreading malicious rumours or negatives about this
        system, or to any of its members or promoters or leaders, would be liable for immediate
        Termination of association, without any notice, and any further funds from such member would
        be forfeited.        
    </li>
    <li>
        Any kind of funds forgery, or hacking to Myedirectory System Site, by any of its members, on
        finding guilty, would be terminated, and liable for further legal actions.
    </li>
</ul>

<h3>FEES:</h3>
<ul>
    <li>
        Myedirectory reserves the right to change the product mix and or price of the product pack
        offered and or the duration of the same. However, this will be applicable with effect from date
        of change. All the Associates / Members will continue the benefits for the duration they have
        become BA or LM. Approx. One month notice will be given in case of major changes.        
    </li>
    <li>
        Business Associate has NO OBJECTION, for this system to take any charges like Admin charges,
        Upgrade charges, etc. For calculating the payout amount.       
    </li>
    <li>
        Applicable Fees can be changed without any prior notice if change in Tax Structure or any
        change in Statutory Requirement as imposed by Law.
    </li>
</ul>

<h3>LIABILITY:</h3>
<ul>
    <li>
        Associate of this company, while registering, and also after becoming a Business Associate of
        this community, is fully aware of the fact that this system does not provide any kind of
        Guaranteed Returns just for becoming Business Associate.    
    </li>
    <li>
        Myedirectory doesn’t not guarantee or claim or promises any Income or Profit guarantee.
        Myedirectory is not liable for any of the losses or damages of the Associates. All payouts are
        released based on individual and Team Performance only.       
    </li>
    <li>
        In case of highly unexpected event or any government rules or laws, if myedirectory has to stop
        its functioning, then its creators, or its admin, or its main promoters, or any leaders, are NOT
        LIABLE for any Financial or emotional losses or damages of whatsoever, the Business Associate is
        fully and expressly aware of this fact.
    </li>
    <li>
        Myedirectory system does not take any responsibility in delay in its site or any services, resulting
        from any reasons beyond our control like act of god, terrorism, riots, acts of civil or military
        actives, fire, floods, accidents, any strikes, etc, etc..
    </li>    
</ul>

<h3>MISCELLANEOUS:</h3>
<ul>
    <li>
        Myedirectory system reserves rights to make any alterations in these TERMS &amp; CONDITIONS,
        from time to time, and anytime.
    </li>
</ul>