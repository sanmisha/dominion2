<style>
		body {
			font-family: sans-serif;
		}
		td 	{
			vertical-align: top;
		}
		.head {
			font-weight:bold;
			font-size: 10pt;
		}

		.type {
			font-weight:bold;
			font-style: italic;
			font-size: 10pt;
		}

		.totals {
			font-weight:bold;
		}
		.items td {
			border: 0.1mm solid #000000;
		}
		table thead td {
			background-color: #EEEEEE;
			text-align: center;
			font-weight:bold;
		}
		.items td.blanktotal {
			background-color: #FFFFFF;
			border: 0mm none #000000;
			border-top: 0.1 mm solid #000000;
			border-right: 0.1 mm solid #000000;
		}
		.items td.totals {
			text-align: right;
			border: 0.1mm solid #000000;
		}
		,
		tr.cancelled td {
			text-decoration: line-through;
			font-weight: bold;
			font-style: italic;
		}
        @page {
            header: html_myHeader;
            footer: html_myFooter;
            margin-top: 3cm;
        }
		</style>
		<htmlpageheader name="myHeader">
			<table width="100%" border="0">
				<tr>
					<td align="right" style="font-size: 9pt; font-style: italic;">
                        <img src="https://dominion.kashelkardesigns.com/img/logo.png" style="width:30%" />
					</td>
				</tr>
			</table>
            <hr />
		</htmlpageheader>
		<htmlpagefooter name="myFooter">
            <hr />
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="left" style="font-size: 9pt; font-style: italic;">
                        Quotation No: <?= $project->quotation_no ?>
					</td>
					<td align="right" style="font-size: 9pt; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<div class="noheader">
		<table style="width:500px;" border="0" cellspacing="0" cellpadding="2" class="noheader">
			<tr>
				<td align="center">
                    <img src="https://dominion.kashelkardesigns.com/img/quotation.png" style="width:100%" />
                </td>
			</tr>
		</table><br />
		<div style="font-weight:bold;font-size:16pt;">
			QUOTATION FOR ARCHITECTURAL &amp; INTERIOR SERVICES
		</div><hr/>
        <h3><?= strtoupper($project->client->client_name) ?></h3>
        <p>
            <?= $project->client->office_address_line_1?><br />
            <?= $project->client->office_address_line_2?><br />
            <?= $project->client->office_city?> - <?= $project->client->office_pincode?>
        </p>
        <br/><br/><br/>
		<table style="width:100%; font-size:10pt; font-weight: bold;" border="1" cellspacing="0" cellpadding="2">
			<tr style="background: lightgray;">
                <td style="width:33%">Quotation No: <?= $project->quotation_no ?></td>
                <td style="width:33%">Date:  <?= $project->quotation_date ?></td>
                <td>Revisions: <?= $project->revision ?></td>
            </tr>
		</table><br />
		<table style="width:100%;" border="1" cellspacing="0" cellpadding="2">
			<tr style="font-weight:bold;font-size:10pt;">
				<td>
					<strong>
                        KASHELKAR DESIGNS PRIVATE LIMITED. <br />
                        Architect & Interior Designers
                    </strong><br />
                    410,411 & 412, 4th Floor,<br />
                    Globe Arcade, Plot No. RP 112,<br />
                    MIDC Residential Zone,<br />
                    Near Ganesh Temple,<br />
                    Dombivili (east).
				</td>
				<br />
			</tr>
		</table>
        </div>
        <pagebreak>
		<table class="items"  width="100%" style="font-size: 10pt;border-collapse: collapse;" cellpadding="10">
			<tr>
				<td width="10%" align="center"><b>Sr No.</b></td>
				<td width="80%" align="center"><b>PARTICULARS</b></td>
				<td align="center"><b>PAGE-NO</b></td>
			</tr>
			<tbody>
				<tr>
					<td align="right">1</td>
					<td>Introduction page</td>
					<td align="center">3</td>
				</tr>
				<tr>
					<td align="right">2</td>
					<td>Annexure-A SCHEME OF WORKS</td>
					<td align="center">4</td>
				</tr>
				<tr>
					<td align="right">3</td>
					<td>Annexure-B PROFESSIONAL FEE</td>
					<td align="center">5</td>
				</tr>
				<tr>
					<td align="right">4</td>
					<td>Annexure-C EFFECTING PAYMENT TO THE ARCHITECT</td>
					<td align="center">7</td>
				</tr>
				<tr>
					<td align="right">5</td>
					<td>Annexure-D SCHEDULE OF SERVICES</td>
					<td align="center">8</td>
				</tr>
				<tr>
					<td align="right">6</td>
					<td>Annexure-E CLIENT&#39;S ROLE AND RESPONSIBILITIES</td>
					<td align="center">9</td>
				</tr>
				<tr>
					<td align="right">7</td>
					<td>Annexure-F EXECUTION OF THE ASSIGNMENT</td>
					<td align="center">10</td>
				</tr><tr>
					<td align="right">8</td>
					<td>Annexure-G TIME SCHEDULE</td>
					<td align="center">11</td>
				</tr>
				<tr>
					<td align="right">9</td>
					<td>Annexure-H INDEMNIFICATION</td>
					<td align="center">12</td>
				</tr>
			</tbody>
		</table>
        <pagebreak>
		<div style="font-size:10pt"><p><b>To,</b></p>
			<h3><?= $project->client->client_name ?></h3>
            <table style="font-size:10pt" width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td style="width:50%">
                        <strong>Office Address</strong><br />
                        <?= $project['client']['office_address_line_1'] ?>,<br />
                        <?= $project['client']['office_address_line_2'] ?>,<br />
                        <?= $project['client']['office_city'] ?> - <?= $project['client']['office_pincode'] ?>
                    </td>
                    <td>
                        <strong>Site Address</strong><br />
                        <?= $project['site_address_line_1'] ?>,<br />
                        <?= $project['site_address_line_2'] ?>,<br />
                        <?= $project['sity_city'] ?> - <?= $project['site_pincode'] ?>
                    </td>
                </tr>
            </table>
		</div><br/>
		<div style="font-weight:bold;font-size:10pt">Kindly Attention: <?= $project->contact_person ?></div><br />
		<div style="font-weight:bold;font-size:10pt">Respected Client,</div>
		<div style="font-size:10pt">
		<p>Thank you for considering Kashelkar Designs Private Limited. for your architectural needs.</p>
		<p>We believe in “Conceptualization to build forms” of human interactive spaces using suitable construction	technologies.</p>
		<p>We want to create unique interactive spaces for informed user through constructive dialogue and criticism.</p>
		<p>Managing projects since 1980, from the initial design phase to completion, and have worked on different kinds of projects such as Architectural, Housing / Residential,
        Bungalow, Building, and Factory / Industrial. We have learned to create innovative structures and give enjoyable experiences to our customers. </p>

		<p>We work with contractors and	subcontractors to make sure your project is completed on time, and done in the right way,
		the	way we have envisaged.</p>

		<p>We look forward to learning more about your vision for your project, and working together to
		create something you&#39;ll absolutely love.</p>

		<p><b>Sincerely,</b></p><br>

		<p>
            <b>
                <?= $project->prepared_by_employee->name ?><br />
                <?= $project->prepared_by_employee->designation->designation ?><br />
            </b>
        </p>

		</p></div><br>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt">
            Annexure-A<br />SCHEME OF WORKS
        </div>
		<div style="font-size:10pt">The Architect is required to provide services in respect of the following:</div><br />
		<div style="font-weight:bold;font-size:10pt">Inclusion :</div>
		<div style="font-size:10pt"><?= $project->inclusion; ?></div>
		<div style="font-weight:bold;font-size:10pt">Exclusion :</div>
		<div style="font-size:10pt"><?= $project->exclusion; ?></div>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt;">Annexure-B<br />PROFESSIONAL FEE</div>
		<div style="font-size:10pt">
			<p>In consideration of the professional services rendered by the Architect, he shall be paid professional fee and other charges in accordance with the Scale of Charges.</p>
			<p>Any tax levied by law, such as GST etc. contingent to professional services rendered by the Architect, shall be payable by the Client, over and above the gross fees charged by the Architect in relation to the services provided.</p>
		</div><br>
		<table class="items" style="font-size: 9pt;border-collapse: collapse;"  width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<td align="center" colspan="6"><b>Kashelkar Designs Private Limited.<br>ARCHITECT AND INTERIOR DESIGNERS</b></td>
			</tr>
			<tr>
				<td align="center" colspan="6"><b>INVOICE SUMMARY</b></td>
			</tr>
			<tr>
				<td colspan="6"><b>Fees Type :</b> <?= $project['fees_type'] ?></td>
			</tr>
			<?php if($project['fees_type']=="Percentage Basis") { ?>
				<tr>
                    <td colspan="3" width="50%">
						<b>Cost Of Construction Per Square Feet :</b><?php echo $this->Number->currency($project['cost_of_construction'],'INR'); ?>
					</td>
                    <td colspan="3" width="50%">
						<b>Total Construction Area :</b><?php echo $this->Number->currency($project['total_construction_area'],'INR'); ?> Sq.Ft
					</td>
				</tr>
				<tr>
                    <td colspan="3" width="50%">
						<b>Total Cost Of Construction :</b><?php echo $this->Number->currency($project['total_cost_of_construction'],'INR'); ?>
					</td>
					<td colspan="3" width="50%">
						<b>Architectural Fees :</b> <?php echo $this->Number->currency($project['architectural_fees'],'INR'); ?> %
					</td>
                </tr>
                <tr>
					<td colspan="6">
						<b>Total Payable Fees :</b> <?php echo $this->Number->currency($project['final_amount'],'INR'); ?>
					</td>
				</tr>
				<?php } ?>
				<tr>
                    <td colspan="3" width="50%">
						<b>Fees  : </b><?php echo $this->Number->currency($project['fees'],'INR'); ?>
					</td>
                    <td colspan="3" width="50%">
                        &nbsp;
                    </td>
		        </tr>
            <tr><td align="center" colspan="6"><b>SUMMARY OF INVOICE THAT WILL BE ISSUED ALONG THE DUE COURSE</b></td></tr>
            <?php
                $gst = $project['cgst'] + $project['sgst'] + $project['igst'];
                $gstAmount = $project['cgst_amount'] + $project['sgst_amount'] + $project['igst_amount'];

            ?>
			<tr>
				<td width="10%" align="center"><b>Stage No.</b></td>
				<td width="30%" align="center"><b>Stage Name</b></td>
				<td width="10%" align="center"><b>Fees Breakup</b></td>
				<td width="17%" align="center"><b>Fees</b></td>
				<td width="17%" align="center"><b>GST (<?=$gst?>%)</b></td>
				<td align="center"><b>Total Fees</b></td>
			</tr>
			<tbody>
            <!-- <tr>
                <td colspan="6"><strong>Fees Breakup</strong></td>
            </tr> -->
            <?php
            $total = 0;
			foreach($project['project_tasks'] as $i=>$phase){
                $fees = $phase['fees'];
				$tax = $fees * ($gst/100);
				$calc = round($fees + $tax);
                $total += $calc;
                // if($phase->fees_type == 'Lumpsum') {
                //     echo "<tr><td colspan='6'><strong>(B) Additional Services</strong></td></tr>";
                // }
			?>
					<tr>
						<td align="center"><?php echo $phase['list_order'] ?></td>
						<td><?php echo $phase['task']['task'] ?></td>
						<td align="right">
							<?php if ($phase['percentage'] == 0 ) { ?>
								<?php echo "---" ?>
							<?php } else { ?>
							    <?php echo $phase['percentage'] ?>
							<?php } ?>
						</td>

						<td align="right">
                        <?php if (empty($phase['fees'])) { ?>
							<?php echo "---" ?>
                        <?php } else { ?>
							<?php echo $this->Number->currency($fees) ?>
						<?php  	} ?>
						</td>

						<td align="right">
							<?php if (empty($phase['fees'])) { ?>
                                <?php echo "---" ?>
                            <?php } else { ?>
    							<?php echo $this->Number->currency($tax) ?>
	    					<?php } ?>
						</td>

						<td align="right">
							<?php if (empty($phase['fees'])) { ?>
                                <?php echo "---" ?>
                            <?php } else { ?>
    							<?php echo $this->Number->currency($calc) ?>
	    					<?php } ?>
						</td>
					</tr>
			<?php } ?>
			<tr>
				<td colspan="3"><b>Total Fees</b></td>
				<td align="right" class="head"><?php echo $this->Number->currency($project->amount); ?></td>
				<td align="right" class="head"><?php echo $this->Number->currency($gstAmount); ?></td>
				<td align="right" class="head"><?php echo $this->Number->currency(round($total)); ?></td>
			</tr>
			</tbody>
		</table><br>
        <pagebreak>
		<div style="font-size:10pt;"><b>REIMBURSABLE EXPENSES :</b></div>
		<div style="font-size:10pt;">
            <table class="items" width="100%" style="font-size: 9pt;border-collapse: collapse;" cellpadding="5">
                <tr>
                    <td width="10%" align="center"><b>Sr No.</b></td>
                    <td align="left"><b>Additional Charges</b></td>
                    <td width="10%" align="center"><b>Unit</b></td>
                    <td width="15%" align="right"><b>Charges</b></td>
                </tr>
                <tbody>
                    <?php
                        $i = 0;
                        if(!empty($project->free_visits)) {
                            $i++;
                    ?>
                    <tr>
                        <td align="center"><?= $i ?></td>
                        <td><?=$project->free_visits ?> Free Vistis</td>
                        <td align="center">Per Visit</td>
                        <td align="right"><?= $this->Number->currency(0) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->visit_charge)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Site Visits</td>
                        <td align="center">Per Visit</td>
                        <td align="right"><?= $this->Number->currency($project->visit_charge) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->documentation_communication_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Architect Documentation & Communication Charges</td>
                        <td align="center"><?= $project->dc_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->documentation_communication_charges) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->travelling_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Travelling (To & Fro) Charges.</td>
                        <td align="center"><?= $project->t_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->travelling_charges) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->lodging_boarding_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Lodging & Boarding Charges</td>
                        <td align="center"><?= $project->lb_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->lodging_boarding_charges) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->models_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Models Presentation Charges  </td>
                        <td align="center"><?= $project->m_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->models_charges) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->computer_simulation_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Computer Simulation Charges. </td>
                        <td align="center"><?= $project->cs_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->computer_simulation_charges) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                        if(!empty($project->drawing_charges)) {
                            $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Drawing Presentation Charges.</td>
                        <td align="center"><?= $project->d_unit ?></td>
                        <td align="right"><?= $this->Number->currency($project->drawing_charges) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
		</div>
        <br />
		<?php
		if(!empty($project['description'])) { ?>
			<div style="font-weight:bold;font-size:10pt">Note :</div>
            <div style="font-size:10pt">
                <?php echo $project['description'] ?>
            </div>
		<?php } ?>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt;">Annexure-C<br />EFFECTING PAYMENT TO THE ARCHITECT</div><br />
		<div style="font-weight:bold;font-size:10pt;"><?php $project['fees_type'] ?></div>

   		 <?php //if($project['fees_type']=="Lumpsum") { ?>
                <!-- <div style="font-size:10pt;">
					<p>1) The fees payable to the Architect shall be computed on below area.</p>
					<p>2) Area considered for Fees calculation.<br/>
						<ol type="a">
						  <li>Approved Area	– 1.0</li>
						  <li>Deductions	– 1.0</li>
						  <li>Balcony		– 1.0</li>
						  <li>Terrace		– 1.0</li>
						  <li>Plinth Area	– 1.0</li>
						</ol>
					</p>
					<p>3) Any revision in the drawings, tenders and documents, once approved, required to be made by
						the Client shall be compensated as additional services rendered by the Architect and paid for @ 50%
						of the fee prescribed for the relevant stage(s).
					</p>
					<p>4) One time approval & one time completion is included in our fees but if more than one time approved
						and completion is required be issued Liaising fees will be charge as per actuality.
					</p>
				</div> -->
        <?php //} ?>
		<?php //if($project['fees_type']=="Percentage Basis") { ?>
            <div style="font-size:10pt;">
				<p>The fee payable to the Architect shall be computed on the actual cost of works on completion.
					The payment due to the Architect at different stages be computed on the following basis.
				</p>
				<p><b>Retainer</b>: On rough estimate of cost.</p>
				<p><b>At Stage 1</b>: On rough estimate of cost.</p>
				<p><b>At Stage 2 to 4</b>: On preliminary estimate of cost.</p>
				<p><b>At Stage 5 to 6b</b>: Accepted tender cost.</p>
				<p><b>At Stage 8</b>: Actual total cost.</p><br />
				<p>Progressive, on account, payments shall be made by the Client to the Architect against any of the above stages
					based on the quantum of work done during that stage, as may be mutually agreed to between the Client and the Architect.
				</p>
				<p>No deductions shall be made from the fee of the Architect on account of penalty,
					liquidated damages, part rates or other sums withheld from payment or recovered from contractors/ suppliers.
				</p>
				<p>When the work is executed wholly or in part with old materials or labour or carriage is provided by the Client,
					the percentage fees shall be calculated as if the work had been executed wholly by the contractor supplying all labour and new materials.
				</p>
				<p>The actual cost of the completed works shall include cost of execution of assigned works, referred to in Scope of Work and also the cost of equipment
					& machinery such as Transformers, DG Sets, Sub-stations, Lifts, Air Conditioning Machines, Pumps & Motors, Water and Sewage Treatment Plant, etc., but excluding the cost of land.
				</p>
				<p>Any revision in the drawings, tenders and documents, once approved, required to be made by the Client shall be compensated as additional services rendered by the Architect and paid for @ 50% of the fee prescribed for the relevant stage(s).</p>
				<p>One time approval & one time completion is included in our fees but if more than one time approved and completion is required be issued Liaising fees will be charge as per actuality.</p>
			</div>
        <?php //} ?>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt">Annexure-D<br />SCHEDULE OF SERVICES</div>
		<div style="font-size:10pt">The Architect shall, after taking instructions from the Client, render the following services:</div><br />
		<div style="font-weight:bold;font-size:10pt">Our Scheme of Works: </div>
        <?php foreach($project['project_tasks'] as $i=>$phase){ ?>
            <div style="font-size:10pt"><b><?php echo($phase['task']['task']) ?> : [Stage <?php echo ($i+1) ?>]</b></div>
            <div style="font-size:10pt"><?php echo($phase['description']) ?></div><br />
		<?php } ?>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt;">Annexure-E<br />CLIENT&#39;S ROLE AND RESPONSIBILITIES</div>
		<div style="font-size:10pt;">
			<p>The Client shall discharge all his obligations connected with the project and engagement of the Architect as follows:</p>
			<p>To provide detailed requirements of the project.</p>
			<p>To provide property lease/ ownership documents.</p>
			<p>To provide a site plan, to a suitable scale, showing boundaries, contours at suitable intervals, existing physical features including any existing roads,
				paths, trees, existing structures, existing service and utility lines and such lines to which the proposed service can be connected. In case such information is not readily available,
				the Client shall arrange for the survey/ collection of necessary information and pay for the same.
			</p>
        <?php if ($project['project_type']['project_type'] !="Structural Audit Report") { ?>
			<p>To furnish reports on soil conditions and test as required by the Architect or pay for the preparation of the same.</p>
        <?php } ?>

			<p>To pay all the fees, levies, security deposits and expenses in respect of statutory sanction.</p>
			<p>To give effect to the professional advice of the Architect and cause no changes in the drawings and documents without the consent of the Architect.</p>
			<p>To honour Architect&#39;s bills within one month of its submission.</p>
			<p>To appoint a Construction Manager (Clerk of Works/ Site Supervisor or Construction Management Agency in case of a large and complex project) as per the Architect&#39;s advice.</p>
		</div>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt;">Annexure-F<br />EXECUTION OF THE ASSIGNMENT</div>
		<div style="font-size:10pt;">
			<p>The Architect shall keep the Client informed about the progress of work in his office.</p>
			<p>The Architect shall appoint specialized consultants in consultation with the Client, if necessary.</p>
			<p>The Architect shall be responsible for the direction and integration of the consultant’s work. The consultants,
				however, shall be fully responsible for the calculations, the detailed design and periodic inspection and evaluation of the work entrusted to them. The Architect shall, if requested, make available the design calculations.
			</p>
			<p>The Architect will advise the Client on the Time Schedule (Bar Chart/PERT/ CPM Network) prepared by the contractors for the completion of work, if required.</p>
			<p>The Architect shall supply to the Client, free of cost, up to three sets of drawings at different stages.</p>
			<p>The Architect shall not make any deviations, alterations or omissions from the approved drawings, involving financial implications without prior consent of the Client.</p>
			<p>Any professional services to be rendered by the Architect at the instance of the Client after the agreed project completion period shall be compensated for on mutually agreed terms.</p>
			<p>The Architect shall exercise all reasonable skill, care and diligence in the discharge of his duties and shall exercise such general superintendence and inspection as may be necessary to ensure that works are being executed in accordance with the Conditions of Contract.</p>
			<p><b>Any revision in the drawings, tenders and documents, once approved, required to be made by the Client shall be compensated as additional services rendered by the Architect and paid for @ 50% of the fee prescribed for the relevant stage(s).</b></p>
			<p>No change shall be made in the approved drawings and specifications at site without the consent of the Architect.</p>
			<p>Any curtailment of the professional services, beyond Stage 2, shall make it obligatory for the client to pay at least 20% of the fee for the remaining Stage(s) of the curtailed work/ Services.</p>
		</div>
        <pagebreak>
		<div style="font-weight:bold;font-size:10pt;">Annexure-G<br />TIME SCHEDULE</div>
		<div style="font-size:10pt;">The Architect shall, in consultation with the Client, prepare a Time Schedule in respect of various services to be rendered and discharge of Client&#39;s obligations.</div><br />
		<table class="items" width="100%" style="font-size: 9pt;border-collapse: collapse;" cellpadding="5">
			<tr>
				<td width="10%" align="center"><b>Sr No.</b></td>
				<td width="50%" align="center"><b>Phase</b></td>
				<td align="center"><b>Tentative Timeline</b></td>
			</tr>
			<tbody>
            <?php foreach($project['project_tasks'] as $i=>$phase){ ?>
				<tr>
                    <td align="left"><?php echo $phase['list_order'] ?></td>
					<td align="left"><?php echo $phase['task']['task'] ?></td>
					<td align="center"><?php echo $phase['days_require'] ?> Days</td>
				</tr><br/>
			<?php } ?>
			</tbody>
		</table>
        <pagebreak>
		<div style="font-size:10pt;"><b>Annexure-H<br />INDEMNIFICATION</b></div>
		<div style="font-size:10pt;">
			In the event that a claim or suit is brought against the Architect or the Consultants by any third party
			for damages arising from personal injury or property damage caused wholly by the Client, or anyone employed by the Client, or anyone for whose acts the Client may be held responsible,
			then the Client shall indemnify the Architect and fully reimburse any loss, damage or expenses, including the attorney&#39;s fees,
			which the Architect may incur in connection therewith.
		</div><br />
		<div style="font-size:10pt;"><b>OWNERSHIP OF COPYRIGHT :</b></div>
		<div style="font-size:10pt;">
			Architectural design is an intellectual property of the Architect. The drawings, specifications, documents and models as instruments of service
			are the property of the Architect whether the project, for which they are made, is executed or not.
			The Client shall retain copies of the Architect&#39;s models, drawings, specifications and other documents for his information and use in connection with the project.
			These shall not be used for any other project by the Client or the Architect or any other person, except for the repetition as stipulated in the Scale of Charges.
		</div><br />
		<div style="font-size:10pt;"><b>TERMINATION OF AGREEMENT :</b></div>
		<div style="font-size:10pt;">
			Agreement between the Architect and the Client may be terminated by either one giving the other a written notice of not less than 30 (thirty) days,
			should either fail substantially to perform his part of responsibilities/duties, so long as the failure is not caused by the one initiating the termination.<br />
			When termination of this Agreement is not related or attributable, directly or indirectly to any act, omission, neglect or default on the part of the Architect,
			the Architect shall be entitled to professional fees as stipulated under Clause 4 and sub-clauses 9.09 and 9.11 of Clause 9.<br />
			In the event of Architect&#39;s firm closing its business or the Client having terminated the agreement, the Client shall have the right to employ another Architect to complete the work, after making payment to the previous architect&#39;s firm.
		</div><br />
		<div style="font-size:10pt;"><b>INTERPRETATION :</b></div>
		<div>
			In case of any ambiguity or difficulty in the interpretation of the Conditions of Engagement and Scale of Charges, the interpretation of the Council of Architecture shall be final and binding on the Architect and the Client.
		</div><br />
		<div style="font-size:10pt;"><b>ARBITRATION :</b></div>
		<div style="font-size:10pt;">
			All disputes or differences which may arise between the Client and the Architect under "Conditions of Engagement and Scale of Charges"
			with regard to the meaning or interpretation or matter or things done or to be done in pursuance hereof, such disputes and differences shall be referred for arbitration to the Council of Architecture. The arbitrator shall be appointed by the President,
			Council of Architecture. The arbitration shall be conducted as per the provisions of the Arbitration and Conciliation Act, 1996. The decision and award of the arbitrator shall be final
		</div>
        <pagebreak>
        <?php if(!empty($project->revisions)) { ?>
            <h3>Revisions :</h3>
            <?php foreach($project->revisions as $i=>$revision) { ?>
                <div style="font-size:10pt;">
                    <strong>Revision Date: <?= $revision->revision_date ?></strong><br />
                    <strong>Revision: <?= ($i + 1) ?></strong><br />
                    <?= $revision->remarks ?>
                </div>
                <hr />
            <?php } ?>
            <pagebreak>
        <?php } ?>
        <div style="font-size:10pt">
            <p>Thank you for reviewing this proposal. We hope you found it informative and useful. Feel free
            to contact us with any questions or concerns you may have — we&#39;re here to help, and always
            enjoy talking with potential clients. We look forward to working together to develop a structure
            that brings your vision to life.</p>
        </div><br>
        <table style="font-size:10pt" width="100%" border="0" cellpadding="10" cellspacing="0">
            <tr>
                <td width="50%" align="left">
                    <span>Thanking you,<br/><br />Yours Faithfully,</span><br/>
                    <span><b>For KASHELKAR DESIGNS PRIVATE LIMITED.</span></b><br/><br/><br/><br/>
                    <span><b>Ar. Kaustubh Kashelkar<br>Architect & Interior Designers</b></span><br/>
                    <span>410,411 & 412, 4th Floor,<br/>Globe Arcade, Plot No. RP 112,<br />MIDC Residential Zone,<br />Near Ganesh Temple,<br/>Dombivili (east).</span><br/>
                    <span>Office No. +91-9920951588</span><br/>
                    <span>Ar Kaustubh Kashelkar. +91-9920951511</span><br />
                    <span>Email:  Kaustubh@archkdpl.com</span>
                </td>
            </tr>
        </table>



