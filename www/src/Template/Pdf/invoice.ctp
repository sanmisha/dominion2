<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #4D4D4D;
				padding: 5px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3.5cm;
			}
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
            <img src="https://dominion.kashelkardesigns.com/img/letterhead.png" style="width:100%" />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
            <div style="text-align:center">
                410,411-412, Globe Arcade, Near Ganesh Mandir, MIDC Residential Zone, Dombivli East 421 203<br />
                <strong>Website:</strong> www.kashelkardesings.com | <strong>Email:</strong> info@archkdpl.com | <strong>Phone:</strong> +91 9920951588 / 0251-2447744<br />
                <strong>CIN: U74999MH2020PTC339226</strong>
            </div>
    	</htmlpagefooter>

		<h2 align="center">TAX INVOICE</h2>
		<table class="table">
			<tr>
				<td width="50%">
                    To,<br />
					<strong><?= $invoice->project->client->client_name ?></strong><br />
                    <?= $invoice->project->client->office_address_line_1 ?><br />
                    <?= $invoice->project->client->office_address_line_2 ?><br />
                    <?= $invoice->project->client->office_city ?> - <?= $invoice->project->client->office_pincode ?><br /><br />

                    <strong>Site Address</strong><br />
                    <?= $invoice->project->site_name ?><br />
                    <?= $invoice->project->site_address_line_1 ?><br />
                    <?= $invoice->project->site_address_line_2 ?><br />
                    <?= $invoice->project->sity_city ?> - <?= $invoice->project->site_pincode ?><br /><br />

                    <strong>
                        GSTIN: <?= $invoice->project->client->gst_no ?><br />
                        State Code: <?= substr($invoice->project->client->gst_no, 0, 2) ?><br />
                        PAN: <?= $invoice->project->client->pan_no ?><br />
                        PO No: <?= $invoice->project->purchase_order_no ?><br />
                    </strong>
                    Email: <?= $invoice->project->client->email ?><br />
                    Mobile: <?= $invoice->project->client->mobile ?><br />
				</td>
				<td align="Left">
                    From,<br />
                        <strong>Kashelkar Designs Private Limited.</strong><br />
                        410,411-412, Globe Arcade,<br />
                        Near Ganesh Mandir, MIDC Residential Zone<br />
                        Dombivli East 421 203<br /><br />
                    <strong>
					    Invoice No : <?= $invoice->invoice_no ?><br />
					    Invoice Date : <?= $invoice->invoice_date ?><br />
                        Project No : <?= $invoice->project->project_no ?><br /><br />
                    </strong>
                    <strong>
                        GSTIN: 27AAICK0669E1ZH<br />
                        State Code: 27<br />
                        PAN: AAICK0669E<br /><br />
                    </strong>
                    Email: accounts@archkdpl.com<br />
                    Mobile: +91 99209 51531<br />
                    Landline: 0251-2447744<br />
				</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td>
                    <strong>Kind Attn: <?= $invoice->project->client_team->name ?></strong>
                </td>
            </tr>
        </table>
		<table autosize="true">
			<thead>
				<tr class="color">
					<th width="7%">Stage</th>
					<th>Service Description</th>
					<th>SAC Code</th>
                    <th width="15%">Fees</th>
					<th width="12%">CGST 9%</th>
					<th width="12%">SGST 9%</th>
					<th width="12%">IGST 0%</th>
					<th width="15%">Amount</th>
				</tr>
			</thead>
            <tbody>
                <?php
                    foreach($invoice->invoice_details as $detail) {
                        $cgst += $detail->cgst_amount;
                        $sgst += $detail->sgst_amount;
                        $igst += $detail->igst_amount;
                        $fees += $detail->fees;
                ?>
                    <tr>
                        <td><?= $detail->project_task->list_order ?></td>
                        <td><?= $detail->project_task->task->task ?></td>
                        <td>998323</td>
                        <td align="right"><?= $this->Number->currency($detail->fees) ?></td>
                        <td align="right"><?= $this->Number->currency($detail->cgst_amount) ?></td>
						<td align="right"><?= $this->Number->currency($detail->sgst_amount) ?></td>
						<td align="right"><?= $this->Number->currency($detail->igst_amount) ?></td>
                        <td align="right"><strong><?= $this->Number->currency($detail->amount) ?></strong></td>
                    </tr>
                <?php } ?>
            </tbody>
			<tfoot>
                <tr class="color">
                    <td colspan="3" align="right"><strong>TOTAL:</strong></td>
                    <th align="right"><?= $this->Number->currency($fees); ?></th>
                    <th align="right"><?= $this->Number->currency($cgst); ?></th>
                    <th align="right"><?= $this->Number->currency($sgst); ?></th>
                    <th align="right"><?= $this->Number->currency($igst); ?></th>
                    <th align="right"><?= $this->Number->currency($invoice->total_amount); ?></th>
                </tr>
                <tr class="color">
                    <td colspan="4" rowspan="5">
                        <strong>Amount In Words:<br /><?= $invoice->amount_in_words ?></strong>
                    </td>
                    <td colspan="3" align="right"><strong>Amount Before Tax:</strong></td>
                    <th align="right"><?= $this->Number->currency($invoice->amount_before_tax); ?></th>
                </tr>
                <tr class="color">
                    <td colspan="3" align="right"><strong>CGST</strong></td>
                    <th align="right"><?= $this->Number->currency($cgst); ?></th>
                </tr>
                <tr class="color">
                    <td colspan="3" align="right"><strong>SGST</strong></td>
                    <th align="right"><?= $this->Number->currency($sgst); ?></th>
                </tr>
                <tr class="color">
                    <td colspan="3" align="right"><strong>IGST</strong></td>
                    <th align="right"><?= $this->Number->currency($igst); ?></th>
                </tr>
                <tr class="color">
                    <td colspan="3" align="right"><strong>Total Amount after Tax</strong></td>
                    <th align="right"><?= $this->Number->currency($invoice->total_amount); ?></th>
                </tr>
			</tfoot>
		</table>
		<table class="table">
            <tr>
				<td colspan="2"><strong>Bank Details</strong></td>
			</tr>
			<tr>
				<td width="30%">Bank Payee Name :</td>
				<td>Kashelkar Designs Private Limited</td>
			</tr>
			<tr>
				<td width="30%">Bank A/c No :</td>
				<td>50200050306037</td>
			</tr>
			<tr>
				<td width="30%">Bank Name & Branch :</td>
				<td>HDFC Bank, Kalyan Shil Road</td>
			</tr>
			<tr>
				<td width="30%">Bank IFSC :</td>
				<td>HDFC0003783</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td>
                    <strong>TERMS & CONDITIONS</strong><br />
                    1. Payment should be made account payee cheque in favour of "Kashelkar Designs Private Limited"<br />
                    2. Payment should be made within 7 days.
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
				<td>
                    Thanking you,<br />
                    Yours Faithfully<br />
                    Sign:<br /><br /><br /><br /><br /><br /><br /><br />

                    <strong>
                        Signed By : Ar. Kaustubh Kashelkar<br />
                        Kashelkar Designs Private Limited<br />
                        Architect & Interior Designers
                    </strong>
                </td>
			</tr>
		</table>
	</body>
</html>
