<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;

			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #4D4D4D;
				padding: 5px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}
			.tr{
				border: none;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3.5cm;
			}
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<img src="https://dominion.kashelkardesigns.com/img/letterhead.png" style="width:100%" />

    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
            <div style="text-align:center">
                410,411-412, Globe Arcade, Near Ganesh Mandir, MIDC Residential Zone, Dombivli East 421 203<br />
                <strong>Website:</strong> www.kashelkardesings.com | <strong>Email:</strong> info@archkdpl.com | <strong>Phone:</strong> +91 9920951588 / 0251-2447744<br />
                <strong>CIN: U74999Mh2020PTC339226</strong>
            </div>
    	</htmlpagefooter>

		<br>


		<h3 align="center"> SUBJECT - Meeting Report / SITE INSPECTION </h3>
        <h3 align="center"> DEPARTMENT - PROJECT  </h3> <br>
        <h3 align="center"> FORMAT NO : - F/PROJ/SV/001 Rev.01 - 13/10/2021  </h3>     
       
		<table class="tr">
            <tr class="tr">
				<td width="50%" class="tr"> Client Name:  :- <strong><?=$meetingReport->project->client->client_name  ?></strong> </td>
			</tr>
			<tr class="tr">
				<td width="50%" class="tr"> Name of work -  :- <strong><?= $meetingReport->id ?></strong> </td>
				<td width="50%" class="tr">Meeting  -  :- <strong><?= $meetingReport->meeting_no ?></strong> </td>
			</tr>
            <tr class="tr">
				<td width="50%" class="tr"> Meeting Report Date -  :- <strong><?= $meetingReport->meeting_date ?></strong> </td>
				<td width="50%" class="tr"> Meeting Report Time - -  :- <strong><?= $meetingReport->meeting_time ?></strong> </td>
			</tr>
			<tr class="tr">
				<td width="50%" class="tr">Meeting Report by -  :- <strong><?= $meetingReport->employee->name ?></strong> </td>
			</tr>
            <tr class="tr">
				<td width="50%" class="tr"> Meeting Report Report - -  :- <strong><?= $meetingReport->report ?></strong> </td>
			</tr>


		</table> <br>
        <div>
        <?php foreach($meetingReport->meeting_report_images as $i=>$image){ ?>			
            <div>
                <?php echo $this->Html->image('/files/meetingreportimages/image/'.$image->image_dir.'/'.$image->image); ?>
            </div>
            <div>
                <strong><?= $image->remarks ?> </strong>
            </div>	<br>
        <?php } ?>
        </div>
        <h5> Notes :-</h5>
        <br>  
        <pagebreak />
        <h4>Attendees Stakeholders</h4>
        <table>                
            <thead>
                <tr>
                    <th width="12%">Sr. No</th>
                    <th width="25%">Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($meetingReport->meeting_report_stakeholders as $id=>$stakeholders) { ?>
                <tr>
                    <td><?= $id+1 ?></td>
                    <td><?= $stakeholders->stakeholder->contractor ?></td>
                    <td><?= $stakeholders->stakeholder->email ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>

		
        <h4>Attendees Visitors</h4> 
        <table>                
            <thead>
                <tr>
                    <th width="12%">Sr. No</th>
                    <th width="25%">Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($meetingReport->meeting_report_visitors as $id=>$visitors) { ?>
                <tr>
                    <td><?= $id+1 ?></td>
                    <td><?= $visitors->name ?></td>
                    <td><?= $visitors->email ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>


	</body>
</html>
