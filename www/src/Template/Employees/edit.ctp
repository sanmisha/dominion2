<!--<script>
	$(document).ready(function(){
		$("#member-country-id").on("change", function(){
			$("#member-state-id").empty();
			if($("#member-country-id").val()!="") {
				$.ajax({
					type: "GET",
					url: "/admin/Members/getStates/"+$("#member-country-id").val(),
					dataType:"json"
				}).done(function(data) {
					$.each(data.states, function(key, value) {
					$('#member-state-id')
						.append($("<option></option>")
									.attr("value",key)
									.text(value)); 
						
					});
					<?php //if(!empty($user->member->state_id)) { ?>
						$('#member-state-id').val("<?php //echo $user->member->state_id; ?>");
					<?php //} ?>
				});
			}
		});
		$("#member-country-id").trigger("change");
	});
</script>-->


<div class="products form large-9 medium-8 columns content box box-block bg-white">
    <?= $this->Form->create($user,['type'=>'file','novalidate'=>true]) ?> 
	<fieldset>
		<legend><?= __('Personal Details') ?></legend>
		<div>
			<div class="row">
			<div class="col-md-6">
					<?php echo $this->Form->hidden('id'); ?>
					<?php echo $this->Form->control('employee.name'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->N->datepicker('employee.dob',['required'=>true]); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->N->datepicker('employee.date_of_joining',['required'=>true]); ?>	
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->control('employee.designation_id', ['options'=>$designation , 'empty'=>'---']); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->control('employee.department_id', ['options'=>$department , 'empty'=>'---']); ?>
				</div>
			</div><br>
			<legend><?= __('Address Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('employee.address_line_1'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->control('employee.altternate_mobile'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('employee.address_line_2'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->control('employee.city'); ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('employee.pincode'); ?>
				</div>
			</div><br>
			<legend><?= __('Attachments') ?></legend>
				<div class="row">
					<div class="col-md-3">
						<?php echo $this->Form->control('employee.id_proof_document',['empty'=>'---','type'=> 'select','options'=>['Passport'=>'Passport','Voter Identification Card'=>'Voter Identification Card','Driving licence'=>'Driving licence','Aadhar Card'=>'Aadhar Card','PAN Card'=>'PAN Card']]);?>
					</div>
					<div class="col-md-3">
						<label>ID Proof</label>
						<?php 
							echo $this->Form->input('employee.id_proof',['type'=>'file','label'=>false]);
							echo $this->Form->hidden('id_proof_dir');
							if(!empty($employees->id_proof_dir)) {
						?>
								<img class="img-fluid" style="width:50%;" src="/files/employees/id_proof/<?php echo $id_proof_dir; ?>/proper_<?php echo $employees->id_proof; ?>" alt="<?php echo $employees->id_proof; ?>"   data-bgposition="center" data-bgfit="Contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina /><br>
						<?php
								echo $this->Html->link($employees->id_proof,'/files/employees/id_proof/'.$employees->id_proof_dir.'/proper_'.$employees->id_proof,['target'=>'_blank', 'style'=>'font-size: 12px;']);
							}
						?>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->control('employee.address_proof_document',['empty'=>'---','type'=> 'select','options'=>['Passport'=>'Passport','Driving licence'=>'Driving licence','Aadhar Card'=>'Aadhar Card']]);?>
					</div>
					<div class="col-md-3">
						<label>Address Proof</label>
						<?php 
							echo $this->Form->input('employee.address_proof',['type'=>'file','label'=>false]);
							echo $this->Form->hidden('address_proof_dir');
							if(!empty($employees->address_proof_dir)) {
						?>
								<img class="img-fluid" style="width:50%;" src="/files/employees/address_proof/<?php echo $employees->address_proof_dir; ?>/proper_<?php echo $employees->address_proof; ?>" alt="<?php echo $employees->address_proof; ?>"   data-bgposition="center" data-bgfit="Contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina /><br>
						<?php
								echo $this->Html->link($employees->address_proof,'/files/employees/address_proof/'.$employees->address_proof_dir.'/proper_'.$employees->address_prroof,['target'=>'_blank', 'style'=>'font-size: 12px;']);
							}
						?>
					</div>
				</div>
			<legend><?= __('Login Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('email'); ?>
				</div>
				<?php if($user->isNew()) { ?>
				<div class="col-md-6">
					<?php echo $this->Form->control('password'); ?>
				</div>
				<?php }	?>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->control('roles._ids', ['options' => $roles, 'id' => 'magicselect', 'multiple'=>'checkbox']); ?>
				</div>		
			</div>
		</div><br>
	</fieldset>
	
	 <fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>
