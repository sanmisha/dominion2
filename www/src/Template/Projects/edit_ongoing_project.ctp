
<script>
	index = 0;
	index = <?php echo count($project['project_stakeholders'])+1; ?>;	

	function add_row() {
		html = "<tr id='row_"+index+"'>";
			html += "<td><a href='javascript:void(0); remove_row("+index+");'><i class='fa fa-minus'></i></a></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='project-stakeholders-"+index+"-stakeholder-id' name='project_stakeholders["+index+"][stakeholder_id]' class='form-control select2'>";
				html += "<option value=''>(Choose One)</option>";
					<?php foreach($stakeholders as $id=>$contractor) { ?>
					html += "<option value='<?php echo $id;?>'><?php echo $contractor;?></option>";
					<?php } ?>
				html += "</select>";
			html += "</div></td>";
		html += "</tr>";
		$(html).appendTo("#tblDetails tbody");
		$('.select2').select2({allowClear: false});
		index++;
		
	}

	function remove_row(i) {
		$("#row_"+i).detach();
		index--;		
	}
</script>
<div class="box box-block bg-white">
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<strong>Quoation No:</strong><?php echo $project['quotation_no']; ?>
			</div>
			<div class="col-md-4">
				<strong>Quoation Date:</strong> <?php echo $project['quotation_date']; ?>
			</div>
			<div class="col-md-4">
				<strong>Client Name:</strong> <?php echo $project['client']['client_name']; ?>
			</div>
		</div>
	</fieldset><br>
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($project,['type'=>'file','novalidate'=>true]) ?>

    <fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('remarks'); ?>
			</div>
			
		</div>
	</fieldset><br>

	<div class="row">
		<div class="col-md-12">
		<legend><span> Stake Holders </span></legend>
		<table class="table table-striped table-condense table-responsive" id="tblDetails">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th style="">Stake Holders </th>					
				</tr>
			</thead>
			<tbody>
			<?php
				if(!empty($project['project_stakeholders'])) {
					foreach($project['project_stakeholders'] as $i=>$stakeholder) {
			?>
			<tr id="row_<?php echo $i;?>">
				<td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
				<td>
					<?php
						echo $this->Form->input('project_stakeholders.'.$i.'.id',['type'=>'hidden']);
						echo $this->N->select2('project_stakeholders.'.$i.'.stakeholder_id',array('label'=>false,'options'=>$stakeholders,'empty'=>'(Choose One)'));
					?>
				</td>			
			</tr>
			<?php  }} ?>
			</tbody>
			<tfoot>
				<tr>
					<td><a href="javascript:void(0); add_row();">+</a></td>
					<td colspan="4"></td>
				</tr>		
			</tfoot>
		</table>
		</div>	
	</div>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end() ?>
</div>

