<?php
use Cake\I18n\Time;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('/projects/closed-project', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('quotation_no') ?></th>
							<th scope="col"><?= $this->Paginator->sort('quotation_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('site_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('client_id') ?></th>
							<th scope="col"><?= $this->Paginator->sort('final_amount') ?></th>
							<th scope="col"><?= $this->Paginator->sort('closed_by','Closed By') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($projects as $project): //debug($projects->toArray());exit;?>
						<tr>
							<td><?= $project->quotation_no?></td>
							<td><?= $project->quotation_date?></td>
                            <td><?= $project->site_name?></td>
							<td><?= !empty($project->client->client_name) ? $project->client->client_name : ''  ?></td>
							<td align="right"><?= $this->Number->currency($project->final_amount,'INR') ?></td>
							<td>
								<?= $project->closed_by_employee->name ?>
								<?= $project->closed_on ?>
							</td>
                            <td class="actions">
								<?= $this->Html->link('View',['controller'=>'Projects','action'=>'view',$project->id],['class'=>'btn btn-success btn-sm','target'=>'_blank']); ?>
                                <a href="/files/projects/quotation/quotation-<?php echo str_replace('/', '-', $project->quotation_no);?>.pdf" target="_blank" class="btn btn-primary btn-sm">Quotation</a>
						    </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
