<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('projects', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('quotation_no', 'Quotation No') ?></th>
							<th scope="col"><?= $this->Paginator->sort('quotation_date', 'Quotation Date') ?></th>
							<th scope="col"><?= $this->Paginator->sort('client_id') ?></th>
							<th scope="col"><?= $this->Paginator->sort('final_amount') ?></th>
							<th scope="col"><?= $this->Paginator->sort('created','Prepared By') ?></th>
							<th scope="col"><?= $this->Paginator->sort('revision','Revision') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($projects as $project): //debug($project);exit;?>
						<tr>
							<td><?= $project->quotation_no?></td>
							<td><?= $project->quotation_date?></td>
							<?php if(!empty($project['client']['client_name'])){ ?>
							<td><?= $project->client->client_name?></td>
							<?php }else{ ?>
							<td></td>
							<?php } ?>
							<td align="right"><?= $this->Number->currency($project->final_amount,'INR') ?></td>
							<td>
								<?php
									//echo '<span class="badge badge-success">'.($project['TrackableCreator']['name']).'</span><br>';
									echo '<span class="tag tag-info">'.($project['created_user']['name']).'</span><br>';
									echo date_format($project['created'], 'jS M Y');
								?>&nbsp;
							</td>
							<td><?= $project->revision?></td>

                            <td class="actions">
							<?php if($project['status'] != 'Approved'){ ?>
								<?= $this->Html->link('Convert',['controller'=>'Projects','action'=>'convert',$project->id],['class'=>'btn btn-danger btn-sm']); ?>
								<?= $this->Html->link('Closed',['controller'=>'Projects','action'=>'closed',$project->id],['class'=>'btn btn-danger btn-sm']); ?>
							<?php } ?>
							<?php if($project['status'] != 'Close'){ ?>
								<?= $this->Html->link('Followups',['controller'=>'Followups','action'=>'edit',0,$project->id],['class'=>'btn btn-success btn-sm']); ?>
                                <?= $this->Html->link('Revisions',['controller'=>'Revisions','action'=>'edit',$project->id],['class'=>'btn btn-success btn-sm']); ?>
							<?php } ?>
								<a href="/files/projects/quotation/quotation-<?php echo str_replace('/', '-', $project->quotation_no);?>.pdf" target="_blank" class="btn btn-primary btn-sm">Quotation</a>
							<?php if($project['status'] != 'Close'){ ?>
                                <?= $this->N->editLink(['action' => 'edit', $project->id]); ?>
							<?php } ?>
                                <?= $this->N->deleteLink(['action' => 'delete', $project->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
