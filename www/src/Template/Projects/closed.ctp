

<div class="box box-block bg-white">
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<strong>Quoation No:</strong><?php echo $project['quotation_no']; ?>
			</div>
			<div class="col-md-4">
				<strong>Quoation Date:</strong> <?php echo $project['quotation_date']; ?>
			</div>
			<div class="col-md-4">
				<strong>Client Name:</strong> <?php echo $project['client']['client_name']; ?>
			</div>
		</div>
	</fieldset><br>
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title; ?></h5>
	</div>
    <?= $this->Form->create($project,['type'=>'file','novalidate'=>true]) ?>

    <fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('close_reason'); ?>
			</div>			
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end() ?>
</div>

