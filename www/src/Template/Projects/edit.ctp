<script>

	projectIndex = 0;
	projectIndex = <?php echo count($project['project_tasks'])+1; ?>;

	$(document).ready(function(){
		$("#project-type-id").on("change", getStages);
		$(".calc").blur(function(){
			calculate();
		});

		$("#fees-type").change(function() {
			if($("#fees-type").val() == "Percentage Basis") {
				$("#costOfConst").show();
				$("#tCostArea").show();
				$("#tCostOfConst").show();
				$("#archFees").show();
				$("#Fees").show();
			}
			if($("#fees-type").val() == "Lumpsum") {
				$("#costOfConst").hide();
				$("#tCostArea").hide();
				$("#tCostOfConst").hide();
				$("#archFees").hide();
				$("#Fees").show();
			}
		});
		$('#fees-type').trigger('change');

		$("#client-id").on("change", function(){
			$("#client-team-id").empty();
			$.ajax({
				type: "GET",
				url: "/ClientTeams/getClientTeamsByClient/"+$("#client-id").val(),
				dataType:"json"
			}).done(function(data) {
				Clients=data.clientTeams;
				$('#client-team-id ')
					 .append($("<option></option>")
						.attr("value",'')
						.text('---'));
				$.each(Clients, function(key, value) {
				$('#client-team-id ')
					 .append($("<option></option>")
							.attr("value",value.id)
							.text(value.site_name + ' (' + value.name + ')'));


				});
				<?php if(!empty($project->client_team_id)) { ?>
					$('#client-team-id').val("<?php echo $project['client_team']['id']; ?>");
				<?php } ?>
			});
		});
		$("#client-id").trigger("change");

		$("#client-team-id").on("change", function(){
			$("#site-address-line-1").empty();
			$("#site-address-line-2").empty();
			$("#sity-city").empty();
			$("#site-pincode").empty();

			$.ajax({
				type: "GET",
				url: "/ClientTeams/getClientTeam/"+$("#client-team-id").val(),
				dataType:"json"
			}).done(function(data) {
				$("#site-address-line-1").val(data.clients.address_line_1);
				$("#site-address-line-2").val(data.clients.address_line_2);
				$("#sity-city").val(data.clients.city);
				$("#site-pincode").val(data.clients.pincode);
				$("#site-name").val(data.clients.site_name);
				$("#email").val(data.clients.user.email);
				$("#mobile").val(data.clients.user.mobile);
				$("#contact-person").val(data.clients.name);

			});
		});
		$("#client-team-id").trigger("change");

        calculate();
	});

	function feesType(i){
		if($("#project-tasks-"+i+"-fees-type").val() == "Percentage") {
			$("#project-tasks-"+i+"-percentage").attr("disabled", false);
		}
		if($("#project-tasks-"+i+"-fees-type").val() == "Lumpsum" || $("#project-tasks-"+i+"-fees-type").val() == "Lumpsum Retainer") {
            $("#project-tasks-"+i+"-percentage").attr("disabled", true);
			$("#project-tasks-"+i+"-percentage").val("");
		}
	}

	function taskChange(i){
        var id = $("#project-tasks-"+i+"-task-id").val();
        $.getJSON('/tasks/get/' + id, function(data){
            $("#project-tasks-"+i+"-service").val(data.service);
            $("#project-tasks-"+i+"-description").val(data.description);
        });
	}

	function add_row(task_id='', fees_type='', days_require='' ,list_order='', percentage='', service='', description='') {
		html = "<tr id='row_"+projectIndex+"'>";
			html += "<td><a href='javascript:void(0); remove_row("+projectIndex+");'><i class='fa fa-minus'></i></a></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='project-tasks-"+projectIndex+"-task-id' name='project_tasks["+projectIndex+"][task_id]' class='form-control' onChange='return taskChange("+projectIndex+")'>";
				html += "<option value=''>(Choose One)</option>";
					<?php foreach($tasks as $id=>$task) { ?>
					    html += "<option value='<?php echo $id;?>'><?php echo $task;?></option>";
					<?php } ?>
				html += "</select>";
			html += "</div></td>";
			html += "<td><div class='form-group'>";
				html += "<select id='project-tasks-"+projectIndex+"-fees-type' name='project_tasks["+projectIndex+"][fees_type]' class='form-control' onChange='return feesType("+projectIndex+")'><br /><br />";
				html += "<option value=''>(Choose One)</option>";
				html += "<option value='Percentage'>Percentage</option>";
				html += "<option value='Lumpsum'>Lumpsum</option>";
                html += "<option value='Lumpsum Retainer'>Lumpsum Retainer</option>";
				html += "</select>";
			html += "</div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-tasks-"+projectIndex+"-days-require'  class='form-control' name='project_tasks["+projectIndex+"][days_require]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-tasks-"+projectIndex+"-list-order'  class='form-control' name='project_tasks["+projectIndex+"][list_order]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-tasks-"+projectIndex+"-percentage'  class='form-control' name='project_tasks["+projectIndex+"][percentage]' onblur='return calculate()'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='project-tasks-"+projectIndex+"-fees'  class='form-control amt' name='project_tasks["+projectIndex+"][fees]' onblur='return calculate()' rel='"+projectIndex+"'></div></td>";
		html += "</tr>";

		// html1 = "<tr id='second_row_"+projectIndex+"'>";
		// 	html1 += "<td>&nbsp;</td>";
		// 	html1 += "<td colspan='12'><input type='text' id='project-tasks-"+projectIndex+"-service'  class='form-control' name='project_tasks["+projectIndex+"][service]' placeholder='Service'></td>";
		// html1 += "</tr>";
		html2 = "<tr id='third_row_"+projectIndex+"'>";
			html2 += "<td>&nbsp;</td>";
			html2 += "<td colspan='12'><input type='text' id='project-tasks-"+projectIndex+"-description'  class='form-control' name='project_tasks["+projectIndex+"][description]' placeholder='Description'></td>";
		html2 += "</tr>";

		$(html).appendTo("#tblDetails tbody");
		// $(html1).appendTo("#tblDetails tbody");
		$(html2).appendTo("#tblDetails tbody");

        $("#project-tasks-"+projectIndex+"-task-id").val(task_id);
        $("#project-tasks-"+projectIndex+"-fees-type").val(fees_type);
		$("#project-tasks-"+projectIndex+"-fees-type").trigger("change");
		$("#project-tasks-"+projectIndex+"-days-require").val(days_require);
        $("#project-tasks-"+projectIndex+"-list-order").val(list_order);
        $("#project-tasks-"+projectIndex+"-service").val(service);
        $("#project-tasks-"+projectIndex+"-description").val(description);

		if($("#project-tasks-"+projectIndex+"-fees-type").val() == "Lumpsum"){
			$("#project-tasks-"+projectIndex+"-percentage").val(0.00);
			$("#project-tasks-"+projectIndex+"-fees").val(percentage);
		}else{
			$("#project-tasks-"+projectIndex+"-percentage").val(percentage);
		}

        $("#project-tasks-"+projectIndex+"-fees").trigger("blur");


		projectIndex++;
		//$(".amt").blur(calculate);
		calculate();
	}

	function amountCalculate(i, retainer) {
        if($("#project-tasks-"+i+"-fees-type").val() == "Percentage") {
            fees = ($("#fees").val() - retainer) * $("#project-tasks-"+i+"-percentage").val() /100 ;
            $("#project-tasks-"+i+"-fees").val(fees.toFixed(2));
        }
    }

	function calculate() {
        if($("#fees-type").val()!='Lumpsum') {
            constructionCost = $("#cost-of-construction").val() * $("#total-construction-area").val();
            $("#total-cost-of-construction").val(constructionCost.toFixed(2));
            fees = parseFloat($("#total-cost-of-construction").val() * $("#architectural-fees").val()/100);
            $("#fees").val(fees.toFixed(2));
        }

        retainerAmount = 0;
        $(".amt").each(function(){
            if($("#project-tasks-"+$(this).attr('rel')+"-fees-type").val() == "Lumpsum Retainer") {
                retainerAmount += $("#project-tasks-"+$(this).attr('rel')+"-fees").val();
            }
        });

        $(".amt").each(function(){
            amountCalculate($(this).attr('rel'), retainerAmount);
            feesType($(this).attr('rel'));
        });

		var total=0;
		var amount=0;

		cgst= $("#amount").val() * $("#cgst").val()/100;
		$("#cgst-amount").val(cgst.toFixed(2));
		sgst= $("#amount").val() * $("#sgst").val()/100;
		$("#sgst-amount").val(sgst.toFixed(2));
		igst= $("#amount").val() * $("#igst").val()/100;
		$("#igst-amount").val(igst.toFixed(2));

		$(".amt").each(function(){
			if(!isNaN($(this).val()) && $(this).val()!="")  {
				amount += parseFloat($(this).val());
				total += parseFloat($(this).val());
			}
		});

		total += parseFloat($("#cgst-amount").val());
		total += parseFloat($("#sgst-amount").val());
		total += parseFloat($("#igst-amount").val());

		$("#amount").val(amount.toFixed(2));
		$("#final-amount").val(total.toFixed(2));
	}

	function remove_row(i) {
		$("#row_"+i).detach();
		$("#second_row_"+i).detach();
		$("#third_row_"+i).detach();

		calculate();
		i--;
	}

	function getStages() {
		if($("#project-type-id").val()!='') {
			$('.stage').show();
			$("#tblDetails tbody").html('');
            projectIndex = 0;
			$.getJSON('/ProjectTypes/getProjectTypeStages/'+$("#project-type-id").val(),function(data){
                for(var i=0; i<data.length; i++) {
                    add_row(data[i].task_id, data[i].fees_type, data[i].days_require,data[i].list_order,data[i].percentage, data[i].task.service, data[i].task.description);
                }

                $(".calc").trigger("blur");
			});
		} else {
			$('.stage').hide();
		}
	}

</script>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($project,['type'=>'file','novalidate'=>true]) ?>

    <fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('quotation_no',['readonly'=>true,'label'=>'Quotation No']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->N->datepicker('quotation_date',['required'=>true,'label'=>'Quotation Date']); ?>
			</div>
		</div>
	</fieldset><br>

	<fieldset>
	<legend><?= __('Clients Details') ?></legend>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->control('client_id',['empty'=>'---']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('client_team_id',['label'=>'Contact Person', 'empty'=>'---']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('project_type_id',['empty'=>'---']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('lead_type_id',['empty'=>'---']); ?>
			</div>
		</div>
	</fieldset><br>

	<fieldset>
	<legend><?= __('Contact Details') ?></legend>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('contact_person'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('mobile'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('email'); ?>
			</div>
		</div>
	</fieldset><br>

	<fieldset>
	<legend><?= __('Site Address Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('site_name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('site_address_line_1',['label'=>'Address Line 1']); ?>
				<?php echo $this->Form->control('site_address_line_2',['label'=>'Address Line 2']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('sity_city',['label'=>'City']); ?>
				<?php echo $this->Form->control('site_pincode',['label'=>'Pincode']); ?>
			</div>
		</div>
	</fieldset><br>

	<fieldset>
	<legend><?= __('Site and Construction Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php //echo $this->Form->control('fees_type'); ?>
				<?php echo $this->Form->control('fees_type',['type'=> 'select','options'=>['Percentage Basis'=>'Percentage Basis','Lumpsum'=>'Lumpsum','Lumpsum Retainer'=>'Lumpsum Retainer']]);?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" id="costOfConst">
				<?php echo $this->Form->control('cost_of_construction',['label'=>"Cost Of Const Per Sq.Ft(Rs)",'class'=>'calc','inputGroup'=>['prepend'=>'fa fa-rupee']]); ?>
			</div>
			<div class="col-md-2" id="tCostArea">
				<?php echo $this->Form->control('total_construction_area',['label'=>"Total Const. Area" ,'class'=>'calc']); ?>
			</div>
			<div class="col-md-3" id="tCostOfConst">
				<?php echo $this->Form->control('total_cost_of_construction',['label'=>'Total Cost Of Construction (Rs)' ,'class'=>'calc','readonly'=>true]); ?>
			</div>
			<div class="col-md-2" id="archFees">
				<?php echo $this->Form->control('architectural_fees',['label'=>'Architectural Fees (%)' ,'class'=>'calc']); ?>
			</div>
			<div class="col-md-2" id="Fees">
				<?php echo $this->Form->control('fees',['label'=>'Fees (Rs)' ,'class'=>'form-control calc']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
            <div class="col-md-4">
				<?php echo $this->Form->control('free_visits'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('visit_charge',['label'=>'Visit Charge (Rs)']); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->N->datepicker('next_followup_date',['required'=>true]); ?>
			</div>
		</div>
		<div class="row">
            <div class="col-md-4">
                <?php echo $this->Form->control('documentation_communication_charges',['label'=>'Architect Documentation & Communication Charges']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('dc_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('travelling_charges',['label'=>'Travelling (To & Fro) Charges.']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('t_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4">
                <?php echo $this->Form->control('lodging_boarding_charges',['label'=>'Lodging & Boarding Charges']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('lb_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
		</div>
		<div class="row">
            <div class="col-md-4">
                <?php echo $this->Form->control('models_charges',['label'=>'Models Presentation Charges']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('m_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('computer_simulation_charges',['label'=>'Computer Simulation Charges.']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('cs_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4">
                <?php echo $this->Form->control('drawing_charges',['label'=>'Drawing Presentation Charges']); ?>
			</div>
            <div class="col-md-4">
                <?php echo $this->Form->control('d_unit',['label'=>'Unit', 'options'=>Cake\Core\Configure::read('units')]); ?>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('description',['label'=>'Note', 'class'=>'summernote']); ?>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-12">
				<?php //echo $this->Form->control('notes_after_discussion', ['class'=>'summernote']); ?>
			</div>
		</div> -->
		<!-- <div class="row">
			<div class="col-md-12">
				<?php //echo $this->Form->control('revise_description',['label'=>'Revise Quotation Description']); ?>
			</div>
		</div> -->
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('inclusion',['class'=>'summernote']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('exclusion',['class'=>'summernote']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('prepared_by',['options'=>$employees, 'empty'=>true]); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
	<legend><?= __('Tasks/Services') ?></legend>
		<div class="row stage">
            <div class="col-md-12">
                <table class="table table-striped table-condense table-responsive" id="tblDetails">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th style="width:20%">Task</th>
                            <th style="width:20%">Fees Type</th>
							<th style="width:20%">Days Require</th>
                            <th style="width:20%">List Order</th>
                            <th style="width:10%">Percentage</th>
                            <th style="width:10%">Fees</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if(!empty($project['project_tasks'])) {
                            foreach($project['project_tasks'] as $i=>$detail) {
                    ?>
                        <tr id="row_<?php echo $i;?>">
                            <td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td>
                            <td>
                                <?php
                                    echo $this->Form->input('project_tasks.'.$i.'.id',['type'=>'hidden']);
                                    echo $this->Form->control('project_tasks.'.$i.'.task_id',array('label'=>false,'options'=>$tasks,'empty'=>'(Choose One)','value'=>$detail['task_id'], 'onChange'=>'return taskChange('.$i.')'));
                                ?>
                            </td>
                            <td><?php echo $this->Form->control('project_tasks.'.$i.'.fees_type',['label'=>false,'options'=>['Percentage'=>'Percentage','Lumpsum'=>'Lumpsum','Lumpsum Retainer'=>'Lumpsum Retainer'],'empty'=>'(Choose One)' ,'value'=>$detail['fees_type'], 'onChange'=>'return feesType('.$i.')']);?></td>
							<td><?php echo $this->Form->control('project_tasks.'.$i.'.days_require',['type'=>'text','label'=>false,'class'=>'form-control' ,'value'=>$detail['days_require']]);?></td>
                            <td><?php echo $this->Form->control('project_tasks.'.$i.'.list_order',['type'=>'text','label'=>false,'class'=>'form-control' ,'value'=>$detail['list_order']]);?></td>
                            <td><?php echo $this->Form->control('project_tasks.'.$i.'.percentage',['type'=>'text','label'=>false,'class'=>'form-control' ,'value'=>$detail['percentage'],'onblur'=>'return calculate()']);?></td>
                            <td><?php echo $this->Form->control('project_tasks.'.$i.'.fees',['type'=>'text','label'=>false,'class'=>'form-control amt' ,'value'=>$detail['fees'] ,'onblur'=>'return calculate()', 'rel'=>$i]);?></td>
                        </tr>
						<!-- <tr id="second_row_<?php //echo $i;?>">
							<td>&nbsp;</td>
							<td colspan="12"><?php //echo $this->Form->control('project_tasks.'.$i.'.service',['type'=>'text','label'=>false,'class'=>'span12' ,'value'=>$detail['service'],'placeholder'=>'Service']);?></td>
						</tr> -->
						<tr id="third_row_<?php echo $i;?>">
							<td>&nbsp;</td>
							<td colspan="12"><?php echo $this->Form->control('project_tasks.'.$i.'.description',['type'=>'text','label'=>false,'class'=>'span12' ,'value'=>$detail['description'],'placeholder'=>'Description']);?></td>
						</tr>

                    <?php  }} ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><a href="javascript:void(0); add_row();">+</a></td>
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"></td>
                            <td align="right">Amount</td>
                            <td align="right">
                                <?php echo $this->Form->control('amount',array('readonly'=>true,'label'=>false,'class'=>'calc')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"></td>
                            <td>
                                <?php echo $this->Form->control('cgst',array('placeholder'=>'CGST Rate %','readonly'=>false,'label'=>'CGST Rate %','class'=>'calc','onblur'=>'return calculate()')); ?>
                            </td>
                            <td>
                                <?php echo $this->Form->control('cgst_amount',array('placeholder'=>'CGST Amount','readonly'=>true,'label'=>'CGST Amount','class'=>'calc')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"></td>
                            <td>
                                <?php echo $this->Form->control('sgst',array('placeholder'=>'SGST Rate %','readonly'=>false,'label'=>'SGST Rate %','class'=>'calc','onblur'=>'return calculate()')); ?>
                            </td>
                            <td>
                                <?php echo $this->Form->control('sgst_amount',array('placeholder'=>'CGST Amount','readonly'=>true,'label'=>'SGST Amount','class'=>'calc')); ?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" align="right"></td>
                            <td>
                                <?php echo $this->Form->control('igst',array('placeholder'=>'IGST Rate %','readonly'=>false,'label'=>'IGST Rate %','class'=>'calc','onblur'=>'return calculate()')); ?>
                            </td>
                            <td>
                                <?php echo $this->Form->control('igst_amount',array('placeholder'=>'IGST Amount','readonly'=>true,'label'=>'SGST Amount','class'=>'calc')); ?>
                            </td>
                        </tr>
					    <tr>
                            <td colspan="5" align="right"></td>
                            <td align="right">Final Amount</td>
                            <td align="right">
                                <?php echo $this->Form->control('final_amount',array('readonly'=>true,'label'=>false,'class'=>'calc')); ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
                <?php echo $this->Form->control('is_revised',array('type'=>'select','options'=>array('Edit'=>'Edit', 'Revised'=>'Revised'),'label'=>false)); ?>
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end() ?>
</div>

