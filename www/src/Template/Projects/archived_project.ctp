<?php
use Cake\I18n\Time;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
    <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
    <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
    <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
    <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('/projects/archived-project', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('project_no','Project No') ?></th>
							<th scope="col"><?= $this->Paginator->sort('project_date','Project Date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('site_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('client_id') ?></th>
							<th scope="col"><?= $this->Paginator->sort('final_amount') ?></th>
							<th scope="col"><?= $this->Paginator->sort('archive_on','Archived By') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($projects as $project): //debug($projects->toArray());exit;?>
						<tr>
							<td><?= $project->project_no?></td>
							<td><?= $project->project_date?></td>
                            <td><?= $project->site_name?></td>
							<?php if(!empty($project['client']['client_name'])){ ?>
							<td><?= $project->client->client_name?></td>
							<?php }else{ ?>
							<td></td>
							<?php } ?>
							<td align="right"><?= $this->Number->currency($project->final_amount,'INR') ?></td>
							<td>
								<?= $project->archived_by_employee->name ?>
								<?= $project->archive_on?>

							</td>
                            <td class="actions">
                                <a href="/files/projects/quotation/quotation-<?php echo str_replace('/', '-', $project->quotation_no);?>.pdf" target="_blank" class="btn btn-primary btn-sm">Quotation</a>
                                <a href="/files/projects/agreement/agreement-<?php echo str_replace('/', '-', $project->project_no);?>.pdf" target="_blank" class="btn btn-primary btn-sm">Agreement</a>
						    </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
