<?php //debug($project);exit; ?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
	<fieldset>
		<legend><?= __('Project') ?></legend>
			<div class="row">
				<div class="col-md-6">
						Quotation No: <?php echo $project['quotation_no']; ?>
				</div>
				<div class="col-md-6">
						Quotation Date: <?php echo $project['quotation_date']; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
						Project No: <?php echo $project['project_no']; ?>
				</div>
				<div class="col-md-6">
						Project Date: <?php echo $project['project_date']; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    Purchase Order Ref.: <?php echo $project['purchase_order_no']; ?>
				</div>
				<div class="col-md-6">
                    Purchase Order Copy:
                    <?php
                        if(!empty($project->purchase_order_img_dir)) {
						    echo $this->Html->link($project->purchase_order_img,'/files/projects/purchase_order_img/'.$project->purchase_order_img_dir.'/'.$project->purchase_order_img,['target'=>'_blank']);
					    }
                    ?>
				</div>
			</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Client Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
						Company Name: <?php echo $project['client']['client_name']; ?>
				</div>
				<div class="col-md-6">
						Email: <?php echo $project['client']['email']; ?>
				</div>
			</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Address Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
						Office Address:
						<?php echo $project['client']['office_address_line_1']; ?>,
						<?php echo $project['client']['office_address_line_2']; ?>,
						<?php echo $project['client']['office_city']; ?>,
						<?php echo $project['client']['office_pincode']; ?>.
				</div>
				<div class="col-md-6">
						Site Address:
						<?php echo $project['site_address_line_1']; ?>,
						<?php echo $project['site_address_line_2']; ?>,
						<?php echo $project['sity_city']; ?>,
						<?php echo $project['site_pincode']; ?>.
				</div>
			</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Contact Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					Mobile: <?php echo $project['mobile']; ?>
				</div>
				<div class="col-md-6">
					Contact Person: <?php echo $project['contact_person']; ?>
				</div>
			</div><br>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Project Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					Project Name : <?php echo $project['site_name']; ?>
				</div>
				<div class="col-md-6">
					Project Type : <?php echo $project['project_type']['project_type']; ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					Total Construction Area : <?php echo $project['total_construction_area']; ?>
				</div>
				<div class="col-md-6">
					Fees Type : <?php echo $project['fees_type']; ?>
				</div>
			</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Project Fees Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					Cost Of Construction Per Square Feet : <?php echo $project['cost_of_construction']; ?>
				</div>
				<div class="col-md-6">
					Total Constructional Area : <?php echo $project['total_construction_area']; ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					Total Cost Of Const : <?php echo $project['total_cost_of_construction']; ?>
				</div>
				<div class="col-md-6">
					Architectural Fees : <?php echo $project['architectural_fees']; ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					Fees : <?php echo $project['fees']; ?>
				</div>
				<div class="col-md-6">
					Visit Charge : <?php echo $project['visit_charge']; ?>
				</div>
			</div><br>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Additional Details') ?></legend>
			<div class="row">
				<div class="col-md-6">
					Note : <?php echo $project['description']; ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					Notes After Discussion : <?php echo $project['total_construction_area']; ?>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-6">
					Inclusion : <?php echo $project['inclusion']; ?>
				</div>
				<div class="col-md-6">
					Exclusion : <?php echo $project['exclusion']; ?>
				</div>
			</div><br>

	</fieldset><br>
	<fieldset>
		<legend><?= __('Task/Services') ?></legend>
		<div class="row">
			<div class="col-md-12">
				<table class='table table-bordered table-hover table-condensed'>
					<thead>
						<tr>
							<th>Task</th>
							<th>Fees Type</th>
							<th>Days Require</th>
							<th>List Order</th>
							<th>Percentage</th>
							<th>Fees Brekup</th>

						</tr>
					</thead>
					<tbody>
						<?php
							if(!empty($project['project_tasks'])) {
								foreach($project['project_tasks'] as $i=>$task) { //debug($task);exit;
						?>
						<tr>
							<td><?php echo $task['task']['task'];?></td>
							<td><?php echo $task['fees_type'];?></td>
							<td><?php echo $task['days_require'];?></td>
							<td><?php echo $task['list_order'];?></td>
							<td><?php echo $task['percentage'];?></td>
							<td><?= $this->Number->currency($task['fees'],'INR') ?></td>
						</tr>
						<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<legend><?= __('Project/Stakeholders') ?></legend>
		<div class="row">
			<div class="col-md-12">
				<table class='table table-bordered table-hover table-condensed'>
					<thead>
						<tr>
							<th>Stakeholder</th>
							<th>Phone No</th>
							<th>Email</th>
							<th>Address</th>
							
						</tr>
					</thead>
					<tbody>
						<?php
							if(!empty($project['project_stakeholders'])) {
								foreach($project['project_stakeholders'] as $i=>$stakeholder) { //debug($task);exit;
						?>
						<tr>
							<td><?php echo $stakeholder['stakeholder']['contractor'];?></td>
							<td><?php echo $stakeholder['stakeholder']['mobile_1'];?></td>
							<td><?php echo $stakeholder['stakeholder']['email'];?></td>
							<td><?php echo $stakeholder['stakeholder']['address_line_1'];?> &nbsp; <?php echo $stakeholder['stakeholder']['address_line_2'];?> &nbsp; <?php echo $stakeholder['stakeholder']['city'];?> &nbsp; <?php echo $stakeholder['stakeholder']['pincode'];?></td>
							
						</tr>
						<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset><br>


</div>
