<?= $this->Form->create($revision,['type'=>'file','novalidate'=>true]) ?>
<div class="box box-block bg-white">
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<strong>Quoation No:</strong><?= $project->project_no; ?>
			</div>
			<div class="col-md-4">
				<strong>Quoation Date:</strong> <?= $project->project_no; ?>
			</div>
			<div class="col-md-4">
				<strong>Client Name:</strong> <?= $project->client->client_name; ?>
			</div>
		</div>
	</fieldset><br>
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
   <fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->hidden('project_id',['value'=>$project->id]); ?>
				<?php echo $this->N->datepicker('revision_date'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('remarks', ['class'=>'summernote']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['controller'=>'Projects','action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>
<?=$this->Form->end() ?>




<div class="box box-block bg-white">
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" width="20%"><?= $this->Paginator->sort('revision_date')?></th>
							<th scope="col"><?= $this->Paginator->sort('remarks')?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($revisions as $revision): ?>
                        <tr>
                            <td><?= $revision->revision_date?></td>
							<td><?= $revision->remarks?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
