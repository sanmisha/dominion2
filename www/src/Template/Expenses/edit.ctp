<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($expense,['type'=>'file','novalidate'=>true]) ?> 
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('voucher_no',['readonly'=>true]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->N->datepicker('date',['required'=>true]); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('project_id', ['options'=>$project , 'empty'=>'---']); ?>								
			</div>				
			<div class="col-md-3">
				<?php echo $this->Form->control('expense_head_id', ['options'=>$expensehead , 'empty'=>'---']); ?>								
			</div>
		</div>	

		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('amount'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('description'); ?>
			</div>	
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>	
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

