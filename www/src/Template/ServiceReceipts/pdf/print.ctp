<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
				
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #4D4D4D;
				padding: 5px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}
			.tr{
				border: none;
			}			

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3.5cm;
			}
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<img src="https://dominion.kashelkardesigns.com/img/letterhead.png" style="width:100%" />		
           
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
            <div style="text-align:center">
                410,411-412, Globe Arcade, Near Ganesh Mandir, MIDC Residential Zone, Dombivli East 421 203<br />
                <strong>Website:</strong> www.kashelkardesings.com | <strong>Email:</strong> info@archkdpl.com | <strong>Phone:</strong> +91 9920951588 / 0251-2447744<br />
                <strong>CIN: U74999MH2020PTC339226</strong>
            </div>
    	</htmlpagefooter>

		<br>
		<table class="tr">
			<tr class="tr">
				<td class="tr">
                    To,<br />
					<strong><?= $serviceReceipt->client->client_name ?></strong><br />
                    <?= $serviceReceipt->client->office_address_line_1 ?><br />
                    <?= $serviceReceipt->client->office_address_line_2 ?><br />
                    <?= $serviceReceipt->client->office_city ?> - <?= $serviceReceipt->client->office_pincode ?><br /><br />


                    <strong>
                        GSTIN: <?= $serviceReceipt->client->gst_no ?><br />
                        State Code: <?= substr($serviceReceipt->client->gst_no, 0, 2) ?><br />
                        PAN: <?= $serviceReceipt->client->pan_no ?><br />
                       
                    </strong>
                    Email: <?= $serviceReceipt->client->email ?><br />
                    Mobile: <?= $serviceReceipt->client->mobile ?><br />
				</td>
			</tr>
		</table>

		<h1 align="center">  Receipts </h1>
		<table class="tr">
			<tr class="tr">
				<td width="50%" class="tr"> Received with thanks from :- <strong><?= $serviceReceipt->client->client_name ?></strong> </td>
				<td width="50%" align="right" class="tr">
					<p>Receipt No. : <?= $serviceReceipt->receipt_no ?> </p>
					<p>Receipt Date. : <?= $serviceReceipt->receipt_date ?> </p>
				</td>
			</tr>
			<tr class="tr">
				<td colspan="2" class="tr" width="100%">
					<p>By :  <strong><?= $serviceReceipt->payment_mode ?></strong> </p>
					<?php if($serviceReceipt->payment_mode == 'Cheque'){ ?>
					<p> <span> Cheque No. :  <strong> <?= $serviceReceipt->cheque_no ?></strong> </span>  &emsp;  <span> Dated : <?= $serviceReceipt->cheque_date ?>  </span> &emsp; <span align="right"> Drawee Bank : <?= $serviceReceipt->bank_name  ?> </span> </p>
					<?php } else if($serviceReceipt->payment_mode == 'Bank Transfer') { ?>
					<p>Trasaction No. : <?= $serviceReceipt->transaction_no ?> &emsp; &emsp; &emsp; Dated : <?= $serviceReceipt->transaction_date  ?>   </p>
					<?php }  ?>
				</td>
			</tr>
			
			
		</table>	
		<br>
		<h3>  In full / part payment of the following invoice/s </h3>	
		<br>
		<table>
			<thead>
				<tr  class="color">
					<th colspan="6"> Particuler </th>
					<th rowspan="2"> Amount </th>
				</tr>
				<tr  class="color">
					<th> Invoice No </th>
					<th> Invoice Date </th>
					<th> Project Code</th>					
					<th> Invoice Amount </th>
					<th> Received Amount</th>
					<th> TDS </th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach($serviceReceipt->service_receipt_invoices as $i=>$receipt){ ?>
					<tr>
						<td><?=  $receipt->service_invoice->invoice_no ?></td>
						<td> <?= $receipt->service_invoice->invoice_date ?></td>
						<td><?= (!empty($receipt->service_invoice->project_id)) ?  $receipt->service_invoice->project->project_no : '' ?></td>
						<td align="right"> <?=  $this->Number->currency( $receipt->service_invoice->total_amount)  ?></td>
						<td align="right"> <?=  $this->Number->currency( $receipt->received_amount) ?></td>
						<td align="right">  <?= $this->Number->currency( $receipt->tds) ?> </td>
						<td align="right"><?= $this->Number->currency( $receipt->total_received_amount)  ?></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr  class="color">
					<td colspan="6" align="right"><strong>  Total:- </strong> </td>
					<td> <strong> <?=  $this->Number->currency($serviceReceipt->amount) ?> </strong></td>
				</tr>
				<tr  class="color">
					<td colspan="3" align="right"> <strong> Amount In Words</strong></td>
					<td colspan="4" align="right"> <strong> <?=  $serviceReceipt->amount_in_words ?></strong></td>
				</tr>
				<tr  class="color">
					<td colspan="3" class="tr">
						<br> <br> <br> <br>
						<p>E. & O.E.<p>			
						<p> <strong> Cheque subject to realisation </strong></p>
					</td>
					<td colspan="4" align="right" class="tr">
						<br> <br> <br> <br>
						<p> <strong> For Kashelkar Designs Pvt. Ltd. </strong> </p>
						<p> &ensp; Authorised Signature</p>
					</td>
				</tr>
			</tfoot>
		</table>	
	
	
     
	</body>
</html>
