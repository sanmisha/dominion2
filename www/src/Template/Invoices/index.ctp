
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('invoices', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('invoice_no')?></th>
							<th scope="col"><?= $this->Paginator->sort('invoice_date')?></th>
                            <th scope="col"><?= $this->Paginator->sort('project.project_no', 'Project No')?></th>
                            <th scope="col"><?= $this->Paginator->sort('project.client.client_name', 'Client')?></th>
                            <th scope="col"><?= $this->Paginator->sort('invoice_details.task.task', 'Task')?></th>
                            <th scope="col"><?= $this->Paginator->sort('total_amount')?></th>
                            <th scope="col"><?= $this->Paginator->sort('balance_amount')?></th>
							<th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($invoices as $invoice): 
                                $class = $invoice->is_cancel ? "cancel" : "";
                        ?>
                        <tr  class="<?=$class?>">
                            <td><?= $invoice->invoice_no?></td>
							<td><?= $invoice->invoice_date?></td>
                            <td><?= $invoice->project->project_no?></td>
                            <td><?= $invoice->project->client->client_name?></td>
                            <td><?= $invoice->invoice_details[0]->task->task?></td>
                            <td><div class="pull-right"><?= $this->Number->currency($invoice->total_amount)?></div></td>
                            <td><div class="pull-right"><?= $this->Number->currency($invoice->balance_amount)?></div></td>
                            <td class="actions">
                               <div class="pull-right">
                                    <?= ($invoice->is_cancel == false) ? $this->N->editLink(['action' => 'edit', $invoice->id]) : '' ; ?>
                                    <?= ($invoice->is_cancel == false) ? $this->Html->link('Cancel', ['action'=>'cancel', $invoice->id] ,['class'=>'btn btn-danger btn-sm', 'confirm'=>'Are you sure you want to cancel this Invoice ?']) : ''; ?>
                                    <?= $this->Html->link('Print', '/files' . DS . 'projects' . DS . 'invoices' . DS . 'invoice-' . $invoice->id . '.pdf' ,['class'=>'btn btn-info btn-sm', 'target'=>'_blank']); ?>
                                    <?= empty($invoice->invoice_no) ? $this->Html->link('Approve', ['action'=>'approve', $invoice->id] ,['class'=>'btn btn-success btn-sm', 'confirm'=>'Are you sure you want to approve this Invoice ?']) : ''; ?>
                                    <?= $this->N->deleteLink(['action' => 'delete', $invoice->id]); ?>
                               </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
