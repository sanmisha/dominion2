<script>

	invoiceIndex = 0;
	invoiceIndex = <?php echo count($invoice['invoice_details'])+1; ?>;

	$(document).ready(function(){
		//$("#project-type-id").on("change", getStages)
		$(".calc").blur(function(){
			amountCalculate();
		});
	});

	function add_row() {
		html = "<tr id='row_"+invoiceIndex+"'>";
			html += "<td><a href='javascript:void(0); remove_row("+invoiceIndex+");'><i class='fa fa-minus'></i></a></td>";
            html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-service'  class='form-control' name='invoice_details["+invoiceIndex+"][service]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-description'  class='form-control' name='invoice_details["+invoiceIndex+"][description]'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-fees'  class='form-control' name='invoice_details["+invoiceIndex+"][fees]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-discountRate'  class='form-control ' name='invoice_details["+invoiceIndex+"][discount]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-discountAmount'  class='form-control ' name='invoice_details["+invoiceIndex+"][dis_amt]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-cgstRate'  class='form-control' name='invoice_details["+invoiceIndex+"][cgst_rate]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-cgstAmount'  class='form-control ' name='invoice_details["+invoiceIndex+"][cgst_amount]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-sgstRate'  class='form-control' name='invoice_details["+invoiceIndex+"][sgst_rate]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-sgstAmount'  class='form-control ' name='invoice_details["+invoiceIndex+"][sgst_amount]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-igstRate'  class='form-control' name='invoice_details["+invoiceIndex+"][igst_rate]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-igstAmount'  class='form-control ' name='invoice_details["+invoiceIndex+"][igst_amount]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";
			html += "<td><div class='form-group'><input type='text' id='invoice-details-"+invoiceIndex+"-amount'  class='form-control amt' name='invoice_details["+invoiceIndex+"][amount]' onblur='return amountCalculate("+invoiceIndex+")'></div></td>";

		html += "</tr>";
		$(html).appendTo("#tblDetails tbody");
		invoiceIndex++;
		$(".amt").blur(calculate);
		calculate();

	}


	function amountCalculate(i) {

		var discountAmount = 0;
		var finalAmount=0;
		var cgstAmount= 0;


		discountAmount = $("#invoice-details-"+i+"-fees").val() * $("#invoice-details-"+i+"-discount").val()/100;
		$("#invoice-details-"+i+"-discountAmount").val(discountAmount.toFixed(2));

		finalAmount = $("#invoice-details-"+i+"-fees").val() - discountAmount;

		grossAmount = finalAmount;

		cgstAmount = grossAmount * $("#invoice-details-"+i+"-cgst-rate").val()/100;
		$("#invoice-details-"+i+"-cgst-amount").val(cgstAmount.toFixed(2));

		sgstAmount= grossAmount * $("#invoice-details-"+i+"-sgst-rate").val()/100;
		$("#invoice-details-"+i+"-sgst-amount").val(sgstAmount.toFixed(2));

		igstAmount= grossAmount * $("#invoice-details-"+i+"-igst-rate").val()/100;
		$("#invoice-details-"+i+"-igst-amount").val(igstAmount.toFixed(2));

        finalAmount = finalAmount + cgstAmount;
		finalAmount = finalAmount + sgstAmount;
		finalAmount = finalAmount + igstAmount;

		$("#invoice-details-"+i+"-amount").val(finalAmount.toFixed(2));
		//$("#invoice-details-"+i+"-final-amount").val(finalAmount.toFixed(2));

		calculate();
	}

	function calculate() {

		var total=0;
		var amount=0;

		$(".amt").each(function(){
			if(!isNaN($(this).val()) && $(this).val()!="")  {
				amount += parseFloat($(this).val());
				total += parseFloat($(this).val());
			}
		});

		$("#amount").val(amount.toFixed(2));
		$("#total-amount").val(total.toFixed(2));
	}

	function remove_row(i) {
		$("#row_"+i).detach();
		calculate();
		i--;
	}
</script>




<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($invoice,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('invoice_no',['readonly'=>true]); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->N->datepicker('invoice_date',['required'=>true]); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('project_id', ['options'=>$projects , 'empty'=>'---']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row stage">
			<legend><span>Invoice Details</span></legend>
			<table class="table table-striped table-condense table-responsive" id="tblDetails">
				<thead>
					<tr>
						<!-- <th>&nbsp;</th> -->
						<th style="width:9%">Project Stage</th>
						<th style="width:9%">Fees</th>
						<th style="width:9%">Discount</th>
						<th style="width:9%">Discount Amount</th>
						<th style="width:9%"> CGST Rate</th>
						<th style="width:9%">CGST Amount</th>
						<th style="width:9%">SGST Rate</th>
						<th style="width:9%">SGST Amount</th>
						<th style="width:9%">IGST Rate</th>
						<th style="width:9%">IGST Amount</th>
						<th style="width:9%">Amount</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(!empty($invoice['invoice_details'])) {
						foreach($invoice['invoice_details'] as $i=>$detail) {
				?>
					<tr id="row_<?php echo $i;?>">
						<!-- <td><a href='javascript:void(0); remove_row("<?php echo $i;?>");'><i class='fa fa-minus'></i></a></td> -->
						<td>
							<?php
								echo $this->Form->input('invoice_details.'.$i.'.id',['type'=>'hidden']);
								echo "<strong>{$detail->project_task->task->task}</strong>";
							?>
						</td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.fees',['type'=>'text','label'=>false,'class'=>'form-control' ,'value'=>$detail['fees'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.discount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['discount']]);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.dis_amt',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['dis_amt'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.cgst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['cgst_rate']]);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.cgst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['cgst_amount'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.sgst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['sgst_rate']]);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.sgst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['sgst_amount'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.igst_rate',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['igst_rate']]);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.igst_amount',['type'=>'text','label'=>false,'class'=>'form-control ' ,'value'=>$detail['igst_amount'],'onblur'=>'return amountCalculate('.$i.')']);?></td>
						<td><?php echo $this->Form->control('invoice_details.'.$i.'.amount',['type'=>'text','label'=>false,'class'=>'form-control amt', 'readonly'=>true ,'value'=>$detail['amount'],'onblur'=>'return amountCalculate('.$i.')']);?></td>

					</tr>
				<?php  }} ?>
				</tbody>
				<tfoot>
					<!-- <tr>
						<td><a href="javascript:void(0); add_row();">+</a></td>
						<td colspan="5"></td>
					</tr> -->
					<tr>
						<td colspan="9" align="right"></td>
						<td align="right"><strong>Total Amount</strong></td>
						<td align="right">
							<?php echo $this->Form->control('total_amount',array('readonly'=>true,'label'=>false,'class'=>'calc')); ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

