<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('InwardOutwards', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('project_id','Project No')?></th>
							<th scope="col"><?= $this->Paginator->sort('register_no')?></th>
                            <th scope="col"><?= $this->Paginator->sort('subject')?></th>
							<th scope="col"><?= $this->Paginator->sort('assigned_to','Assigned To') ?></th>
							<th scope="col"><?= $this->Paginator->sort('created', 'Timestamp') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('file_id', 'File No') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Status') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($inwardOutwards as $inwardOutward): ?>
                        <tr>
                            <td><?= $inwardOutward->project ? $inwardOutward->project->project_no . ' ' . $inwardOutward->project->client->client_name : '---'; ?></td>
                            <td><?= $inwardOutward->register_no?></td>
                            <td><?= $inwardOutward->subject?></td>
                            <td>
                                <span class="tag tag-info"><?= $inwardOutward->assigned_employee ? $inwardOutward->assigned_employee->name : ''?></span><br />
                                <?= $inwardOutward->assigned_on ?>
                            </td>
                            <td><?= $inwardOutward->created; ?></td>
                            <td><?= $inwardOutward->location ? $inwardOutward->location->location : ''; ?> - <?= $inwardOutward->file_no ? $inwardOutward->file_no->file_no : ''; ?></td>
                            <td>
                                <?php
                                    if($inwardOutward->status == 'Inward') {
                                        echo "<span class='tag tag-success'>Inward</span>";
                                    } elseif($inwardOutward->status == 'Assigned') {
                                        echo "<span class='tag tag-warning'>Assigned</span>";
                                    } elseif($inwardOutward->status == 'Archive') {
                                        echo "<span class='tag tag-info'>Archive</span>";
                                    } elseif($inwardOutward->status == 'Outward') {
                                        echo "<span class='tag tag-danger'>Outward</span>";
                                    }
                                ?>
                            </td>
							<td class="actions">
								<?php
                                    if(!$inwardOutward->assigned_employee && $inwardOutward->type == 'Inward') {
                                        echo $this->Html->link('Assign',['controller'=>'InwardOutwards','action'=>'assign', $inwardOutward->id], ['class'=>'btn btn-primary btn-sm']);
                                    } elseif($inwardOutward->type == 'Inward' && $inwardOutward->status != 'Archive') {
                                        echo $this->Html->link('Archive',['controller'=>'InwardOutwards','action'=>'archive', $inwardOutward->id], ['class'=>'btn btn-primary btn-sm']);
                                    }
                                ?>
                                <?= $this->N->editLink(['action' => 'edit', $inwardOutward->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
