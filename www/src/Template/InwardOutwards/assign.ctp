

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($inwardOutward,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('type', ['type'=>'text', 'readonly'=>true]); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('register_no', ['readonly'=>true]); ?>
			</div>
			<div class="col-md-6">
                <?php echo $this->Form->control('subject', ['readonly'=>true]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->control('description', ['type'=>'textarea', 'readonly'=>true]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
                <?php echo $this->Form->control('handler', ['readonly'=>true]); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('handler_id', ['type'=>'text', 'readonly'=>true]); ?>
			</div>
			<div class="col-md-3">
			    <label>Attachment</label>
                <br />
                <br />
				<?php
					if(!empty($inwardOutward->file_copy_dir)) {
						echo $this->Html->link($inwardOutward->file_copy,'/files/inwardoutwards/file_copy/'.$inwardOutward->file_copy_dir.'/'.$inwardOutward->file_copy,['target'=>'_blank']);
					}
				?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('assigned_to',['options'=>$employees, 'empty'=>'---']); ?>
			</div>
        </div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

