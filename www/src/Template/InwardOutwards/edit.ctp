

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($inwardOutward,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
            <div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->N->select2('project_id',['empty'=>'=== No Project Linked ===']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('type', ['options'=>['Inward'=>'Inward', 'Outward'=>'Outward']]); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('register_no', ['readonly'=>true]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
                <?php echo $this->Form->control('subject'); ?>
				<?php echo $this->Form->control('description', ['type'=>'textarea']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
                <?php echo $this->Form->control('handler'); ?>
			</div>
			<div class="col-md-3">
                <?php echo $this->Form->control('handler_id', ['type'=>'text', 'label'=>'Handler ID Proof']); ?>
			</div>
			<div class="col-md-3">
			    <label>Document Attachment</label>
				<?php
					echo $this->Form->control('file_copy',['type'=>'file','label'=>false]);
					echo $this->Form->hidden('file_copy_dir');
					if(!empty($inwardOutward->file_copy_dir)) {
						echo $this->Html->link($inwardOutward->file_copy,'/files/inwardoutwards/file_copy/'.$inwardOutward->file_copy_dir.'/'.$inwardOutward->file_copy,['target'=>'_blank']);
					}
				?>
			</div>
			<div class="col-md-3">
			    <label>Handover Supporting Attachment</label>
				<?php
					echo $this->Form->control('courier_slip',['type'=>'file','label'=>false]);
					echo $this->Form->hidden('courier_slip_dir');
					if(!empty($inwardOutward->courier_slip_dir)) {
						echo $this->Html->link($inwardOutward->courier_slip,'/files/inwardoutwards/courier_slip/'.$inwardOutward->courier_slip_dir.'/'.$inwardOutward->courier_slip,['target'=>'_blank']);
					}
				?>
			</div>
        </div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
                <?php if($inwardOutward->isNew()) { ?>
				<button class="btn btn-primary">Save</button>
                <?php } ?>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

