<?php
    use Cake\Core\Configure;
?>
<script>
    $(document).ready(function() {
        $('.ch').on("change", function() {
            var data = $('#project-id').select2('data');
            data = data[0].text.split(" ");
            data = data[0].split("/");
            data = data[1];
            // alert(data);

            if(data != null) {
                file_no = data + '/' + $("#file-type").val() + '/' + $("#sub-category").val() + '/';
            } else {
                file_no = $("#file-type").val() + '/' + $("#sub-category").val() + '/';
            }

            if($("#id").val() == null) {
                $("#file-no").val(file_no);
            }
        });
    });
</script>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($fileNo,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->N->select2('project_id',['empty'=>'=== No Project Linked ===', 'class'=>'ch']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('file_type',['options'=>Configure::read('Files.file_types'), 'empty'=>'---', 'class'=>'ch']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('sub_category',['options'=>Configure::read('Files.sub_categories'), 'empty'=>'---', 'class'=>'ch']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->control('file_no'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>

