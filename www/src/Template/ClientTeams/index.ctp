
<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index', $client_id],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('client_teams', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit',0,$client_id)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('site_name')?></th>
                            <th scope="col"><?= $this->Paginator->sort('name')?></th>
							<th scope="col"><?= $this->Paginator->sort('designation')?></th>
							<th scope="col"><?= $this->Paginator->sort('mobile_1')?></th>
							<th scope="col"><?= $this->Paginator->sort('dob')?></th>
                            <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($clientTeams as $clientTeam): ?>
                        <tr>
                            <td><?= $clientTeam->site_name?></td>
                            <td><?= $clientTeam->name?></td>
							<td><?= $clientTeam->designation?></td>
							<td><?= $clientTeam->mobile_1?></td>
							<td><?= $clientTeam->dob?></td>
                            <td class="actions">
                                <?= $this->N->editLink(['action' => 'edit', $clientTeam->id , $client_id]); ?>
                                <?= $this->N->deleteLink(['action' => 'delete', $clientTeam->id,$client_id]);?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
