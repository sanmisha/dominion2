<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?= $this->Form->create($user,['type'=>'file','novalidate'=>true]) ?>
    <fieldset>
		<legend>Client Team Details</legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->hidden('id'); ?>
				<?= $this->Form->hidden('client_team.client_id',['value'=>$client_id]); ?>
				<?php echo $this->Form->control('client_team.name'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->N->datepicker('client_team.dob',['required'=>true]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('client_team.designation'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('client_team.department'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
	<!-- <legend>Site Address</legend>
        <div class="row">
			<div class="col-md-12">
				<?php //echo $this->Form->control('client_team.site_name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php //echo $this->Form->control('client_team.address_line_1'); ?>
			</div>
			<div class="col-md-6">
				<?php //echo $this->Form->control('client_team.city'); ?>
			</div>
		</div>
	</fieldset><br> -->
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('client_team.address_line_2'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('client_team.pincode'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<legend>Login Details</legend>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('email'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('mobile'); ?>
			</div>
			<?php if($user->isNew()) { ?>
			<div class="col-md-4">
				<?php echo $this->Form->control('password'); ?>
			</div>
			<?php } ?>
		</div>
	</fieldset>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['controller'=>'Clients','action'=>'edit',$client_id]); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
	<?= $this->Form->end(); ?>
</div>






