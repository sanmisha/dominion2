<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package $package
 */
?>
<?= $this->Form->create($clients,['type'=>'file','novalidate'=>true]) ?>
<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
   <fieldset>
	<legend><?= __('Client Details') ?></legend>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->Form->hidden('id'); ?>
				<?php echo $this->Form->control('client_name'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('gst_no',['label'=>'GST No']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('pan_no',['label'=>'PAN No']); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>

	<fieldset>
	<legend><?= __('Office Address  Details') ?></legend>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('office_address_line_1'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('office_city'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->control('office_address_line_2'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->control('office_pincode'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->control('mobile'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('landline'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->control('email'); ?>
			</div>
		</div>
	</fieldset><br>
	<fieldset>
		<div class="row pull-right">
			<div class="col-md-12">
				<?= $this->N->cancelLink(['action'=>'index']); ?>
				<button class="btn btn-primary">Save</button>
			</div>
		</div>
	</fieldset><br>
</div>
<?=$this->Form->end() ?>


<?php if(!empty($clients->id)) {  ?>


<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
		<?= $this->N->addLink(array('controller'=>'ClientTeams','action'=>'edit',0,$clients->id)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('site_name')?></th>
                            <th scope="col"><?= $this->Paginator->sort('name')?></th>
							<th scope="col"><?= $this->Paginator->sort('designation')?></th>
							<th scope="col"><?= $this->Paginator->sort('mobile')?></th>
							<th scope="col"><?= $this->Paginator->sort('email')?></th>
                            <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($clientTeams as $clientTeam): ?>
                        <tr>
                            <td><?= $clientTeam->site_name?></td>
                            <td><?= $clientTeam->name?></td>
							<td><?= $clientTeam->designation?></td>
							<td><?= $clientTeam->user->mobile?></td>
							<td><?= $clientTeam->user->email?></td>
                            <td class="actions">
                                <?= $this->N->editLink(['controller' => 'ClientTeams','action' => 'edit', $clientTeam->id , $clients->id]); ?>
								<?= $this->N->deleteLink(['controller' => 'ClientTeams','action' => 'delete', $clientTeam->id , $clients->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
<?php  }  ?>

