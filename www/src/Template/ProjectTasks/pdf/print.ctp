<?php //debug($projecttasks->toArray()); exit; ?>
<html>
    <head>
		<style>
			body {
				font-family: sans-serif;
				font-size: 11px;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table, th, td {
				border: 1px solid #BFBFBF;
				padding: 5px;
				vertical-align: top;
			}
			th {
				align: center;
				vertical-align: middle;
			}

			tr.rejected td, tr.rejected th {
				text-decoration: line-through;
			}

			.color {
				background-color: #E5E5E5;
			}

			h3, hr {
				padding :0;
				margin :0;
			}
		/** Define the margins of your page **/
			@page {
        		header: html_myHeader;
        		footer: html_myFooter;
				margin-top: 3cm;
			}

			div.page-break {
        		page-break-before: always;
		    }
		</style>
	</head>
	<body>
		<htmlpageheader name="myHeader" style="display:none">
			<h2 align="center">Dominion</h2>
			<p align="center"><strong>PROJECT RESPONSIBILITY CHART for <?= $project->project_with_client ?> as on <?= date('d/m/Y H:i a') ?></strong></p>
			<hr />
    	</htmlpageheader>

		<htmlpagefooter name="myFooter" style="display:none">
			<table width="100%" border="0">
				<tr>
					<td width="70%" align="center" style="font-weight: bold; font-style: italic;">
						&nbsp;
					</td>
					<td align="right" style="font-weight: bold; font-style: italic;">
						Page {PAGENO}/{nbpg}
					</td>
				</tr>
			</table>
		</htmlpagefooter>
		<table>
			<thead>
				<tr class="color">
					<th width="3%">Sr No</th>
					<th>Particulars / Project Stage</th>
                    <th width="8%">Allotted Date</th>
                    <th width="8%">Target Date</th>
                    <th width="8%">Completion Date</th>
					<th width="8%">Responsible Person 1</th>
                    <th width="8%">Email 1</th>
                    <th width="8%">Mobile No 1</th>
					<th width="8%">Responsible Person 2</th>
                    <th width="8%">Email 2</th>
                    <th width="8%">Mobile No 2</th>
				</tr>
			</thead>
			<tbody>
            <?php
                foreach($projectTasks as $i => $projectTask) {
            ?>
            <tr>
                <td><?= ($i+1) ?></td>
                <td>
                    <strong><?= $projectTask->task->task ?></strong><br />
                    <?= $projectTask->description ?>
                </td>
                <td><?= !empty($projectTask->assigned_datetime) ? $projectTask->assigned_datetime->format('d/m/Y') : '' ?></td>
                <td><?= $projectTask->target_date ?></td>
                <td><?= !empty($projectTask->done_datetime) ? $projectTask->done_datetime->format('d/m/Y') : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee1->name : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee1->user->email : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee1->user->mobile : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee2->name : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee2->user->email : '' ?></td>
                <td><?= !empty($projectTask->responsible_employee1) ? $projectTask->responsible_employee2->user->mobile : '' ?></td>
            </tr>
			<?php } ?>
			</tbody>
		</table>
	</body>
</html>
