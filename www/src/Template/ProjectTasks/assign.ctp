    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-block bg-white">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title_for_layout; ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-block bg-white">
	    <legend>Task</legend>
		 <div class="row">
			<div class="col-sm-6">
				<strong>Project No:</strong> <?php echo $projectTask->project->project_with_client;	?>
			</div>
			<div class="col-sm-3">
				<strong>Project Date:</strong> <?php echo $projectTask->project->project_date; ?>
			</div>
            <div class="col-sm-3">
				<strong>Days Require:</strong> <?php echo $projectTask->days_require;	?>
            </div>
		</div><br>
		<div class="row">
			<div class="col-sm-6">
				<strong>Service:</strong> <?php echo $projectTask->service;	?>
			</div>
			<div class="col-sm-6">
				<strong>Description:</strong> <?php echo $projectTask->description;	?>
			</div>
		</div><br>
	</div>
    <?php echo $this->Form->create($projectTask, array('type'=>'file','noValidate'=>true)); ?>
	<div class="box box-block bg-white">
		 <div class="row">
			<div class="col-sm-4">
				<?php
					echo $this->Form->hidden('id');
				    echo $this->Form->control('assigned_to', ['options'=>$employees,'empty'=>'---']);
				?>
			</div>
			<div class="col-sm-4">
				<?php echo $this->N->datepicker('target_date'); ?>
           </div>
		</div>
	</div>
	<div class="box box-block bg-white">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
                    <?= $this->N->cancelLink(['action'=>'index', $projectTask->project_id]); ?>
                    <button class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>
    <?php echo $this->Form->end(); ?>
