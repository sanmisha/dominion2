<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('project_tasks', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
    <div class="clearfix mb-1">
		<div class="float-xs-right">
			<?= $this->N->addLink(array('action'=>'edit')); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('task_id')?></th>
							<th scope="col"><?= $this->Paginator->sort('assigned_by','Assigned By') ?></th>
							<th scope="col"><?= $this->Paginator->sort('assigned_to','Assigned To') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('done_datetime','Done on') ?></th>
							<th scope="col"><?= $this->Paginator->sort('target_date','Target Date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('responsible_1') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('responsible_2') ?></th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($projectTasks as $projectTask): //debug($projectTask);exit; ?>
                        <tr>
							<td><?= $projectTask->task->task?></td>
							<td>
                                <?php
                                    if(!empty($projectTask['assigned_employee'])) {
                                        echo '<span class="tag tag-info">'.($projectTask['assigned_user']['name']).'</span><br>';
                                    }
                                ?>
                            </td>
							<td>
								<?php
                                    if(!empty($projectTask['assigned_employee'])) {
                                        echo '<span class="tag tag-info">'.($projectTask['assigned_employee']['name']).'</span><br>';
                                        echo date_format($projectTask['assigned_datetime'], 'jS M Y');
                                    }
								?>
							</td>
                            <td><?= $projectTask->done_datetime; ?></td>
							<td><?= $projectTask->target_date; ?></td>
							<td>
								<?= !empty($projectTask->responsible_employee1) ? "<span class='tag tag-info'>{$projectTask->responsible_employee1->name}</span>" : '' ?>
							</td>
							<td>
								<?= !empty($projectTask->responsible_employee2) ? "<span class='tag tag-info'>{$projectTask->responsible_employee2->name}</span>" : '' ?>
							</td>
							<td class="actions">
								<?= $this->Html->link('Assign',['controller'=>'ProjectTasks','action'=>'assign',$projectTask->id],['class'=>'btn btn-primary btn-sm']); ?>
                                <?php
                                    if(empty($projectTask->done_datetime)) {
                                        echo $this->Html->link('Done',['controller'=>'ProjectTasks','action'=>'done',$projectTask->id],['class'=>'btn btn-success btn-sm', 'confirm'=>'Are you sure ?']);
                                    }
                                ?>
                                <?= $this->N->editLink(['action' => 'edit', $projectTask->id]); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
