<?php
use Cake\I18n\Time;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="box box-block bg-white">
	<div class="clearfix mb-1">
		<h5 class="float-xs-left"><?= $title_for_layout; ?></h5>
	</div>
    <?php echo $this->Form->create(null, ['class'=>'form-inline','valueSources' => 'query','id'=>'SearchForm']); ?>
        <?php echo $this->Form->input('search',['placeholder'=>'Search','label'=>false]); ?>
        <?php echo $this->Form->button('Search', ['class'=>'btn btn-bordered-info']); ?>
        <?php echo $this->Html->link('Reset', ['action' => 'index'],['class'=>'btn btn-primary']); ?>
        <?php echo $this->Form->end(); ?>
	<script>
		$(document).ready(function(){
			$("#SearchForm").one("submit", function(e) {
				e.preventDefault();
				$.post('projects', $('#SearchForm').serialize()).done(function(data){
					NProgress.start();
					$("#ajaxDiv").empty().append(data);
					NProgress.done();
				});;
			});
		});
	</script>
</div>
<div class="box box-block bg-white">
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
                    <thead>
                        <tr>
							<th scope="col"><?= $this->Paginator->sort('project_no','Project No') ?></th>
							<th scope="col"><?= $this->Paginator->sort('project_date','Project Date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('site_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('client_id') ?></th>
                            <th scope="col">Total Tasks</th>
                            <th scope="col">Assigned Tasks</th>
                            <th scope="col">Completed Tasks</th>
						    <th scope="col" width="15%"><?= $this->Paginator->sort('Actions') ?></th>
				        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($projects as $project) {
                                $assigned = 0;
                                $completed = 0;
                                foreach($project->project_tasks as $task) {
                                    if(!empty($task->done_by)) {
                                        $completed++;
                                    }

                                    if(!empty($task->assigned_to) && empty($task->done_by)) {
                                        $assigned++;
                                    }
                                }
                        ?>
						<tr>
							<td><?= $project->project_no?></td>
							<td><?= $project->project_date?></td>
                            <td><?= $project->site_name?></td>
							<td><?= $project->client->client_name?></td>
                            <td align="center"><strong><?= count($project->project_tasks) ?></strong></td>
                            <td align="center"><strong><?= $assigned ?></strong></td>
                            <td align="center"><strong><?= $completed ?></strong></td>
                            <td class="actions">
								<?= $this->AuthUser->link('Tasks',['controller'=>'Project Tasks','action'=>'index',$project->id],['class'=>'btn btn-success btn-sm']); ?>
                                <?= $this->AuthUser->link('Responsibility sheet',['controller'=>'ProjectTasks','action'=>'print', $project->id],['class'=>'btn btn-success btn-sm', 'target'=>'_blank']); ?>
						    </td>
                        </tr>
                        <?php } ?>
                    </tbody>
				</table>
			</div>
       </div>
	</div>
</div>
<?php echo $this->element('Neptune.pagination'); ?>
