<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ClientTeamsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index($client_id) { //debug($client_id);exit;
		$this->set('title_for_layout', 'ClientTeams');
		
		$client = TableRegistry::get('Clients');
		
		$query = $this->ClientTeams
        ->find('search', ['search' => $this->request->query]); //debug($query->toArray());exit;
        
         $clients = $client->get($client_id); 
         $clientTeams = $query->find('all',['conditions'=>['ClientTeams.client_id'=>$client_id]]); 
         
        $clientTeams=$this->paginate($clientTeams,['order' => [
            'ClientTeams.id' => 'desc'
        ]]); 
        
		$this->set(compact('clientTeams','client_id','clients'));
        $this->set('_serialize', ['clientTeams','client_id','clients']);
    }
    
	public function edit($id = null,$client_id) {
		$this->set(compact('client_id'));
        $Users = TableRegistry::get('Users');			
		$Users->hasOne('ClientTeams', ['foreignKey' => 'id']);

		if(empty($id)) {
			$user = $Users->newEntity(); 
            $this->set('title_for_layout', 'Add ClientTeams Details');
		} else {
			$this->set('title_for_layout', 'Edit ClientTeams Details');
			$user = $Users->get($id, [
				'contain' => ['ClientTeams']
			]);
			$this->set('title_for_layout', 'Edit ClientTeams');
		}
        
        if ($this->request->is(['patch', 'post', 'put'])) {

			
			if ($this->ClientTeams->save2($this->request->getData(), $user)) {
                $this->Flash->success(__('The ClientTeams has been saved.'));
				return $this->redirect(['controller'=>'Clients','action' => 'edit',$client_id]);
            }
            $this->Flash->error(__('The ClientTeams could not be saved. Please, try again.'));
		}

		$this->set(compact('user'));	
	}
	
	public function delete($id=null,$client_id) {
		$this->request->allowMethod(['post','delete']);
		$clientTeams = $this->ClientTeams->get($id,['contain'=>'Users']);
		
		if($this->ClientTeams->delete($clientTeams)) {
			//debug($clientTeams);exit;
			$this->Flash->success('The ClientTeam has been deleted');
			return $this->redirect(['controller'=>'Clients','action' => 'edit',$client->id]);
		} else {
			$this->Flash->error('The ClientTeam could not be deleted try again');
		}
		return $this->redirect(['controller'=>'Clients','action'=>'edit',$client->id]);
	}
	
	public function getClientTeamsByClient($clientId=null) {
		
		$clientTeams=$this->ClientTeams->find('all',['conditions'=>['ClientTeams.client_id'=>$clientId]]);

		$this->set('clientTeams', $clientTeams);
		$this->set('_serialize', ['clientTeams']);
	}

	public function getClientTeam($client_team_id=null) {
		
		$clients = $this->ClientTeams->get($client_team_id,['contain'=>'Users']);

		$this->set('clients', $clients);
		$this->set('_serialize', ['clients']);
	}
}



