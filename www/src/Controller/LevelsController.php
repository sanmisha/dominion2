<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LevelsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function index($project_id) {
        $project = $this->Levels->Projects->get($project_id, ['contain'=>['Clients']]);
		$this->set('title_for_layout', "Levels for {$project->project_with_client}");

        $this->set('project_id', $project_id);

        $query = $this->Levels
        	->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['project_id' => $project_id]);

        $this->paginate = ['order'=>['level'=>'ASC']];
		$this->set('levels', $this->paginate($query));
        $this->set('_serialize', ['levels']);
	}

	public function edit($id=null, $project_id) {
        $project = $this->Levels->Projects->get($project_id, ['contain'=>['Clients']]);
		$this->set('title_for_layout', "Level for {$project->project_with_client}");

		if(empty($id)) {
			$level = $this->Levels->newEntity();
            $level->project_id = $project_id;
		} else {
			$level = $this->Levels->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$level = $this->Levels->patchEntity($level, $this->request->getData());
			if($this->Levels->save($level)) {
				$this->Flash->success('The Level has been saved');
				return $this->redirect(['action'=>'index', $level->project_id]);
			} else {
				$this->Flash->error('The Level could not be saved try again');
			}
		}
		$this->set(compact('level'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$level = $this->Levels->get($id);

		if($this->Levels->delete($level)) {
			$this->Flash->success('The Level has been deleted');
		} else {
			$this->Flash->error('The Level could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}
}





