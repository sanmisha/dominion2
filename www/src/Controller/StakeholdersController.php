<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class StakeholdersController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Stake Holder');

		$query = $this->Stakeholders
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Users']);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('stakeholders', $this->paginate($query));
        $this->set('_serialize', ['stakeholders']);   		
	}


	public function edit($id = null) {
        $Users = TableRegistry::get('Users');			
		$Users->hasOne('Stakeholders', ['foreignKey' => 'id']);

		if(empty($id)) {
            $user = $Users->newEntity(); 
            $this->set('title_for_layout', 'Add Stake Holder Details');
		} else {
			$this->set('title_for_layout', 'Edit Stake Holder Details');
			$user = $Users->get($id, [
				'contain' => ['Roles', 'Stakeholders']
			]);			
		}
        
        if ($this->request->is(['patch', 'post', 'put'])) {
			if ($this->Stakeholders->save2($this->request->getData(), $user)) {
                $this->Flash->success(__('The Stake Holder has been saved.'));
				return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Stake Holder could not be saved. Please, try again.'));
        }

		$this->set(compact('user'));	
    }

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$stakeholders = $this->Stakeholders->get($id);
		
		if($this->Stakeholders->delete($stakeholders)) {
			$this->Flash->success('The Stake Holder has been deleted.');
		} else {
			$this->Flash->error('The Stake Holder could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



