<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class BuildingsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function index($project_id) {
        $project = $this->Buildings->Projects->get($project_id, ['contain'=>['Clients']]);
		$this->set('title_for_layout', "Buildings for {$project->project_with_client}");

        $this->set('project_id', $project_id);

        $query = $this->Buildings
        	->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['project_id' => $project_id]);

        $this->paginate = ['order'=>['building'=>'ASC']];
		$this->set('buildings', $this->paginate($query));
        $this->set('_serialize', ['buildings']);
	}

	public function edit($id=null, $project_id) {
        $project = $this->Buildings->Projects->get($project_id, ['contain'=>['Clients']]);
		$this->set('title_for_layout', "Building for {$project->project_with_client}");

		if(empty($id)) {
			$building = $this->Buildings->newEntity();
            $building->project_id = $project_id;
		} else {
			$building = $this->Buildings->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$building = $this->Buildings->patchEntity($building, $this->request->getData());
			if($this->Buildings->save($building)) {
				$this->Flash->success('The Building has been saved');
				return $this->redirect(['action'=>'index', $building->project_id]);
			} else {
				$this->Flash->error('The Building could not be saved try again');
			}
		}
		$this->set(compact('building'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$building = $this->Buildings->get($id);

		if($this->Buildings->delete($building)) {
			$this->Flash->success('The Building has been deleted');
		} else {
			$this->Flash->error('The Building could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}
}





