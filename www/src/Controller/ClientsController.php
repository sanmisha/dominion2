<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ClientsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Clients');

		$query = $this->Clients
			->find('search', ['search' => $this->request->getQueryParams()]);

        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('clients', $this->paginate($query));
		//$this->set(compact('clients'));
        $this->set('_serialize', ['clients']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Clients');

		if(empty($id)) {
			$clients = $this->Clients->newEntity();
			//$clients['client_no']=$this->Clients->getNo();

		} else {
			$clients = $this->Clients->get($id);

			$ClientTeams = TableRegistry::get('ClientTeams');

			$clientTeams = $ClientTeams->find('all',[
				'conditions'=>['ClientTeams.client_id' => $id],
                'contain'=>['Users']
			]);

			$this->set(compact('clientTeams'));
		}

		if($this->request->is(['patch','post','put'])) {
			//debug($this->request->data);exit;
			$clients = $this->Clients->patchEntity($clients, $this->request->getData());

			if($this->Clients->save($clients)) {
				$this->Flash->success('The Clients has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Clients could not be saved try again');
			}
		}
		$this->set(compact('clients'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$clients = $this->Clients->get($id);

		if($this->Clients->delete($clients)) {
			$this->Flash->success('The Clients has been deleted');
		} else {
			$this->Flash->error('The Clients could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}



}



