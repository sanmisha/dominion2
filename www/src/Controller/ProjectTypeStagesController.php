<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ProjectTypeStagesController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'ProjectTypeStages');

		$query = $this->ProjectTypeStages
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('projecttypestages', $this->paginate($query));
        $this->set('_serialize', ['projecttypestages']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'ProjectTypeStages');

		if(empty($id)) {
			$projecttypestages = $this->ProjectTypeStages->newEntity();
		} else {
			$projecttypestages = $this->ProjectTypeStages->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$projecttypestages = $this->ProjectTypeStages->patchEntity($projecttypestages, $this->request->getData());	
			if($this->ProjectTypeStages->save($projecttypestages)) {
				$this->Flash->success('The ProjectTypeStages has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The ProjectTypeStages could not be saved try again');
			}
		}
		$this->set(compact('projecttypestages'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$projecttypestages = $this->ProjectTypeStages->get($id);
		
		if($this->ProjectTypeStages->delete($projecttypestages)) {
			$this->Flash->success('The ProjectTypeStages has been deleted');
		} else {
			$this->Flash->error('The ProjectTypeStages could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



