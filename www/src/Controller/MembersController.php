<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;

class MembersController extends AppController
{
	public function initialize() {	
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}
	
	public function index()
	{
		$this->set('title_for_layout', 'Members');
		
		$query = $this->Members
        ->find('search', ['search' => $this->request->getQueryParams()])
        ->contain(['Users']);
		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('members', $this->paginate($query));
        $this->set('_serialize', ['members']);   		
	}

    public function edit($id = null) {
        $Users = TableRegistry::get('Users');			
		$Users->hasOne('Members', ['foreignKey' => 'id']);
        if(empty($id)) {
			$user = $Users->newEntity(); 
            $this->set('title_for_layout', 'Add Member Details');
		} else {
			$this->set('title_for_layout', 'Edit Member Details');
			$user = $Users->get($id, [
				'contain' => ['Roles', 'Members']
			]);
			$this->set('title_for_layout', 'Edit Members');
		}
        
        if ($this->request->is(['patch', 'post', 'put'])) {
			if ($this->Members->save2($this->request->getData(), $user)) {
                $this->Flash->success(__('The Member has been saved.'));
				return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Member could not be saved. Please, try again.'));
        }
		$roles=$Users->Roles->find('list');

		$Country = TableRegistry::get('Countries');
		$countries = $Country->find('list');

        $this->set(compact('user', 'roles','countries'));
    }

    public function delete($id = null)
    {
		$this->request->allowMethod(['post', 'delete']);
		$this->Members->hasOne('Users',['foreignKey' => 'id', 'dependent'=>true]);		
		$member = $this->Members->get($id,['contain'=>'Users']);

        if ($this->Members->delete($member)) {
            $this->Flash->success(__('The Member has been deleted.'));
        } else {
            $this->Flash->error(__('The Member could not be deleted. Please, try again.'));
        }
		return $this->redirect(['action' => 'index']);
    }
    
    public function getAddress($id=null) {
		
		$DeliveryAddress = TableRegistry::get('DeliveryAddresses');
		$deliveryAddress=$DeliveryAddress->find('all',[
			'conditions'=>['DeliveryAddresses.member_id'=>$id]
		])->toArray();
		
		$this->set('deliveryAddress', $deliveryAddress);
		$this->set('_serialize', ['deliveryAddress']);
	}

    public function sudo($id=null) {
		$this->Members->Users->hasOne('Teams', [
            'foreignKey'=>'id'
        ]);
		$user = $this->Members->Users->get($id, [
			'contain'=>['Roles', 'Teams', 'Members'=>['DeliveryAddresses']]
		]);
		$this->Auth->setUser($user->toArray());
		return $this->redirect("/");
	}
		
	public function getStates($country_id=null) {		
		$State = TableRegistry::get('States');
		$states=$State->find('list', ['conditions'=>['States.country_id'=>$country_id]]);
		$this->set('states', $states);
		$this->set('_serialize', ['states']);
	}
}
