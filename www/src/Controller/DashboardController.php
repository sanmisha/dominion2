<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Utility\Hash;

class DashboardController extends AppController
{

    public function initialize() {
        parent::initialize();
    }
	
    public function index() {
        /*
        DELETE FROM associates WHERE id NOT IN (1, 17 , 18);
        UPDATE associates SET left_id = NULL, right_id = NULL;
        DELETE FROM teams WHERE associate_id NOT IN (SELECT id FROM associates);
        DELETE FROM users WHERE id NOT IN (SELECT id FROM teams);
        DELETE FROM roles_users WHERE user_id NOT IN (SELECT id FROM users);
        */

        $Announcements = TableRegistry::get('Announcements');
        $announcement = $Announcements->find()
            ->order(['id'=>'DESC'])
            ->first();
        $this->set(compact('announcement'));
       
    }  

}
