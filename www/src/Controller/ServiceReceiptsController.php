<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class ServiceReceiptsController extends AppController
{
    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}


    public function index()
    {
        $this->set('title', 'Service Receipts');


		$query = $this->ServiceReceipts
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Clients', 'ServiceReceiptInvoices'=>['ServiceInvoices'] ]);

		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('serviceReceipts', $this->paginate($query));
        $this->set('_serialize', ['serviceReceipts']);

    }

    public function edit($id = null)
    {
        if(empty($id)){
            $this->set('title', ' Add Service Receipts');
            $serviceReceipt = $this->ServiceReceipts->newEntity();
        } else {
            $this->set('title', ' Edit Service Receipts');
            $serviceReceipt = $this->ServiceReceipts->get($id, [
                'contain' => ['ServiceReceiptInvoices'=>['ServiceInvoices'], 'Clients'],
            ]);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            foreach($data['service_receipt_invoices'] as $i => $invoice) {
                if(empty($invoice['total_received_amount'])) {
                    unset($data['service_receipt_invoices'][$i]);
                }
            }

            $serviceReceipt = $this->ServiceReceipts->patchEntity($serviceReceipt, $data);
            $serviceReceipt->amount_in_words = $this->amountToWords($serviceReceipt->amount);
            if($serviceReceipt->isNew()){
                $serviceReceipt->receipt_no = $this->ServiceReceipts->getNo($serviceReceipt->receipt_date);
            }
            if ($this->ServiceReceipts->save($serviceReceipt)) {
                $ServiceInvoices = TableRegistry::get('ServiceInvoices');
                $ServiceReceiptInvoices = TableRegistry::get('ServiceReceiptInvoices');

                foreach($serviceReceipt->service_receipt_invoices as $receipts){
                    $query = $ServiceReceiptInvoices->find();
                    $query->select([
                        'total_received_amount' => $query->func()->sum('total_received_amount'),
                        'service_invoice_id'
                    ])
                    ->group('service_invoice_id')
                    ->having(['service_invoice_id' => $receipts->service_invoice_id]);

                    $invoice = $ServiceInvoices->get($receipts->service_invoice_id);
                    $invoice->received_amount = $query->first()->total_received_amount;
                    $ServiceInvoices->save($invoice);
                }

                $this->Flash->success(__('The service receipt has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service receipt could not be saved. Please, try again.'));
        }

        if(empty($serviceReceipt['service_receipt_invoices'])) {
			$serviceReceipt['service_receipt_invoices']= [];
		}


        $clients = $this->ServiceReceipts->Clients->find('list', ['limit' => 200]);
        $this->set(compact('serviceReceipt', 'clients'));
    }


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $serviceReceipt = $this->ServiceReceipts->get($id, ['contain'=>['ServiceReceiptInvoices']]);
        $ServiceInvoices = TableRegistry::get('ServiceInvoices');

        if ($this->ServiceReceipts->delete($serviceReceipt)) {
            $ServiceInvoices = TableRegistry::get('ServiceInvoices');
            $ServiceReceiptInvoices = TableRegistry::get('ServiceReceiptInvoices');

            foreach($serviceReceipt->service_receipt_invoices as $receipts){
                $query = $ServiceReceiptInvoices->find();
                $query->select([
                    'total_received_amount' => $query->func()->sum('total_received_amount'),
                    'service_invoice_id'
                ])
                ->group('service_invoice_id')
                ->having(['service_invoice_id' => $receipts->service_invoice_id]);

                $invoice = $ServiceInvoices->get($receipts->service_invoice_id);
                if($query->count() == 0) {
                    $invoice->received_amount = 0;
                } else {
                    $invoice->received_amount = $query->first()->total_received_amount;
                }

                $ServiceInvoices->save($invoice);
            }

            $this->Flash->success(__('The service receipt has been deleted.'));
        } else {
            $this->Flash->error(__('The service receipt could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function print($id = null)
    {
        $serviceReceipt = $this->ServiceReceipts->get($id, ['contain' => ['ServiceReceiptInvoices'=>['ServiceInvoices'=>['Projects']], 'Clients']]);

        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => false,
                'filename' => "ServiceReceipt-{$serviceReceipt->id}.pdf",               
            ]
        ]);

        $this->set(compact('serviceReceipt'));

    }
}
