<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;

class InwardOutwardsController extends AppController
{
	public function initialize() {
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Inward Outward');

		if($this->Auth->user('id') == 1 ) {
			$query = $this->InwardOutwards
               ->find('search', ['search' => $this->request->getQueryParams()]);
		} else {
			$query = $this->InwardOutwards
              ->find('search', ['search' => $this->request->getQueryParams()])
              ->where(['InwardOutwards.assigned_to' =>$this->Auth->user('id')]);
		}
		$this->paginate = ['contain'=>['Projects'=>['Clients'], 'Locations', 'FileNos', 'AssignedEmployee'],'order'=>['InwardOutwards.id'=>'DESC']];

		$this->set('inwardOutwards', $this->paginate($query));
        $this->set('_serialize', ['inwardOutwards']);
    }

	public function edit($id=null) {
		$this->set('title_for_layout', 'Inward Outward');

		if(empty($id)) {
			$inwardOutward = $this->InwardOutwards->newEntity();
		} else {
			$inwardOutward = $this->InwardOutwards->get($id, [
                'contain' => ['Locations', 'FileNos', 'AssignedEmployee']
            ]);
		}

		if($this->request->is(['patch','post','put'])) {
			$inwardOutward = $this->InwardOutwards->patchEntity($inwardOutward, $this->request->getData());
            $inwardOutward->status = $inwardOutward->type;

            if($inwardOutward->isNew()) {
                $inwardOutward->register_no = $this->InwardOutwards->getNo($inwardOutward->type);
            }

            if($this->InwardOutwards->save($inwardOutward)) {
				$this->Flash->success('The Inward Outward has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Inward Outward could not be saved try again');
			}
		}
        $projects = $this->InwardOutwards->Projects->find('list')->contain(['Clients']);
        $this->set(compact('inwardOutward', 'projects'));
	}

	public function assign($id=null) {
		$this->set('title_for_layout', 'Inward Outward');

		if(empty($id)) {
			$inwardOutward = $this->InwardOutwards->newEntity();
		} else {
            $inwardOutward = $this->InwardOutwards->get($id, [
                'contain' => ['Locations', 'FileNos', 'AssignedEmployee']
            ]);
		}

		if($this->request->is(['patch','post','put'])) {
			$inwardOutward = $this->InwardOutwards->patchEntity($inwardOutward, $this->request->getData());
            $inwardOutward->status = "Assigned";
            $inwardOutward->assigned_on = date('Y-m-d H:i:s');

            if($this->InwardOutwards->save($inwardOutward)) {
                if(isset($newTask)) {
                    $this->InwardOutwards->save($newTask);
                }
				$this->Flash->success('The Inward Outward has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Inward Outward could not be saved try again');
			}
		}
        $employees = $this->InwardOutwards->AssignedEmployee->find('list');
        $this->set(compact('inwardOutward', 'employees'));
	}

	public function archive($id=null) {
		$this->set('title_for_layout', 'Inward Outward');

		if(empty($id)) {
			$inwardOutward = $this->InwardOutwards->newEntity();
		} else {
            $inwardOutward = $this->InwardOutwards->get($id, [
                'contain' => ['Locations', 'FileNos', 'AssignedEmployee']
            ]);
		}

		if($this->request->is(['patch','post','put'])) {
			$inwardOutward = $this->InwardOutwards->patchEntity($inwardOutward, $this->request->getData());
            $inwardOutward->status = "Archive";

            if($this->InwardOutwards->save($inwardOutward)) {
                if(isset($newTask)) {
                    $this->InwardOutwards->save($newTask);
                }
				$this->Flash->success('The Inward Outward has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Inward Outward could not be saved try again');
			}
		}
        $employees = $this->InwardOutwards->AssignedEmployee->find('list');
        $locations = $this->InwardOutwards->Locations->find('list');
        $fileNos = $this->InwardOutwards->FileNos->find('list');
        $this->set(compact('inwardOutward', 'employees', 'locations', 'fileNos'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);

        $inwardOutward = $this->InwardOutwards->get($id);

        if($this->InwardOutwards->delete($inwardOutward)) {
			$this->Flash->success('The Inward Outward has been deleted');
		} else {
			$this->Flash->error('The Inward Outward could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}
}



