<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;

class DocumentDrivesController extends AppController
{
	public function initialize() {
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function projects() {
		$this->set('title_for_layout', 'Projects');

        $query = $this->DocumentDrives->Projects
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['Projects.status' => 'Converted']);

        $this->paginate = ['contain'=>['Clients']];

		$this->set('projects', $this->paginate($query));
        $this->set('_serialize', ['projects']);
    }

	public function index($project_id) {
        $project = $this->DocumentDrives->Projects->get($project_id,[
            'contain'=>['Clients']
        ]);
        $this->set('title_for_layout', 'Document Drives - ' . $project->project_with_client);

        $query = $this->DocumentDrives
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['DocumentDrives.project_id'=>$project_id, 'latest'=>true]);

        $this->paginate = ['contain'=>['DrawingNos', 'Levels', 'Buildings', 'UploadedBy'],'order'=>['DocumentDrives.id'=>'DESC']];

		$this->set('documentDrives', $this->paginate($query));
        $this->set('project_id', $project_id);
        $this->set('_serialize', ['documentDrives']);
    }

	public function print($project_id) {
        $project = $this->DocumentDrives->Projects->get($project_id,[
            'contain'=>['Clients']
        ]);

        $this->set('project', $project);

        $documents = $this->DocumentDrives->find()
            ->where(['DocumentDrives.project_id'=>$project_id, 'latest'=>true])
            ->contain(['DrawingNos', 'Levels', 'Buildings', 'UploadedBy']);


        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => false,
                'filename' => 'Document-Drives - ' . $project->project_with_client . '.pdf',
            ]
        ]);

		$this->set('documents', $documents);
    }

	public function edit($project_id=null) {
        $project = $this->DocumentDrives->Projects->get($project_id,[
            'contain'=>['Clients']
        ]);
		$this->set('title_for_layout', 'Document Drives - ' . $project->project_with_client);

		if(empty($id)) {
			$document = $this->DocumentDrives->newEntity();
            $document->project_id = $project_id;
		} else {
			$document = $this->DocumentDrives->get($id, [
                'contain' => ['Projects'=>['Clients'], 'DrawingNos', 'UploadedBy']
            ]);
		}
       

		if($this->request->is(['patch','post','put'])) {
			$document = $this->DocumentDrives->patchEntity($document, $this->request->getData());

            $count = $this->DocumentDrives->find()
                ->where([
                    'project_id' => $document->project_id,
                    'drawing_no_id' => $document->drawing_no_id,
                    'level_id' => $document->level_id,
                    'building_id' => $document->building_id,
                ])->count();

            if($count == 0) {
                if($this->DocumentDrives->save($document)) {
                    $this->Flash->success('The Document has been saved');
                    return $this->redirect(['action'=>'index', $document->project_id]);
                } else {
                    $this->Flash->error('The Document could not be saved try again');
                }
            } else {
                $this->Flash->error('This Document is already Exists. Please revise Existing Document');
            }
		}

        $drawingNos = $this->DocumentDrives->DrawingNos->find('list');   
        $levels = $this->DocumentDrives->Levels->find('list')->where(['project_id' => $project_id]);
        $buildings = $this->DocumentDrives->Buildings->find('list')->where(['project_id' => $project_id]);
        $this->set(compact('document', 'project', 'drawingNos', 'levels', 'buildings'));
	}

	public function revise($id=null) {
        $oldDocument = $this->DocumentDrives->get($id, [
            'contain' => ['Projects'=>['Clients'], 'DrawingNos', 'Levels', 'Buildings', 'UploadedBy']
        ]);

        $document = $this->DocumentDrives->newEntity();
        $document->project_id = $oldDocument->project_id;
        $document->drawing_no_id = $oldDocument->drawing_no_id;
        $document->level_id = $oldDocument->level_id;
        $document->building_id = $oldDocument->building_id;
        $document->version = $oldDocument->version + 1;

		$this->set('title_for_layout', 'Revise Document for - ' . $oldDocument->project->project_with_client);

		if($this->request->is(['patch','post','put'])) {
			$document = $this->DocumentDrives->patchEntity($document, $this->request->getData());

            $oldDocument->latest = false;

            if($this->DocumentDrives->save($document) && $this->DocumentDrives->save($oldDocument)) {
				$this->Flash->success('The Document has been saved');
				return $this->redirect(['action'=>'index', $document->project_id]);
			} else {
				$this->Flash->error('The Document could not be saved try again');
			}
		}

        $projects = $this->DocumentDrives->Projects->find('list')->contain(['Clients']);
        $drawingNos = $this->DocumentDrives->DrawingNos->find('list');
        $this->set(compact('document', 'oldDocument', 'projects', 'drawingNos'));
	}

	public function history($document_id) {
		$this->set('title_for_layout', 'History');

        $document = $this->DocumentDrives->get($document_id);

        $documents = $this->DocumentDrives->find()
            ->where([
                'project_id'        => $document->project_id,
                'drawing_no_id'    => $document->drawing_no_id
            ])
            ->order(['DocumentDrives.id'=>'DESC'])
            ->contain(['Projects'=>['Clients'], 'DrawingNos', 'UploadedBy']);

		$this->set(compact('documents'));
        $this->set('_serialize', ['documents']);
    }
}



