<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class InvoicesController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Invoices');

		$query = $this->Invoices
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Projects'=>['Clients'],'InvoiceDetails'=>['Tasks']]);

		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('invoices', $this->paginate($query));
        $this->set('_serialize', ['invoices']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Invoices');

		if(empty($id)) {
			$invoice = $this->Invoices->newEntity();
		} else {
			$invoice = $this->Invoices->get($id,['contain' => ['InvoiceDetails'=>['ProjectTasks'=>['Tasks']]]]);
            // debug($invoice); exit;
		}

		if($this->request->is(['patch','post','put'])) {
			$invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());
            $invoice->amount_before_tax = 0;
            $invoice->cgst = 0;
            $invoice->sgst = 0;
            $invoice->igst = 0;

            foreach($invoice->invoice_details as $detail) {
                $invoice->amount_before_tax += ($detail->fees - $detail->dis_amt);
                $invoice->cgst += $detail->cgst_amount;
                $invoice->sgst += $detail->sgst_amount;
                $invoice->igst += $detail->igst_amount;
            }
            $invoice->total_amount = round($invoice->total_amount);

            if($this->Invoices->save($invoice)) {
                $this->generateInvoice($invoice->id);
				$this->Flash->success('The Invoice has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Invoice could not be saved try again');
			}
		}

		if(empty($invoice['invoice_details'])) {
			$invoice['invoice_details']= [];
		}


		$projects = $this->Invoices->Projects->find('list')->contain(['Clients']);
		$this->set(compact('invoice','projects'));
	}

	public function generateInvoice($invoiceId) {
        $invoice = $this->Invoices->get($invoiceId, ['contain' => ['Projects'=>['Clients' ,'ClientTeams'], 'InvoiceDetails'=>['ProjectTasks'=>['Tasks']]]]);
        // debug($invoice); exit;

        $CakePdf = new \CakePdf\Pdf\CakePdf();
		$CakePdf->template('invoice', 'default');

        $this->viewVars = ['invoice'=>$invoice];
		$this->viewBuilder()->setLayout('pdf');

		$CakePdf->viewVars($this->viewVars);
		$pdf = $CakePdf->output();
		$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'projects' . DS . 'invoices' . DS . 'invoice-' . $invoiceId . '.pdf');
	}

    public function approve($invoiceId) {
        $invoice = $this->Invoices->get($invoiceId);

        $invoice->invoice_no = $this->Invoices->getNo($invoice->invoice_date);
        $invoice->amount_in_words = $this->amountToWords($invoice->total_amount);
        $invoice->is_draft = false;
        $this->Invoices->save($invoice);
        $this->generateInvoice($invoice->id);
        $this->Flash->success('The Invoice is approved');
        return $this->redirect(['action'=>'index']);
	}

	public function cancel($id){
        $invoice = $this->Invoices->get($id);
        $invoice->is_cancel = true;
       $this->Invoices->save($invoice);
        $this->Flash->success(__('The service invoice has been cancel.'));
        return $this->redirect(['action'=>'index']);

    }

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$invoice = $this->Invoices->get($id);

		if($this->Invoices->delete($invoice)) {
			$this->Flash->success('The Invoice has been deleted');
		} else {
			$this->Flash->error('The Invoice could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	 public function getPendingInvoices($client_id = null){
        $invoices = $this->Invoices
            ->find()
            ->where([
                'Projects.client_id' => $client_id,
                'Invoices.received_amount < Invoices.total_amount',
				'is_cancel' => false,
                'is_draft' => false
            ])
			->contain(['Projects']);

        $this->set('invoices', $invoices);
		$this->set('_serialize', ['invoices']);
    }

}



