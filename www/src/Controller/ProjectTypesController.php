<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;

class ProjectTypesController extends AppController
{
	public function initialize() {
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function index()
	{
		$this->set('title_for_layout', 'Project Types');

		$query = $this->ProjectTypes
        ->find('search', ['search' => $this->request->getQueryParams()]);


        $this->paginate = ['order'=>['ProjectTypes.id'=>'DESC']];

		$this->set('projectTypes', $this->paginate($query));
        $this->set('_serialize', ['projectTypes']);
    }

	 public function edit($id = null) {

        $this->set('title_for_layout', 'Project Types');

		if(empty($id)){
			$projectTypes = $this->ProjectTypes->newEntity();
		} else {
			$projectTypes = $this->ProjectTypes->get($id,
				['contain' => ['ProjectTypeDetails']
			]);
        }


		if($this->request->is(['patch', 'post', 'put'])) {
            //$projectTypes = $this->ProjectTypes->patchEntity($projectTypes, $this->request->getData());
		    if ($this->ProjectTypes->save2($projectTypes, $this->request->getData())) {
				$this->Flash->success(__('The Project Types has been saved.'));
                return $this->redirect(['controller'=>'ProjectTypes' ,'action' => 'index']);
            }
            $this->Flash->error(__('The Project Types could not be saved. Please, try again.'));
        }

        if(empty($projectTypes['project_type_details'])) {
			$projectTypes['project_type_details']= [];
		}

		$task = TableRegistry::get('Tasks');
        $tasks = $task->find('list');

    	$this->set(compact('projectTypes','tasks'));
    }

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$projectTypes=$this->ProjectTypes->get($id);
		if($this->ProjectTypes->delete($projectTypes)) {
			$this->Flash->success('The Project Types has been deleted');
		}
		else {
			$this->Flash->error('The Project Types could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function getProjectTypeStages($ProjectTypeId=null) {
        $Projects = TableRegistry::get('Projects');
		$ProjectTypeDetails = TableRegistry::get('ProjectTypeDetails');
		$data = $ProjectTypeDetails
            ->find()
            ->where(['ProjectTypeDetails.project_type_id'=>$ProjectTypeId])
            ->contain(['Tasks']);

		$this->set('data', $data);
		$this->set('_serialize', 'data');
	}
}



