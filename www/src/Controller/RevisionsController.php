<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class RevisionsController extends AppController
{
	public function initialize() {
		parent::initialize();
	}

	public function edit($projectId) {
		$Projects = TableRegistry::get('Projects');
	 	$project = $Projects->get($projectId,['contain'=>'Clients']);
		$this->set(compact('project'));

		$this->set('title_for_layout', 'Revisions');

		if(empty($id)) {
			$revision = $this->Revisions->newEntity();
		} else {
			$revision = $this->Revisions->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
            $revision = $this->Revisions->patchEntity($revision,$this->request->getData());
			if($this->Revisions->save($revision)) {
                $project->revision++;
                $Projects->save($project);
				$this->Flash->success('The Revisions has been saved');
				return $this->redirect(['controller'=>'Projects','action'=>'index']);
			} else {
				$this->Flash->error('The Revisions could not be saved try again');
			}
		}
		$revisions = $this->Revisions->find('all',['conditions'=>['Revisions.project_id'=>$projectId]]);
		$this->set(compact('revision','revisions'));
	}
}



