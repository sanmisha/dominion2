<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DrawingNosController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Drawing Nos');

		$query = $this->DrawingNos
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('drawingNos', $this->paginate($query));
        $this->set('_serialize', ['drawingNos']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'DrawingNos');

		if(empty($id)) {
			$drawingNo = $this->DrawingNos->newEntity();
		} else {
			$drawingNo = $this->DrawingNos->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$drawingNo = $this->DrawingNos->patchEntity($drawingNo, $this->request->getData());
			if($this->DrawingNos->save($drawingNo)) {
				$this->Flash->success('The Document No has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Document No could not be saved try again');
			}
		}
		$this->set(compact('drawingNo'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$drawingNo = $this->DrawingNos->get($id);

		if($this->DrawingNos->delete($drawingNo)) {
			$this->Flash->success('The Document No has been deleted');
		} else {
			$this->Flash->error('The Document No could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}
}





