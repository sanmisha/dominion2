<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Chronos\Chronos;


class ProjectsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'ongoingProject', 'archivedProject', 'closedProject']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Quotations');

		$query = $this->Projects
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['LeadTypes','ProjectTypes','Clients','ProjectTasks', 'ApprovedUser', 'CreatedUser'])
			->where(['Projects.status' => 'Open']);

        $this->paginate = ['order'=>['quotation_no'=>'DESC']];

		$this->set('projects', $this->paginate($query));
        $this->set('_serialize', ['projects']);
	}

	public function ongoingProject() {
		$this->set('title_for_layout', 'Ongoing Projects');

		$query = $this->Projects
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['LeadTypes','ProjectTypes','Clients','ProjectTasks', 'ApprovedUser', 'CreatedUser', 'ProjectStakeholders'])
			->where(['Projects.status' => 'Converted']);

        $this->paginate = ['order'=>['project_no'=>'DESC']];

		$this->set('projects', $this->paginate($query));
		$this->set('_serialize', ['projects']);
	}

	public function archivedProject(){
		$this->set('title_for_layout', 'Archived Project');

        $query = $this->Projects
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['LeadTypes','ProjectTypes','Clients','ProjectTasks', 'ArchivedByEmployee', 'CreatedUser'])
			->where(['Projects.status' => 'Archived']);

        $this->paginate = ['order'=>['project_no'=>'DESC']];

		$this->set('projects', $this->paginate($query));
		$this->set('_serialize', ['projects']);
	}

	public function closedProject(){
		$this->set('title_for_layout', 'Closed Project');

        $query = $this->Projects
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->contain(['LeadTypes','ProjectTypes','Clients','ProjectTasks', 'ClosedByEmployee', 'CreatedUser'])
            ->where(['Projects.status' => 'Closed']);

        $this->paginate = ['order'=>['project_no'=>'DESC']];

        $this->set('projects', $this->paginate($query));
        $this->set('_serialize', ['projects']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Quotation Details');
		if(empty($id)) {
			$project = $this->Projects->newEntity();
			$project['quotation_no']=$this->Projects->getNo();
		} else {
			$project = $this->Projects->get($id,
			    ['contain' => ['ProjectTasks'=>['sort'=>['ProjectTasks.list_order']],'ClientTeams']]);
		}

		if($this->request->is(['patch','post','put'])) {
            $this->Projects->patchEntity($project, $this->request->getData());

            $projectTasks = [];
            if(isset($this->request->getData()['project_tasks'])) {
                $projectTasks = $this->request->getData()['project_tasks'];
            }

            $percentage = array_sum(Hash::extract($projectTasks, "{n}.percentage"));

            if(($percentage == 100 || $percentage == 0) && !empty($projectTasks)) {
                if($this->Projects->save2($project)) {
                    $this->generateQuotation($project->id);
                    if($this->request->getData()['is_revised']=='Revised') {
                        $this->Flash->success('The Quotations Revsied.');
                        return $this->redirect(['controller'=>'revisions', 'action'=>'edit', $project->id]);
                    } else {
                        $this->Flash->success('The Quotations has been saved');
                        return $this->redirect(['action'=>'index']);
                    }
                } else {
                    $this->Flash->error('The Quotations could not be saved try again');
                }
            } else {
                $this->Flash->error('Please check Fees Distribution');
            }
		} else {
            if(empty($project['project_tasks'])) {
                $project['project_tasks']= [];
            }
        }

		$task = TableRegistry::get('Tasks');
        $tasks = $task->find('list');

		$leadTypes = $this->Projects->LeadTypes->find('list');
		$projectTypes = $this->Projects->ProjectTypes->find('list');
		$clients = $this->Projects->Clients->find('list');
        $employees = $this->Projects->PreparedByEmployee->find('list');
		$this->set(compact('project','projectTypes','leadTypes','clients','tasks', 'employees'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$project = $this->Projects->get($id);

		if($this->Projects->delete($project)) {
			$this->Flash->success('The Quotations has been deleted');
		} else {
			$this->Flash->error('The Quotations could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function get() {
		$projectTypeId = $this->request->query('projectTypeId');

		$ProjectTypeDetails = TableRegistry::get('ProjectTypeDetails');
		$projectTypeDetails = $ProjectTypeDetails->find('all',[
			'conditions'=>['ProjectTypeDetails.project_type_id'=>$projectTypeId],
			'order'=>['ProjectTypeDetails.id'],
			'recursive'=>-1
		]);

		$this->set('projectTypeDetails',$projectTypeDetails);
		$this->set('_serialize', ['projectTypeDetails']);
	}

	public function generateQuotation($project_id) {
		$project = $this->Projects->get($project_id,
			['contain' => ['ProjectTasks'=>['sort'=>['ProjectTasks.list_order']], 'ProjectTasks.Tasks','ProjectTypes','Clients', 'PreparedByEmployee' => ['Designations'], 'Revisions']]);

		$CakePdf = new \CakePdf\Pdf\CakePdf();
		$CakePdf->template('quotation', 'default');
		$this->viewBuilder()->setLayout('pdf');
		$this->viewVars = ['project'=>$project];

		$CakePdf->viewVars($this->viewVars);
		$pdf = $CakePdf->output();
		$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'projects' . DS . 'quotation' . DS . 'quotation-' . str_replace('/', '-', $project->quotation_no) . '.pdf');
	}

	public function generateAgreement($project_id) {
		$project = $this->Projects->get($project_id,
			['contain' => ['ProjectTasks'=>['sort'=>['ProjectTasks.list_order']], 'ProjectTasks.Tasks','ProjectTypes','Clients', 'PreparedByEmployee' => ['Designations'], 'Revisions']]);

        // debug($project->project_tasks); exit;
		$CakePdf = new \CakePdf\Pdf\CakePdf();
		$CakePdf->template('agreement', 'default');
		$this->viewBuilder()->setLayout('pdf');
		$this->viewVars = ['project'=>$project];

		$CakePdf->viewVars($this->viewVars);
		$pdf = $CakePdf->output();
		$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'projects' . DS . 'agreement' . DS . 'agreement-' . str_replace('/', '-', $project->project_no) . '.pdf');
	}

	public function viewQuotation($project_id) {
		$project = $this->Projects->get($project_id);
		$this->viewBuilder()->setClassName('CakePdf.Pdf');
		$this->viewBuilder()->options([
			'pdfConfig' => [
				'download' => false, // This can be omitted if "filename" is specified.
				'filename' => 'quotation-'.$project->project_no.'.pdf', // This can be omitted if you want file name based on URL.
				'pageSize' => 'A4',
				'orientation' => 'portrait'
			]
		]);
		$this->set(compact('project'));
	}

	public function convert($projectId){
		$this->set('title_for_layout', 'Convert');

		$project = $this->Projects->get($projectId,
			['contain' => ['ProjectTasks','ClientTeams','Clients', 'ProjectStakeholders'=>['Stakeholders']]]);
       

		if(!empty($this->request->data)){
			$project['remarks'] =  $this->request->data['remarks'];
            $project['purchase_order_no'] =  $this->request->data['purchase_order_no'];
			$project['purchase_order_img'] =  $this->request->data['purchase_order_img'];
			$project['purchase_order_img_dir'] =  $this->request->data['purchase_order_img_dir'];
			$project['status'] =  "Converted";			
			
			
			$project['approved_datetime'] = date('Y-m-d H:i:s');
			$project['approved_by'] = $this->Auth->user('id');
		}

		if($this->request->is(['patch','post','put'])) {
            $project->project_no = $this->Projects->getProjectNo();
            $project->project_date = date('d/m/Y');
			// debug($this->request->getData()); exit;
			$project = $this->Projects->patchEntity($project, $this->request->getData());
			// debug($project); exit;
			if($this->Projects->save($project)) {
                $this->generateAgreement($project->id);
				$this->Flash->success('The Quotations has been Converted');
				return $this->redirect(['action'=>'ongoing-project']);
			} else {
				$this->Flash->error('The Quotations could not be Convert try again');
			}
		}		
		if(empty($project['project_stakeholders'])) {
			$project['project_stakeholders']= [];
		}
		
		$Stakeholders = TableRegistry::get('stakeholders');
		$stakeholders = $Stakeholders->find('list');
		$this->set(compact('project', 'stakeholders'));
	}
	public function archived($projectId){
		$this->set('title', 'Archived');
		$project = $this->Projects->get($projectId,
			['contain' => ['ProjectTasks','ClientTeams','Clients']]);
			$this->set(compact('project'));
			if(!empty($this->request->data)){
				$project['archive_status'] =  $this->request->data['archive_status'];
				$project['status'] =  "Archived";
				$project['approved_datetime'] = date('Y-m-d H:i:s');
				$project['archive_on'] = date('Y-m-d H:i:s');
				$project['archived_by'] = $this->Auth->user('id');
			}

			if($this->request->is(['patch','post','put'])) {
				if($this->Projects->save($project, $this->request->getData())) {
					$this->Flash->success('The Project has been Archived');
					return $this->redirect(['action'=>'archived-project']);
				} else {
					$this->Flash->error('The Project could not be Archied try again');
				}
			}

			$this->set(compact('project'));

	}
	public function closed($projectId){
		$this->set('title', 'Closed');
		$project = $this->Projects->get($projectId,
			['contain' => ['ProjectTasks','ClientTeams','Clients']]);
			$this->set(compact('project'));

			if(!empty($this->request->data)){
				$project['close_reason'] =  $this->request->data['close_reason'];
				$project['status'] =  "Closed";
				$project['approved_datetime'] = date('Y-m-d H:i:s');
				$project['closed_on'] = date('Y-m-d H:i:s');
				$project['closed_by'] = $this->Auth->user('id');
			}

			if($this->request->is(['patch','post','put'])) {
				if($this->Projects->save($project, $this->request->getData())) {
					$this->Flash->success('The Quotations has been Closed');
					return $this->redirect(['action'=>'closed-project']);
				} else {
					$this->Flash->error('The Quotations could not be Closed try again');
				}
			}

			$this->set(compact('project'));

	}

	public function view($projectId){
		$this->set('title_for_layout', 'View');

		$project = $this->Projects->get($projectId, [
            'contain' => ['ProjectTasks'=>['Tasks'],'ProjectTypes','Clients', 'ProjectStakeholders'=>['Stakeholders']]]);
		$this->set(compact('project'));
	}

    public function genAgree() {
        $projects = $this->Projects->find()->where(['status'=>'Converted']);
        foreach($projects as $project) {
            $this->generateAgreement($project->id);
        }
        echo "done";
        exit;

    }
	public function getProjects($clientId=null) {
		$projects = $this->Projects->find('all')->where(['Projects.client_id' =>$clientId, 'status'=>'Converted']);
		$this->set('projects', $projects);
		$this->set('_serialize', ['projects']);

	}
	public function getStakeholders($id){
		$projects = $this->Projects->get($id,['contain'=>['ProjectStakeholders'=>['Stakeholders']]]);
		$this->set('projects', $projects);
		$this->set('_serialize', ['projects']);

	}
	public function editOngoingProject($id = null){
		$this->set('title_for_layout', 'Edit');

		$project = $this->Projects->get($id,
			['contain' => ['ProjectTasks','ClientTeams','Clients', 'ProjectStakeholders'=>['Stakeholders']]]);
       

		if(!empty($this->request->data)){
			$project['approved_datetime'] = date('Y-m-d H:i:s');
			$project['approved_by'] = $this->Auth->user('id');
		}

		if($this->request->is(['patch','post','put'])) {           
            $project->project_date = date('d/m/Y');
			// debug($this->request->getData()); exit;
			$project = $this->Projects->patchEntity($project, $this->request->getData());
			// debug($project); exit;
			if($this->Projects->save($project)) {             
				$this->Flash->success('The Quotations has been Edited');
				return $this->redirect(['action'=>'ongoing-project']);
			} else {
				$this->Flash->error('The Quotations could not be Edited try again');
			}
		}		
		if(empty($project['project_stakeholders'])) {
			$project['project_stakeholders']= [];
		}
		
		$Stakeholders = TableRegistry::get('stakeholders');
		$stakeholders = $Stakeholders->find('list');
		$this->set(compact('project', 'stakeholders'));

	}
}
