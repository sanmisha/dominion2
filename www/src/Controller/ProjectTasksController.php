<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;

class ProjectTasksController extends AppController
{
	public function initialize() {
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function projects() {
		$this->set('title_for_layout', 'Projects');

        $query = $this->ProjectTasks->Projects
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['Projects.status' => 'Converted']);

        $this->paginate = ['contain'=>['Clients', 'ProjectTasks']];

		$this->set('projects', $this->paginate($query));
        $this->set('_serialize', ['projects']);
    }

	public function print($project_id) {
        $project = $this->ProjectTasks->Projects->get($project_id,[
            'contain'=>['Clients']
        ]);

        $this->set('project', $project);

        $projectTasks = $this->ProjectTasks->find()
            ->where(['ProjectTasks.project_id'=>$project_id])
            ->contain(['Projects'=>['Clients'],'Tasks','AssignedUser','AssignedEmployee', 'ResponsibleEmployee1'=>['Users'], 'ResponsibleEmployee2'=>['Users']]);


        // debug($projectTasks->toArray()); exit;

        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => false,
                'filename' => 'responsibility-sheet.pdf',
                'orientation' => 'landscape'
            ]
        ]);

		$this->set('projectTasks', $projectTasks);
    }


	public function index($project_id) {
        $project = $this->ProjectTasks->Projects->get($project_id,[
            'contain'=>['Clients']
        ]);
        $this->set('title_for_layout', 'Documents - ' . $project->project_with_client);

		if($this->Auth->user('id') == 1 ){
			$query = $this->ProjectTasks
			    ->find('search', ['search' => $this->request->getQueryParams()])
                ->where(['ProjectTasks.project_id' => $project_id]);
		}else{
			$query = $this->ProjectTasks
			    ->find('search', ['search' => $this->request->getQueryParams()])
			    ->where([
                    'ProjectTasks.assigned_to' =>$this->Auth->user('id'),
                    'ProjectTasks.project_id' => $project_id
                ]);
		}
		$this->paginate = ['contain'=>['Projects'=>['Clients'],'Tasks','AssignedUser','AssignedEmployee', 'ResponsibleEmployee1', 'ResponsibleEmployee2'],'order'=>['ProjectTasks.list_order'=>'ASC']];

		$employee = TableRegistry::get('Employees');
		$employees = $employee->find('list');

		$this->set(compact('employees'));
		$this->set('projectTasks', $this->paginate($query));
        $this->set('_serialize', ['projectTasks']);
    }

	public function edit($id) {
		$this->set('title_for_layout', 'Project Tasks');

        $projectTask = $this->ProjectTasks->get($id, ['contain'=>['Projects'=>['Clients']]]);

        if($this->request->is(['patch','post','put'])) {
			$projectTask = $this->ProjectTasks->patchEntity($projectTask, $this->request->getData());

			if($this->ProjectTasks->save($projectTask)) {
				$this->Flash->success('The Project Tasks has been saved');
				return $this->redirect(['action'=>'index', $projectTask->project_id]);
			} else {
				$this->Flash->error('The Project Tasks could not be saved try again');
			}
		}

		$employees = TableRegistry::get('Employees')->find('list');
        $this->set(compact('projectTask', 'employees'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$projectTasks=$this->ProjectTasks->get($id);
		if($this->ProjectTasks->delete($projectTasks)) {
			$this->Flash->success('The Project Tasks has been deleted');
		}
		else {
			$this->Flash->error('The Project Tasks could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function assign($taskId=null) {
		$this->set('title_for_layout', 'Assign');
        $projectTask = $this->ProjectTasks->get($taskId, ['contain'=>['Projects'=>['Clients']]]);
		$employees = TableRegistry::get('Employees')->find('list');
		$this->set(compact('projectTask','employees'));

		$User = TableRegistry::get('Users');
		$users = $User->get($this->Auth->user('id'));

        if ($this->request->is('post') || $this->request->is('put')) {
			$projectTask = $this->ProjectTasks->patchEntity($projectTask, $this->request->getData());
            $projectTask->assigned_by = $users['id'];
            $projectTask->assigned_datetime = date('Y-m-d H:i:s');

            if($this->ProjectTasks->save($projectTask)) {
                $this->Flash->success('The Project Tasks has been Assign');
                return $this->redirect(['action'=>'index', $projectTask->project_id]);
            } else {
                $this->Flash->error('The Project Tasks could not be Assign try again');
            }
        }
    }

	public function done($taskId=null, $page=1) {
        $projectTask = $this->ProjectTasks->get($taskId, ['contain'=>['Projects']]);

        $projectTask->done_by = $this->Auth->user('id');
        $projectTask->done_datetime = date('Y-m-d H:i:s');

        $this->ProjectTasks->save($projectTask);

        $Invoices = TableRegistry::get('Invoices');

        $data['invoice_date'] = date('d/m/Y');
        $data['project_id'] = $projectTask->project_id;
        $data['amount_before_tax'] = $projectTask->fees;

        $data['invoice_details'][0]['project_task_id'] = $projectTask->id;
        $data['invoice_details'][0]['task_id'] = $projectTask->task_id;
        $data['invoice_details'][0]['service'] = $projectTask->service;
        $data['invoice_details'][0]['description'] = $projectTask->description;

        $data['invoice_details'][0]['fees'] = $projectTask->fees;
        $data['invoice_details'][0]['discount'] = 0;
        $data['invoice_details'][0]['dis_amt'] = 0;

        $data['invoice_details'][0]['cgst_rate'] = $projectTask->project->cgst;
        $data['invoice_details'][0]['cgst_amount'] = ($projectTask->fees * $projectTask->project->cgst) / 100;
        $data['invoice_details'][0]['sgst_rate'] = $projectTask->project->sgst;
        $data['invoice_details'][0]['sgst_amount'] = ($projectTask->fees * $projectTask->project->sgst) / 100;
        $data['invoice_details'][0]['igst_rate'] = $projectTask->project->igst;
        $data['invoice_details'][0]['igst_amount'] = ($projectTask->fees * $projectTask->project->igst) / 100;
        $data['invoice_details'][0]['amount'] = round($data['invoice_details'][0]['fees'] + $data['invoice_details'][0]['cgst_amount'] + $data['invoice_details'][0]['sgst_amount'] + $data['invoice_details'][0]['igst_amount']);


        $data['total_amount'] = round($data['invoice_details'][0]['amount']);
        $data['amount_in_words'] = $this->amountToWords($data['total_amount']);


        $invoice = $Invoices->newEntity();
        $invoice = $Invoices->patchEntity($invoice, $data);
        $Invoices->save($invoice);

        $invoicesController =  new \App\Controller\InvoicesController();
        $invoicesController->generateInvoice($invoice->id);

        $this->Flash->success('The Project Tasks has been Done');
        return $this->redirect(['action'=>'index', $projectTask->project_id]);
    }
}
