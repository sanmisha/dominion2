<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CheckListItems Controller
 *
 * @property \App\Model\Table\CheckListItemsTable $CheckListItems
 *
 * @method \App\Model\Entity\CheckListItem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CheckListItemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}
    public function index()
    {
        $this->set('title', 'Checklist Item');
       
        //debug($checkListItems); exit();
        $query = $this->CheckListItems->find('search', ['search' => $this->request->getQueryParams()])->contain('CheckListSections');
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('checkListItems', $this->paginate($query));
        $this->set('_serialize', ['checkListItems']);

    }
    /**
     * Edit method
     *
     * @param string|null $id Check List Item id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Checklist Item');
        if(empty($id)) {
			$checkListItem = $this->CheckListItems->newEntity();
           
		} else {
            $checkListItem = $this->CheckListItems->get($id, [ 'contain' => ['CheckListSections'],]); 
           
        }
         if ($this->request->is(['patch', 'post', 'put'])) {
            $checkListItem = $this->CheckListItems->patchEntity($checkListItem, $this->request->getData());
            if ($this->CheckListItems->save($checkListItem)) {
                $this->Flash->success(__('The check list item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The check list item could not be saved. Please, try again.'));
        }
        $checkListSections = $this->CheckListItems->CheckListSections->find('list');
        $this->set(compact('checkListItem', 'checkListSections'));
        
    }

    /**
     * Delete method
     *
     * @param string|null $id Check List Item id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $checkListItem = $this->CheckListItems->get($id);
        if ($this->CheckListItems->delete($checkListItem)) {
            $this->Flash->success(__('The check list item has been deleted.'));
        } else {
            $this->Flash->error(__('The check list item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
