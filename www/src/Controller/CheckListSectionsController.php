<?php
namespace App\Controller;

use App\Controller\AppController;


class CheckListSectionsController extends AppController
{
 
    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

    public function index()
    {
        $this->set('title','Check List Sections');       

        $query = $this->CheckListSections->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('checkListSections', $this->paginate($query));
        $this->set('_serialize', ['checkListSections']);
    }  
    

   
    public function edit($id = null)
    {
        $this->set('title', 'Checklist Section');
        if(empty($id)) {
			$checkListSection = $this->CheckListSections->newEntity();
		} else {
            $checkListSection = $this->CheckListSections->get($id, ['contain' => [],]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $checkListSection = $this->CheckListSections->patchEntity($checkListSection, $this->request->getData());
            if ($this->CheckListSections->save($checkListSection)) {
                $this->Flash->success(__('The check list section has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The check list section could not be saved. Please, try again.'));
        }
        $this->set(compact('checkListSection'));
    }

    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $checkListSection = $this->CheckListSections->get($id);
        if ($this->CheckListSections->delete($checkListSection)) {
            $this->Flash->success(__('The check list section has been deleted.'));
        } else {
            $this->Flash->error(__('The check list section could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
   

    public function get($id){
        $checkListSection = $this->CheckListSections->get($id, ['contain'=>'CheckListItems']);
        $this->set('checkListSection', $checkListSection);
        $this->set('_serialize', 'checkListSection');    
       
    }
}
