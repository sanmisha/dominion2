<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ExpenseHeadsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Expense Heads');

		$query = $this->ExpenseHeads
        	->find('search', ['search' => $this->request->getQueryParams()]);
			//debug($this->request->getQueryParams());exit;
		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('expense_heads', $this->paginate($query));
        $this->set('_serialize', ['expense_heads']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Expense Heads');

		if(empty($id)) {
			$expense_head = $this->ExpenseHeads->newEntity();
		} else {
			$expense_head = $this->ExpenseHeads->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$expense_head = $this->ExpenseHeads->patchEntity($expense_head, $this->request->getData());	
			if($this->ExpenseHeads->save($expense_head)) {
				$this->Flash->success('The Expense Head has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Expense Head could not be saved try again');
			}
		}
		$this->set(compact('expense_head'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$expense_head = $this->ExpenseHeads->get($id);
		
		if($this->ExpenseHeads->delete($expense_head)) {
			$this->Flash->success('The Expense Head has been deleted');
		} else {
			$this->Flash->error('The Expense Head could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



