<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\I18n\Number;
use Muffin\Footprint\Auth\FootprintAwareTrait;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use FootprintAwareTrait;
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        //parent::initializ();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Tools.MultiColumn' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'columns' => ['email'],
                    'finder' => 'auth'
                ],
            ],
            'loginAction' => [
                'controller' => 'Account',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Dashboard',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Account',
                'action' => 'login'
            ],
            'authorize' => [
                'TinyAuth.Tiny' => [
                    'multiRole'=>true,
                    'superAdminRole'=>1,
                    'pivotTable'=>'roles_users',
                ]
            ],
            'flash' => [
                'element' => 'warning',
                //'key' => 'auth'
            ],
            // If unauthorized, return them to page they were just on
            //'unauthorizedRedirect' => $this->referer(),
            'unauthorizedRedirect' => true,
            'authError' => 'Please login again with your login details.',
            'checkAuthIn'=> 'Controller.initialize'
        ]);

        $this->loadComponent('TinyAuth.AuthUser',[
            'multiRole'=>true,
            'superAdminRole'=>1,
            'pivotTable'=>'roles_users'
        ]);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function beforeRender(\Cake\Event\Event $event) {
        $this->viewBuilder()->setTheme('Neptune');
    }

    public function amountToWords($no, $currency='INR', $formatString = '%UNIT% %WHOLE% and %FRACTION% %SUBUNIT% %ONLY%', $option = 0) {
        if ($no == 0)
            return "Zero";
        $words = $formatString;
        $whole = '';
        $fraction = '';

        $wholeInt = floor($no);
        $whole = ($wholeInt == 0) ? 'Zero' : $this->words_big($wholeInt, $option);
		$fracInt = (int) (($no - $wholeInt) * 100) % 100;
        $fraction = ($fracInt == 0) ? 'Zero' : $this->words_tens($fracInt);

        // TBD: Lookup unit, subunit for $currency & replace values below
        $curr_unit = 'Rupees';
        $curr_subunit = 'paise';
        $last='only';

        $words = str_replace('%WHOLE%', $whole, $words);
        $words = str_replace('%FRACTION%', $fraction, $words);
        $words = str_replace('%UNIT%', $curr_unit, $words);
        $words = str_replace('%SUBUNIT%', $curr_subunit, $words);
        $words=str_replace('%ONLY%',$last,$words);
        return ucfirst($words);
    }

    protected function words_big($no, $option) {
        $descSingular = $option == 0 ? array("Thousand", "Lakh", "Crore", "Thousand Crore") : array("Thousand", "Million", "Billion", "Trillion");
        $descPlural = $option == 0 ? array("Thousand", "Lakhs", "Crores", "Thousand Crores") : array("Thousand", "Million", "Billion", "Trillion");
        $compare = $option == 0 ? array(1000, 100000, 10000000, 10000000000) : array(1000, 1000000, 1000000000, 1000000000000);

        $divide = count($descSingular);
        $loop = $divide;
        $words = '';

        /* handle upto thousand */
        for ($i = $loop - 1; $i >= 0; $i--) {
            $nos = ($i == $loop - 1) ? $no : $no % $compare[$i + 1];
            $split = (int) floor($nos / $compare[$i]);

            if ($split > 0) {
                if ($split < 100)
                    $words .= ' ' . $this->words_tens($split) . ' ' . ($split == 1 ? $descSingular[$i] : $descPlural[$i]);
                if ($split >= 100 && $split < 1000)
                    $words .= ' ' . $this->words_hundreds($split) . ' ' . $descPlural[$i];
                else if ($split >= 1000)
                    $words .= ' ' . $this->words_big($split, $option) . ' ' . $descPlural[$i];
            }
            if ($i == 0) {
                $hundred = (int) $no % 1000;
                if ($hundred > 0 && $hundred < 999)
                    $words .= ' ' . $this->words_hundreds($hundred);
            }
        }
        return trim($words);
    }

    protected function words_hundreds($no) {
        $words = '';
        if ($no > 999)
            throw new Exception("Invalid call to hundreds function");
        $hundreds = 0;
        if ($no > 99) {
            $hundreds = floor($no / 100);
            $words = $this->words_units($hundreds) . ' ' . "Hundred";
        }
        $words .= ' ' . $this->words_tens($no - (100 * $hundreds));
        return trim($words);
    }

    protected function words_tens($no) {
        $words = '';
        if ($no > 99)
            throw new Exception("Invalid call to Tens function");
        $tens = floor($no / 10);
        if ($tens > 1) {
            switch ($tens) {
                case 2:
                    $words = "Twenty";
                    break;
                case 3:
                    $words = "Thirty";
                    break;
                case 4:
                    $words = "Forty";
                    break;
                case 5:
                    $words = "Fifty";
                    break;
                case 6:
                    $words = "Sixty";
                    break;
                case 7:
                    $words = "Seventy";
                    break;
                case 8:
                    $words = "Eighty";
                    break;
                case 9:
                    $words = "Ninety";
                    break;
                default:
                    break;
            }
            $words .= ' ' . $this->words_units($no - $tens * 10);
        }
        else
            $words = $this->words_units($no);
        return trim($words);
    }

    protected function words_units($no) {
        $words = '';
        if ($no > 19)
            throw new Exception("Invalid call to Units function");
        switch ($no) {
            case 1:
                $words = "One";
                break;
            case 2:
                $words = "Two";
                break;
            case 3:
                $words = "Three";
                break;
            case 4:
                $words = "Four";
                break;
            case 5:
                $words = "Five";
                break;
            case 6:
                $words = "Six";
                break;
            case 7:
                $words = "Seven";
                break;
            case 8:
                $words = "Eight";
                break;
            case 9:
                $words = "Nine";
                break;
            case 10:
                $words = "Ten";
                break;
            case 11:
                $words = "Eleven";
                break;
            case 12:
                $words = "Twelve";
                break;
            case 13:
                $words = "Thirteen";
                break;
            case 14:
                $words = "Fourteen";
                break;
            case 15:
                $words = "Fifteen";
                break;
            case 16:
                $words = "Sixteen";
                break;
            case 17:
                $words = "Seventeen";
                break;
            case 18:
                $words = "Eighteen";
                break;
            case 19:
                $words = "Nineteen";
                break;
            default:
                break;
        }
        return $words;
    }
}
