<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class MeetingReportsController extends AppController
{
    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
   
    public function index()
    {
       $this->set('title', 'Meeting Reports');
       $query = $this->MeetingReports
        ->find('search',['search'=>$this->request->getQueryParams()])
        ->contain(['Employees', 'Projects'=>['Clients'],'MeetingReportImages', 'MeetingReportStakeholders'=>['Stakeholders'], 'MeetingReportVisitors']);
        $this->paginate = ['order'=>['id'=>'DESC']];
        $this->set('meetingReports', $this->paginate($query));
        $this->set('_serialize', ['meetingReport']);
        
    }

    public function edit($id = null)
    {
        $this->set('title', 'Meeting Reports');
        if(empty($id)){
            $meetingReport = $this->MeetingReports->newEntity();
            $meetingReport->meeting_date = date('d/m/Y');
        } else{
            $meetingReport = $this->MeetingReports->get($id, [
                'contain' => ['Employees', 'Projects'=>['Clients'],'MeetingReportImages', 'MeetingReportStakeholders'=>['Stakeholders'], 'MeetingReportVisitors']]);

        }
       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();	
            $meetingReport = $this->MeetingReports->patchEntity($meetingReport, $this->request->getData());
            if($meetingReport->isNew()){
                $project = $this->MeetingReports->Projects->get($meetingReport->project_id);				
				$meetingReport->meeting_no = $this->MeetingReports->getNo($meetingReport->project_id, $project->project_no);

            }
            if(!empty($meetingReport->meeting_time)){					
                $meetingReport->meeting_time = Time::createFromFormat('Y-m-d H:i a', date('Y-m-d').' '.$data['meeting_time']);
                } else {
                    $this->Flash->error('Visit Time is require');
                }    
            if ($this->MeetingReports->save($meetingReport)) {
                $this->generateMeetingReport($meetingReport->id);
                $this->Flash->success(__('The meeting report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The meeting report could not be saved. Please, try again.'));
        }
        if(empty($meetingReport['meeting_report_stakeholders'])){
            $meetingReport['meeting_report_stakeholders'] = [];
        }
        if(empty($meetingReport['meeting_report_images'])){
            $meetingReport['meeting_report_images'] = [];
        }
        if(empty($meetingReport['meeting_report_visitors'])){
            $meetingReport['meeting_report_visitors'] = [];
        }
        $Stakeholders = TableRegistry::get('stakeholders');
		$stakeholders = $Stakeholders->find('list');
        $employees = $this->MeetingReports->Employees->find('list');
        $projects = $this->MeetingReports->Projects->find('list', ['keyField'=>'id','valueField'=>'project_with_client'])->where(['status'=>'Converted'])->contain('Clients');
        $this->set(compact('meetingReport', 'employees', 'projects', 'stakeholders'));
    }
  
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingReport = $this->MeetingReports->get($id);
        if ($this->MeetingReports->delete($meetingReport)) {
            $this->Flash->success(__('The meeting report has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function generateMeetingReport($id) {
        $meetingReport = $this->MeetingReports->get($id, [
            'contain' => ['Employees', 'Projects'=>['Clients'],'MeetingReportImages', 'MeetingReportStakeholders'=>['Stakeholders'], 'MeetingReportVisitors']]);

        $CakePdf = new \CakePdf\Pdf\CakePdf();
		$CakePdf->template('meeting_report', 'default');
        // debug($meetingReport); exit;

        $this->viewVars = ['meetingReport'=>$meetingReport];
		$this->viewBuilder()->setLayout('pdf');
      

		$CakePdf->viewVars($this->viewVars);      
		$pdf = $CakePdf->output();
       	$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'meeting_report'  . DS . 'meeting_report-' . $meetingReport->meeting_no . '.pdf');
	}
}
