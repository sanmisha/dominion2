<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class TasksController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Tasks');

		$query = $this->Tasks
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('tasks', $this->paginate($query));
        $this->set('_serialize', ['tasks']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Tasks');

		if(empty($id)) {
			$tasks = $this->Tasks->newEntity();
		} else {
			$tasks = $this->Tasks->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$tasks = $this->Tasks->patchEntity($tasks, $this->request->getData());
			if($this->Tasks->save($tasks)) {
				$this->Flash->success('The Tasks has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Tasks could not be saved try again');
			}
		}
		$this->set(compact('tasks'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$task = $this->Tasks->get($id);

		if($this->Tasks->delete($task)) {
			$this->Flash->success('The Task has been deleted');
		} else {
			$this->Flash->error('The Task could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function get($id) {
		$task = $this->Tasks->get($id);
		$this->set('task', $task);
		$this->set('_serialize', 'task');
	}
}





