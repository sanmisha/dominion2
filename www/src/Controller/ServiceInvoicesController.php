<?php
namespace App\Controller;

use App\Controller\AppController;


class ServiceInvoicesController extends AppController
{

    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title', 'Service Invoices');

		$query = $this->ServiceInvoices
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Clients', 'ServiceInvoiceDetails'=>['Tasks'], 'Projects'=>['Clients']]);

		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('serviceInvoices', $this->paginate($query));
        $this->set('_serialize', ['serviceInvoices']);
	}

    public function edit($id = null)
    {
        if(empty($id)){
            $this->set('title', ' Add Service Invoice');
            $serviceInvoice = $this->ServiceInvoices->newEntity();
        } else {
            $this->set('title', 'Edit Service Invoice');
            $serviceInvoice = $this->ServiceInvoices->get($id, [
                'contain' => ['ServiceInvoiceDetails'=>'Tasks',  'Projects'=>'Clients'],
            ]);

        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $serviceInvoice = $this->ServiceInvoices->patchEntity($serviceInvoice, $this->request->getData());
           // debug($serviceInvoice->toArray()); exit();
            $serviceInvoice->amount_before_tax = 0;
            $serviceInvoice->cgst = 0;
            $serviceInvoice->sgst = 0;
            $serviceInvoice->igst = 0;
            $serviceInvoice->discount = 0;
            $serviceInvoice->fees  = 0;

            if(!empty($serviceInvoice->service_invoice_details)) {
                foreach($serviceInvoice->service_invoice_details as $detail) {
                    $serviceInvoice->fees += $detail->fees;
                    $serviceInvoice->discount += $detail->dis_amt;
                    $serviceInvoice->amount_before_tax += ($detail->fees - $detail->dis_amt);
                    $serviceInvoice->cgst += $detail->cgst_amount;
                    $serviceInvoice->sgst += $detail->sgst_amount;
                    $serviceInvoice->igst += $detail->igst_amount;
                }
            }
            $serviceInvoice->total_amount = round($serviceInvoice->total_amount);
            $serviceInvoice->amount_in_words = $this->amountToWords($serviceInvoice->total_amount);

            if($serviceInvoice->isNew()){
                $serviceInvoice->invoice_no = $this->ServiceInvoices->getNo($serviceInvoice->invoice_date);
            }
            if ($this->ServiceInvoices->save($serviceInvoice)) {
                $this->generateServiceInvoice($serviceInvoice->id);
                $this->Flash->success(__('The service invoice has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service invoice could not be saved. Please, try again.'));
        }
        if(empty($serviceInvoice['service_invoice_details'])) {
			$serviceInvoice['service_invoice_details']= [];
		}
        $clients = $this->ServiceInvoices->Clients->find('list');
        $tasks = $this->ServiceInvoices->Tasks->find('list');
        // $projects = $this->ServiceInvoices->Projects->find('list')->contain(['Clients']);
        $this->set(compact('serviceInvoice', 'clients', 'tasks'));
    }

    public function generateServiceInvoice($serviceInvoiceId) {
        $serviceInvoice = $this->ServiceInvoices->get($serviceInvoiceId, ['contain' => ['Clients' , 'ServiceInvoiceDetails'=>'Tasks', 'Projects']]);
        // debug($serviceInvoice); exit;

        $CakePdf = new \CakePdf\Pdf\CakePdf();
		$CakePdf->template('service_invoice', 'default');

        $this->viewVars = ['serviceInvoice'=>$serviceInvoice];
		$this->viewBuilder()->setLayout('pdf');

		$CakePdf->viewVars($this->viewVars);
		$pdf = $CakePdf->output();
		$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'services' . DS . 'invoices' . DS . 'service_invoice-' . $serviceInvoiceId . '.pdf');
	}


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $serviceInvoice = $this->ServiceInvoices->get($id);
        if ($this->ServiceInvoices->delete($serviceInvoice)) {
            $this->Flash->success(__('The service invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The service invoice could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function cancel($id){
        $serviceInvoice = $this->ServiceInvoices->get($id);
        $serviceInvoice->is_cancel = true;
        $this->ServiceInvoices->save($serviceInvoice);
        $this->Flash->success(__('The service invoice has been cancel.'));
        return $this->redirect(['action'=>'index']);
    }

    public function getPendingInvoice($client_id = null){
        $invoices = $this->ServiceInvoices
            ->find()
            ->where([
                'ServiceInvoices.client_id' => $client_id,
                'ServiceInvoices.received_amount < ServiceInvoices.total_amount',
                'is_cancel' => false
            ]);

        $this->set('invoices', $invoices);
		$this->set('_serialize', ['invoices']);
    }
}
