<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class FollowupsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Followups'); 

		$query = $this->Followups
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('followups', $this->paginate($query));
        $this->set('_serialize', ['followups']);   		
	}

	public function edit($id=null,$projectId) {
		
		$ProjectId = $projectId;
		$Projects = TableRegistry::get('Projects');
	 	$projects = $Projects->get($ProjectId,['contain'=>'Clients']);
		$this->set(compact('ProjectId','projects'));
		
		
		$this->set('title_for_layout', 'Followups');

		if(empty($id)) {
			$followup = $this->Followups->newEntity();
		} else {
			$followup = $this->Followups->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
		
			//$followup = $this->Followups->patchEntity($followup, $this->request->getData());	
			if($this->Followups->save2($followup,$this->request->getData())) {
				$this->Flash->success('The Followups has been saved');
				return $this->redirect(['controller'=>'Projects','action'=>'index']);
			} else {
				$this->Flash->error('The Followups could not be saved try again');
			}
		}

		$followupList = $this->Followups->find('all',['conditions'=>['Followups.project_id'=>$projectId]]);
		$projectStatus = Configure::read("projectStatus");
		$this->set(compact('followup','followupList','projectStatus'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$followup = $this->Followups->get($id);
		
		if($this->Followups->delete($followup)) {
			$this->Flash->success('The Followups has been deleted');
		} else {
			$this->Flash->error('The Followups could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	


}



