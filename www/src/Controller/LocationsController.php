<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LocationsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Locations');

		$query = $this->Locations
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('locations', $this->paginate($query));
        $this->set('_serialize', ['locations']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Locations');

		if(empty($id)) {
			$location = $this->Locations->newEntity();
		} else {
			$location = $this->Locations->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$location = $this->Locations->patchEntity($location, $this->request->getData());
			if($this->Locations->save($location)) {
				$this->Flash->success('The Location has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Location could not be saved try again');
			}
		}
		$this->set(compact('location'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$location = $this->Locations->get($id);

		if($this->Locations->delete($location)) {
			$this->Flash->success('The Location has been deleted');
		} else {
			$this->Flash->error('The Location could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function get($id) {
		$location = $this->Locations->get($id);
		$this->set('location', $location);
		$this->set('_serialize', 'location');
	}
}





