<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class AnnouncementsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}
	
	public function index()
	{
		$this->set('title_for_layout', 'Announcements');
		$query = $this->Announcements
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('announcements', $this->paginate($query));
        $this->set('_serialize', ['states']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Announcements');

		if(empty($id)) {
			$announcement = $this->Announcements->newEntity();
		} else {
			$announcement = $this->Announcements->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$announcement = $this->Announcements->patchEntity($announcement, $this->request->getData());	
			if($this->Announcements->save($announcement)) {
				$this->Flash->success('The Announcement has been saved');
				return $this->redirect(['action'=>'index']);
			}
			else {
				$this->Flash->error('The Announcement could not be saved try again');
			}
		}
		$this->set(compact('announcement'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$announcement = $this->Announcements->get($id);
		
		if($this->Announcements->delete($announcement)) {
			$this->Flash->success('The Announcement has been deleted');
		}
		else {
			$this->Flash->error('The Announcement could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



