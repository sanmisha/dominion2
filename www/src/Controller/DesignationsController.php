<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DesignationsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Designations');

		$query = $this->Designations
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('designations', $this->paginate($query));
        $this->set('_serialize', ['designations']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Designations');

		if(empty($id)) {
			$designation = $this->Designations->newEntity();
		} else {
			$designation = $this->Designations->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$designation = $this->Designations->patchEntity($designation, $this->request->getData());	
			if($this->Designations->save($designation)) {
				$this->Flash->success('The Designation has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Designation could not be saved try again');
			}
		}
		$this->set(compact('designation'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$designation = $this->Designations->get($id);
		
		if($this->Designations->delete($designation)) {
			$this->Flash->success('The Designation has been deleted');
		} else {
			$this->Flash->error('The Designation could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



