<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DepartmentsController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Departments');

		$query = $this->Departments
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('departments', $this->paginate($query));
        $this->set('_serialize', ['departments']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Departments');

		if(empty($id)) {
			$department = $this->Departments->newEntity();
		} else {
			$department = $this->Departments->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$department = $this->Departments->patchEntity($department, $this->request->getData());	
			if($this->Departments->save($department)) {
				$this->Flash->success('The Department has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Department could not be saved try again');
			}
		}
		$this->set(compact('department'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$department = $this->Departments->get($id);
		
		if($this->Departments->delete($department)) {
			$this->Flash->success('The Department has been deleted');
		} else {
			$this->Flash->error('The Department could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



