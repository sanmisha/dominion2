<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\App;
use Cake\Routing\Router;
use TransactionRequest;
use TransactionResponse;
use Cake\Core\Configure;

class AssociatesController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
			'actions' => ['index']
        ]);	 
    }

    public function index() {       
        $this->set('title_for_layout', 'Associates');
        $this->Associates->recover();
        $query = $this->Associates
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->where(['Associates.parent_id IS NOT'=>null])
            ->contain(['Referral']);
        
		$this->set('associates', $this->paginate($query));
        $this->set('_serialize', ['Associates']);
    }

    public function edit($id=null, $side=null, $type='pearl') {
        $redirect = ['controller'=>'Dashboard', 'action' => 'index'];
        if($this->AuthUser->hasRoles([1,2,3])) {
            $redirect = ['action' => 'index'];
        }
        if(empty($id)) {
            $associate = $this->Associates->newEntity();
            $associate->associate_type = $type;
        } else {    	
            $associate = $this->Associates->get($id, [
                'contain' => ['Teams'=>['Users']]
            ]);
        }
        $associate->ref_id = $this->Auth->user('team.associate.id');
        $cost = Configure::read('cost');
        
        $this->set('title_for_layout', 'Add Associates');       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $val = false;
            if(empty($data['id'])) {
                $data['cost'] = $cost[$data['associate_type']];
                $data['listings_count'] = Configure::read('listings_count')[$data['associate_type']];
                $val = $this->Associates->add($associate, $data, $side); 
                if($val) {
                    if($data['pay_later']) {
                        $this->Flash->success(__('The associate has been saved.'));
                        return $this->redirect($redirect);
                    } else {
                        return $this->redirect(['action'=>'payment', $associate->id]);
                    }
                }
            } else {
                $associate = $this->Associates->patchEntity($associate, $data);
                $val = $this->Associates->save($associate);
            }
		
			if ($val) {
                $this->Flash->success(__('The associate has been saved.'));
                return $this->redirect($redirect);
	        } else {
                $this->Flash->error(__('The Associates could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('associate', 'side', 'cost'));
    }

    public function payment($id) {
        $associate = $this->Associates->get($id, ['contain'=>['Teams'=>['Users']]]);
        //debug($associate->teams[0]->user); exit; 

        $transactionRequest = new TransactionRequest();

        $transactionRequest->setMode("live");
        $transactionRequest->setLogin(112268);
        $transactionRequest->setPassword("3cddd69a");
        $transactionRequest->setProductId("YOGESHWARI");
        $transactionRequest->setReqHashKey("5516fc0ccddfb97e3b");

        /*$transactionRequest->setMode("test");
        $transactionRequest->setLogin(197);
        $transactionRequest->setPassword("Test@123");
        $transactionRequest->setProductId("NSE");
        $transactionRequest->setReqHashKey("KEY123657234");*/
        
        $url = Router::url(array(
            'controller' => 'Associates',
            'action' => 'payment_response'
        ), true);

        $address = urlencode(implode(", ", array(
            $associate->address_line_1, $associate->address_line_2, $associate->state, $associate->city, $associate->pincode
        )));
        $transactionRequest->setAmount($associate->cost);
        $transactionRequest->setTransactionCurrency("INR");
        $transactionRequest->setTransactionAmount(0);
        $transactionRequest->setReturnUrl($url);
        $transactionRequest->setClientCode($associate->id);
        $transactionRequest->setTransactionId($associate->id);
        $transactionRequest->setTransactionDate(str_replace(" ", "%20", date("d/m/Y h:m:s")));
        $transactionRequest->setCustomerName($associate->associate_name);
        $transactionRequest->setCustomerEmailId($associate->teams[0]->user->email);
        $transactionRequest->setCustomerMobile($associate->teams[0]->user->mobile);
        $transactionRequest->setCustomerBillingAddress($address);
        $transactionRequest->setCustomerAccount("639827");		
        
        $url = $transactionRequest->getPGUrl();	
        //debug($url); exit;
        $this->redirect($url);        
    }

    public function payment_response() {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transactionResponse = new TransactionResponse();
            $transactionResponse->setRespHashKey("93e3c6990e223685c4");
            //$transactionResponse->setRespHashKey("KEYRESP123657234");
            $data = $this->request->getData();
            //debug($data); exit;
            $associate = $this->Associates->get($data['mer_txn'], [
                'contain' => ['Teams'=>['Users']]
            ]);

            $associate->mmp_txn = $data['mmp_txn'];
            $associate->date_txn = $data['date'];
            $associate->bank_txn = $data['bank_txn'];
            $associate->f_code = $data['f_code'];
            $associate->bank_name = $data['bank_name'];
            $associate->discriminator = $data['discriminator'];
            $associate->mmp_txn = $data['mmp_txn'];
    
            if($data['f_code']=='Ok') {
                $associate->active = true;
                $associate->joining_date = date('d/m/Y');
                $this->Associates->activate($associate);
            } else {
                $this->Associates->save($associate);
            }

			if ($data['f_code']=='Ok') {
                $this->Flash->success(__('Your Transaction is successful.'));
	        } else {
                $this->Flash->error(__('Your Transaction is failed. Please login and try again.'));
            }            
            return $this->redirect(['controller'=>'Dashboard', 'action'=>'index']);
        }
    }

    public function activate($id=null) {
        $associate = $this->Associates->get($id, [
            'contain' => ['Teams'=>['Users']]
        ]);

        if(empty($associate->joining_date)) {
            $associate->joining_date = date('d/m/Y');
        }
    
        $this->set('title_for_layout', 'Activate Associate');
       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
		
			if ($this->Associates->activate($associate, $data)) {
                $this->Flash->success(__('The associate has been activated.'));
                return $this->redirect(['action'=>'index']);
	        } else {
                $this->Flash->error(__('The Associates could not be activated. Please, try again.'));
            }
        }
        $this->set(compact('associate'));
    }    

    public function tree() {       
        $this->set('title_for_layout', 'Associates');
        $this->Associates->recover();
        $associate = $this->Associates->get($this->Auth->user('team.associate.id'));
        $children = $this->Associates->find('children', ['for' => $this->Auth->user('team.associate.id')])
            ->find('threaded');
        
        //debug($children->toArray()); exit;
		$this->set(compact('associate', 'children'));
    }

    public function profile(){ 
        $associate = $this->Associates->get($this->Auth->user('team.associate_id'), [
            'contain' => ['Teams'=>['Users']]
        ]);

        //debug($associate); exit;
        $this->set('title_for_layout', 'Update Profile');
     
        if ($this->request->is(['patch', 'post', 'put'])) {
            $entity = $this->Associates->patchEntity($associate, $this->request->getData(),['associated' => ['Teams'=>['Users']]]);
            if ($this->Associates->save($entity)) {
                $Users = TableRegistry::get('Users');
                $user = $Users->get($entity->teams[0]->user['id']);
                $user->mobile = $entity->teams[0]->user['mobile'];
                $user->email = $entity->teams[0]->user['email'];
                $Users->save($user);
                $this->Flash->success(__('The Profile has been updated.'));
            } else {
                $this->Flash->error(__('Unable to update Profile, try again.'));
            }
        }
      $this->set(compact('associate'));
    }    
    
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Associates->get($id);
        if ($this->Associates->delete($company)) {
            $this->Flash->success(__('The Company has been deleted.'));
        } else {
            $this->Flash->error(__('The Company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function sudo($id=null) {
        $Users = TableRegistry::get('Users');	

		$user = $Users->get($id, [
			'contain'=>['Teams'=>['Associates'],'Roles']
        ]);
		$this->Auth->setUser($user->toArray());
		return $this->redirect(['prefix'=>'cp','controller'=>'Dashboard','action'=>'index']);
	}	    
}
