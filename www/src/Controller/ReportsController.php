<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\ViewBuilder;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;

class ReportsController extends AppController {
	public function initialize() {
		parent::initialize();
	}

	public function projects() {
		$this->set('title_for_layout', 'Projects');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['project_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['project_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['status'])) {
                $conditions['status'] = $data['status'];
            } else{
                $conditions['status IN'] = ['Converted', 'Archived'];
            }

			$Projects = TableRegistry::get('Projects');
			$projects = $Projects->find()
				->where($conditions)
                ->contain(['ProjectTypes','Clients']);

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'projects.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('projects'));
            } else {
				$_serialize = 'projects';
				$_header = [
                    'Quotation No', 'Date', 'Project No', 'Date', 'Site', 'Client', 'Project Type',
                    'Amount', 'Status'
				];

				$_extract = [
                    'quotation_no', 'quotation_date', 'project_no', 'project_date', 'site_name', 'client.client_name', 'project_type.project_type',
                    'final_amount', 'status'
				];

				$this->response = $this->response->withDownload('projects.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('projects', '_serialize', '_header', '_extract'));
            }
		}
	}
    public function quatations() {
		$this->set('title_for_layout', 'Quatations');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['project_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['project_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['status'])) {
                $conditions['status'] = $data['status'];
            } else{
                $conditions['status IN'] = ['Open', 'Closed'];
            }

			$Projects = TableRegistry::get('Projects');
			$projects = $Projects->find()
				->where($conditions)
                ->contain(['ProjectTypes','Clients']);

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'projects.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('projects'));
            } else {
				$_serialize = 'projects';
				$_header = [
                    'Quotation No', 'Date', 'Site', 'Client', 'Project Type',
                    'Amount', 'Status'
				];

				$_extract = [
                    'quotation_no', 'quotation_date', 'site_name', 'client.client_name', 'project_type.project_type',
                    'final_amount', 'status'
				];

				$this->response = $this->response->withDownload('quatations.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('projects', '_serialize', '_header', '_extract'));
            }
		}
	}

	public function invoices() {
		$this->set('title_for_layout', 'Invoices');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['invoice_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['invoice_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['project_id'])) {
                $conditions['project_id'] = $data['project_id'];
            }

            if($data['is_draft'] == 'draft') {
                $conditions['is_draft'] = true;
            }

            if($data['is_cancel'] == 'cancel') {
                $conditions['is_cancel'] = true;
            }

            if($data['payment_status'] == 'pending') {
                $conditions[] = ['received_amount < total_amount'];
            }
            
            if($data['payment_status'] == 'paid'){
                $conditions[] = ['received_amount >= total_amount'];
            }    
         

			$Invoices = TableRegistry::get('Invoices');
			$invoices = $Invoices->find()
				->where($conditions)
                ->contain(['Projects'=>['Clients'],'InvoiceDetails'=>['Tasks']]);
        
            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'invoices.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('invoices'));
            } else {
				$_serialize = 'invoices';
				$_header = [
                    '#', 'Date', 'Project No', 'Client', 'Task', 'Before Tax', 'GST', 'Invoice Amount', 'Received Amount', 'Balanced Amount', 'Cancel'
				];

				$_extract = [
                    'invoice_no', 'invoice_date', 'project.project_no', 'project.client.client_name',
                    function($data) {
                        return $data['invoice_details'][0]['task']['task'];
					},
                    'amount_before_tax',
                    function($data) {
                        return $data['total_amount'] - $data['amount_before_tax'];
					},
                    'total_amount', 'received_amount', 'balance_amount', 'is_cancel'
				];

				$this->response = $this->response->withDownload('invoices.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('invoices', '_serialize', '_header', '_extract'));
            }
		}
        $Projects = TableRegistry::get('Projects');
        $projects = $Projects->find('list')->contain(['Clients']);
        $this->set(compact('projects'));
	}
    
	public function serviceInvoices() {
		$this->set('title_for_layout', 'Service Invoices');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();
            $ServiceInvoices = TableRegistry::get('ServiceInvoices');

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['invoice_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['invoice_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['project_id'])) {
                $conditions['project_id'] = $data['project_id'];
            }         
            if($data['is_cancel'] == 'cancel') {
                $conditions['is_cancel'] = true;
            } else {
                $conditions['is_cancel'] = false;
            }

            if($data['payment_status'] == 'pending') {
                $conditions[] = ['received_amount < total_amount'];
            }
            
            if($data['payment_status'] == 'paid'){
                $conditions[] = ['received_amount >= total_amount'];
            }    
          
          

			
			$serviceInvoices = $ServiceInvoices->find()
				->where($conditions)
                ->contain(['Projects'=>['Clients'],'ServiceInvoiceDetails'=>['Tasks']]);
            //debug($serviceInvoices->toArray()); exit();

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'serviceInvoices.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('serviceInvoices'));
            } else {
				$_serialize = 'serviceInvoices';
				$_header = [
                    '#', 'Date', 'Project No', 'Client', 'Task', 'Before Tax', 'GST', 'Invoice Amount', 'Received Amount', 'Balanced Amount', 'Cancel'
				];

				$_extract = [
                    'invoice_no', 'invoice_date', 'project.project_no', 'project.client.client_name', 
                    function($data) {
                        return $data['service_invoice_details'][0]['task']['task'];
					},
                    'amount_before_tax',
                    function($data) {
                        return $data['total_amount'] - $data['amount_before_tax'];
					},
                    'total_amount', 'received_amount', 'balance_amount', 'is_cancel'
				];

				$this->response = $this->response->withDownload('serviceInvoices.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('serviceInvoices', '_serialize', '_header', '_extract'));
            }
		}
        $Projects = TableRegistry::get('Projects');
        $projects = $Projects->find('list')->contain(['Clients']);
        $this->set(compact('projects'));
	}
    public function invoiceReceipt() {
		$this->set('title_for_layout', 'Invoice Receipt');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();
            $InvoiceReceipts = TableRegistry::get('InvoiceReceipts');

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['receipt_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['receipt_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['client_id'])) {
                $conditions['client_id'] = $data['client_id'];
            }  
			
			$invoiceReceipts = $InvoiceReceipts->find()
				->where($conditions)
                ->contain(['Clients','InvoiceReceiptDetails'=>['Invoices']]);       

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'invoiceReceipts.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('invoiceReceipts'));
            } else {
				$_serialize = 'invoiceReceipts';
				$_header = [
                     'Receipt No', 'Date',  'Client', 'Payment Mode'
				];

				$_extract = [
                    'receipt_no', 'receipt_date', 'client.client_name', 'payment_mode',                     
				];

				$this->response = $this->response->withDownload('invoiceReceipts.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('invoiceReceipts', '_serialize', '_header', '_extract'));
            }
		}
        $Clients = TableRegistry::get('clients');
        $clients = $Clients->find('list');
        $this->set(compact('clients'));
	}
    public function serviceReceipt() {
		$this->set('title_for_layout', 'Service Receipts');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();
            $ServiceReceipts = TableRegistry::get('ServiceReceipts');

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['receipt_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['receipt_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['client_id'])) {
                $conditions['client_id'] = $data['client_id'];
            }   

			$serviceReceipts = $ServiceReceipts->find()
				->where($conditions)
                ->contain(['Clients','ServiceReceiptInvoices']);           
            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'serviceReceipts.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('serviceReceipts'));
            } else {
				$_serialize = 'serviceReceipts';
				$_header = [
                    'Receipt No', 'Date',  'Client', 'Payment Mode'
               ];

               $_extract = [
                   'receipt_no', 'receipt_date', 'client.client_name', 'payment_mode',                     
               ];

               $this->response = $this->response->withDownload('serviceReceipts.csv');
               $this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('serviceReceipts', '_serialize', '_header', '_extract'));
            }
		}
        $Clients = TableRegistry::get('Clients');
        $clients = $Clients->find('list');
        $this->set(compact('Clients'));
	}


	public function dailyTasks() {
		$this->set('title_for_layout', 'Daily Tasks');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['due_date >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['due_date <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['project_id'])) {
                $conditions['project_id'] = $data['project_id'];
            }
            if(!empty($data['activity_id'])) {
                $conditions['activity_id'] = $data['activity_id'];
            }
            if(!empty($data['assigned_to'])) {
                $conditions['assigned_to'] = $data['assigned_to'];
            }
            if(!empty($data['status'])) {
                $conditions['DailyTasks.status'] = $data['status'];
            }

			$DailyTasks = TableRegistry::get('DailyTasks');
			$dailyTasks = $DailyTasks->find()
				->where($conditions)
                ->contain(['Projects'=>['Clients'], 'Activities', 'AssignedEmployee','CreatedEmployee']);

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'dailyTasks.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('dailyTasks'));
            } else {
				$_serialize = 'dailyTasks';
				$_header = [
                    'Due Date', 'Project', 'Activity', 'Description', 'Assigned To', 'Done Date', 'Duration', 'Status'
				];

				$_extract = [
                    'due_date',
                    function($data) {
                        return $data['project'] ? $data['project']['project_no'] . ' ' . $data['project']['client']['client_name'] : '---';
					},
                    'activity.activity',
                    'description',
                    'assigned_employee.name',
                    'done_date',
                    'duration',
                    'status',
				];

				$this->response = $this->response->withDownload('dailyTasks.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('dailyTasks', '_serialize', '_header', '_extract'));
            }
		}
        $Projects = TableRegistry::get('Projects');
        $projects = $Projects->find('list')->contain(['Clients']);

        $Activities = TableRegistry::get('Activities');
        $activities = $Activities->find('list');

        $Employees = TableRegistry::get('Employees');
        $employees = $Employees->find('list');
        $this->set(compact('projects', 'activities', 'employees'));
	}

	public function inwardOutwardRegister() {
		$this->set('title_for_layout', 'Inward Outward Register');
		if($this->request->is(['patch','post','put'])) {
            $data = $this->request->getData();

            $conditions = [];
            if(!empty($data['from_date'])) {
                $conditions['InwardOutwards.created >='] = \DateTime::createFromFormat("d/m/Y", $data['from_date'])->format('Y-m-d');
            }
            if(!empty($data['to_date'])) {
                $conditions['InwardOutwards.created <='] = \DateTime::createFromFormat("d/m/Y", $data['to_date'])->format('Y-m-d');
            }
            if(!empty($data['assigned_to'])) {
                $conditions['assigned_to'] = $data['assigned_to'];
            }
            if(!empty($data['status'])) {
                $conditions['status'] = $data['status'];
            }

			$InwardOutwards = TableRegistry::get('InwardOutwards');
			$inwardOutwards = $InwardOutwards->find()
				->where($conditions)
                ->contain(['Locations', 'FileNos', 'AssignedEmployee']);

            if($data['export']=='pdf') {
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'download' => false,
                        'filename' => 'inwardOutwardRegister.pdf',
                        'orientation' => 'landscape',
                    ]
                ]);
                $this->set(compact('inwardOutwards'));
            } else {
				$_serialize = 'inwardOutwards';
				$_header = [
                    'Register No', 'Subject', 'Description', 'Assigned To', 'Assigned On', 'Timestamp', 'Location', 'File No', 'Status', 'Handler', 'Hander ID'
				];

				$_extract = [
                    'register_no',
                    'subject',
                    'description',
                    'assigned_employee.name',
                    'assigned_on',
                    'created',
                    'location.location',
                    'file_no.file_no',
                    'status',
                    'handler',
                    'handler_id'
				];

				$this->response = $this->response->withDownload('inwardOutwardRegister.csv');
				$this->viewBuilder()->className('CsvView.Csv');
				$this->set(compact('inwardOutwards', '_serialize', '_header', '_extract'));
            }
		}

        $Employees = TableRegistry::get('Employees');
        $employees = $Employees->find('list');
        $this->set(compact('employees'));
	}
}
