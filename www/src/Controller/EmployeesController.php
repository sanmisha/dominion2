<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class EmployeesController extends AppController
{
	public function initialize() {	
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Employee');
		$query = $this->Employees
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->contain(['Users','Designations','Departments']);
         
		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('employees', $this->paginate($query));
        $this->set('_serialize', ['employees']);
    }

    public function edit($id = null) {
        $Users = TableRegistry::get('Users');			
		$Users->hasOne('Employees', ['foreignKey' => 'id']);

		if(empty($id)) {
            $user = $Users->newEntity(); 
            $this->set('title_for_layout', 'Add Employee Details');
		} else {
			$this->set('title_for_layout', 'Edit Employee Details');
			$user = $Users->get($id, [
				'contain' => ['Roles', 'Employees']
			]);
			$this->set('title_for_layout', 'Edit Employees');
		}
        
        if ($this->request->is(['patch', 'post', 'put'])) {
			if ($this->Employees->save2($this->request->getData(), $user)) {
                $this->Flash->success(__('The Employee has been saved.'));
				return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Employee could not be saved. Please, try again.'));
        }

        $designation = $this->Employees->Designations->find('list');
        $department = $this->Employees->Departments->find('list');
        $Roles = TableRegistry::get('Roles');
        $roles = $Roles->find('list');
		$this->set(compact('user','designation','department','roles'));	
    }

    public function delete($id = null)
    {
		$this->request->allowMethod(['post', 'delete']);
		$this->Employees->hasOne('Users',['foreignKey' => 'id', 'dependent'=>true]);		
		$employee = $this->Employees->get($id,['contain'=>'Users']);

        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The Employee has been deleted.'));
        } else {
            $this->Flash->error(__('The Employee could not be deleted. Please, try again.'));
        }
		return $this->redirect(['action' => 'index']);
    }
    
}



