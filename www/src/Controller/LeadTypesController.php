<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LeadTypesController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Lead Types');

		$query = $this->LeadTypes
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('leadTypes', $this->paginate($query));
        $this->set('_serialize', ['leadTypes']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Lead Types');

		if(empty($id)) {
			$leadType = $this->LeadTypes->newEntity();
		} else {
			$leadType = $this->LeadTypes->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$leadType = $this->LeadTypes->patchEntity($leadType, $this->request->getData());	
			if($this->LeadTypes->save($leadType)) {
				$this->Flash->success('The Lead Type has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Lead Type could not be saved try again');
			}
		}
		$this->set(compact('leadType'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$leadType = $this->LeadTypes->get($id);
		
		if($this->LeadTypes->delete($leadType)) {
			$this->Flash->success('The Lead Type has been deleted');
		} else {
			$this->Flash->error('The Lead Type could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



