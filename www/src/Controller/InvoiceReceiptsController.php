<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class InvoiceReceiptsController extends AppController
{
    public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}


    public function index()
    {
        $this->set('title', 'Invoice Receipts');


        $query = $this->InvoiceReceipts
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Clients','InvoiceReceiptDetails'=>['Invoices']]);

		$this->paginate = ['order'=>['InvoiceReceipts.id'=>'DESC']];
		$this->set('invoiceReceipts', $this->paginate($query));
        $this->set('_serialize', ['invoiceReceipts']);

    }

    public function edit($id = null)
    {
        if(empty($id)){
            $this->set('title', ' Add Invoice Receipts');
            $invoiceReceipt = $this->InvoiceReceipts->newEntity();
        } else{
            $this->set('title', ' Edit Invoice Receipts');
            $invoiceReceipt = $this->InvoiceReceipts->get($id, [
                'contain' => ['InvoiceReceiptDetails'=>['Invoices'], 'Clients'], ]);

        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            foreach($data['invoice_receipt_details'] as $i => $invoice) {
                if(empty($invoice['total_received_amount'])) {
                    unset($data['invoice_receipt_details'][$i]);                }

            }
            $invoiceReceipt = $this->InvoiceReceipts->patchEntity($invoiceReceipt, $data);
            $invoiceReceipt->amount_in_words = $this->amountToWords($invoiceReceipt->amount);
            if($invoiceReceipt->isNew()) {
                $invoiceReceipt->receipt_no = $this->InvoiceReceipts->getNo($invoiceReceipt->receipt_date);
            }
            if ($this->InvoiceReceipts->save($invoiceReceipt)) {
                $Invoices = TableRegistry::get('Invoices');
                $InvoiceReceiptDetails = TableRegistry::get('InvoiceReceiptDetails');

                foreach($invoiceReceipt->invoice_receipt_details as $receipts) { 
                    $query = $InvoiceReceiptDetails->find();
                    $query->select([
                        'total_received_amount' => $query->func()->sum('total_received_amount'),
                        'invoice_id'
                    ])
                    ->group('invoice_id')
                    ->having(['invoice_id' => $receipts->invoice_id]);
                    $invoice = $Invoices->get($receipts->invoice_id);
                    $invoice->received_amount = $query->first()->total_received_amount;
                    $Invoices->save($invoice);
                }

                $this->Flash->success(__('The invoice receipt has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invoice receipt could not be saved. Please, try again.'));
        }

        if(empty($invoiceReceipt['invoice_receipt_details'])) {
			$invoiceReceipt['invoice_receipt_details']= [];
		}

        $clients = $this->InvoiceReceipts->Clients->find('list');
        $this->set(compact('invoiceReceipt', 'clients'));
    }    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $invoiceReceipt = $this->InvoiceReceipts->get($id, ['contain'=>['InvoiceReceiptDetails']]);
        $Invoices = TableRegistry::get('Invoices');

        if ($this->InvoiceReceipts->delete($invoiceReceipt)) {
            $Invoices = TableRegistry::get('Invoices');
            $InvoiceReceiptDetails = TableRegistry::get('InvoiceReceiptDetails');

            foreach($invoiceReceipt->invoice_receipt_details as $receipts){
                $query = $InvoiceReceiptDetails->find();
                $query->select([
                    'total_received_amount' => $query->func()->sum('total_received_amount'),
                    'invoice_id'
                ])
                ->group('invoice_id')
                ->having(['invoice_id' => $receipts->invoice_id]);

                $invoice = $Invoices->get($receipts->invoice_id);
                if($query->count() == 0) {
                    $invoice->received_amount = 0;
                } else {
                    $invoice->received_amount = $query->first()->total_received_amount;
                }

                $Invoices->save($invoice);
            }

            $this->Flash->success(__('The service receipt has been deleted.'));
        } else {
            $this->Flash->error(__('The service receipt could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function print($id = null)
    {
        $invoiceReceipt = $this->InvoiceReceipts->get($id, ['contain' => ['InvoiceReceiptDetails'=>['Invoices'=>['Projects']], 'Clients']]);
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => false,
                'filename' => "InvoiceReceipt-{$invoiceReceipt->id}.pdf",               
            ]
        ]);
        $this->set(compact('invoiceReceipt'));

    }
}
