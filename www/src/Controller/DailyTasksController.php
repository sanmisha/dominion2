<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;

class DailyTasksController extends AppController
{
	public function initialize() {
		parent::initialize();
		 $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Project Tasks');

		if($this->Auth->user('id') == 1 ) {
			$query = $this->DailyTasks
               ->find('search', ['search' => $this->request->getQueryParams()]);
		} else {
			$query = $this->DailyTasks
              ->find('search', ['search' => $this->request->getQueryParams()])
              ->where([
                  'OR' => [
                    'DailyTasks.assigned_to' =>$this->Auth->user('id'),
                    'DailyTasks.created_by' =>$this->Auth->user('id')
                  ]
                ]);
		}
		$this->paginate = ['contain'=>['Projects'=>['Clients'], 'Activities', 'AssignedEmployee','CreatedEmployee'],'order'=>['DailyTasks.due_date'=>'DESC']];

		$this->set('dailyTasks', $this->paginate($query));
        $this->set('_serialize', ['dailyTasks']);
    }

	public function edit($id=null) {
		$this->set('title_for_layout', 'Project Tasks');

		if(empty($id)) {
			$dailyTask = $this->DailyTasks->newEntity();
		} else {
			$dailyTask = $this->DailyTasks->get($id, [
                'contain' => ['Projects'=>['Clients'], 'Activities', 'AssignedEmployee','CreatedEmployee']
            ]);
		}

		if($this->request->is(['patch','post','put'])) {
			$dailyTask = $this->DailyTasks->patchEntity($dailyTask, $this->request->getData());

			if($this->DailyTasks->save($dailyTask)) {
				$this->Flash->success('The Tasks has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Tasks could not be saved try again');
			}
		}
        $projects = $this->DailyTasks->Projects->find('list')->contain(['Clients']);
        $employees = $this->DailyTasks->AssignedEmployee->find('list');
        $activities = $this->DailyTasks->Activities->find('list');
        $this->set(compact('dailyTask', 'projects', 'employees', 'activities'));
	}

	public function progress($id=null) {
		$this->set('title_for_layout', 'Project Tasks');

		if(empty($id)) {
			$dailyTask = $this->DailyTasks->newEntity();
		} else {
            $dailyTask = $this->DailyTasks->get($id, [
                'contain' => ['Projects'=>['Clients'], 'Activities', 'AssignedEmployee','CreatedEmployee']
            ]);
		}

		if($this->request->is(['patch','post','put'])) {
			$dailyTask = $this->DailyTasks->patchEntity($dailyTask, $this->request->getData());

            if($dailyTask->status == 'Done') {
                $dailyTask->done_date = date('d/m/Y');
            } else {
                $newTask = $this->DailyTasks->newEntity();
                $newTask->project_id = $dailyTask->project_id;
                $newTask->activity_id = $dailyTask->activity_id;
                $newTask->assigned_to = $dailyTask->assigned_to;

                $newDueDate = \DateTime::createFromFormat('d/m/Y', $dailyTask->due_date)->add(new \DateInterval('P1D'));
                $newTask->due_date = $newDueDate->format('d/m/Y');

                $newTask->done_date = $dailyTask->done_date;
            }

            if($this->DailyTasks->save($dailyTask)) {
                if(isset($newTask)) {
                    $this->DailyTasks->save($newTask);
                }
				$this->Flash->success('The Tasks has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Tasks could not be saved try again');
			}
		}
        $projects = $this->DailyTasks->Projects->find('list')->contain(['Clients']);
        $employees = $this->DailyTasks->AssignedEmployee->find('list');
        $activities = $this->DailyTasks->Activities->find('list');
        $this->set(compact('dailyTask', 'projects', 'employees', 'activities'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);

        $dailyTask = $this->DailyTasks->get($id);

        if($this->DailyTasks->delete($dailyTask)) {
			$this->Flash->success('The Tasks has been deleted');
		} else {
			$this->Flash->error('The Tasks could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}
}



