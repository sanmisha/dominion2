<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class EmailTemplatesController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Email Templates');

		$query = $this->EmailTemplates
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('emailTemplates', $this->paginate($query));
        $this->set('_serialize', ['emailTemplates']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Email Templates');

		if(empty($id)) {
			$emailTemplate = $this->EmailTemplates->newEntity();
		} else {
			$emailTemplate = $this->EmailTemplates->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$emailTemplate = $this->EmailTemplates->patchEntity($emailTemplate, $this->request->getData());	
			if($this->EmailTemplates->save($emailTemplate)) {
				$this->Flash->success('The Email Templates has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Email Templates could not be saved try again');
			}
		}
		$this->set(compact('emailTemplate'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$emailTemplate = $this->EmailTemplates->get($id);
		
		if($this->EmailTemplates		->delete($emailTemplate)) {
			$this->Flash->success('The Email Templates has been deleted');
		} else {
			$this->Flash->error('The Email Templates could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



