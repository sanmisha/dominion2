<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class FileNosController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'File Nos');

		$query = $this->FileNos
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['contain'=>['Projects']];
		$this->set('fileNos', $this->paginate($query));
        $this->set('_serialize', ['fileNos']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'File Nos');

		if(empty($id)) {
			$fileNo = $this->FileNos->newEntity();
		} else {
			$fileNo = $this->FileNos->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$fileNo = $this->FileNos->patchEntity($fileNo, $this->request->getData());
			if($this->FileNos->save($fileNo)) {
				$this->Flash->success('The File No has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The File No could not be saved try again');
			}
		}

        $projects = $this->FileNos->Projects->find('list')->contain(['Clients']);
		$this->set(compact('fileNo', 'projects'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$fileNo = $this->FileNos->get($id);

		if($this->FileNos->delete($fileNo)) {
			$this->Flash->success('The File No has been deleted');
		} else {
			$this->Flash->error('The File No could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function get($id) {
		$fileNo = $this->FileNos->get($id);
		$this->set('fileNo', $fileNo);
		$this->set('_serialize', 'fileNo');
	}
}





