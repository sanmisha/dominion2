<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ActivitiesController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Activities');

		$query = $this->Activities
        	->find('search', ['search' => $this->request->getQueryParams()]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('activities', $this->paginate($query));
        $this->set('_serialize', ['activities']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Activities');

		if(empty($id)) {
			$activity = $this->Activities->newEntity();
		} else {
			$activity = $this->Activities->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$activity = $this->Activities->patchEntity($activity, $this->request->getData());
			if($this->Activities->save($activity)) {
				$this->Flash->success('The Activity has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Activity could not be saved try again');
			}
		}
		$this->set(compact('activity'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$activity = $this->Activities->get($id);

		if($this->Activities->delete($activity)) {
			$this->Flash->success('The Activity has been deleted');
		} else {
			$this->Flash->error('The Activity could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function get($id) {
		$activity = $this->Activities->get($id);
		$this->set('activity', $activity);
		$this->set('_serialize', 'activity');
	}
}





