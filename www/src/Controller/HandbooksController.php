<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class HandbooksController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Handbooks');

		$query = $this->Handbooks
			->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Designations','Departments']);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('handbooks', $this->paginate($query));
        $this->set('_serialize', ['handbooks']);
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Handbooks');

		if(empty($id)) {
			$handbook = $this->Handbooks->newEntity();
		} else {
			$handbook = $this->Handbooks->get($id);
		}

		if($this->request->is(['patch','post','put'])) {
			$handbook = $this->Handbooks->patchEntity($handbook, $this->request->getData());
			if($this->Handbooks->save($handbook)) {
				$this->Flash->success('The Handbooks has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Handbooks could not be saved try again');
			}
		}
		$designation = $this->Handbooks->Designations->find('list');
		$department = $this->Handbooks->Departments->find('list');

		$this->set(compact('handbook','designation','department'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$handbook = $this->Handbooks->get($id);

		if($this->Handbooks->delete($handbook)) {
			$this->Flash->success('The Handbooks has been deleted');
		} else {
			$this->Flash->error('The Handbooks could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}


	public function display() {
		$this->set('title_for_layout', 'Display');

		$Handbooks = TableRegistry::get('Handbooks');
		$data = $Handbooks->get($this->Auth->user('id'));

		$handbooks = $this->Handbooks->find('all',[
			'conditions'=>[
                'OR' => [
                    'Handbooks.department_id' => $data['department_id'],
                    'Handbooks.designation_id' => $data['designation_id'],
                ]
			],
			'contain'=>['Designations','Departments']

		]);

		$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('handbooks', $this->paginate($handbooks));
		$this->set(compact('handbooks'));
	}
}



