<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Cake\I18n\Time;

class SiteVisitsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}

	public function index() {
		$this->set('title_for_layout', 'Site Visit');

		$query = $this->SiteVisits
        	->find('search', ['search' => $this->request->getQueryParams()])
			->contain(['Employees', 'Projects'=>['Clients']]);
        $this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('siteVisit', $this->paginate($query));
        $this->set('_serialize', ['siteVisit']);
	}

	
	public function edit($id=null) {
		$this->set('title_for_layout', 'Site Visit');

		if(empty($id)) {
			$siteVisit = $this->SiteVisits->newEntity();
			$siteVisit->visit_date = date('d/m/Y');
		} else {
			$siteVisit = $this->SiteVisits->get($id, ['contain'=>['SiteVisitDetails'=>['CheckListItems', 'CheckListSections'], 'SiteVisitImages', 'SiteVisitStakeholders'=>['Stakeholders'], 'SiteVisitVisitors']]);
		}

		if($this->request->is(['patch','post','put'])) {
			$data = $this->request->getData();			
            $siteVisit = $this->SiteVisits->patchEntity($siteVisit, $this->request->getData());
			// debug($siteVisit); exit;

			if($siteVisit->isNew()){
				$project = $this->SiteVisits->Projects->get($siteVisit->project_id);				
				$siteVisit->visit_no = $this->SiteVisits->getNo($siteVisit->project_id, $project->project_no);
			}
			if(!empty($siteVisit->visit_time)){					
			$siteVisit->visit_time = Time::createFromFormat('Y-m-d H:i a', date('Y-m-d').' '.$data['visit_time']);
			} else {
				$this->Flash->error('Visit Time is require');
			}    

			if($this->SiteVisits->save($siteVisit)) {
                $this->generateSiteVisit($siteVisit->id);
				$this->Flash->success('The Site Visit has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Site Visit could not be saved try again');
			}

		}
		if(empty($siteVisit['site_visit_details'])) {
			$siteVisit['site_visit_details'] = [];
		}
		if(empty($siteVisit['site_visit_images'])) {
			$siteVisit['site_visit_images'] = [];
		}		
		if(empty($siteVisit['site_visit_stakeholders'])) {
			$siteVisit['site_visit_stakeholders'] = [];
		}
		if(empty($siteVisit['site_visit_visitors'])) {
			$siteVisit['site_visit_visitors'] = [];
		}
		$Stakeholders = TableRegistry::get('stakeholders');
		$stakeholders = $Stakeholders->find('list');
		$project = $this->SiteVisits->Projects->find('list', ['keyField'=>'id','valueField'=>'project_with_client'])->where(['status'=>'Converted'])->contain('Clients');
		$employees = $this->SiteVisits->Employees->find('list');
		$checkListSections = $this->SiteVisits->CheckListSections->find('list');
		$this->set(compact('siteVisit','project', 'employees', 'checkListSections', 'stakeholders'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$sitevisit = $this->SiteVisits->get($id, ['contain'=>['SiteVisitDetails', 'SiteVisitImages']]);

		if($this->SiteVisits->delete($sitevisit)) {
			$this->Flash->success('The Site Visit has been deleted');
		} else {
			$this->Flash->error('The Site Visit could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}

	public function generateSiteVisit($id)
	{
		$siteVisit = $this->SiteVisits->get($id, ['contain'=>['Employees', 'SiteVisitImages','SiteVisitDetails'=>['CheckListSections', 'CheckListItems'], 'Projects'=>['Clients'], 'SiteVisitStakeholders'=>['Stakeholders'], 'SiteVisitVisitors']]);
				
		$CakePdf = new \CakePdf\Pdf\CakePdf();		
		$CakePdf->template('site_visit', 'default');
			

        $this->viewVars = ['siteVisit'=>$siteVisit];	
		$this->viewBuilder()->setLayout('pdf');
		
		$CakePdf->viewVars($this->viewVars);	
		$pdf = $CakePdf->output();		
		$pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . 'site_visits' . DS . 'site_visit-' .  $siteVisit->visit_no . '.pdf');
		//debug($siteVisit); exit;

		$this->set(compact('siteVisit'));
	}
	
}



