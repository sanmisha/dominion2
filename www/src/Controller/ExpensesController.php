<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ExpensesController extends AppController
{
	public function initialize() {	
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index', 'list']
        ]);
	}
	
	public function index() {
		$this->set('title_for_layout', 'Expenses');

		$query = $this->Expenses
        	->find('search', ['search' => $this->request->getQueryParams()]);
			$this->paginate = ['order'=>['id'=>'DESC']];
		$this->set('expenses', $this->paginate($query));
        $this->set('_serialize', ['expenses']);   		
	}

	public function edit($id=null) {
		$this->set('title_for_layout', 'Expenses');

		if(empty($id)) {
			$expense = $this->Expenses->newEntity();
			$expense['voucher_no']=$this->Expenses->getNo();

		} else {
			$expense = $this->Expenses->get($id);
		}

		if($this->request->is(['patch','post','put'])) {			
			$expense = $this->Expenses->patchEntity($expense, $this->request->getData());	
			if($this->Expenses->save($expense)) {
				$this->Flash->success('The Expense has been saved');
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error('The Expense could not be saved try again');
			}
		}
		$project = $this->Expenses->Projects->find('list',['conditions'=>['Projects.status'=>'Open']]);
		$expensehead = $this->Expenses->ExpenseHeads->find('list');
		$this->set(compact('expense','project','expensehead'));
	}

	public function delete($id=null) {
		$this->request->allowMethod(['post','delete']);
		$expense = $this->Expenses->get($id);
		
		if($this->Expenses->delete($expense)) {
			$this->Flash->success('The Expense has been deleted');
		} else {
			$this->Flash->error('The Expense could not be deleted try again');
		}
		return $this->redirect(['action'=>'index']);
	}	
}



