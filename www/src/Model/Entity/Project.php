<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Project extends Entity
{

    protected $_accessible = [
        '*' => true,
    ];

    protected $_virtual = ['project_with_client'];

    protected function _getProjectWithClient()
    {
        if(isset($this->client->client_name) && !empty($this->client->client_name)) {
            return $this->project_no . ' ' . $this->client->client_name;
        }
        return $this->project_no;
    }
}
