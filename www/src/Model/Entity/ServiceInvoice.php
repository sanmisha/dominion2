<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ServiceInvoice extends Entity
{

    protected $_accessible = [
        '*' => true,
    ];

    protected $_virtual = ['balance_amount'];

    protected function _getBalanceAmount()
    {
        return $this->total_amount - $this->received_amount;       
    }
}
