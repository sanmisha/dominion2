<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->belongsToMany('Roles');

        $this->hasOne('Employees', [
            'foreignKey' => 'id'
        ]);

        $this->hasOne('Stakeholders', [
            'foreignKey' => 'id'
        ]);

        $this->hasOne('ClientTeams', [
            'foreignKey' => 'id'
        ]);


        $this->hasMany('Projects', [
            'foreignKey' => 'approved_by',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('ProjectTasks', [
            'foreignKey' => 'assigned_by',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * @return \Search\Manager
     */
    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name', 'email']
            ])
            ->add('foo', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                }
            ]
        );
        return $searchManager;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function validationDefault(Validator $validator)
    {
        $validator
			//->requirePresence('username')
            ->notEmpty('username', 'This field is compulsory');
        $validator
			//->requirePresence('email')
            ->notEmpty('email', 'This field is compulsory');
/*        $validator
			->requirePresence('mobile')
			->notEmpty('mobile', 'Please fill this Mobile Number')
			->add('mobile', [
				  'length' => [
					'rule' => ['maxLength', 10],
					'message' => 'Mobile No must be 10 digit',
				]
			]);
            */
        $validator
            //->requirePresence('password')
            ->notEmpty('password', 'Please enter Password')
			->add('password', ['checkPassword' => [
				'rule' => 'checkPassword',
				'provider'=>'table',
				'message' => 'Passwords do not match. Please, try again.',
			]]
        );

        return $validator;
    }

    public function checkPassword($check, array $context) {

        if(isset($context['data']['verify_password'])) {
            if (isset($context['data']['password'])) {
                if ($check != $context['data']['verify_password']) {
                    return false;
                }
            }
        }
		return true;
	}

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['mobile']));
        return $rules;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $this->hasOne('Employees', [
            'foreignKey'=>'id'
        ]);

        $this->hasOne('Clients', [
            'foreignKey'=>'id'
        ]);

        $this->hasOne('Stakeholders', [
            'foreignKey'=>'id'
        ]);

        $this->hasOne('ClientTeams', [
            'foreignKey'=>'id'
        ]);

        $query
            ->where(['Users.active' => 1])
            ->contain(['Roles','Employees','Clients','Stakeholders','ClientTeams']);
        return $query;
    }
}
