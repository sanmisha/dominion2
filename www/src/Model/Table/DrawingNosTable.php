<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class DrawingNosTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('drawing_no');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {

        $validator->notEmpty('section', 'This field is compulsory');
        $validator->notEmpty('document', 'This field is compulsory');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['drawing_stage', 'drawing_type']
            ]
        );
        return $searchManager;
    }
}
