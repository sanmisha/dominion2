<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;


class MembersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->setTable('members');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Muffin/Footprint.Footprint');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
       /* $this->addBehavior('Seipkon.DateFormat', [
			'fields' => ['joining_date','leaving_date','dob']
		]);*/

        $this->hasOne('Users', [
            'foreignKey' => 'id'
        ]);
        
       $this->hasMany('DeliveryAddresses', [
            'foreignKey' => 'member_id'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id'
        ]);
		$this->belongsTo('States', [
            'foreignKey' => 'state_id'
        ]);
    }
    
    public function validationDefault(Validator $validator) {
	    $validator
			->notEmptyString('first_name', 'Please enter Name');
        $validator
            ->notEmptyString('last_name', 'Please enter Name');

     /*   $validator
        //->requirePresence('billing_address_1')
        ->notEmpty('billing_address_1', 'Please fill this Billing Address 1');

        $validator
        //->requirePresence('billing_address_2')
        ->notEmpty('billing_address_2', 'Please fill this Billing Address 2');

        $validator
        //->requirePresence('country_id')
        ->notEmpty('country_id', 'Please fill this Country');

        $validator
        //->requirePresence('state_id')
        ->notEmpty('state_id', 'Please fill this State');

        $validator
        //->requirePresence('city')
        ->notEmpty('city', 'Please fill this City');

        $validator
        //->requirePresence('pincode')
        ->notEmpty('pincode', 'Please fill this Mobile Number')
        ->add('pincode', [
                'length' => [
                'rule' => ['minLength', 6],
                'message' => 'pincode must be 6 digit',
            ]
        ]);*/
            
	   return $validator;
    }

    public function save2($data, $user) {  
       
    	$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        $fullName = $data['member']['first_name'].' '.$data['member']['last_name'];
    	$data['name'] = $fullName;
        $data['roles']['_ids'][] = 10;
    
        $user = $this->Users->patchEntity($user, $data, [
            'associated' => ['Roles', 'Members']
        ]);        

        
        $val = $val && $this->Users->save($user);
        
		if($val){
			$connection->commit();
		} else {
			$connection->rollback();
        }
		return $val;
	}
	
	public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('name', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['first_name','last_name']
            ])->add('search', 'Search.Callback', [
			'callback' => function ($query, $args, $filter) {
				$query->where(['OR'=>[
                    'Members.pincode LIKE'=>"%".$args['search']."%",
                    'Users.email LIKE'=>"%".$args['search']."%",
                    'Users.mobile LIKE'=>"%".$args['search']."%"
                ]])->contain(['Users']);
			}
		]);
        return $searchManager;
    } 


  
	
}
