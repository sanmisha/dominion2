<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class SiteVisitDetailsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('site_visit_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SiteVisits', [
            'foreignKey' => 'site_visit_id',
        ]);
        $this->belongsTo('CheckListSections', [
            'foreignKey' => 'check_list_section_id',
        ]);
        $this->belongsTo('CheckListItems', [
            'foreignKey' => 'check_list_item_id',
        ]);
      
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->notEmpty('check_list_section_id', 'this field is required');
       

        return $validator;
    }   

   
}
