<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class InvoiceDetailsTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoice_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');


		$this->belongsTo('ProjectTasks', [
            'foreignKey' => 'project_task_id'
        ]);

		$this->belongsTo('Tasks', [
            'foreignKey' => 'task_id'
        ]);

		$this->belongsTo('Invoice', [
            'foreignKey' => 'invoice_id'
        ]);
    }

      public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('fees', 'This field is compulsory');

		return $validator;
    }
}

