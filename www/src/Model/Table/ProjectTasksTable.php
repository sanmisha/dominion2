<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ProjectTasksTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('project_tasks');
        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['target_date']
        ]);

    	$this->belongsTo('Projects', [
            'foreignKey' => 'project_id'
        ]);

        $this->belongsTo('AssignedUser', [
            'className' => 'Users',
            'foreignKey' => 'assigned_by'

        ]);

        $this->belongsTo('AssignedEmployee', [
            'className' => 'Employees',
            'foreignKey' => 'assigned_to'
        ]);

        $this->belongsTo('ResponsibleEmployee1', [
            'className' => 'Employees',
            'foreignKey' => 'responsible_1'
        ]);

        $this->belongsTo('ResponsibleEmployee2', [
            'className' => 'Employees',
            'foreignKey' => 'responsible_2'
        ]);

		$this->belongsTo('Tasks', [
            'foreignKey' => 'task_id'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('project_id', 'This field is compulsory');
        $validator->notEmpty('task_id', 'This field is compulsory');
        $validator->notEmpty('fees_type', 'This field is compulsory');
        $validator->notEmpty('days_require', 'This field is compulsory');
        $validator->notEmpty('list_order', 'This field is compulsory');
        $validator->notEmpty('percentage', 'This field is compulsory');
        $validator->notEmpty('fees', 'This field is compulsory');
        $validator->notEmpty('assigned_to', 'This field is compulsory');
        $validator->notEmpty('target_date', 'This field is compulsory');
        $validator->notEmpty('responsible_1', 'This field is compulsory');
        $validator->notEmpty('responsible_2', 'This field is compulsory');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['Projects.project_no','Tasks.task']
            ]
        );
        return $searchManager;
    }

}

