<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class InvoiceReceiptDetailsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoice_receipt_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('InvoiceReceipts', [
            'foreignKey' => 'invoice_receipt_id',            
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

      

        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['invoice_receipt_id'], 'InvoiceReceipts'));      

        return $rules;
    }

  
}
