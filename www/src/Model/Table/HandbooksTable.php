<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class HandbooksTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');
        $this->setDisplayField('topic');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Designations', [
            'className' => 'Designations',
            'foreignKey' => 'designation_id'
        ]);

        $this->belongsTo('Departments', [
            'className' => 'Departments',
            'foreignKey' => 'department_id'
        ]);

		$this->addBehavior('Proffer.Proffer', [
            'attachment_1' => [
                'dir' => 'attachment_1_dir'
            ],
            'attachment_2' => [
                'dir' => 'attachment_2_dir'
            ],

        ]);
    }

    public function validationDefault(Validator $validator) {

        $validator->notEmpty('topic', 'This field is compulsory');
        $validator->allowEmpty('attachment_1');
        $validator->allowEmpty('attachment_2');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['topic']
            ]
        );
        return $searchManager;
    }
}
