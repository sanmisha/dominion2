<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class CheckListItemsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('check_list_items');
        $this->setDisplayField('item');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->belongsTo('CheckListSections', [
            'foreignKey' => 'check_list_section_id',
        ]);
    }

  
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('item')
            ->maxLength('item', 255)
            ->notEmpty('item', 'This field is require');
        $validator
            ->notEmpty('check_list_section_id', 'this field is require');

        return $validator;
    }

   
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['check_list_section_id'], 'CheckListSections'));

        return $rules;
    }
    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['item']
            ]
        );
        return $searchManager;
    }
}
