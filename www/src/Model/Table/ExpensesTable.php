<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class ExpensesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setTable('expenses');  
        $this->setDisplayField('amount');
        $this->setPrimaryKey('id');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['date']
        ]);
        $this->belongsTo('Projects', [
            'className' => 'Projects',
            'foreignKey' => 'project_id'
        ]); 
        $this->belongsTo('ExpenseHeads', [
            'className' => 'ExpenseHeads',
            'foreignKey' => 'expense_head_id'
        ]); 
    }

    public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('amount', 'This field is compulsory');
        $validator
            ->notEmpty('date', 'This field is compulsory');
        return $validator;
    }   
    public function getNo(){
		$conn=ConnectionManager::get('default');
		$strSql="SELECT IFNULL(RIGHT(MAX(voucher_no),5),0) + 1 AS max_id FROM expenses";
		$result=$conn->execute($strSql)->fetchAll('assoc');
		return str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
     }  

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['voucher_no']
            ]
        );
        return $searchManager;
    }    
}
