<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;

class AssociatesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');

        $this->setTable('associates');
        $this->setDisplayField('associate_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Tree', ['level'=>'level']);

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['joining_date']
		]);

        $this->hasMany('Teams', [
            'foreignKey' => 'associate_id',
            'dependent' => true,
			'cascadeCallbacks' => true,
        ]);

        $this->hasMany('Listings', [
            'foreignKey' => 'associate_id',
            'dependent' => true,
			'cascadeCallbacks' => true,
        ]);        

        $this->belongsTo('Referral', [
            'className' => 'Associates',
            'foreignKey' => 'ref_id'
        ]);         

        $this->belongsTo('LeftAssociate', [
            'className' => 'Associates',
            'foreignKey' => 'left_id'
        ]);  
                
        $this->belongsTo('RightAssociate', [
            'className' => 'Associates',
            'foreignKey' => 'right_id'
        ]);          
        
        $this->addBehavior('Proffer.Proffer', [
            'logo' => [
                'dir' => 'logo_dir',
                'thumbnailSizes' => [
                    'proper' => [
                        'w' => 400,
                        'fit' => true,
                        'jpeg_quality' => 100
                    ]
                ],                
            ],
            'letterhead' => [
                'dir' => 'letterhead_dir',
                'thumbnailSizes' => [
                    'proper' => [
                        'w' => 400,
                        'fit' => true,
                        'jpeg_quality' => 100
                    ]
                ],                
            ],
        ]);        
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['associate_name']
            ]
        )->add('active', 'Search.Callback', [
			'callback' => function ($query, $args, $filter) {
                if($args['active'] == 'Active') {
                    $query->where(['Associates.active'=>true]);
                } elseif($args['active'] == 'Non Active') {
                    $query->where(['Associates.active'=>false]);
                }
			}
        ])
        ->add('referral', 'Search.Callback', [
			'callback' => function ($query, $args, $filter) {
                $query->where(['Referral.associate_name LIKE '=>'%'.$args['referral'].'%']);
			}
		]);
        return $searchManager;
    }    
    
    public function add($entity, $data, $side) { 
		$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();       
        
        $entity = $this->patchEntity($entity, $data);
        
        if($entity->ref_id == 1) {
            // For Root
            $entity->parent_id = 1;
            $val = $val && $this->save($entity);
        }  else {
            $ref = $this->get($entity->ref_id);
            $parent = $this->findParent($ref, $side);
            $entity->parent_id = $parent->id;

            $val = $val && $this->save($entity);

            if($side == 'left') {
                $parent->left_id = $entity->id;
            }
            if($side == 'right') {
                $parent->right_id = $entity->id;
            }
            $val = $val && $this->save($parent);
        }

        $Users = TableRegistry::get('Users');

	    $data = [
            'name' => $entity->associate_name,
            'username' => $data['username'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' =>$data['password'],
            'activation_key' => Security::hash('sanjeev', 'sha1', true),            
            
            'roles'=>[
                '_ids'=> [4]
            ],
            
            'team'=>[
				'associate_id'=>$entity->id,
				'name' => $entity->associate_name,
			]
        ];

        $user = $Users->newEntity();
        $user = $Users->patchEntity($user,$data,[
            'associated'=>['Roles','Teams']
        ]);

        $val = $val && $Users->save($user);

        if($val){
			$connection->commit();
		} else {
			$connection->rollback();
        }
		return $val;
    }

    public function activate($entity, $data = null) { 
		$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();   
        
        if(!empty($data)) {
            $entity = $this->patchEntity($entity, $data);
        }
        
        $entity->active = true;
        $val = $val && $this->save($entity);

        $Payouts = TableRegistry::get('Payouts');
        $Payouts->updateAll(
            [
                'payout_date'=>\DateTime::createFromFormat("Y-m-d", $entity->joining_date),
                'approved'=>1
            ],                                
            [
                'associate_id'=>$entity->id,
                'approved'=> 0
            ]                                
        );  
        $path = $this->find('path', ['for'=>$entity->id]);
        foreach ($path as $parent) {
            if(!in_array($parent->id, [1, $entity->id])) {    
                $leftCount = 0;
                if(!empty($parent->left_id)) {
                    $leftCount = $this->find('children', ['for' => $parent->left_id])
                        ->where(['active'=>true, 'associate_type'=>$entity->associate_type])
                        ->count();

                    $left = $this->get($parent->left_id);
                    if($left->active && $left->associate_type == $entity->associate_type) {
                        $leftCount++;
                    }
                }

                $rightCount = 0;
                if(!empty($parent->right_id)) {
                    $rightCount = $this->find('children', ['for' => $parent->right_id])
                        ->where(['active'=>true, 'associate_type'=>$entity->associate_type])
                        ->count();                    

                    $right = $this->get($parent->right_id);
                    if($right->active && $right->associate_type == $entity->associate_type) {
                        $rightCount++;
                    }
                }

                $parent->{$entity->associate_type . "_left"} = $leftCount;
                $parent->{$entity->associate_type . "_right"} = $rightCount;
                if($leftCount!=0 && $rightCount!=0) {
                    if($leftCount == $rightCount) {
                        if($parent->{$entity->associate_type . "_pairs"} != $leftCount) {
                            $parent->{$entity->associate_type . "_pairs"} = $leftCount;
                        }
                    }
                    if($leftCount < $rightCount) {
                        if($parent->{$entity->associate_type . "_pairs"} != $leftCount) {
                            $parent->{$entity->associate_type . "_pairs"} = $leftCount;
                        }
                    }
                    if($rightCount < $leftCount) {
                        if($parent->{$entity->associate_type . "_pairs"} != $rightCount) {
                            $parent->{$entity->associate_type . "_pairs"} = $rightCount;
                        }
                    }                    
                }
                $payout = $parent->isDirty($entity->associate_type . "_pairs");
                $val = $val && $this->save($parent);
                if($payout) {
                    $eligible = Configure::read('eligible')[$parent->associate_type];

                    if(in_array($entity->associate_type, $eligible) && $parent->{$entity->associate_type . "_pairs"} % 3 != 0) {
                        $leftDirectCount = $this->find('children', ['for' => $parent->left_id])
                            ->where(['active'=>true, 'ref_id'=>$parent->id])
                            ->count();
                        $rightDirectCount = $this->find('children', ['for' => $parent->right_id])
                            ->where(['active'=>true, 'ref_id'=>$parent->id])
                            ->count();         
                            
                        $approved = 0;
                        if($leftDirectCount >= 1 && $rightDirectCount >= 1) {
                            $approved = 1;
                        }
                        $Payouts = TableRegistry::get('Payouts');
                        $payoutEntity = $Payouts->newEntity();
                        $joiningDate = \DateTime::createFromFormat("Y-m-d", $entity->joining_date);
                        $payoutEntity->payout_date = $joiningDate->format('d/m/Y');
                        $payoutEntity->associate_id = $parent->id;
                        $payoutEntity->ref_id = $entity->id;
                        $payoutEntity->pair_type = $entity->associate_type;
                        $payoutEntity->pair_no = $parent->{$entity->associate_type . "_pairs"};
                        $payoutEntity->amount = Configure::read("payouts")[$entity->associate_type];
                        $payoutEntity->admin_charges = $payoutEntity->amount * 0.10;
                        $payoutEntity->payout = $payoutEntity->amount - $payoutEntity->admin_charges;
                        $payoutEntity->approved = $approved;
                        $val = $val && $Payouts->save($payoutEntity);

                        if($approved) {
                            $Payouts->updateAll(
                                [
                                    'payout_date'=>\DateTime::createFromFormat("Y-m-d", $entity->joining_date),
                                    'approved'=>1
                                ],                                
                                [
                                    'associate_id'=>$parent->id,
                                    'approved'=> 0
                                ]                                
                            );
                        }
                    }
                }

                if(!empty($parent->commission)) {
                    $approved = 0;
                    if($parent->active) {
                        $approved = 1;
                    }

                    $Payouts = TableRegistry::get('Payouts');
                    $payoutEntity = $Payouts->newEntity();
                    $joiningDate = \DateTime::createFromFormat("Y-m-d", $entity->joining_date);
                    $payoutEntity->payout_date = $joiningDate->format('d/m/Y');
                    $payoutEntity->associate_id = $parent->id;
                    $payoutEntity->ref_id = $entity->id;
                    $payoutEntity->pair_type = "Commission";
                    $payoutEntity->pair_no = 0;
                    $payoutEntity->amount = $parent->commission;
                    $payoutEntity->admin_charges = $payoutEntity->amount * 0.10;
                    $payoutEntity->payout = $payoutEntity->amount - $payoutEntity->admin_charges;
                    $payoutEntity->approved = $approved;
                    $val = $val && $Payouts->save($payoutEntity);                    
                }
            }
        }
        /* Stakeholders Payout */
        $Stakeholders = TableRegistry::get('Stakeholders');
        $StakeholderPayouts = TableRegistry::get('StakeholderPayouts');

        $stakeholders = $Stakeholders->find()->all();
        foreach($stakeholders as $stakeholder) {
            $stake = $StakeholderPayouts->find()
                ->where([
                    'stakeholder_id'=>$stakeholder->id,
                    'payout_date'=>$entity->joining_date
                ])->first();
            if(!$stake) {
                $stake = $StakeholderPayouts->newEntity();
                $stake->payout_date = $entity->joining_date;
                $stake->stakeholder_id = $stakeholder->id; 
                $stake->{$entity->associate_type} = 0;
                $stake->payout = 0; 
            }
            $stake->{$entity->associate_type} = $stake->{$entity->associate_type} + 1;
            $stake->payout = $stake->payout + $stakeholder->{$entity->associate_type . "_commission"};

            $val = $val && $StakeholderPayouts->save($stake);
        }
        //debug($entity); $connection->rollback(); exit;
        if($val){
            $connection->commit();
		} else {
			$connection->rollback();
        }
		return $val;
    }    
    
    public function findParent($entity, $side) {
        if($side == 'left') {
            if(empty($entity->left_id)) {
                return $entity;
            } else {
                $leftId = $entity->left_id;
                while(!empty($leftId)) {
                    $parent = $this->get($leftId);
                    $leftId = $parent->left_id;
                }
                return $parent;
            }
        }
        if($side == 'right') {
            if(empty($entity->right_id)) {
                return $entity;
            } else {
                $rightId = $entity->right_id;
                while(!empty($rightId)) {
                    $parent = $this->get($rightId);
                    $rightId = $parent->right_id;
                }
                return $parent;                
            }
        }
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('associate_name', 'This field is required');
        /*$validator
            ->notEmpty('joining_date', 'This field is required');*/
        $validator
            ->add('username', 'custom', [
                'rule' => function ($value, $context) {
                    $Users = TableRegistry::get('Users');
                    $query = $Users->find('all',[
                        'conditions' => ['Users.username' => $value]
                    ]);
                    return $query->count()===0;
                },
                'message' => 'Username is already used in the system.',
            ])
            ->requirePresence('username', 'create')
            ->notEmpty('username');            
        
        $validator
            ->notEmpty('address_line_1');      
        $validator
            ->notEmpty('state');      
        $validator
            ->notEmpty('city');      
        $validator
            ->notEmpty('pincode');      
        $validator
            ->requirePresence('mobile', 'create')
            ->maxLength('mobile', 10)
            ->notEmpty('mobile');            
        $validator        
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');          
        $validator        
            ->requirePresence('password', 'create')
            ->notEmpty('password');  
                
        $validator
            ->allowEmpty('logo', 'update');
        $validator
			->allowEmpty('letterhead', 'update');
        return $validator;
    }
}
