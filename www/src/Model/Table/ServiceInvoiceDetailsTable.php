<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class ServiceInvoiceDetailsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_invoice_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->belongsTo('Tasks', [
            'foreignKey' => 'task_id'
        ]);

        $this->belongsTo('ServiceInvoices', [
            'foreignKey' => 'invoice_id',
           
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->notEmpty('service');

        $validator
            ->notEmpty('description');


     
        $validator
            ->decimal('fees')
            ->notEmpty('fees');

         $validator
            ->decimal('discount')
            ->notEmpty('discount');


        $validator
            ->decimal('cgst_rate')
            ->notEmpty('cgst_rate');

        $validator
            ->decimal('sgst_rate')
            ->notEmpty('sgst_rate');

        $validator
            ->decimal('igst_rate')
            ->notEmpty('igst_rate');

        $validator
            ->decimal('amount')
            ->notEmpty('amount');
      

    

        return $validator;
    }

 

  
}
