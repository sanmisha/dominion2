<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class ServiceInvoicesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
        ]);

        $this->addBehavior('Search.Search');
        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['invoice_date']
        ]);

        $this->belongsTo('Tasks', [
            'foreignKey' => 'task_id'
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id'
        ]);

        $this->hasMany('ServiceInvoiceDetails', [
            'foreignKey' => 'invoice_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }


    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('invoice_no')
            ->maxLength('invoice_no', 50)
            ->allowEmptyString('invoice_no');

        $validator
            ->notEmpty('invoice_date');

        $validator
            ->notEmpty('client_id');

        $validator
            ->decimal('amount_before_tax')
            ->notEmpty('amount_before_tax');

        $validator
            ->decimal('cgst')
            ->notEmpty('cgst');

        $validator
            ->decimal('sgst')
            ->notEmpty('sgst');

        $validator
            ->decimal('igst')
            ->notEmpty('igst');

        $validator
            ->decimal('total_amount')
            ->notEmpty('total_amount');


        $validator
            ->boolean('cancel')
            ->allowEmptyString('cancel');

        return $validator;
    }

    public function getNo($date = null){
        if(empty($date)) return;
        $date = \DateTime::createFromFormat('d/m/Y', $date);

        $conn=ConnectionManager::get('default');
		if(in_array($date->format('m'),[1,2,3])) {
			$fromYear = $date->format('Y') - 1;
			$toYear = $date->format('y');

            $fromDate = $date->format('Y') - 1 . '-04-01';
            $toDate = $date->format('Y') . '-03-31';
		} else {
			$fromYear = $date->format('Y');
			$toYear = $date->format('y') + 1;

            $fromDate = $date->format('Y') . '-04-01';
            $toDate = $date->format('Y') + 1 . '-03-31';
        }


		$strSql="
            SELECT IFNULL(MAX(RIGHT(invoice_no, 5)),0) + 1 AS max_id
            FROM service_invoices
            WHERE invoice_date >= '".$fromDate."' AND invoice_date <='".$toDate."'";

        //$result =$this->query($strSql);
        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = $fromYear."-".$toYear."/S".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        return $code;
     }

     public function searchManager() {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['invoice_no', 'Clients.client_name']
            ]
        );
        return $searchManager;
    }


}
