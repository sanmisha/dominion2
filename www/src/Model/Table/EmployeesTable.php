<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;


class EmployeesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->setTable('employees');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Muffin/Footprint.Footprint');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['date_of_joining','dob']
        ]);

        $this->addBehavior('Proffer.Proffer', [
            'id_proof' => [
                'dir' => 'id_proof_dir',
                'thumbnailSizes' => [
                    'proper' => [
                        'w' => 630,
                        'h' => 420,
                        'fit' => true,
                        'jpeg_quality' => 100
                    ]
                ],
            ],
            'address_proof' => [
                'dir' => 'address_proof_dir',
                'thumbnailSizes' => [
                    'proper' => [
                        'w' => 630,
                        'h' => 420,
                        'fit' => true,
                        'jpeg_quality' => 100
                    ]
                ],
            ],
        ]);
		

        $this->hasOne('Users', [
            'foreignKey' => 'id'
        ]);

        $this->belongsTo('Designations', [
            'className' => 'Designations',
            'foreignKey' => 'designation_id'
        ]); 

        $this->belongsTo('Departments', [
            'className' => 'Departments',
            'foreignKey' => 'department_id'
        ]); 

        $this->hasMany('ProjectTasks', [
            'foreignKey' => 'assigned_to',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

       // $this->belongsToMany('Roles'); 
        
    }
    
    public function validationDefault(Validator $validator) {
        
        $validator
            ->allowEmpty('id_proof', 'This field is compulsory'); 

        $validator
            ->allowEmpty('address_proof', 'This field is compulsory'); 

        $validator
            ->notEmpty('name', 'This field is compulsory'); 

        $validator
            ->notEmpty('date_of_joining', 'This field is compulsory'); 
            
        $validator
			->notEmpty('department', 'This field is compulsory'); 
            
        $validator
			->notEmpty('address_line_1', 'This field is compulsory'); 
        
        $validator
            ->notEmpty('address_line_2', 'This field is compulsory');
         
        $validator
			->requirePresence('altternate_mobile')
			->notEmpty('altternate_mobile', 'Please fill this Mobile Number')
			->add('altternate_mobile', [
				  'length' => [
					'rule' => ['minLength', 10],
					'message' => 'Mobile No must be 10 digit',
				]
            ]);
            
        $validator
		    ->requirePresence('pincode')
			->notEmpty('pincode', 'Please fill this Mobile Number')
			->add('mobile_1', [
				  'length' => [
					'rule' => ['minLength', 6],
					'message' => 'pincode must be 6 digit',
				]
			]);
            
        $validator
            ->notEmpty('email', 'This field is compulsory');

        $validator
            ->notEmpty('password', 'This field is compulsory');

	   return $validator;
    }

    public function save2($data, $user) { 
        //debug($user);exit;        
    	$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        $data['name'] = $data['employee']['name'];
    
        $user = $this->Users->patchEntity($user, $data, [
            'associated' => ['Roles', 'Employees']
        ]);  


        
        $val = $val && $this->Users->save($user);

        if($val){
			$connection->commit();
		} else {
			$connection->rollback();
        }
		return $val;
	}
	
	/*public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('name', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name']
            ])->add('search', 'Search.Callback', [
			'callback' => function ($query, $args, $filter) {
				$query->where(['OR'=>[
                    'Employees.pincode LIKE'=>"%".$args['search']."%",
                    'Users.email LIKE'=>"%".$args['search']."%",
                    'Users.mobile LIKE'=>"%".$args['search']."%"
                ]])->contain(['Users']);
			}
		]);
        return $searchManager;*/

        public function searchManager()
        {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name']
            ]
        );
        return $searchManager;
     }  
} 

