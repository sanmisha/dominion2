<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class FollowupsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');  
        $this->setDisplayField('followup_date');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['followup_date']
        ]);
    }

    public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('remarks', 'This field is compulsory');

        $validator
           ->notEmpty('followup_date', 'This field is compulsory');
        
        return $validator;
    }   


    public function save2($followup, $data) { 

		$val = true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
 
        $entity = $this->patchEntity($followup, $data);			
        $val = $val && $this->save($entity);

        $Projects = TableRegistry::get('Projects');
        $projectData = $Projects->get($entity->project_id);
        $projectData['status'] = $entity->project_status;

        $val = $val && $Projects->save($projectData);

        if($val){
			$connection->commit(); 
		} else {
			$connection->rollback();
		}
		return $val;
    } 

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['followup_date']
            ]
        );
        return $searchManager;
    }    
}
