<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class ClientTeamsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setTable('client_teams');
        $this->setDisplayField('name');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['dob']
        ]);

        $this->hasOne('Users', [
            'foreignKey' => 'id'
        ]);

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id'
        ]);

    }

    public function validationDefault(Validator $validator) {

        $validator
            ->notEmpty('name', 'This field is compulsory');

        // $validator
        //     ->notEmpty('site_name', 'This field is compulsory');

        return $validator;
    }

    public function save2($data, $user) {
    	$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        $data['name'] = $data['client_team']['name'];
        $data['roles']['_ids'][] = 6;

        $user = $this->Users->patchEntity($user, $data, [
            'associated' => ['Roles', 'ClientTeams']
        ]);

        $val = $val && $this->Users->save($user);

        if($val){
            $connection->commit();
		} else {
            $connection->rollback();
        }
		return $val;
	}

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name']
            ]
        );
        return $searchManager;
    }

}
