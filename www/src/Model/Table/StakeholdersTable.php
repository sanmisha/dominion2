<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class StakeholdersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');  
        $this->setTable('stakeholders');
        $this->setDisplayField('contractor');

        $this->setPrimaryKey('id');
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->hasOne('Users', [
            'foreignKey' => 'id'
        ]);
    }

    public function save2($data, $user) { 
        //debug($user);exit;        
    	$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        $data['name'] = $data['stakeholder']['contractor'];
        $data['mobile'] = $data['stakeholder']['mobile_1'];
        
        $data['roles']['_ids'][] = 7;
        $user = $this->Users->patchEntity($user, $data, [
            'associated' => ['Roles', 'Stakeholders']
        ]); 
        //debug($user); 
        
        $val = $val && $this->Users->save($user);
       // debug($val);exit();
        
		if($val){
            $connection->commit();
		} else {
			$connection->rollback();
        }
        return $val;
        
    }
    
	

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['stakeholder']
            ]
        );
        return $searchManager;
    } 

   
}
