<?php
namespace App\Model\Table;
use App\Model\Entity\Consultation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
//use Cake\Event\Event;

class ProjectTypesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setTable('project_types');
        $this->setDisplayField('project_type');
        $this->setPrimaryKey('id');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['enquiry_date','next_followup_date']
        ]);
		
		$this->hasMany('ProjectTypeDetails', [
            'foreignKey' => 'project_type_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        
    }

    /*public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('project_type', 'This field is compulsory');
        
        return $validator;
    }*/  

    
	
	public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['project_type']
            ]
        );
        return $searchManager;
    }    
    
   public function save2($projectTypes, $data) { 

		$val = true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        
        $isNew = true;
        if(!empty($data['id'])){
			$isNew = false;
        }
       
        $entity = $this->patchEntity($projectTypes, $data);
        
        $projectType = [];
  
        if(!empty($projectTypes['project_type_details'])) { 
             foreach($projectTypes['project_type_details'] as $i=>$detail) { //debug($projectTypes);exit;
                 $projectType['project_type_details'][$i]['project_type_id'] = $projectTypes['id'];
                 $projectType['project_type_details'][$i]['task_id'] = $detail['task_id'];
                 $projectType['project_type_details'][$i]['fees_type'] = $detail['fees_type'];
                 $projectType['project_type_details'][$i]['days_require'] = $detail['days_require'];
                 $projectType['project_type_details'][$i]['list_order'] = $detail['list_order'];
                 $projectType['project_type_details'][$i]['percentage'] = $detail['percentage'];
                
                 $data = $projectType;
             }
                 
             if($isNew==false){
                 $ProjectTypeDetails = TableRegistry::get('ProjectTypeDetails');
                 $val = $val && $ProjectTypeDetails->deleteAll([
                     'ProjectTypeDetails.project_type_id'=>$projectTypes['id'],
                 ]);
             } 
         
             $entity = $this->patchEntity($projectTypes, $data);			
             $val = $val && $this->save($entity);
         }
		
		if($val){
			$connection->commit();
		} else {
			$connection->rollback();
		}
		return $val;
    }    
}
