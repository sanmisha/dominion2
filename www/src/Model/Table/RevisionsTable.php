<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class RevisionsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['revision_date']
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
           ->notEmpty('revision_date', 'This field is compulsory');
        $validator
            ->notEmpty('remarks', 'This field is compulsory');
        return $validator;
    }
}
