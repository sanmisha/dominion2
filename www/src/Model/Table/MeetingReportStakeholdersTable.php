<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class MeetingReportStakeholdersTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('meeting_report_stakeholders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('MeetingReports', [
            'foreignKey' => 'meeting_report_id',
        ]);
        $this->belongsTo('Stakeholders', [
            'foreignKey' => 'stakeholder_id',
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

   
 
}
