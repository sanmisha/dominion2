<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class ExpenseHeadsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setTable('expense_heads');  
        $this->setDisplayField('expense_head');
        $this->setPrimaryKey('id');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
     
    }

    public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('expense_head', 'This field is compulsory');
       
        return $validator;
    }   

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['expense_head']
            ]
        );
        return $searchManager;
    }    
}
