<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ProjectTypeDetailsTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('project_type_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');


		$this->belongsTo('ProjectTypes', [
            'foreignKey' => 'project_type_id'
        ]);

		$this->belongsTo('Tasks', [
            'foreignKey' => 'task_id'
        ]);
    }

      public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

		return $validator;
    }
}

