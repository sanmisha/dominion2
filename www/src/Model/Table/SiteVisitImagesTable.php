<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class SiteVisitImagesTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('site_visit_images');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SiteVisits', [
            'foreignKey' => 'site_visit_id',
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'image' => [
                'dir' => 'image_dir'        
            ],
           
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->notEmpty('caption', 'this field is required');
        $validator
            ->notEmpty('remarks', 'this field is required');

        $validator
            ->notEmpty('image', 'this field is required');


      
        return $validator;
    }

   
   
}
