<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class SiteVisitsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');  
        $this->setTable('site_visits');
        $this->setDisplayField('visit_no');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['visit_date']
        ]);
        $this->addBehavior('Neptune.TimeFormat', [
			'fields' => ['time']
        ]);
        $this->belongsTo('Employees', [
            'className' => 'Employees',
            'foreignKey' => 'employee_id'
        ]); 
        $this->belongsTo('CheckListSections', [
            'foreignKey' => 'check_list_section_id',
        ]);
        $this->hasMany('SiteVisitImages', [
            'foreignKey' => 'site_visit_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace',
        ]);     
        $this->hasMany('SiteVisitDetails', [          
            'foreignKey' => 'site_visit_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
            'saveStrategy'=>'replace',
        ]); 
        $this->belongsTo('Projects', [
            'className' => 'Projects',
            'foreignKey' => 'project_id',
        ]); 
        $this->hasMany('SiteVisitStakeholders', [
            'foreignKey' => 'site_visit_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('SiteVisitVisitors', [
            'foreignKey' => 'site_visit_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
      
    }

   public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('visit_no', 'This field is compulsory');     

        $validator
            ->notEmpty('project_id', 'This field is compulsory');

        $validator
            ->notEmpty('visit_time', 'This field is compulsory');


        $validator
            ->notEmpty('employee_id', 'This field is compulsory');
       
        
        return $validator;
    }  
  
    public function getNo($project_id = null, $project_no){
      

        $conn=ConnectionManager::get('default');
		
		$strSql="
            SELECT IFNULL(MAX(RIGHT(visit_no, 4)),0) + 1 AS max_id
			FROM site_visits
            WHERE project_id = '" . $project_id . "'";

        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = $project_no."/S".str_pad($result[0]['max_id'],4,'0',STR_PAD_LEFT);
        return $code;
     }


    

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['visit_no', 'Employees.name']
            ]
        );
        return $searchManager;
    }    
}
