<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class DailyTasksTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['due_date', 'done_date']
        ]);

    	$this->belongsTo('Projects', [
            'foreignKey' => 'project_id'
        ]);

    	$this->belongsTo('Activities', [
            'foreignKey' => 'activity_id'
        ]);

        $this->belongsTo('AssignedEmployee', [
            'className' => 'Employees',
            'foreignKey' => 'assigned_to'
        ]);

        $this->belongsTo('CreatedEmployee', [
            'className' => 'Employees',
            'foreignKey' => 'created_by'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('task', 'This field is compulsory');
        $validator
            ->notEmpty('assigned_to', 'This field is compulsory');
         $validator
            ->notEmpty('due_date', 'This field is compulsory');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['task','description']
            ]
        );
        return $searchManager;
    }

}

