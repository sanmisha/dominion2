<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;


class MeetingReportsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('meeting_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['meeting_date']
        ]);
        $this->addBehavior('Neptune.TimeFormat', [
			'fields' => ['time']
        ]);

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('MeetingReportImages', [
            'foreignKey' => 'meeting_report_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('MeetingReportStakeholders', [
            'foreignKey' => 'meeting_report_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('MeetingReportVisitors', [
            'foreignKey' => 'meeting_report_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');     

        $validator          
            ->notEmpty('meeting_date');

        $validator           
            ->notEmpty('meeting_time');

        $validator           
            ->notEmpty('employee_id'); 

        $validator           
            ->notEmpty('project_id');       

      

        return $validator;
    }

    public function getNo($project_id = null, $project_no){
      

        $conn=ConnectionManager::get('default');
		
		$strSql="
            SELECT IFNULL(MAX(RIGHT(meeting_no, 4)),0) + 1 AS max_id
			FROM meeting_reports
            WHERE project_id = '" . $project_id . "'";

        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = $project_no."/M".str_pad($result[0]['max_id'],4,'0',STR_PAD_LEFT);
        return $code;
     }


    

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['meeting_no', 'Employees.name']
            ]
        );
        return $searchManager;
    }    

   
}
