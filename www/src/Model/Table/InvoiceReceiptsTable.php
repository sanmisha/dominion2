<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class InvoiceReceiptsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoice_receipts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['receipt_date', 'cheque_date', 'transaction_date']
        ]);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Search.Search');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
        ]);
        $this->hasMany('InvoiceReceiptDetails', [
            'foreignKey' => 'invoice_receipt_id',
            'dependent' => true,
        ]);
    }


    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->notEmpty('client_id');
        $validator
            ->notEmpty('payment_mode');
        $validator
            ->notEmpty('receipt_date');

        return $validator;
    }

    public function getNo($receiptDate = null){
        if(empty($receiptDate)) return;
        $receiptDate = \DateTime::createFromFormat('d/m/Y', $receiptDate);

        $conn=ConnectionManager::get('default');
		if(in_array($receiptDate->format('m'),[1,2,3])) {
			$fromYear = $receiptDate->format('Y') - 1;
			$toYear = $receiptDate->format('y');

            $fromDate = $receiptDate->format('Y') - 1 . '-04-01';
            $toDate = $receiptDate->format('Y') . '-03-31';
		} else {
			$fromYear = $receiptDate->format('Y');
			$toYear = $receiptDate->format('y') + 1;

            $fromDate = $receiptDate->format('Y') . '-04-01';
            $toDate = $receiptDate->format('Y') + 1 . '-03-31';
        }

		$strSql="
            SELECT IFNULL(MAX(RIGHT(receipt_no, 5)),0) + 1 AS max_id
			FROM invoice_receipts
            WHERE receipt_date >= '" . $fromDate . "'
            AND receipt_date <= '" . $toDate . "'";

        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = $fromYear."-".$toYear."/R".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        return $code;
     }

    public function searchManager() {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['receipt_no', 'Clients.client_name']
            ]
        );
        return $searchManager;
    }



}
