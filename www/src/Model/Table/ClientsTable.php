<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class ClientsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');
        $this->setDisplayField('client_name');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->hasMany('ClientTeams', [
            'foreignKey' => 'client_id',
            'dependent' => true,
			'cascadeCallbacks' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {

        $validator
            ->notEmpty('client_name', 'This field is compulsory');

        $validator
            ->notEmpty('office_address_line_1', 'This field is compulsory');

        $validator
            ->notEmpty('office_city', 'This field is compulsory');

        $validator
            ->notEmpty('mobile', 'Please fill this Mobile Number')
            ->numeric('mobile', 'Mobile No should be numeric')
			->add('mobile', [
				  'length' => [
					'rule' => ['minLength', 10],
					'message' => 'Mobile No must be 10 digit',
				]
		]);
        $validator
            ->notEmpty('email', 'This field is compulsory');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['client_name']
            ]
        );
        return $searchManager;
    }
}
