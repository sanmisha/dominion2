<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class PayoutsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');

        $this->setTable('payouts');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['payout_date']
		]);

        $this->belongsTo('Associates', [
            'className' => 'Associates',
            'foreignKey' => 'associate_id'
        ]);

        $this->belongsTo('Referral', [
            'className' => 'Associates',
            'foreignKey' => 'ref_id'
        ]);
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['payout_no']
            ]
        );
        return $searchManager;
    }    
}
