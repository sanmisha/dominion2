<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class AnnouncementsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
    }

	 public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
			->requirePresence('announcement')
			->notEmpty('announcement', 'Please fill this field');
    
        return $validator;
    }

   
	
	public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['announcement']
            ]
        );
        return $searchManager;
    }    
	
}
