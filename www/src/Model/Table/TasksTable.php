<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class TasksTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');  
        $this->setDisplayField('task');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->hasMany('ProjectTasks', [
            'foreignKey' => 'task_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
            
        $validator
            ->notEmpty('task', 'This field is compulsory');
        
        return $validator;
    }   

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['task']
            ]
        );
        return $searchManager;
    }    
}
