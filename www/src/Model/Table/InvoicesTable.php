<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class InvoicesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');
        $this->setTable('invoices');
        $this->setDisplayField('invoice_no');
        $this->setPrimaryKey('id');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['invoice_date']
        ]);
        $this->belongsTo('Projects', [
            'className' => 'Projects',
            'foreignKey' => 'project_id'
        ]);

        $this->hasMany('InvoiceDetails', [
            'foreignKey' => 'invoice_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /*public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('project_id', 'This field is compulsory');
        $validator
            ->notEmpty('invoice_date', 'This field is compulsory');
        return $validator;
    }*/

    public function getNo($date = null){
        if(empty($date)) return;
        $date = \DateTime::createFromFormat('d/m/Y', $date);

        $conn=ConnectionManager::get('default');
		if(in_array($date->format('m'),[1,2,3])) {
			$fromYear = $date->format('Y') - 1;
			$toYear = $date->format('y');

            $fromDate = $date->format('Y') - 1 . '-04-01';
            $toDate = $date->format('Y') . '-03-31';
		} else {
			$fromYear = $date->format('Y');
			$toYear = $date->format('y') + 1;

            $fromDate = $date->format('Y') . '-04-01';
            $toDate = $date->format('Y') + 1 . '-03-31';
        }

		$strSql="
            SELECT IFNULL(MAX(RIGHT(invoice_no, 5)),0) + 1 AS max_id
            FROM invoices
            WHERE invoice_date >= '".$fromDate."' AND invoice_date <='".$toDate."'";

        //$result =$this->query($strSql);
        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = $fromYear."-".$toYear."/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        return $code;

     }

    public function searchManager() {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['invoice_no']
            ]
        );
        return $searchManager;
    }
}
