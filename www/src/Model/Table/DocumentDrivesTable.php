<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class DocumentDrivesTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');

    	$this->belongsTo('Projects', [
            'foreignKey' => 'project_id'
        ]);

    	$this->belongsTo('DrawingNos', [
            'foreignKey' => 'drawing_no_id'
        ]);

    	$this->belongsTo('Levels');
        $this->belongsTo('Buildings');

        $this->belongsTo('UploadedBy', [
            'className' => 'Users',
            'foreignKey' => 'created_by'
        ]);

		$this->addBehavior('Proffer.Proffer', [
            'document' => [
                'dir' => 'document_dir'
            ]
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('project_id', 'This field is compulsory');
        $validator->notEmpty('document_no_id', 'This field is compulsory');
        $validator->notEmpty('level_id', 'This field is compulsory');
        $validator->notEmpty('building_id', 'This field is compulsory');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['register_no','subject', 'description']
            ]
        );
        return $searchManager;
    }

    public function getNo($type){
        $conn=ConnectionManager::get('default');

		$strSql = "SELECT IFNULL(RIGHT(MAX(register_no),5),0) + 1 AS max_id FROM inward_outwards WHERE inward_outwards.type = '{$type}'";

        $result = $conn->execute($strSql)->fetchAll('assoc');

        if($type == 'Inward') {
            $code = "IN/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        } else {
            $code = "OUT/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        }

        return $code;
     }

}

