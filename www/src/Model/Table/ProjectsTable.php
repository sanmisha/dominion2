<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;


class ProjectsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

		$this->addBehavior('Muffin/Footprint.Footprint');
        $this->setDisplayField('project_with_client');

        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['quotation_date', 'project_date','next_followup_date']
        ]);

		$this->belongsTo('ModifiedUser', [
			'className' => 'Users',
            'foreignKey' => 'modified_by'
		]);

		$this->belongsTo('CreatedUser', [
			'className' => 'Users',
            'foreignKey' => 'created_by'
        ]);

        $this->belongsTo('LeadTypes', [
            'className' => 'LeadTypes',
            'foreignKey' => 'lead_type_id'
        ]);

        $this->belongsTo('ProjectTypes', [
            'className' => 'ProjectTypes',
            'foreignKey' => 'project_type_id'
        ]);

        $this->belongsTo('ClientTeams', [
            'className' => 'ClientTeams',
            'foreignKey' => 'client_team_id'
        ]);

        $this->belongsTo('Clients', [
            'className' => 'Clients',
            'foreignKey' => 'client_id'
        ]);

        $this->hasMany('ProjectTasks', [
            'foreignKey' => 'project_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);

        $this->hasMany('Revisions', [
            'foreignKey' => 'project_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('ProjectStakeholders', [
            'foreignKey' => 'project_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy'=>'replace'
        ]);

        $this->belongsTo('PreparedByEmployee', [
            'className' => 'Employees',
            'foreignKey' => 'prepared_by'
        ]);
        $this->belongsTo('ClosedByEmployee', [
            'className' => 'Users',
            'foreignKey' => 'closed_by'
        ]);
        $this->belongsTo('ArchivedByEmployee', [
            'className' => 'Users',
            'foreignKey' => 'archived_by'
        ]);

        $this->belongsTo('ApprovedUser', [
            'className' => 'Users',
            'foreignKey' => 'approved_by'
        ]);

		$this->addBehavior('Proffer.Proffer', [
            'purchase_order_img' => [
                'dir' => 'purchase_order_img_dir',
                'thumbnailSizes' => [
                    'proper' => [
                        'w' => 630,
                        'h' => 420,
                        'fit' => true,
                        'jpeg_quality' => 100
                    ]
                ],
            ],
        ]);

    }

    public function validationDefault(Validator $validator) {

        $validator
            ->allowEmpty('visit_charge', 'This field is compulsory');
        $validator
            ->allowEmpty('purchase_order_img', 'update');
        $validator
            ->notEmpty('close_reason', 'This field is compulsory');
        $validator
            ->notEmpty('archive_status', 'This field is compulsory');

        return $validator;
    }


    /*public function getCode($project) {
        $strSql="SELECT IFNULL(MAX(RIGHT(project_no,5)),0) + 1 AS max_id FROM projects WHERE LENGTH(project_no) <= 10;";
        $id =$this->query($strSql);

        if($project=="Architecture") {
            $code = "AR/".str_pad($id[0][0]['max_id'],5,'0',STR_PAD_LEFT);
        } else if($project=="Interior") {
            $code = "INT/".str_pad($id[0][0]['max_id'],5,'0',STR_PAD_LEFT);
        } else if($project=="Other") {
            $code = "O/".str_pad($id[0][0]['max_id'],5,'0',STR_PAD_LEFT);
        }
        return $code;
    }*/


    public function getNo() {
		$conn=ConnectionManager::get('default');
		if(in_array(date('m'),[1,2,3])) {
			$from_date = (date('Y')-1).'-'.'04-01';
			$to_date = (date('Y')).'-'.'03-31';
			$fromYear = date('Y')-1;
			$toYear = date('y');
		} else {
			$from_date = (date('Y')).'-'.'04-01';
			$to_date = (date('Y')+1).'-'.'03-31';
			$fromYear = date('Y');
			$toYear = date('y')+1;
		}
		$strSql="SELECT IFNULL(MAX(RIGHT(quotation_no, 5)),0) + 1 AS max_id FROM projects WHERE `status` = 'Open'";

        //$result =$this->query($strSql);
        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = "Q".$fromYear."-".$toYear."/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        return $code;
     }

     public function getProjectNo(){
		$conn=ConnectionManager::get('default');
		if(in_array(date('m'),[1,2,3])) {
			$from_date = (date('Y')-1).'-'.'04-01';
			$to_date = (date('Y')).'-'.'03-31';
			$fromYear = date('Y')-1;
			$toYear = date('y');
		} else {
			$from_date = (date('Y')).'-'.'04-01';
			$to_date = (date('Y')+1).'-'.'03-31';
			$fromYear = date('Y');
			$toYear = date('y')+1;
		}
		$strSql="SELECT IFNULL(MAX(RIGHT(project_no, 5)),0) + 1 AS max_id FROM projects WHERE `status` = 'Converted'";

        $result=$conn->execute($strSql)->fetchAll('assoc');
        // $code = "P".$fromYear."-".$toYear."/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        $code = "P".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);

        return $code;
     }

     public function revision(){
        $conn=ConnectionManager::get('default');
        $strSql="SELECT IFNULL(RIGHT(MAX(revision),5),0) + 1 AS max_id
			FROM projects";
        $result=$conn->execute($strSql)->fetchAll('assoc');
        $code = str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        return $code;
     }

    public function save2($project) {
		$val = true;
        $connection = ConnectionManager::get('default');
        $connection->begin();

        $project->status = 'Open';

        $val = $val && $this->save($project);

        if($val){
            $connection->commit();
        } else {
            $connection->rollback();
        }
        return $val;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['quotation_no', 'project_no' , 'site_name']
            ]
        );
        return $searchManager;
    }
}
