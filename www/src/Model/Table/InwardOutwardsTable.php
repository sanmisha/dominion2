<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class InwardOutwardsTable extends Table {

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Timestamp');
		$this->addBehavior('Search.Search');

    	$this->belongsTo('Projects', [
            'foreignKey' => 'project_id'
        ]);

    	$this->belongsTo('Locations', [
            'foreignKey' => 'location_id'
        ]);

    	$this->belongsTo('FileNos', [
            'foreignKey' => 'file_id'
        ]);

        $this->belongsTo('AssignedEmployee', [
            'className' => 'Employees',
            'foreignKey' => 'assigned_to'
        ]);

		$this->addBehavior('Proffer.Proffer', [
            'file_copy' => [
                'dir' => 'file_copy_dir'
            ],
            'courier_slip' => [
                'dir' => 'courier_slip_dir'
            ],

        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('subject', 'This field is compulsory');
        $validator->allowEmpty('file_copy');
        $validator->allowEmpty('courier_slip');

        return $validator;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['register_no','subject', 'description']
            ]
        );
        return $searchManager;
    }

    public function getNo($type){
        $conn=ConnectionManager::get('default');

		$strSql = "SELECT IFNULL(RIGHT(MAX(register_no),5),0) + 1 AS max_id FROM inward_outwards WHERE inward_outwards.type = '{$type}'";

        $result = $conn->execute($strSql)->fetchAll('assoc');

        if($type == 'Inward') {
            $code = "IN/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        } else {
            $code = "OUT/".str_pad($result[0]['max_id'],5,'0',STR_PAD_LEFT);
        }

        return $code;
     }

}

