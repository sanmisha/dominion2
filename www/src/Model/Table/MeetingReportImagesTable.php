<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class MeetingReportImagesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('meeting_report_images');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('MeetingReports', [
            'foreignKey' => 'meeting_report_id',
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'image' => [
                'dir' => 'image_dir'        
            ],
           
        ]);
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');
            
        $validator
            ->notEmpty('remarks', 'this field is required');

        $validator
            ->notEmpty('image', 'this field is required');


     

        return $validator;
    }

  
}
