<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ServiceReceiptInvoicesTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_receipt_invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ServiceReceipts', [
            'foreignKey' => 'service_receipt_id',
        ]);
        
        $this->belongsTo('ServiceInvoices', [
            'foreignKey' => 'service_invoice_id',
        ]);
    }

  
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

     
        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['service_receipt_id'], 'ServiceReceipts'));      

        return $rules;
    }

   

   
  
}
