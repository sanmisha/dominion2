<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;


class TeamsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->setTable('teams');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
		$this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Neptune.DateFormat', [
			'fields' => ['joining_date','leaving_date']
		]);

        $this->belongsTo('Associates', [
            'foreignKey' => 'associate_id'
        ]);
        $this->hasOne('Users', [
            'foreignKey' => 'id'
        ]);
    }


	/*public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
			->requirePresence('address_line_1')
			->notEmpty('address_line_1', 'Please fill this Name');
		return $validator;
    }*/
	

    public function save2($data, $user) {  //debug($data);exit;
		$val=true;
        $connection = ConnectionManager::get('default');
        $connection->begin();
        $data['team']['name'] = $data['name'];
        
		$user = $this->Users->patchEntity($user, $data, [
            'associated' => ['Roles', 'Teams']
        ]);
        $val = $val && $this->Users->save($user);
		if($val){
			$connection->commit();
		} else {
			$connection->rollback();
        }
		return $val;
	}
	
	public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager            
            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name']
            ]
        );
        return $searchManager;
    }    
	
}
