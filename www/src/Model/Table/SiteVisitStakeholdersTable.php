<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SiteVisitStakeholdersTable extends Table
{
  
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('site_visit_stakeholders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

      
        $this->belongsTo('Stakeholders', [
            'foreignKey' => 'stakeholder_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

   
}
