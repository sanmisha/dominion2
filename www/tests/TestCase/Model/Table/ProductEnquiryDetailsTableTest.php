<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductEnquiryDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductEnquiryDetailsTable Test Case
 */
class ProductEnquiryDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductEnquiryDetailsTable
     */
    public $ProductEnquiryDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_enquiry_details',
        'app.enquiries',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductEnquiryDetails') ? [] : ['className' => ProductEnquiryDetailsTable::class];
        $this->ProductEnquiryDetails = TableRegistry::getTableLocator()->get('ProductEnquiryDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductEnquiryDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
