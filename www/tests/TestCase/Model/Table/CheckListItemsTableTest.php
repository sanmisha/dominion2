<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CheckListItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CheckListItemsTable Test Case
 */
class CheckListItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CheckListItemsTable
     */
    public $CheckListItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CheckListItems',
        'app.CheckListSections',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CheckListItems') ? [] : ['className' => CheckListItemsTable::class];
        $this->CheckListItems = TableRegistry::getTableLocator()->get('CheckListItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CheckListItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
