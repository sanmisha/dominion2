<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoreProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoreProductsTable Test Case
 */
class StoreProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StoreProductsTable
     */
    public $StoreProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.store_products',
        'app.products',
        'app.stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StoreProducts') ? [] : ['className' => StoreProductsTable::class];
        $this->StoreProducts = TableRegistry::getTableLocator()->get('StoreProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoreProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
