<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServiceEnquiryDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServiceEnquiryDetailsTable Test Case
 */
class ServiceEnquiryDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServiceEnquiryDetailsTable
     */
    public $ServiceEnquiryDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.service_enquiry_details',
        'app.enquiries',
        'app.services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServiceEnquiryDetails') ? [] : ['className' => ServiceEnquiryDetailsTable::class];
        $this->ServiceEnquiryDetails = TableRegistry::getTableLocator()->get('ServiceEnquiryDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServiceEnquiryDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
