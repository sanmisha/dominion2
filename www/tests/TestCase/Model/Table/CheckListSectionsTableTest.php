<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CheckListSectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CheckListSectionsTable Test Case
 */
class CheckListSectionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CheckListSectionsTable
     */
    public $CheckListSections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CheckListSections',
        'app.CheckListItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CheckListSections') ? [] : ['className' => CheckListSectionsTable::class];
        $this->CheckListSections = TableRegistry::getTableLocator()->get('CheckListSections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CheckListSections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
