<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServiceReceiptsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServiceReceiptsTable Test Case
 */
class ServiceReceiptsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ServiceReceiptsTable
     */
    public $ServiceReceipts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ServiceReceipts',
        'app.Clients',
        'app.ServiceReceiptInvoices',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServiceReceipts') ? [] : ['className' => ServiceReceiptsTable::class];
        $this->ServiceReceipts = TableRegistry::getTableLocator()->get('ServiceReceipts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServiceReceipts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
