<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsQuotationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductsQuotationsTable Test Case
 */
class ProductsQuotationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsQuotationsTable
     */
    public $ProductsQuotations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products_quotations',
        'app.companies',
        'app.clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductsQuotations') ? [] : ['className' => ProductsQuotationsTable::class];
        $this->ProductsQuotations = TableRegistry::getTableLocator()->get('ProductsQuotations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductsQuotations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
