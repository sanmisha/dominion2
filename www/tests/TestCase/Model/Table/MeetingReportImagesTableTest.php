<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MeetingReportImagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MeetingReportImagesTable Test Case
 */
class MeetingReportImagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MeetingReportImagesTable
     */
    public $MeetingReportImages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MeetingReportImages',
        'app.MeetingReports',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MeetingReportImages') ? [] : ['className' => MeetingReportImagesTable::class];
        $this->MeetingReportImages = TableRegistry::getTableLocator()->get('MeetingReportImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MeetingReportImages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
