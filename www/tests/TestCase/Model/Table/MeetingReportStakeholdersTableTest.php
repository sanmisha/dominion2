<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MeetingReportStakeholdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MeetingReportStakeholdersTable Test Case
 */
class MeetingReportStakeholdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MeetingReportStakeholdersTable
     */
    public $MeetingReportStakeholders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MeetingReportStakeholders',
        'app.MeetingReports',
        'app.Stakeholders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MeetingReportStakeholders') ? [] : ['className' => MeetingReportStakeholdersTable::class];
        $this->MeetingReportStakeholders = TableRegistry::getTableLocator()->get('MeetingReportStakeholders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MeetingReportStakeholders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
