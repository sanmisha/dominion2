<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContractInvoiceDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContractInvoiceDetailsTable Test Case
 */
class ContractInvoiceDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContractInvoiceDetailsTable
     */
    public $ContractInvoiceDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contract_invoice_details',
        'app.contract_invoices',
        'app.services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContractInvoiceDetails') ? [] : ['className' => ContractInvoiceDetailsTable::class];
        $this->ContractInvoiceDetails = TableRegistry::getTableLocator()->get('ContractInvoiceDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContractInvoiceDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
