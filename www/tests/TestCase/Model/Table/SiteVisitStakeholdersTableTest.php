<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SiteVisitStakeholdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SiteVisitStakeholdersTable Test Case
 */
class SiteVisitStakeholdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SiteVisitStakeholdersTable
     */
    public $SiteVisitStakeholders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SiteVisitStakeholders',
        'app.Projects',
        'app.Stakeholders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SiteVisitStakeholders') ? [] : ['className' => SiteVisitStakeholdersTable::class];
        $this->SiteVisitStakeholders = TableRegistry::getTableLocator()->get('SiteVisitStakeholders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SiteVisitStakeholders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
