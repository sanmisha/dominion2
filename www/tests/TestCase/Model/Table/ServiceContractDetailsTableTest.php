<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServiceContractDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServiceContractDetailsTable Test Case
 */
class ServiceContractDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServiceContractDetailsTable
     */
    public $ServiceContractDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.service_contract_details',
        'app.service_contracts',
        'app.services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServiceContractDetails') ? [] : ['className' => ServiceContractDetailsTable::class];
        $this->ServiceContractDetails = TableRegistry::getTableLocator()->get('ServiceContractDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServiceContractDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
