<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectStakeholdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectStakeholdersTable Test Case
 */
class ProjectStakeholdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectStakeholdersTable
     */
    public $ProjectStakeholders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProjectStakeholders',
        'app.Projects',
        'app.Stakeholders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProjectStakeholders') ? [] : ['className' => ProjectStakeholdersTable::class];
        $this->ProjectStakeholders = TableRegistry::getTableLocator()->get('ProjectStakeholders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectStakeholders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
