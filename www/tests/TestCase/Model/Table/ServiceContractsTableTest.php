<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServiceContractsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServiceContractsTable Test Case
 */
class ServiceContractsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServiceContractsTable
     */
    public $ServiceContracts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.service_contracts',
        'app.companies',
        'app.clients',
        'app.contract_invoices',
        'app.service_contract_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServiceContracts') ? [] : ['className' => ServiceContractsTable::class];
        $this->ServiceContracts = TableRegistry::getTableLocator()->get('ServiceContracts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServiceContracts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
