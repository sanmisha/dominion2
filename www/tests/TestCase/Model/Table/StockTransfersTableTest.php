<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockTransfersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockTransfersTable Test Case
 */
class StockTransfersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StockTransfersTable
     */
    public $StockTransfers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stock_transfers',
        'app.companies',
        'app.from_stores',
        'app.to_stores',
        'app.stock_transfer_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StockTransfers') ? [] : ['className' => StockTransfersTable::class];
        $this->StockTransfers = TableRegistry::getTableLocator()->get('StockTransfers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StockTransfers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
