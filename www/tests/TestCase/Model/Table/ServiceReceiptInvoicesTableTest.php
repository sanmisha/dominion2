<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServiceReceiptInvoicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServiceReceiptInvoicesTable Test Case
 */
class ServiceReceiptInvoicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ServiceReceiptInvoicesTable
     */
    public $ServiceReceiptInvoices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ServiceReceiptInvoices',
        'app.ServiceReceipts',
        'app.ServiceInvoices',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServiceReceiptInvoices') ? [] : ['className' => ServiceReceiptInvoicesTable::class];
        $this->ServiceReceiptInvoices = TableRegistry::getTableLocator()->get('ServiceReceiptInvoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServiceReceiptInvoices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
