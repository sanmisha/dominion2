<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicesContractTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServicesContractTable Test Case
 */
class ServicesContractTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServicesContractTable
     */
    public $ServicesContract;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.services_contract',
        'app.companies',
        'app.clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServicesContract') ? [] : ['className' => ServicesContractTable::class];
        $this->ServicesContract = TableRegistry::getTableLocator()->get('ServicesContract', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServicesContract);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
