<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicesQuotationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServicesQuotationsTable Test Case
 */
class ServicesQuotationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServicesQuotationsTable
     */
    public $ServicesQuotations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.services_quotations',
        'app.companies',
        'app.clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ServicesQuotations') ? [] : ['className' => ServicesQuotationsTable::class];
        $this->ServicesQuotations = TableRegistry::getTableLocator()->get('ServicesQuotations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServicesQuotations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
