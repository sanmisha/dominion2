<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockTransferDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockTransferDetailsTable Test Case
 */
class StockTransferDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StockTransferDetailsTable
     */
    public $StockTransferDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stock_transfer_details',
        'app.stock_transfers',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StockTransferDetails') ? [] : ['className' => StockTransferDetailsTable::class];
        $this->StockTransferDetails = TableRegistry::getTableLocator()->get('StockTransferDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StockTransferDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
