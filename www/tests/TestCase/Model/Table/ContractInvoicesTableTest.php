<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContractInvoicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContractInvoicesTable Test Case
 */
class ContractInvoicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContractInvoicesTable
     */
    public $ContractInvoices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contract_invoices',
        'app.companies',
        'app.stores',
        'app.contracts',
        'app.clients',
        'app.contract_invoice_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContractInvoices') ? [] : ['className' => ContractInvoicesTable::class];
        $this->ContractInvoices = TableRegistry::getTableLocator()->get('ContractInvoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContractInvoices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
