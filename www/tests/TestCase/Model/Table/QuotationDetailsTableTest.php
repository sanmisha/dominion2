<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuotationDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuotationDetailsTable Test Case
 */
class QuotationDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QuotationDetailsTable
     */
    public $QuotationDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.quotation_details',
        'app.quotations',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QuotationDetails') ? [] : ['className' => QuotationDetailsTable::class];
        $this->QuotationDetails = TableRegistry::getTableLocator()->get('QuotationDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuotationDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
