<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsQuotationDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductsQuotationDetailsTable Test Case
 */
class ProductsQuotationDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsQuotationDetailsTable
     */
    public $ProductsQuotationDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products_quotation_details',
        'app.product_quotations',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductsQuotationDetails') ? [] : ['className' => ProductsQuotationDetailsTable::class];
        $this->ProductsQuotationDetails = TableRegistry::getTableLocator()->get('ProductsQuotationDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductsQuotationDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
