<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockAdjustmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockAdjustmentsTable Test Case
 */
class StockAdjustmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StockAdjustmentsTable
     */
    public $StockAdjustments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stock_adjustments',
        'app.comapnies',
        'app.stores',
        'app.stock_adjustment_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StockAdjustments') ? [] : ['className' => StockAdjustmentsTable::class];
        $this->StockAdjustments = TableRegistry::getTableLocator()->get('StockAdjustments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StockAdjustments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
