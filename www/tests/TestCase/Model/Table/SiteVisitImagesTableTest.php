<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SiteVisitImagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SiteVisitImagesTable Test Case
 */
class SiteVisitImagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SiteVisitImagesTable
     */
    public $SiteVisitImages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SiteVisitImages',
        'app.SiteVisits',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SiteVisitImages') ? [] : ['className' => SiteVisitImagesTable::class];
        $this->SiteVisitImages = TableRegistry::getTableLocator()->get('SiteVisitImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SiteVisitImages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
