<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MeetingReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MeetingReportsTable Test Case
 */
class MeetingReportsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MeetingReportsTable
     */
    public $MeetingReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MeetingReports',
        'app.Employees',
        'app.Projects',
        'app.MeetingReportImages',
        'app.MeetingReportStakeholders',
        'app.MeetingReportVisitors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MeetingReports') ? [] : ['className' => MeetingReportsTable::class];
        $this->MeetingReports = TableRegistry::getTableLocator()->get('MeetingReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MeetingReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
