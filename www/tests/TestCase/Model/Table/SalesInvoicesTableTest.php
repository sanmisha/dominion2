<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesInvoicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesInvoicesTable Test Case
 */
class SalesInvoicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesInvoicesTable
     */
    public $SalesInvoices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_invoices',
        'app.companies',
        'app.stores',
        'app.quotations',
        'app.clients',
        'app.sales_invoice_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SalesInvoices') ? [] : ['className' => SalesInvoicesTable::class];
        $this->SalesInvoices = TableRegistry::getTableLocator()->get('SalesInvoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesInvoices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test searchManager method
     *
     * @return void
     */
    public function testSearchManager()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getNo method
     *
     * @return void
     */
    public function testGetNo()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test save2 method
     *
     * @return void
     */
    public function testSave2()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
