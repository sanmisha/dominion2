<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConsumptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConsumptionsTable Test Case
 */
class ConsumptionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConsumptionsTable
     */
    public $Consumptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.consumptions',
        'app.companies',
        'app.stores',
        'app.teams',
        'app.consumption_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Consumptions') ? [] : ['className' => ConsumptionsTable::class];
        $this->Consumptions = TableRegistry::getTableLocator()->get('Consumptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Consumptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
