
<div class="custom-scroll custom-scroll-light">
	<ul class="sidebar-menu">
		<li class="compact-hide">
			<a href="/Dashboard/index" class="waves-effect  waves-light">
				<span class="s-icon"><i class="ti-anchor"></i></span>
				<span class="s-text">Dashboard</span>
			</a>
		</li>
		<?php
            if(
                $this->AuthUser->hasAccess(['controller'=>'Announcements','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Clients','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'Stakeholders','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Tasks','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'LeadTypes','action' => 'index'])||
				$this->AuthUser->hasAccess(['controller'=>'ProjectTypes','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Activities','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Departments','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Designations','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Employees','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Locations','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'FileNos','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'DrawingNos','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'CheckListSections', 'action'=>'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'CheckListItems', 'action'=>'index'])
		    ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Masters</span>
			</a>
			<ul>
                <li><?= $this->AuthUser->link(__('Announcements'), ['controller'=>'Announcements','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Client Information'), ['controller'=>'Clients','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Stake Holders'), ['controller'=>'Stakeholders','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Projects Tasks'), ['controller'=>'Tasks','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Lead Types'), ['controller'=>'LeadTypes','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Project Types'), ['controller'=>'ProjectTypes','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Activities'), ['controller'=>'Activities','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Departments'), ['controller'=>'Departments','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Designations'), ['controller'=>'Designations','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Employees'), ['controller'=>'Employees','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Locations'), ['controller'=>'Locations','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('File Nos'), ['controller'=>'FileNos','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Drawing Nos'), ['controller'=>'DrawingNos','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Checklist Sections'),['controller'=>'CheckListSections', 'action'=>'index'])?></li>
				<li><?= $this->AuthUser->link(__('Checklist Items'), ['controller'=>'CheckListItems', 'action'=>'index']) ?></li>
			</ul>
		</li>
		<?php } ?>

		<?php
            if(
				$this->AuthUser->hasAccess(['controller'=>'Projects','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Projects','action' => 'ongoingProject'])
		    ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Projects</span>
			</a>
			<ul>
                <li><?= $this->AuthUser->link(__('Quotations'), ['controller'=>'Projects','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Ongoing Projects'), ['controller'=>'Projects','action' => 'ongoingProject']) ?></li>
				<li><?= $this->AuthUser->link(__('Closed Projects'), ['controller'=>'Projects','action' => 'closedProject']) ?></li>
				<li><?= $this->AuthUser->link(__('Archived Projects'), ['controller'=>'Projects','action' => 'archivedProject']) ?></li>
			</ul>
		</li>
		<?php } ?>

		<?php
            if(
				$this->AuthUser->hasAccess(['controller'=>'ProjectTasks','action' => 'projects']) ||
                $this->AuthUser->hasAccess(['controller'=>'DailyTasks','action' => 'index'])
	        ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Task Management</span>
			</a>
			<ul>
                <li><?= $this->AuthUser->link(__('Project Tasks'), ['controller'=>'ProjectTasks','action' => 'projects']) ?></li>
                <li><?= $this->AuthUser->link(__('Daily Tasks'), ['controller'=>'DailyTasks','action' => 'index']) ?></li>
			</ul>
		</li>
		<?php } ?>

		<?php
            if(
                $this->AuthUser->hasAccess(['controller'=>'InwardOutwards','action' => 'index'])
	        ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Inward / Outward</span>
			</a>
			<ul>
                <li><?= $this->AuthUser->link(__('Inward / Outwards'), ['controller'=>'InwardOutwards','action' => 'index']) ?></li>
			</ul>
		</li>
		<?php } ?>

		<?php
            if(
                $this->AuthUser->hasAccess(['controller'=>'DocumentDrives','action' => 'projects'])
	        ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Documents Drive</span>
			</a>
			<ul>
                <li><?= $this->AuthUser->link(__('Document Drive'), ['controller'=>'DocumentDrives','action' => 'projects']) ?></li>
			</ul>
		</li>
		<?php } ?>

		<?php
            if(
                $this->AuthUser->hasAccess(['controller'=>'Expenses','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'Invoices','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'ServiceInvoices','action' => 'index']) 
						
	        ) {
        ?>

		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Invoicing</span>
			</a>
			<ul>
				<li><?= $this->AuthUser->link(__('Expenses'), ['controller'=>'Expenses','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Invoices'), ['controller'=>'Invoices','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Service Invoices'), ['controller'=>'ServiceInvoices','action' => 'index']) ?></li>
		
			</ul>
		</li>
		<?php } ?>

		<?php
			if(			
				$this->AuthUser->hasAccess(['controller'=>'InvoiceReceipts','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'ServiceReceipts','action' => 'index'])
			) {
		?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Receipts</span>
			</a>
			<ul>
				<li><?= $this->AuthUser->link(__('Invoice Receipts'), ['controller'=>'InvoiceReceipts','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Service Receipts'), ['controller'=>'ServiceReceipts','action' => 'index']) ?></li>
			</ul>
		</li>
		<?php } ?>

        <?php
            if(
				$this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'quatations']) ||
                $this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'projects']) ||
                $this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'invoices']) ||
				$this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'serviceInvoices']) ||
				$this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'invoiceReceipt']) ||
				$this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'serviceReceipt']) ||
                $this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'dailyTasks']) ||
                $this->AuthUser->hasAccess(['controller'=>'Reports','action' => 'inwardOutwardRegister'])
            ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Reports</span>
			</a>
			<ul>
				<li><?= $this->AuthUser->link('Quatations', ['controller'=>'Reports','action' => 'quatations']) ?></li>
				<li><?= $this->AuthUser->link('Projects', ['controller'=>'Reports','action' => 'projects']) ?></li>
                <li><?= $this->AuthUser->link('Invoices', ['controller'=>'Reports','action' => 'invoices']) ?></li>
				<li><?= $this->AuthUser->link('Service Invoices', ['controller'=>'Reports','action' => 'serviceInvoices']) ?></li>
				<li><?= $this->AuthUser->link('Invoice Receipts', ['controller'=>'Reports','action' => 'invoiceReceipt']) ?></li>
				<li><?= $this->AuthUser->link('Service Receipts', ['controller'=>'Reports','action' => 'serviceReceipt']) ?></li>
                <li><?= $this->AuthUser->link('Daily Tasks', ['controller'=>'Reports','action' => 'dailyTasks']) ?></li>
                <li><?= $this->AuthUser->link('Inward Outward Register', ['controller'=>'Reports','action' => 'inwardOutwardRegister']) ?></li>
			</ul>
		</li>
        <?php } ?>

		<?php
            if(
				$this->AuthUser->hasAccess(['controller'=>'SiteVisits','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'MeetingReports','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Handbooks','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'ExpenseHeads','action' => 'index']) ||
				$this->AuthUser->hasAccess(['controller'=>'EmailTemplates','action' => 'index'])
	        ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">Others</span>
			</a>
			<ul>
				<li><?= $this->AuthUser->link(__('Site Visits'), ['controller'=>'SiteVisits','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Meeting Reports'), ['controller'=>'MeetingReports','action' => 'index']) ?></li>
                <li><?= $this->AuthUser->link(__('Handbooks'), ['controller'=>'Handbooks','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Expense Heads'), ['controller'=>'ExpenseHeads','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Email Templates'), ['controller'=>'EmailTemplates','action' => 'index']) ?></li>
			</ul>
		</li>
		<?php } ?>

        <?php
            if(
                $this->AuthUser->hasAccess(['controller'=>'Roles','action' => 'index']) ||
                $this->AuthUser->hasAccess(['controller'=>'Users','action' => 'index'])
            ) {
        ?>
		<li class="with-sub">
			<a href="#" class="waves-effect  waves-light">
				<span class="s-caret"><i class="fa fa-angle-down"></i></span>
				<span class="s-icon"><i class="fa fa-file-text"></i></span>
				<span class="s-text">System Administration</span>
			</a>
			<ul>
				<li><?= $this->AuthUser->link(__('Roles'), ['controller'=>'Roles','action' => 'index']) ?></li>
				<li><?= $this->AuthUser->link(__('Users'), ['controller'=>'Users','action' => 'index']) ?></li>
			</ul>
		</li>
        <?php } ?>
	</ul>
</div>
