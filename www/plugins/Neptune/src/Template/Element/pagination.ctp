<div class="box box-block bg-white mb-0">
	<div class="row">
		<div class="col-md-6 col-sm-12 mb-1 mb-md-0">
			<p class="font-90 text-muted mb-1">
				<?php echo $this->Paginator->counter(array(
					'format' => __('<span class="mailbox_count_msg"><strong>{{start}}</strong>-<strong>{{end}}</strong> of <strong>{{count}}</strong></span>')
				));	?>		
			</p>
		</div>
		<div class="col-md-6 col-sm-12 mb-1 mb-md-0">
			<nav class="pull-right">
				<?php
					echo $this->Paginator->numbers();
				?>				
			</nav>
		</div>
	</div>
</div>
