/*
SQLyog Community v13.1.9 (64 bit)
MySQL - 10.4.24-MariaDB : Database - dominion2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dominion2` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `dominion2`;

/*Table structure for table `activities` */

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `activities` */

insert  into `activities`(`id`,`activity`,`modified`,`created`) values 
(1,'Phone Calls','2022-03-10 18:37:26','2022-03-10 18:37:26'),
(2,'Sending Emails','2022-03-10 18:37:33','2022-03-10 18:37:33'),
(3,'Filing','2022-03-11 10:21:29','2022-03-11 10:21:29'),
(4,'Marketing emails','2022-04-07 15:10:06','2022-04-07 15:10:06');

/*Table structure for table `announcements` */

DROP TABLE IF EXISTS `announcements`;

CREATE TABLE `announcements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `announcements` */

insert  into `announcements`(`id`,`announcement`,`modified`,`created`) values 
(5,'Welcome to Dominon. Next Generation ERP Software','2021-05-31 16:20:42','2020-12-28 12:41:59');

/*Table structure for table `buildings` */

DROP TABLE IF EXISTS `buildings`;

CREATE TABLE `buildings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `building` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `buildings` */

insert  into `buildings`(`id`,`project_id`,`building`) values 
(1,1,'Bulding 1'),
(2,2,'A1');

/*Table structure for table `check_list_items` */

DROP TABLE IF EXISTS `check_list_items`;

CREATE TABLE `check_list_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `check_list_section_id` int(11) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;

/*Data for the table `check_list_items` */

insert  into `check_list_items`(`id`,`check_list_section_id`,`item`,`modified`,`created`) values 
(1,1,'All verticals mundhas should be properly fixed in ground.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(2,1,'ss','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(3,1,'All verticle mundhas should be properly fixed in ground.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(4,1,'Railing shall be straight in water level.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(5,1,'Nails used for column center shall be 40 mm.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(6,1,'Railing shall not obstruct vehicular traffic.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(7,1,'Margins','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(8,1,'Front side. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(9,1,'Right side.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(10,1,'Left side. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(11,1,'Rear side. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(12,1,'Road width.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(13,1,'Plinth height decided.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(14,1,'All centres of columns checked.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(15,1,'Diagonal checked.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(16,2,'Excavated till hard strata.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(17,2,'Get the strata checked from structural engineer and go ahead with Soling and PCC.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(18,2,'Strata should not be sloping.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(19,2,'Check whether the excavated pits are de-watered.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(20,2,'Loose materiels in the pit should be properly clean.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(21,2,'Excavation for Column No.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(22,3,'Ensure bed for soling is approved.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(23,3,'Thickness of soling should be 230 mm i.e., 23cm (9\")','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(24,3,'Stones shall be placed vertically and not flat.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(25,3,'Check whether stones are weathered, Porous, soft or decayed.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(26,3,'Ensure small stones (Chippis)filled  properly in gaps. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(27,3,'Check Compaction','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(28,4,'Confirm murum and soling is well compacted. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(29,4,'Surface should not be uneven. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(30,4,'Check Dimension of PCC according to working drawing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(31,4,'Check shuttering box','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(32,5,'Contractor is casting footing for column no.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(33,5,'Check Length, Breadth, Depth of footing ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(34,5,'Check shuttering','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(35,5,'Check if theres any leakage in shuttering','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(36,5,'Steel Reinforcement shall be checked by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(37,5,'Check cover.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(38,5,'Check centre of colums and footing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(39,6,'Contractor is casting column no. upto plinth beam bottom. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(40,6,'Check center of Columns according to column numbers. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(41,6,'All measurement shall be recorded in M.B.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(42,6,'Checking reinforcement for Column by RCC consultant. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(43,6,'Check cover.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(44,7,'Plinth beam ready for casting. Plinth beam no. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(45,7,'Check Cover for Plinth beam. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(46,7,'Check the shuttering.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(47,7,'Check if proper supports are provided to form work.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(48,7,'Checking of reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(49,8,'Earth fiiling completed till plinth. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(50,8,'Check if appropriate & sanctioned Earth filling material is used. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(51,8,'Check that Earth filling has been done in layers.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(52,8,'Watering & Ramming is done properly.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(53,9,'Ensure the bed for soling is approved ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(54,9,'Thickness should be 230 mm, 23cm (9\")','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(55,9,'Stones hall be placed vertically and not flat ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(56,9,'Check whether stones are weathered, Porous, soft or decayed.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(57,9,'Ensure small stones (Chippis)filled  properly in gaps. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(58,9,'Check Compaction','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(59,10,'Confirm murum and soling is well compacted. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(60,10,'Surface should not be uneven. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(61,10,'Check Dimension of PCC according to working drawing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(62,10,'Check shuttering box','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(63,11,'RCC Columns ready for casting upto lintel bottom. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(64,11,'Checking reinforcement in column by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(65,11,'Check the cover for column. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(66,11,'Check the center of column and there faces. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(67,11,'Check form work if theres any leakage.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(68,11,'Casting of Column is completed for column no.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(69,12,'RCC lintle beam ready for casting.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(70,12,'Checking of reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(71,12,'Check the cover for beam. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(72,12,'Check form work if theres any leakage.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(73,12,'Check if proper supports are provided to form work ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(74,13,'Check the marked area for slab casting. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(75,13,'Slab ready to cast mark the column numbers. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(76,13,'Check reinforcement in slab.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(77,13,'Slabs checked by engineer.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(78,13,'Slabs casted no.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(79,14,'Staircase no ready for casting.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(80,14,'Checking reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(81,14,'Check depth of waist slab. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(82,14,'Staircase casting completed for staircase no.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(83,15,'Check shear wall thickness.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(84,15,'Checking reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(85,15,'Check cover for shear wall. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(86,15,'Check thickness for lift raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(87,15,'Check cover for lift raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(88,15,'Casting of Shear wall no & raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(89,16,'Check excavation is complete upto required depth and strata.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(90,16,'Structural engineer has checked and approved strata.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(91,16,'Check soling and PCC for raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(92,16,'Check depth / thickness of raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(93,16,'Check formwork dimension','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(94,16,'Check if theres any leakage in formwork ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(95,16,'Checking reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(96,16,'Check cover for raft.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(97,16,'Casting of walls & raft ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(98,17,'Check the marked area for slab casting. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(99,17,'Slab ready to cast mark the column numbers. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(100,17,'Check reinforcement in slab.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(101,17,'Slabs checked by engineer.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(102,17,'Slabs casted no.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(103,18,'Check excavation is complete upto required depth and strata.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(104,18,'Structural engineer has checked and approved strata.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(105,18,'Check soling and PCC for raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(106,18,'Check depth / thickness of raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(107,18,'Check formwork dimension','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(108,18,'Check if theres any leakage in formwork. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(109,18,'Checking reinforcement by RCC consultant.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(110,18,'Check cover for raft.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(111,18,'Casting of walls & raft. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(112,19,'Brick used should be of uniform shape, size, colour.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(113,19,'Brick work should be placed in single line without mortar. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(114,19,'Location of windows, oors, opening should be approved by architect.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(115,19,'Brick work ready upto lintle level.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(116,19,'Brick work form lintle beam to floor beam ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(117,19,'Brick work ready.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(118,20,'Internal Plaster','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(119,21,'External Plaster','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(120,22,'Flooring','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(121,23,'Electrification','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(122,24,'Plumbing & Sanitation','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(123,25,'Compond Wall,','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(124,26,'Gate','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(125,27,'Security Cabin','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(126,28,'Internal Roads','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(127,29,'Loading & Unloading Platform','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(128,30,'Landscape','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(129,31,'Chimney Foundation','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(130,32,'ETP','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(131,33,'Painting External','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(132,34,'Truss Checked by Engineer ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(133,35,'Purlins & other members of truss. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(134,36,'Crain','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(135,37,'Break, insert plates etc. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(136,38,'Core cut shall be taken and shall be tested in Lab. ','2022-05-13 11:12:44','2022-05-13 11:12:44');

/*Table structure for table `check_list_sections` */

DROP TABLE IF EXISTS `check_list_sections`;

CREATE TABLE `check_list_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `check_list_sections` */

insert  into `check_list_sections`(`id`,`section`,`modified`,`created`) values 
(1,'Line out','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(2,'Excavation & Foundation','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(3,'Soling for Column footing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(4,'PCC for column footing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(5,'Footing.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(6,'Column.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(7,'Plinth beam. ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(8,'Earth filling for plinth.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(9,'Soling at plinth level','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(10,'PCC at plinth level ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(11,'RCC for Column form plinth beam to lintel beams.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(12,'RCC for Lintle beams & Floor beams ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(13,'RCC Floor Slab ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(14,'RCC Staircase ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(15,'Lift ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(16,'U.G Tank','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(17,'Lift machine room top slab ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(18,'Pump room','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(19,'Brick work ','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(20,'Internal Plastering.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(21,'External Plastering.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(22,'Flooring.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(23,'Electric.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(24,'Plumber.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(25,'Compound Wall.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(26,'Gate.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(27,'Security Cabin.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(28,'Internal road.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(29,'Loading & Unloading.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(30,'Landscape.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(31,'Chimney Foundation.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(32,'ETP','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(33,'External Painting.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(34,'Truss checked.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(35,'Purlins & other members of truss.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(36,'Crain.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(37,'Break, Insert Plates.','2022-05-13 11:12:44','2022-05-13 11:12:44'),
(38,'Core cut of RCC.','2022-05-13 11:12:44','2022-05-13 11:12:44');

/*Table structure for table `client_teams` */

DROP TABLE IF EXISTS `client_teams`;

CREATE TABLE `client_teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `alternative_mobile` varchar(20) DEFAULT NULL,
  `mobile_1` varchar(10) DEFAULT NULL,
  `mobile_2` varchar(10) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;

/*Data for the table `client_teams` */

insert  into `client_teams`(`id`,`client_id`,`name`,`designation`,`department`,`alternative_mobile`,`mobile_1`,`mobile_2`,`dob`,`site_name`,`address_line_1`,`address_line_2`,`city`,`pincode`) values 
(19,1,'Shrinivas D, Shembekar','General Manager Project','',NULL,NULL,NULL,NULL,'AOPL Unit 4','Plot No. C 8/6 & 8/7, MIDC Ambernath ','Near AMP Gate, Ambernath','Thane',421506),
(21,2,'Ganesh Kinikar','','',NULL,NULL,NULL,NULL,'Plot No. PAP 52 & 53','Next to MIDC Office','Anand Nagar, Add. MIDC ','Ambernath',421501),
(22,3,'Pradeep Choudhary','','',NULL,NULL,NULL,NULL,'Plot No. OS-16','D-53,55,57,60','MIDC. Phase II, Kalyan-Shill Road','Dombivli',421204),
(23,3,'Pradeep Choudhary','','',NULL,NULL,NULL,NULL,'Plot No. D 53 & 54','MIDC, Phase II','Kalyan-Shill Road','Dombivli',421204),
(24,134,'Sunil Joshi','','',NULL,NULL,NULL,NULL,'Plot No. W-100','MIDC, Phase II','MIDC.','Dombivli',421204),
(25,135,'Mr. Bhaskar','Admin','',NULL,NULL,NULL,NULL,'Plot No. W-34 ','MIDC, Phase-II,','Dombivli Industrial Area','Dombivli East , Thane',421203),
(26,135,'Mr. Bhaskar','Admin','',NULL,NULL,NULL,NULL,'Plot No. C-96 & C-97 ','MIDC Industrial Zone','MAHAD MIDC','MAHAD ',NULL),
(27,136,'Balabhadra samantara','','',NULL,NULL,NULL,NULL,'Plot No.3/1','Taloja MIDC, Dist- Raigad','','Taloja',410208),
(28,81,'Hariharan','Manager','Customs',NULL,NULL,NULL,NULL,'Plot No. T2','Plot No. T2, Taloja Industrial Area ','MIDC, Taloja , Dist. Raigad. ','Taloja',410208),
(29,102,'Vinayak Patil','Manager','',NULL,NULL,NULL,NULL,'Plot No. B-27/29','Phase 1, MIDC','MIDC Industrial Area','Dombivli',421203),
(30,137,'Ganpat Patil','MD','',NULL,NULL,NULL,NULL,'Plot No. 126/1','Chikhaloli, MIDC','MIDC Industrial Area','Ambernath',421505),
(31,138,'Sachetan Gharat','','',NULL,NULL,NULL,NULL,'Plot Infra Narhen','Narhen Village Industrial ','Logistic Park, Palava','Dombivli',NULL),
(32,139,'Mr. Prasoon Kumar Shah','Partner','',NULL,NULL,NULL,NULL,'Plot No. W17','MIDC Badlapur','','Thane',NULL),
(33,140,'Mr Sanjeev Kumar Mandholia','','',NULL,NULL,NULL,NULL,'B-42, ','TTC Industrial area, , Village Elthan, Kalwa Block, Digha, ','MIDC Pipe Line Road , Airoli ,','Navi Mumbai',-3),
(34,102,'Mr. Vinayak A Patil','','',NULL,NULL,NULL,NULL,'A-84, MIDC','MIDC Phase I','Dombivli East ','Thane ',421203),
(35,141,'Mr. Niranjan Athalye','','',NULL,NULL,NULL,NULL,'Ganadish Co-operative Housing Society Ltd., ','Ganadish Co-operative Housing Society Ltd., Plot No.: R/19','Dombivli (East). ','Sudershan Nagar, MIDC Residential Zone , ',421203),
(37,142,'Kamlesh Kumar Lader','','',NULL,NULL,NULL,NULL,'Plot No. - G15, G-16 & G-17, ','Plot No. - G15, G-16 & G-17, Taloja MIDC Industrial Area, ','At Village - Padge, ','Panvel ',410208),
(39,81,'Rohit Soni','','',NULL,NULL,NULL,NULL,'IGPL- T2/part','T2/Part, ','Taloja MIDC','Taloja',NULL),
(40,97,'Ankush Ajagekar','Sr.Officer ','Procurement',NULL,NULL,NULL,NULL,'Plot No. 3/1','Plot No. 3/1, ','Maharashtra','Raidag',410208),
(41,92,'Mr. Maruti Darur','','',NULL,NULL,NULL,NULL,'Plot No.: 74, Ambernath MIDC','Plot No.: 74, MIDC Chickaloli','MIDC Ambernath','Thane ',NULL),
(43,108,'Mr. Ardhendu Purkayastha','','',NULL,NULL,NULL,NULL,'W-40, W-41, W-42','W-40, W-41, W-42','','',NULL),
(44,108,'Mr. Shripati','','',NULL,NULL,NULL,'2022-01-18','W-256, W-257  & W-258','W-256, W-257  & W-258','MIDC Dombivli East','Thane ',421204),
(45,143,'Mr. Ardhendu.Purkayastha','','',NULL,NULL,NULL,NULL,'W-40/41/42, MIDC Ambernath West','W-40/41/42, MIDC Industrial Area, ','Morivali Village, Ambernath West, ','Thane. ',NULL),
(46,3,'Pradeep Choudhary','','',NULL,NULL,NULL,NULL,'Plot No.: D-55/60','Plot No. D-55 / 60, ','MIDC Phase II, Kalyan Shill Road, Dombivli','Thane ',421204),
(47,144,'Mr.Kaushik ','','',NULL,NULL,NULL,NULL,'Hotel Project ','Dombivli ','','Thane ',NULL),
(48,149,'Mr. Jayesh Awati','Company Secretary','',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL),
(54,154,'Mr. M.M. Bhate','Dy. General Manager','Project',NULL,NULL,NULL,NULL,NULL,NULL,'10, Bruce Street (Homi Mody Street), 1st Floor, Fort, Mumbai ',NULL,400001),
(56,152,'Mr.Makarand Joshi','','',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL),
(59,145,'Mr. Madhav Singh','','',NULL,NULL,NULL,NULL,NULL,NULL,'Plot No.: 16/2, 17/4, 17/5, 20/4, 21/1, 21/2, Village Borle, Neral,  Karjat, Raigad',NULL,NULL);

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `pan_no` varchar(30) DEFAULT NULL,
  `office_address_line_1` varchar(255) DEFAULT NULL,
  `office_address_line_2` varchar(255) DEFAULT NULL,
  `office_pincode` int(6) DEFAULT NULL,
  `office_city` varchar(150) DEFAULT NULL,
  `mobile_1` varchar(15) DEFAULT NULL,
  `mobile_2` varchar(15) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `landline` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;

/*Data for the table `clients` */

insert  into `clients`(`id`,`client_name`,`gst_no`,`pan_no`,`office_address_line_1`,`office_address_line_2`,`office_pincode`,`office_city`,`mobile_1`,`mobile_2`,`mobile`,`landline`,`email`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,'Ambernath Organics Pvt Ltd','27AAACA3839N1ZE','AAACA3839N','222, The Summit Business Bay (Omkar)','Near W.E.H. Metro Station, Opp. Cine max Theater,  Andheri - Kurla Road',400093,' Andheri (E), Mumbai ',NULL,NULL,'9930014600','02226814600','info@ambernathorganics.com',2147483647,NULL,2147483647,NULL),
(2,'Kinikar Construction Pvt Ltd','','AAFCK4470Q','Office No. 101 A Wing,','Jai Plaza, Shivaji Chowk',421501,'Ambernath (E), Thane',NULL,NULL,'9892479614','','kinikar701@outlook.com',2147483647,NULL,2147483647,NULL),
(3,'Aarti Industries Limited','27AABCA2787L1ZC','AABCA2787L','Plot No. D-53,55,57,60','MIDC, Phase- II, Kalyan-Shill Road',421204,'Dombivli East',NULL,NULL,'9637190629','','pradip.chaudhari@aarti-industries.com',20,'2021-10-25 14:48:47',2147483647,NULL),
(72,'Vidushi Wires Pvt Ltd','27AAACV5621M1Z3','AAACV5621M','Plot No C-6 , , Additional Ambernath ,,',' Anand Nagar MIDC , ',NULL,'Ambernath E ','2147483647',NULL,NULL,NULL,'sunilpoddar@vidushiwire.com',NULL,NULL,NULL,NULL),
(73,'Soumya Constructions ','27AXGPS7005N1ZY','AXGPS7005N','Flat No. 302, Sr.No 93A/6,7','Maruti Dasturi Naka, Mahad',402301,'Raigad','9028093261','',NULL,'','sheelavant_am@rediffmail.com',NULL,NULL,NULL,NULL),
(74,'Platinum Polymers Pvt Ltd','27AAECP2031K1ZJ','AAECP2031K','F-1/3, M.I.D.C.,Badlapur Industrial Area,','Mankivali,Kulgaon , Kalyan-Thane',421503,'Thane','7028975741','',NULL,'2698831','infopolymers@gmail.com',NULL,NULL,NULL,NULL),
(75,'Finetech Construction Co','27AADFF9124B1ZR','AADFF9124B','Gurudatta height, Shop No-5','Sunil Nagar,Dombivli-East',421201,'Kalyan-Thane','9869006501','',NULL,'','kokitkarsn@yahoo.com',NULL,NULL,NULL,NULL),
(76,'Spak Orgochem ( India ) Pvt Ltd','27AABCS9228G1Z7','AABCS9228G','A-2/3 , Suman Nagar,','Sion Trombay Road, Chembur ',400071,'Mumbai','9820049708','',NULL,'25293450','sarojmukund1@gmail.com',NULL,NULL,NULL,NULL),
(77,'Chacha Lifestyle Pvt Ltd','27AAFCC4591G1ZJ','AAFCC4591G','Unit No-306A,Floor-3,Dhanraj Industrial Estate','Sitaram Jadhav Marg,Delisle Road,Lower Parel',400013,'Mumbai ','9004352618','',NULL,'24980708','hardik@chachagrp.com',NULL,NULL,NULL,NULL),
(78,'Test Client','111','111','Thane','Mulund',421523,'Badlapur','8899889988','',NULL,'','gavalikamlesh@gmail.com',NULL,NULL,NULL,NULL),
(79,'Sajjan India Limited ','24AAACS6498M1ZR','AAACS6498M','Matulya Center, # 2 Ground Floor,','Senapati Bapat Marg,Lower Parel,',400013,'Mumbai.','7046611150','7046611151',NULL,'','vijaynair@sajjan.com',NULL,NULL,NULL,NULL),
(81,'IG Petrochemicals Limited','27AAACI4115R1ZB','AAACI4115R','401-404, Raheja Center, Free Press Journal Marg,','214, Nariman Point,',400021,'Mumbai.','9082347063','9820095731','9892908402','','jksaboo@igpetro.com',1,'2021-10-18 13:25:40',NULL,NULL),
(82,'Transition HR India','27ABFPK3435R1Z6','ABFPK3435R','Office no 101-102, First Floor , A1- Wing , Nagesh Tower, ','Near Hari Niwas Circle , Thane west.',400602,'Mumbai','9819420007','',NULL,'25388150','milind@transitionhrindia.com',NULL,NULL,NULL,NULL),
(83,'  Shivaji Vidhyalaya ','','','  Plot No 40, Sector 6A ',' In Front of Dudhe Building',410209,'Kamothe','7057011969','',NULL,'','gavhan.scsvrr5@gmail.com',NULL,NULL,NULL,NULL),
(84,'Siddhi Hospital & Nursing Home','','','Kamal Prasad Bldg, Phadke Road, ','Baji Prabhu Chowk, ',421201,'Dombivli- East','9324484939','9967980879',NULL,'0251286366','ulhas.kolhatkar@gmail.com',NULL,NULL,NULL,NULL),
(85,'Ambernath Organics Pvt. Ltd.','27AAACA3839N1ZE','AAACA3839N','222, The Summit Business Bay (Omkar),  ','Near W.E.H. Metro Station, Opp. Cine max Theater,  Andheri - Kurla Road, ',400093,' Andheri (E), Mumbai ','9820219823','',NULL,'22-2681 4600','shembekar@ambernathorganic.com',NULL,NULL,NULL,NULL),
(86,'Solar Resins Pvt Ltd','','','D-38 ,MIDC Dombivli ,Phase - II , ','Dombivli- East',421204,'Dombivli','9821574357','',NULL,'251-2870231','solarresins@gmail.com',NULL,NULL,NULL,NULL),
(87,'Ambernath Organics Pvt. Ltd..','27AAACA3839N1ZE','AAACA3839N','222, The Summit Business Bay (Omkar),  ','Near W.E.H. Metro Station,Opp. Cine max Theater,Andheri - Kurla Road, ',400093,'Andheri-East.','9820349596','',NULL,'26814623','shembekar@ambernathorganic.com',NULL,NULL,NULL,NULL),
(88,'J M Mhatre Infra Pvt Ltd','27AACCJ2959E1ZJ','AACCJ2959E','JMM Infra Private Limited','Plot No 492,Market Yard',410206,'Panvel,Maharashtra','9930426986','',NULL,'','jmmipl2016@gmail.com',NULL,NULL,NULL,NULL),
(89,'VGS Construction Co','27ANEPV8971C2ZI','ANEPV8971C','C3, Shiv Basav Nagar, Shiv Mandir Road ','Opp. Kunal ICE Cream',421501,'Ambernath (E)','9822068523','9029891852',NULL,'','abcd@gmail.com',NULL,NULL,NULL,NULL),
(90,'S N Constructions','27ABKFS0317D1ZG','ABKFS0317D','Plot No. W-65 ','1st floor, MIDC, Phase- II',421203,'Dombivli (East)','9820024255','',NULL,'','sncon9@gmail.com',NULL,NULL,NULL,NULL),
(91,'P. D. Associates','27AAIFP9710G1Z2','AAIFP9710G','A-4, Nav Vrushali CHS','Agarkar Road',421201,'Dombivli (East)','98202 11199','',NULL,'','rajeevprabhune2@gmail.com',NULL,NULL,NULL,NULL),
(92,'Centaur Pharmaceuticals Pvt Ltd','27AAACC0444K1ZV','AAACC0444K','Centaur House, Shanti Nagar','Vakola, Santacruz.(E)',400055,'Mumbai','9653212132','',NULL,'2682968','marutid@centaurlab.com',NULL,NULL,NULL,NULL),
(93,'Kinomera Biosciences Pvt Ltd','27AAHCK8654J1ZY','AAHCK8654J','Shop No. 6, Om Shraddha Co-Op Housing Society Ltd','Link Road, Near Don Bosco School',400091,'Borivali (West)','9920042425','',NULL,'','bhagwatchemistry@gmail.com',NULL,NULL,NULL,NULL),
(94,'Vasant Valley CHS Ltd.','','AAFAV0304G','Vasant Valley CHS Ltd.','Khadakpada Road',421301,'Kalyan (West)','9152729411','8830649295',NULL,'','vvchs12@gmail.com',NULL,NULL,NULL,NULL),
(95,'Galaxy IEC India Pvt Ltd.','27AAECG0079L1ZE','AAECG0079L','Plot No. RP-14, MIDC Phase-II','Opp. AIM Hospital',421203,'Dombivli (E). ','8080451717','9930688249',NULL,'2444155','vinay.haldikar@galaxyiec.com',NULL,NULL,NULL,NULL),
(96,'Pitambari Products Pvt Ltd.','27AABCM1570F1ZQ','AABCM1570F','Hemendra Co.Hsg.Soc.Ltd.','3rd Floor,Gokhale Road',400602,'Thane','9833351634','',NULL,'62281616/1','kiran.kumar@pitambari.com',NULL,NULL,NULL,NULL),
(97,'Apcotex Industries Limited ','27AAACA3427G1Z1','AAACA3427G','PLOT NO.3/1','Apcotex industries limited ',410208,'Taloja, Dist Raigad','9619977435','',NULL,'','yvfirake@apcotex.com',NULL,NULL,NULL,NULL),
(98,'Right Sports Apparels','27AAMFR4083L1ZL','AAMFR4083L','101 ,Tulsi Niwas , 33 Pushtikar Society','Jogeshwari (W)',400102,'Mumbai ','9322020197','',NULL,'','right@rightapparels.co.in',NULL,NULL,NULL,NULL),
(99,'Sandu Brothers Pvt Ltd',' 27AADCS9226L1ZW','AADCS9226L','Sandu Nagar, D.K. Sandu Marg','Chembur',400071,'Mumbai','9821335303','','7738393117','','dr.nageshsandu@sandu.in',1970,NULL,NULL,NULL),
(100,'Matru Alankar Society','','','Dombivli East','Dombivli East',421201,'Dombivli East','9833434166','',NULL,'','paragsp@gmail.com',NULL,NULL,NULL,NULL),
(101,'Transition HR India','27ABFPK3435R1Z6','ABFPK3435R','A-80 , First Floor','Thane Wagle Estate, Midc ',400602,'THANE','9819420007','',NULL,'','milind@transitionhrindia.com',NULL,NULL,NULL,NULL),
(102,'Gharda Chemicals Limited','27AAACG1255E1Z1','AAACG1255E','B27/29, MIDC Phase - I','MIDC',421203,'Dombivli','9167525471','','9167525471','2803380','vpatil@gharda.com',20,'2021-10-23 16:30:59',NULL,NULL),
(103,'Mechemco Resins Pvt. Ltd','27AADCM8896L1ZK','AADCM8896L','27, Kewal Industrial Estate  ','S. B. Marg, Lower Parel ',NULL,'Mumbai , 400013','9821035160','',NULL,'','piyathakkar@mechemco.com',NULL,NULL,NULL,NULL),
(104,' Altra Agro Chem Pvt Ltd','27AASCA4797R1ZD','AASCA4797R',' Plot No- 45, MIDC PHASE II','Jindal Compound, Manpada Road,',421204,'Dombivli (East)','9820808184','',NULL,'','altrapurechem@yahoo.com',NULL,NULL,NULL,NULL),
(105,'Om Chemicals','','','Plot No. F-91/4','Anand Nagar, MIDC',421506,'Ambernath (East)','9137073561','',NULL,'','www.shrutichem.ind@gmail.com',NULL,NULL,NULL,NULL),
(106,'Corrugated Packaging Industries','27AAGFC1169C1ZS','AAGFC1169C','F-15/1, Add. Ambernath MIDC','Anand Nagar, Near N-65, Modi Pump',421506,'Ambernath (East)','9137073561','',NULL,'','anil@cpind.in',NULL,NULL,NULL,NULL),
(107,'Om Shiv Chhaya Co-Op Housing Society','','','Plot No. RH-111, Phase- II','MIDC Residential Zone ',421203,'Dombivli','9323242007','',NULL,'','omshiv.chs@gmail.com',NULL,NULL,NULL,NULL),
(108,'Herbert Brown Pharmaceutical & Research Laboratories','27AAAFH7347B1ZN','AAAFH7347B','W-256A','Shivaji Udyog Nagar, MIDC, Phase II',421203,'Dombivli (East)','9920313169','',NULL,'','shrikant.acharya@acharyagroup.com',NULL,NULL,NULL,NULL),
(109,'M P Engineering Constructions India Pvt Ltd','27AACCM9499E1ZZ','AACCM9499E','1003/1004, Lodha Supremus III, I Think ','Techno Campus , Off JVLR , Kanjurmarg (E)',400042,'Mumbai ','9757392143','9820219880',NULL,'25750000','mpvarghese@mpengg.com',NULL,NULL,NULL,NULL),
(134,'Chemstar','27ADIPA1351M1ZO','ADIPA1351M','W-100, MIDC Phase 2 ','MIDC, Dombivli',421204,'Dombivli',NULL,NULL,'9820979016','02512445893','chemstarindia@gmail.com',1970,NULL,1970,NULL),
(135,'Unilab Chemicals And Pharmaceuticals Pvt. Ltd','27AAACU0707D1ZU','AAACU0707D','F/12,4th Floor, Pinnacle Business Park, Shantinagar','Next To Ahura Business Park, Mahakali Caves Road,',400093,'Andheri (East),Mumbai',NULL,NULL,'9820030780','2870632','rvora@unilabchem.com',1970,NULL,1970,NULL),
(136,'Apcotex Industries Limited Taloja','27AAACA3427G1Z1','AAACA3427G','Plot No. 3/1','Dist. Raigad, Maharastra',410208,'Taloja',NULL,NULL,'9136323494','','balabhadra.s@apcotex.com',1,'2021-10-11 18:21:39',1,'2021-10-11 18:21:39'),
(137,'Suma Pharmaceuticals Pvt Ltd','27AAGCS1935H1ZA','AAGCS1935H','Plot no 126, Chikhaloli MIDC','MIDC, Ambernath',421505,'Ambernath',NULL,NULL,'9322282270','0251-2688163','patilaic@gmail.com',1,'2021-10-29 09:42:32',1,'2021-10-29 09:42:32'),
(138,'Macrotech Developers Limited','27AAACL1490J1ZG','AAACL1490J','412, Floor No-4, 17G,Vardhaman Chamber','Cawasji Patel Road, Horniman Circle, Fort',400001,'Mumbai',NULL,NULL,'9833779167','','roshni.jaiswal@lodhagroup.com',1,'2021-10-29 12:53:29',1,'2021-10-29 12:53:29'),
(139,'Chemsynth Labs','27AAQFC2836C1Z1','AAQFC2836C','4B-33A, Fourth Floor, Highland Corporate Centre, ','Near Big Bazaar,, Kapurbawadi,',400608,'Thane West ',NULL,NULL,'9890085085','','chemsynthlabs2021@gmail.com',36,'2022-01-12 17:10:05',1,'2021-11-09 15:39:51'),
(140,'Mediair Healthcare Products Pvt Ltd','27AALCM1210A1Z5','AALCM1210A','1,Kanti Mahal CHS , ','Opp Imperial Palace Hotel , Teli Geli, Andheri East , ',400069,'Mumbai , ',NULL,NULL,'9833997465','','MEDIAIRHEALTHCARE@GMAIL.COM',1,'2021-12-03 16:03:17',1,'2021-12-03 16:03:17'),
(141,'Ganadish Co-operative Housing Society Ltd., ','','','Plot No. R/19, Sudershan Nagar','Dombivli (East) ',421203,'MIDC Residential Zone',NULL,NULL,'9920713679','','niranjan.athalye@gmail.com',1,'2021-12-17 12:28:29',1,'2021-12-17 12:28:29'),
(142,'Balmer Lawrie & Co. Ltd., ','','','Plot No. G-15, G-16, &amp; G-17, M.I.D.C.,','Industrial Area, Taloja, Village Padge,',410208,'Taluka Panvel, Dist., Raigad',NULL,NULL,'7718831931','','lader.kk@balmerlawrie.com',36,'2022-02-21 18:07:11',36,'2021-12-18 11:10:04'),
(143,'Acharya Chemicals','27AACFA1735G1ZU','AACFA1735G','W-256A, Shivaji Udyog Nagar, ','MIDC Phase II, Dombivli East',NULL,'Thane',NULL,NULL,'9892475166','','ardhendu.purkayastha@acharyagroup.com',36,'2022-01-22 14:37:55',36,'2022-01-22 14:37:55'),
(144,'Modern Monarch JV','','','Dombivali','',NULL,'Thane',NULL,NULL,'9769251479','','modernmonarchjv@gmail.com',36,'2022-01-25 16:50:21',36,'2022-01-25 16:50:21'),
(145,'GLOBE CONSTRUCTION ','27AASFG5162B1ZD','AASFG5162B','Unit 1, Ground Floor, Globe Estate, ','Plot No. C-9, New Kalyan Road, MIDC Phase 1,  Dombivli East',421203,'Thane ',NULL,NULL,'9819807070','','globe.madhavsingh@gmail.com',36,'2022-04-29 13:20:55',36,'2022-02-09 16:02:38'),
(146,'Rajendra Shikshan Sanstha, Mumbai','','','Vidya Niketan, 2/1, Everest Society, ','Deendayal Marg,Vishnunagar, Dombivli (W)',421202,'Thane',NULL,NULL,'7045792502','','vns.essb@gmail.com',36,'2022-02-11 10:45:56',36,'2022-02-11 10:03:51'),
(147,'Heni Chemical Industries',':27AAAFH1222M1ZM','AAAFH1222M','A-161,Main Road,Opp. Honda Show Room,Wagle Industrial Estate,','Thane-400604.(Maharashtra). India.    ',400604,'Thane',NULL,NULL,'0000000000','','kapil.girotra@henich',36,'2022-02-21 18:19:51',36,'2022-02-21 18:19:51'),
(148,'G. N. SINGH','','','Dombivli','',NULL,'Thane',NULL,NULL,'0000000000','','mail@gnsystech.com',36,'2022-02-21 18:21:37',36,'2022-02-21 18:21:37'),
(149,'Zenzi Pharmaceutical Industries Private Limited','27AAFCA6695R1ZP','AAFCA6695R','Plot No. H-53, Additional Murbad MIDC','Kudavali Village, Tal. Murbad',421401,'Thane',NULL,NULL,'7738251521','','ja@alpha-pharma.com',36,'2022-02-23 17:32:42',36,'2022-02-23 17:32:42'),
(150,'Fine Organic Industries Pvt. Ltd., ','27AAACF7911Q1Z6','AAACF7911Q','Fine House, Anandji Street, ','Off. M. G. Road , Ghatkopar (East), ',400077,'Mumbai - 400 077',NULL,NULL,'8605011765','','nandan_khopkar@fineorganics.com',20,'2022-04-19 11:38:51',36,'2022-03-15 15:50:47'),
(152,'Anuvi Chemicals Ltd.','27AAACA1007G1ZD','AAACA1007G','Laxmi Industrial Estate, Vartak Nagar, ','Thane West. ',400606,'Thane ',NULL,NULL,'9819884251','','makarand.joshi@anuvi.in',36,'2022-04-25 13:30:00',36,'2022-03-17 15:15:10'),
(153,'Mr. G.G. Mhatre','','','Kandpile Co-operative Housing Society','Plot No.: 292, A-Wing, 6th Floor,',410206,'Panvel',NULL,NULL,'7506418602','','mhatregg@gmail.com',36,'2022-03-24 17:08:25',36,'2022-03-24 17:08:25'),
(154,'Sadhana Nitro Chem Limited, ','27AABCS1231R1Z5','AABCS1231R','First Floor, Hira Baug','Kasturba Chowk, C.P.Tank',400004,'Mumbai , ',NULL,NULL,'9820615622','','bhate@sncl.com',1,'2022-04-05 16:59:45',36,'2022-03-25 16:03:05'),
(155,'Bombay Chlorides Pvt. Ltd., ','','','W/72 MIDC Ind. Area,','Badlapur MIDC, ',421503,'Thane',NULL,NULL,'0000000000','','harkabchemicalsw56c@gmail.com',36,'2022-04-12 15:23:18',36,'2022-04-12 15:23:18');

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `owner` tinyint(1) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gstin` varchar(15) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `companies` */

insert  into `companies`(`id`,`company`,`owner`,`address_line_1`,`address_line_2`,`city`,`pincode`,`country`,`telephone`,`mobile`,`email`,`gstin`,`modified`,`created`) values 
(2,'WIM PLAST LIMITED',NULL,'Survey no-324/4 to 7','Swami Narayan Gurukul road, village-kachigum ','Daman','396210','India','(022) 26863426 / 26863427 / 26864630','9819169573 / 7208878469','','26AAACW0420B1Z4','2021-09-16 11:15:05','2021-07-27 12:39:36'),
(3,'Sanmisha Technologies1',NULL,'','','','','','','','','','2022-04-25 11:00:33','2022-04-25 10:33:49');

/*Table structure for table `daily_tasks` */

DROP TABLE IF EXISTS `daily_tasks`;

CREATE TABLE `daily_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `done_date` date DEFAULT NULL,
  `duration` decimal(10,2) DEFAULT NULL,
  `status` enum('Open','In Progress','Done') DEFAULT 'Open',
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `daily_tasks` */

insert  into `daily_tasks`(`id`,`project_id`,`activity_id`,`description`,`assigned_to`,`due_date`,`done_date`,`duration`,`status`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,1,1,'something',20,'2022-06-22',NULL,8.00,'In Progress',1,'2022-06-08 17:36:40',1,'2022-06-08 17:36:21'),
(2,1,1,'what',20,'2022-06-23','2022-06-08',7.50,'Done',1,'2022-06-08 17:37:22',1,'2022-06-08 17:36:40');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `department` varchar(100) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `departments` */

insert  into `departments`(`id`,`department`,`modified_by`,`modified`,`created_by`,`created`) values 
(5,'IT',12,'2020-04-28 13:04:19',12,'2020-04-28 13:04:19'),
(6,'Production',12,'2020-04-28 13:04:32',12,'2020-04-28 13:04:32'),
(16,'Payroll ',41,'2020-05-21 13:42:49',41,'2020-05-21 13:42:49'),
(18,'Purchase',40,'2020-05-22 20:05:50',40,'2020-05-22 20:05:50'),
(22,'Administration',57,'2020-06-16 16:15:57',57,'2020-06-16 16:15:57'),
(25,'Operations',86,'2020-06-30 22:30:50',86,'2020-06-30 22:30:50'),
(28,'Business Development',20,'2021-10-28 16:24:50',87,'2020-07-01 18:38:17'),
(30,'Customer Service',87,'2020-07-01 18:38:55',87,'2020-07-01 18:38:55'),
(34,'Accounts',98,'2020-07-20 18:23:24',98,'2020-07-20 18:23:24'),
(39,'HR Dept',101,'2020-07-25 22:39:39',101,'2020-07-25 22:39:39'),
(41,'Sales',20,'2021-10-28 16:25:05',20,'2021-10-28 16:25:05'),
(42,'Design and Development',20,'2021-10-28 16:25:26',20,'2021-10-28 16:25:26'),
(43,'Auditor/Supervior',20,'2021-10-28 16:26:56',20,'2021-10-28 16:26:56'),
(44,'Management',36,'2022-04-14 18:29:46',36,'2022-04-14 18:29:46');

/*Table structure for table `designations` */

DROP TABLE IF EXISTS `designations`;

CREATE TABLE `designations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

/*Data for the table `designations` */

insert  into `designations`(`id`,`designation`,`modified_by`,`modified`,`created_by`,`created`) values 
(2,'Manager',NULL,'2020-03-21 20:05:06',NULL,'2020-03-21 20:05:06'),
(3,'Kamlesh Designation',2,'2020-04-21 18:11:35',2,'2020-04-21 18:11:35'),
(4,'Executive',12,'2020-04-28 13:03:01',12,'2020-04-28 13:03:01'),
(5,'Senior Executive',12,'2020-04-28 13:03:17',12,'2020-04-28 13:03:17'),
(6,'Officer',12,'2020-04-28 13:03:57',12,'2020-04-28 13:03:57'),
(7,'Executive',29,'2020-05-01 21:05:06',29,'2020-05-01 21:05:06'),
(8,'Senior Executive',29,'2020-05-01 21:05:21',29,'2020-05-01 21:05:21'),
(9,'Officers',12,'2020-05-10 14:11:10',12,'2020-05-06 14:04:12'),
(10,'Executive',40,'2020-05-13 12:56:26',40,'2020-05-13 12:56:26'),
(11,'Senior Executive',40,'2020-05-13 12:56:36',40,'2020-05-13 12:56:36'),
(12,'Manager',40,'2020-05-13 12:56:46',40,'2020-05-13 12:56:46'),
(13,'Senior Manager',40,'2020-05-13 12:57:01',40,'2020-05-13 12:57:01'),
(14,'Assistant',40,'2020-05-13 12:57:11',40,'2020-05-13 12:57:11'),
(15,'Officer ',41,'2020-05-21 13:41:58',41,'2020-05-21 13:41:58'),
(16,'Team leader',41,'2020-05-21 13:42:14',41,'2020-05-21 13:42:14'),
(17,'Manager ',41,'2020-05-21 13:42:26',41,'2020-05-21 13:42:26'),
(18,'GM',40,'2020-05-22 19:56:20',40,'2020-05-22 19:56:20'),
(19,'Executive',72,'2020-06-11 20:34:18',72,'2020-06-11 20:34:18'),
(20,'Manager',72,'2020-06-11 20:34:28',72,'2020-06-11 20:34:28'),
(21,'Office Assistant',57,'2020-06-16 16:08:03',57,'2020-06-16 16:08:03'),
(22,'Executive',57,'2020-06-16 16:08:17',57,'2020-06-16 16:08:17'),
(23,'Officer',72,'2020-06-30 13:20:55',72,'2020-06-30 13:20:55'),
(24,'Sr.Manager',72,'2020-06-30 16:22:17',72,'2020-06-30 16:22:17'),
(25,'Chief Operating Officer ',86,'2020-06-30 22:33:30',86,'2020-06-30 22:33:30'),
(26,'Field Manager',86,'2020-07-01 18:50:38',86,'2020-06-30 22:34:02'),
(27,'Executive',86,'2020-07-01 18:49:58',86,'2020-06-30 22:36:07'),
(28,'Executive ',86,'2020-07-01 18:50:19',86,'2020-06-30 22:40:30'),
(29,'Assistant ',86,'2020-07-01 18:50:08',86,'2020-07-01 13:19:03'),
(30,'Software Engineer',28,'2020-07-01 15:33:07',28,'2020-07-01 15:33:07'),
(31,'Senior Software Engineer',28,'2020-07-01 15:33:46',28,'2020-07-01 15:33:46'),
(32,'Trainee Software Engineer',28,'2020-07-01 15:34:11',28,'2020-07-01 15:34:11'),
(33,'Sales Manager',87,'2020-07-01 18:35:01',87,'2020-07-01 18:35:01'),
(34,'Accountant',87,'2020-07-01 18:35:13',87,'2020-07-01 18:35:13'),
(35,'Admin Officer',87,'2020-07-01 18:35:37',87,'2020-07-01 18:35:37'),
(36,'Office Boy',87,'2020-07-01 18:35:54',87,'2020-07-01 18:35:54'),
(37,'Reception',87,'2020-07-01 18:36:11',87,'2020-07-01 18:36:11'),
(38,'Branch Manager',87,'2020-07-01 18:36:48',87,'2020-07-01 18:36:48'),
(39,'customer service',87,'2020-07-01 18:37:25',87,'2020-07-01 18:37:25'),
(40,'Business Executive',40,'2020-07-09 10:56:28',40,'2020-07-09 10:56:28'),
(41,'Area Business Manager',40,'2020-07-09 10:56:40',40,'2020-07-09 10:56:40'),
(42,'Business Executive',72,'2020-07-09 11:19:16',72,'2020-07-09 11:19:16'),
(43,'Area Business Manager',72,'2020-07-09 11:19:26',72,'2020-07-09 11:19:26'),
(44,'Executive',98,'2020-07-20 18:22:59',98,'2020-07-20 18:22:59'),
(45,'Business Development Manager',100,'2020-07-20 21:38:08',100,'2020-07-20 21:38:08'),
(46,'HR Manager',101,'2020-07-25 22:39:13',101,'2020-07-25 22:39:13'),
(47,'Finance Manager',101,'2020-07-25 23:16:48',101,'2020-07-25 23:16:48'),
(48,'Store Manager',86,'2020-08-15 20:27:53',86,'2020-08-15 20:27:53'),
(49,'Executive',109,'2020-08-26 13:08:23',109,'2020-08-26 13:08:23'),
(50,'CEO & Principal Officer',100,'2020-08-28 10:28:13',100,'2020-08-28 10:28:13'),
(51,'Insurance Sales Person',100,'2020-08-28 10:35:22',100,'2020-08-28 10:35:22'),
(52,'Business Development Executive',100,'2020-08-28 10:44:47',100,'2020-08-28 10:44:47'),
(53,'Draftsman',1,'2022-03-22 17:35:54',1,'2022-03-22 17:35:54'),
(54,'Director',36,'2022-04-14 18:27:40',36,'2022-04-14 18:27:40'),
(55,'Project Engineer',36,'2022-04-20 11:12:20',36,'2022-04-20 11:12:20');

/*Table structure for table `document_drives` */

DROP TABLE IF EXISTS `document_drives`;

CREATE TABLE `document_drives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `drawing_no_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `document_dir` varchar(255) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `version` int(11) DEFAULT 0,
  `latest` tinyint(1) DEFAULT 1,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `document_drives` */

insert  into `document_drives`(`id`,`project_id`,`drawing_no_id`,`level_id`,`building_id`,`document`,`document_dir`,`remarks`,`version`,`latest`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,1,1,1,1,'four-corners_1140_658_80_s.jpg','c33016ab-9d8d-4463-8751-140ebb5ab9e9','',0,1,1,'2022-04-08 11:03:28',1,'2022-04-08 11:03:28'),
(2,1,1,3,1,'baby-max_2183067b.jpg','f2df3568-0154-4232-954e-789bc3951ac4','sdfsd',0,0,1,'2022-04-08 11:06:33',1,'2022-04-08 11:03:41'),
(3,1,1,3,1,'baby-max_2183067b.jpg','503fd878-20b6-49ce-88cf-3afa2dfb1709','Final Drawing',1,1,1,'2022-04-08 11:06:33',1,'2022-04-08 11:06:33'),
(4,2,1,2,2,'1.png','2d9817a4-b967-495d-9652-0f6b20a444f0','',0,1,1,'2022-04-08 11:07:03',1,'2022-04-08 11:07:03'),
(5,1,4,3,1,'License OF Plumber.pdf','a5b98cd0-d82a-4145-8476-904ba3a7036b','',0,1,1,'2022-05-24 10:46:41',1,'2022-05-24 10:46:41');

/*Table structure for table `drawing_nos` */

DROP TABLE IF EXISTS `drawing_nos`;

CREATE TABLE `drawing_nos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `drawing_stage` varchar(255) DEFAULT NULL,
  `drawing_type` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `drawing_nos` */

insert  into `drawing_nos`(`id`,`drawing_stage`,`drawing_type`,`modified`,`created`) values 
(1,'Section 1','FL1','2022-04-01 13:22:10','2022-04-01 12:56:42'),
(2,'Section 1','FL2','2022-04-05 10:49:39','2022-04-05 10:49:39'),
(3,'Section 2','FL1','2022-04-05 11:00:50','2022-04-05 11:00:50'),
(4,'AO','Brick Work','2022-05-24 10:45:52','2022-05-24 10:45:52');

/*Table structure for table `email_templates` */

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(500) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `email_templates` */

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `designation_id` int(100) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `altternate_mobile` varchar(10) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employees` */

insert  into `employees`(`id`,`name`,`designation_id`,`department_id`,`date_of_joining`,`dob`,`address_line_1`,`address_line_2`,`city`,`pincode`,`altternate_mobile`,`modified_by`,`modified`,`created_by`,`created`) values 
(20,'Madhurani Joshi',34,34,'2022-02-01','1985-01-23','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9920951531',NULL,'2022-04-20 11:03:30',1,'2021-09-21 16:30:17'),
(36,'Darpan Powale',35,22,'2021-07-03','1976-12-01','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9920951588',NULL,'2022-04-20 11:01:35',1,'2021-12-17 16:48:08'),
(38,'Kaustubh Madhav Kashelkar',54,44,'2021-07-01','1987-02-09','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9920951511',NULL,'2022-04-20 11:00:33',1,'2021-12-25 14:42:45'),
(53,'Pramod  Prabhakar Sawant',53,42,'2021-07-01','1983-09-02','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9619267888',NULL,'2022-04-20 10:57:49',1,'2022-03-22 17:40:50'),
(55,'Madhav Rajaram Kashelkar',54,44,'2021-07-01','1955-02-04','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9920654121',NULL,'2022-04-20 10:54:28',NULL,'2022-04-06 18:27:01'),
(57,'Amey Atul Kulkarni ',55,25,'2021-10-01','1996-10-14','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9920951532',1,'2022-04-20 14:35:06',NULL,'2022-04-20 11:08:56'),
(58,'Prasad Date',53,42,'2021-07-01','1988-11-25','410,411,412, Globe Arcade, Fourth Floor, RP-112','MIDC Residential Zone, Nr. MIDC Ganpati Mandir, Dombivli East','Kalyan ','421203','9619267888',NULL,'2022-04-20 12:09:36',NULL,'2022-04-20 12:09:36');

/*Table structure for table `expense_heads` */

DROP TABLE IF EXISTS `expense_heads`;

CREATE TABLE `expense_heads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `expense_head` varchar(255) DEFAULT NULL,
  `sac_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `expense_heads` */

insert  into `expense_heads`(`id`,`expense_head`,`sac_code`) values 
(1,'Visit Expense',''),
(2,'A4-Colour',''),
(3,'A3-Colour',''),
(4,'A1-Colour',''),
(5,'A0-Colour',''),
(6,'A2-Colour',''),
(7,'A1-B/W',''),
(8,'A0-B/W',''),
(9,'A2-B/W',''),
(10,'A3-B/W',''),
(11,'Tender Books',''),
(12,'Travelling & Conveyance Expenses',''),
(13,'Postal & Couier Expenses',''),
(14,'A1- Jumbo Print',''),
(16,'A4-Spiral Baiding ',''),
(17,'Printing & Stationery',''),
(18,'A4-B/W ',''),
(19,'A4-Xerox ',''),
(20,'A3-Xerox','');

/*Table structure for table `expenses` */

DROP TABLE IF EXISTS `expenses`;

CREATE TABLE `expenses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `expense_head_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT 0.00,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `expenses` */

/*Table structure for table `file_nos` */

DROP TABLE IF EXISTS `file_nos`;

CREATE TABLE `file_nos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `file_type` varchar(2) DEFAULT NULL,
  `sub_category` varchar(5) DEFAULT NULL,
  `file_no` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `file_nos` */

insert  into `file_nos`(`id`,`project_id`,`file_type`,`sub_category`,`file_no`) values 
(5,NULL,'AO','','  General Correspond'),
(6,1,'AO','DD','00001/AO/DD/');

/*Table structure for table `follow_ups` */

DROP TABLE IF EXISTS `follow_ups`;

CREATE TABLE `follow_ups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enquiry_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `follow_up_date` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `follow_ups` */

/*Table structure for table `followups` */

DROP TABLE IF EXISTS `followups`;

CREATE TABLE `followups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `followups` */

insert  into `followups`(`id`,`project_id`,`followup_date`,`project_status`,`remarks`) values 
(1,9,'2021-10-15','Quotation Sent','adsdf'),
(2,17,'2021-12-09','Open','First Call'),
(3,17,'2021-12-09','Open','First Call'),
(4,17,'2021-12-16','Open','second');

/*Table structure for table `handbook` */

DROP TABLE IF EXISTS `handbook`;

CREATE TABLE `handbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `topic` varchar(500) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `list_order` varchar(2) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `handbook` */

/*Table structure for table `handbooks` */

DROP TABLE IF EXISTS `handbooks`;

CREATE TABLE `handbooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `topic` varchar(500) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `list_order` varchar(2) DEFAULT NULL,
  `attachment_1` varchar(255) DEFAULT NULL,
  `attachment_1_dir` varchar(255) DEFAULT NULL,
  `attachment_2` varchar(255) DEFAULT NULL,
  `attachment_2_dir` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `handbooks` */

insert  into `handbooks`(`id`,`department_id`,`designation_id`,`topic`,`content`,`list_order`,`attachment_1`,`attachment_1_dir`,`attachment_2`,`attachment_2_dir`,`modified`,`created`) values 
(7,NULL,NULL,'Human Resource Manual ','<p>Key Points of HR Policy of our Company:</p><p>1)&nbsp;</p>','01',NULL,'',NULL,'','2022-06-07 13:23:33','2022-06-07 13:23:33');

/*Table structure for table `invoice_details` */

DROP TABLE IF EXISTS `invoice_details`;

CREATE TABLE `invoice_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `dis_amt` decimal(10,2) DEFAULT NULL,
  `cgst_rate` decimal(10,2) DEFAULT NULL,
  `cgst_amount` decimal(10,2) DEFAULT NULL,
  `sgst_rate` decimal(10,2) DEFAULT NULL,
  `sgst_amount` decimal(10,2) DEFAULT NULL,
  `igst_rate` decimal(10,2) DEFAULT NULL,
  `igst_amount` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

/*Data for the table `invoice_details` */

insert  into `invoice_details`(`id`,`invoice_id`,`project_task_id`,`task_id`,`service`,`description`,`fees`,`discount`,`dis_amt`,`cgst_rate`,`cgst_amount`,`sgst_rate`,`sgst_amount`,`igst_rate`,`igst_amount`,`amount`) values 
(1,1,15,5,'','',120312.49,0.00,0.00,9.00,10828.12,9.00,10828.12,0.00,0.00,141969.00),
(4,4,51,32,'','',50000.00,0.00,0.00,9.00,4500.00,9.00,4500.00,0.00,0.00,59000.00),
(5,5,52,27,'','',7000.00,0.00,0.00,9.00,630.00,9.00,630.00,0.00,0.00,8260.00),
(7,7,90,39,'','',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,0.00,0.00,0.00),
(8,8,1,5,'On Signing of the Agreement','Retainer/advance will be converted to our fees after signing of agreement.',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,0.00,0.00,118000.00),
(9,9,2,16,'','',26500.00,0.00,0.00,9.00,2385.00,9.00,2385.00,0.00,0.00,31270.00),
(10,10,3,17,'On Submitting Conceptual Design and rough cost of estimate','Ascertain Client',53000.00,0.00,0.00,9.00,4770.00,9.00,4770.00,0.00,0.00,62540.00),
(11,11,4,18,'On incorporating clients suggestion and finalising the design after brief 3d Visualisation','Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client',53000.00,0.00,0.00,9.00,4770.00,9.00,4770.00,0.00,0.00,62540.00),
(12,12,5,19,'Submitting the final Design to statutory bodies approval like MIDC CIDCO KDMC BMC ','Prepare drawings necessary for Client',53000.00,0.00,0.00,9.00,4770.00,9.00,4770.00,0.00,0.00,62540.00),
(13,13,6,20,'Final Working drawings Specifications and BOQ','Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method of payments, quality control procedures on materia',53000.00,0.00,0.00,9.00,4770.00,9.00,4770.00,0.00,0.00,62540.00),
(14,14,29,5,'','',302500.00,0.00,0.00,9.00,27225.00,9.00,27225.00,0.00,0.00,356950.00),
(15,15,7,21,'On Inviting receiving and analysing tenders and advising clients on appointment of contractor and Vendors','Invite, receive and analyze tenders; advise Client on appointment of contractors.',53000.00,0.00,0.00,9.00,4770.00,9.00,4770.00,0.00,0.00,62540.00),
(16,16,104,36,'','',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(17,17,105,22,'','',60000.00,0.00,0.00,9.00,5400.00,9.00,5400.00,NULL,0.00,70800.00),
(18,18,106,23,'','',60000.00,0.00,0.00,9.00,5400.00,9.00,5400.00,NULL,0.00,70800.00),
(19,19,111,11,'','',175000.00,0.00,0.00,9.00,15750.00,9.00,15750.00,NULL,0.00,206500.00),
(20,20,112,15,'','',75000.00,0.00,0.00,9.00,6750.00,9.00,6750.00,NULL,0.00,88500.00),
(21,21,114,12,'','',50000.00,0.00,0.00,9.00,4500.00,9.00,4500.00,NULL,0.00,59000.00),
(22,22,116,5,'','',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,0.00,0.00,118000.00),
(23,23,2,16,'','',23350.00,0.00,0.00,9.00,2101.50,9.00,2101.50,0.00,0.00,27553.00),
(24,24,3,17,'On Submitting Conceptual Design and rough cost of estimate','Ascertain Client',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(25,25,4,18,'On incorporating clients suggestion and finalising the design after brief 3d Visualisation','Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(26,26,5,19,'Submitting the final Design to statutory bodies approval like MIDC CIDCO KDMC BMC ','Prepare drawings necessary for Client',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(27,27,6,20,'Final Working drawings Specifications and BOQ','Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method of payments, quality control procedures on materia',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(28,28,7,21,'On Inviting receiving and analysing tenders and advising clients on appointment of contractor and Vendors','Invite, receive and analyze tenders; advise Client on appointment of contractors.',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(29,29,1,5,'On Signing of the Agreement','Retainer/advance will be converted to our fees after signing of agreement.',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,0.00,0.00,118000.00),
(31,31,126,5,'','',95000.00,0.00,0.00,9.00,8550.00,9.00,8550.00,0.00,0.00,112100.00),
(33,33,89,15,'','',55125.00,0.00,0.00,9.00,4961.25,9.00,4961.25,0.00,0.00,65048.00),
(34,34,69,31,'','',50000.00,0.00,0.00,9.00,4500.00,9.00,4500.00,0.00,0.00,59000.00),
(35,35,68,11,'','',117500.00,0.00,0.00,9.00,10575.00,9.00,10575.00,0.00,0.00,138650.00),
(36,36,67,5,'','',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,0.00,0.00,118000.00),
(37,37,225,27,'','After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',84250.00,0.00,0.00,9.00,7582.50,9.00,7582.50,NULL,0.00,99415.00),
(38,38,231,9,'','DG 1250 Civil Consultancy Charges.',47321.00,0.00,0.00,9.00,4258.89,9.00,4258.89,NULL,0.00,55839.00),
(39,39,237,55,'','',500000.00,0.00,0.00,9.00,45000.00,9.00,45000.00,NULL,0.00,590000.00),
(40,40,238,56,'','',500000.00,0.00,0.00,9.00,45000.00,9.00,45000.00,NULL,0.00,590000.00),
(47,47,240,5,'','Retainer is an advance payment from our total fees , and should be released after signing of agreement',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(48,48,241,38,'','',700000.00,0.00,0.00,9.00,63000.00,9.00,63000.00,NULL,0.00,826000.00),
(49,49,263,5,'','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(50,50,14,36,'Submitting of working drawings and details required for commencement of work at site','Prepare and issue working drawings and details for proper execution of works during construction.Approve samples of various elements and components.Check and approve shop drawings submitted by the contractor/ vendors.Visit the site of work, at intervals m',23350.00,0.00,0.00,9.00,2101.50,9.00,2101.50,0.00,0.00,27553.00),
(51,51,8,22,'Completion of Footings and Plinths Beams','',46700.00,0.00,0.00,9.00,4203.00,9.00,4203.00,0.00,0.00,55106.00),
(52,52,9,23,'','',28020.00,0.00,0.00,9.00,2521.80,9.00,2521.80,0.00,0.00,33064.00),
(53,53,146,5,'','',20000.00,0.00,0.00,9.00,1800.00,9.00,1800.00,0.00,0.00,23600.00),
(54,54,147,16,'','',7133.00,0.00,0.00,9.00,641.97,9.00,641.97,0.00,0.00,8417.00),
(55,55,148,17,'','',14266.00,0.00,0.00,9.00,1283.94,9.00,1283.94,0.00,0.00,16834.00),
(56,56,149,18,'','',14266.00,0.00,0.00,9.00,1283.94,9.00,1283.94,0.00,0.00,16834.00),
(57,57,150,19,'','',14266.00,0.00,0.00,9.00,1283.94,9.00,1283.94,0.00,0.00,16834.00),
(58,58,161,15,'','',25000.00,0.00,0.00,9.00,2250.00,9.00,2250.00,0.00,0.00,29500.00),
(59,59,16,16,'','',18046.88,0.00,0.00,9.00,1624.22,9.00,1624.22,0.00,0.00,21295.00),
(60,60,17,17,'','',18046.88,0.00,0.00,9.00,1624.22,9.00,1624.22,0.00,0.00,21295.00),
(61,61,18,18,'','',50531.25,0.00,0.00,9.00,4547.81,9.00,4547.81,0.00,0.00,59627.00),
(62,62,19,19,'','',36093.75,0.00,0.00,9.00,3248.44,9.00,3248.44,0.00,0.00,42591.00),
(63,63,20,20,'','',36093.75,0.00,0.00,9.00,3248.44,9.00,3248.44,0.00,0.00,42591.00),
(64,64,127,16,'','',0.00,0.00,0.00,9.00,0.00,9.00,0.00,NULL,0.00,0.00),
(65,65,132,21,'','',142500.00,0.00,0.00,9.00,12825.00,9.00,12825.00,NULL,0.00,168150.00),
(66,66,487,5,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(67,67,422,5,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(68,68,450,36,NULL,'',480000.00,0.00,0.00,9.00,43200.00,9.00,43200.00,NULL,0.00,566400.00),
(69,69,451,22,NULL,'',288000.00,0.00,0.00,9.00,25920.00,9.00,25920.00,NULL,0.00,339840.00),
(70,70,452,23,NULL,'',288000.00,0.00,0.00,9.00,25920.00,9.00,25920.00,NULL,0.00,339840.00),
(71,71,453,24,NULL,'',288000.00,0.00,0.00,9.00,25920.00,9.00,25920.00,NULL,0.00,339840.00),
(72,72,454,25,NULL,'',288000.00,0.00,0.00,9.00,25920.00,9.00,25920.00,NULL,0.00,339840.00),
(73,73,346,5,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',60000.00,0.00,0.00,9.00,5400.00,9.00,5400.00,NULL,0.00,70800.00),
(74,74,525,5,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',150000.00,0.00,0.00,9.00,13500.00,9.00,13500.00,NULL,0.00,177000.00),
(75,75,350,5,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',500000.00,0.00,0.00,9.00,45000.00,9.00,45000.00,NULL,0.00,590000.00),
(76,76,347,55,NULL,'',100000.00,0.00,0.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00),
(77,77,348,56,NULL,'',75000.00,0.00,0.00,9.00,6750.00,9.00,6750.00,NULL,0.00,88500.00),
(78,78,349,57,NULL,'',65000.00,0.00,0.00,9.00,5850.00,9.00,5850.00,NULL,0.00,76700.00),
(79,79,30,37,'','',247500.00,0.00,0.00,9.00,22275.00,9.00,22275.00,0.00,0.00,292050.00),
(80,80,264,16,'','Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',50000.00,0.00,0.00,9.00,4500.00,9.00,4500.00,NULL,0.00,59000.00),
(82,82,169,5,'','',20000.00,0.00,0.00,9.00,1800.00,9.00,1800.00,NULL,0.00,23600.00),
(83,83,171,31,'','',50000.00,0.00,0.00,9.00,4500.00,9.00,4500.00,NULL,0.00,59000.00),
(84,84,172,30,'','',25000.00,0.00,0.00,9.00,2250.00,9.00,2250.00,NULL,0.00,29500.00),
(85,85,174,41,'','',25000.00,0.00,0.00,9.00,2250.00,9.00,2250.00,NULL,0.00,29500.00),
(86,86,107,24,'','',60000.00,0.00,0.00,9.00,5400.00,9.00,5400.00,NULL,0.00,70800.00),
(87,87,269,5,'','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',5000.00,0.00,0.00,9.00,450.00,9.00,450.00,NULL,0.00,5900.00);

/*Table structure for table `invoice_receipt_details` */

DROP TABLE IF EXISTS `invoice_receipt_details`;

CREATE TABLE `invoice_receipt_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_receipt_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `received_amount` decimal(10,2) DEFAULT 0.00,
  `tds` decimal(10,2) DEFAULT 0.00,
  `total_received_amount` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `invoice_receipt_details` */

insert  into `invoice_receipt_details`(`id`,`invoice_receipt_id`,`invoice_id`,`received_amount`,`tds`,`total_received_amount`) values 
(4,34,79,100000.00,2000.00,102000.00),
(5,35,79,100000.00,2000.00,102000.00);

/*Table structure for table `invoice_receipts` */

DROP TABLE IF EXISTS `invoice_receipts`;

CREATE TABLE `invoice_receipts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(20) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `payment_mode` enum('Cash','Bank','Bank Transfer') DEFAULT NULL,
  `transaction_no` varchar(50) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `cheque_no` varchar(6) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `amount_in_words` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `invoice_receipts` */

insert  into `invoice_receipts`(`id`,`receipt_no`,`receipt_date`,`client_id`,`payment_mode`,`transaction_no`,`transaction_date`,`cheque_no`,`cheque_date`,`bank_name`,`amount`,`amount_in_words`,`modified_by`,`modified`,`created_by`,`created`) values 
(34,'2022-23/R00001','2022-07-19',3,'Cash','',NULL,'',NULL,'',102000.00,'Rupees One Lakh Two Thousand and Zero paise only',1,'2022-07-20 12:40:37',1,'2022-07-20 12:40:37'),
(35,'2022-23/R00002','2022-07-20',3,'Bank','',NULL,'589595','2022-07-20','canara bank',102000.00,'Rupees One Lakh Two Thousand and Zero paise only',1,'2022-07-20 12:41:04',1,'2022-07-20 12:41:04');

/*Table structure for table `invoices` */

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(50) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `amount_before_tax` decimal(10,2) DEFAULT 0.00,
  `cgst` decimal(10,2) DEFAULT 0.00,
  `sgst` decimal(10,2) DEFAULT 0.00,
  `igst` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `received_amount` decimal(10,2) DEFAULT 0.00,
  `amount_in_words` varchar(255) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT 1,
  `is_cancel` tinyint(1) DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

/*Data for the table `invoices` */

insert  into `invoices`(`id`,`invoice_no`,`invoice_date`,`project_id`,`amount_before_tax`,`cgst`,`sgst`,`igst`,`total_amount`,`received_amount`,`amount_in_words`,`is_draft`,`is_cancel`,`modified`,`created`) values 
(1,'2021-22/00001','2021-08-16',2,120312.49,0.00,0.00,0.00,141969.00,0.00,'Rupees One Lakh Forty One Thousand Nine Hundred Sixty Eight and Seventy Three paise only',1,0,'2022-06-13 17:19:40','2021-09-29 11:11:56'),
(4,'2021-22/00002','2021-10-04',6,50000.00,0.00,0.00,0.00,59000.00,0.00,'Rupees Fifty Nine Thousand and Zero paise only',1,0,'2021-10-05 11:10:18','2021-10-04 15:39:50'),
(5,'2021-22/00003','2021-10-04',6,7000.00,0.00,0.00,0.00,8260.00,0.00,'Rupees Eight Thousand Two Hundred Sixty and Zero paise only',1,0,'2021-10-16 17:25:44','2021-10-04 15:39:53'),
(7,'2021-22/00004','2021-10-19',8,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2021-12-14 16:18:40','2021-10-19 17:26:16'),
(8,'2021-22/00005','2021-10-19',1,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-06-13 17:57:30','2021-10-19 17:38:10'),
(9,'2021-22/00006','2021-10-19',1,26500.00,0.00,0.00,0.00,31270.00,0.00,'Rupees Thirty One Thousand Two Hundred Seventy and Zero paise only',1,0,'2022-06-13 17:57:30','2021-10-19 17:38:18'),
(10,'2021-22/00007','2021-10-19',1,53000.00,0.00,0.00,0.00,62540.00,0.00,'Rupees Sixty Two Thousand Five Hundred Forty and Zero paise only',1,0,'2022-06-13 17:57:30','2021-10-19 17:38:27'),
(11,'2021-22/00008','2021-10-19',1,53000.00,0.00,0.00,0.00,62540.00,0.00,'Rupees Sixty Two Thousand Five Hundred Forty and Zero paise only',1,0,'2022-06-13 17:57:30','2021-10-19 17:38:37'),
(12,'2021-22/00009','2021-10-19',1,53000.00,0.00,0.00,0.00,62540.00,0.00,'Rupees Sixty Two Thousand Five Hundred Forty and Zero paise only',1,0,'2022-06-13 17:57:30','2021-10-19 17:38:57'),
(13,'2021-22/00010','2021-10-19',1,53000.00,0.00,0.00,0.00,62540.00,0.00,'Rupees Sixty Two Thousand Five Hundred Forty and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-19 17:39:06'),
(14,'2021-22/00011','2021-10-25',3,302500.00,27225.00,27225.00,0.00,356950.00,0.00,'Rupees Three Lakhs Fifty Six Thousand Nine Hundred Fifty and Zero paise only',1,0,'2022-06-13 18:03:04','2021-10-25 12:56:58'),
(15,'2021-22/00012','2021-10-28',1,53000.00,0.00,0.00,0.00,62540.00,0.00,'Rupees Sixty Two Thousand Five Hundred Forty and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-28 14:57:21'),
(16,'2021-22/00014','2021-10-29',13,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2021-10-29 10:45:04','2021-10-29 10:39:13'),
(17,'2021-22/00015','2021-10-29',13,60000.00,0.00,0.00,0.00,70800.00,0.00,'Rupees Seventy Thousand Eight Hundred and Zero paise only',1,0,'2021-10-29 10:46:14','2021-10-29 10:39:23'),
(18,'2021-22/00016','2021-10-29',13,60000.00,0.00,0.00,0.00,70800.00,0.00,'Rupees Seventy Thousand Eight Hundred and Zero paise only',1,0,'2021-10-29 10:47:03','2021-10-29 10:39:31'),
(19,'2021-22/00017','2021-10-29',13,175000.00,0.00,0.00,0.00,206500.00,0.00,'Rupees Two Lakhs Six Thousand Five Hundred and Zero paise only',1,0,'2021-10-29 10:47:12','2021-10-29 10:39:46'),
(20,'2021-22/00018','2021-10-29',13,75000.00,0.00,0.00,0.00,88500.00,0.00,'Rupees Eighty Eight Thousand Five Hundred and Zero paise only',1,0,'2021-10-29 10:47:21','2021-10-29 10:39:56'),
(21,'2021-22/00019','2021-10-29',13,50000.00,0.00,0.00,0.00,59000.00,0.00,'Rupees Fifty Nine Thousand and Zero paise only',1,0,'2021-10-29 10:47:31','2021-10-29 10:40:05'),
(22,'2021-22/00020','2021-10-29',14,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2021-10-29 15:29:34','2021-10-29 15:29:18'),
(23,'2021-22/00021','2021-10-30',1,23350.00,0.00,0.00,0.00,27553.00,0.00,'Rupees Twenty Seven Thousand Five Hundred Fifty Three and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:29:50'),
(24,'2021-22/00022','2021-10-30',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:34:03'),
(25,'2021-22/00023','2021-10-30',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:38:11'),
(26,'2021-22/00024','2021-10-30',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:46:13'),
(27,'2021-22/00025','2021-10-30',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:48:12'),
(28,'2021-22/00026','2021-10-30',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:31','2021-10-30 12:48:49'),
(29,'2021-22/00027','2021-10-30',1,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-06-13 17:57:32','2021-10-30 14:45:23'),
(31,'2021-22/00029','2021-12-17',15,95000.00,8550.00,8550.00,0.00,112100.00,0.00,'Rupees One Lakh Twenty One Thousand One Hundred and Zero paise only',1,0,'2022-03-21 11:46:39','2021-12-17 17:22:47'),
(33,'2021-22/00033','2021-12-17',8,55125.00,0.00,0.00,0.00,65048.00,0.00,'Rupees Sixty Five Thousand Forty Eight and Zero paise only',1,0,'2021-12-17 18:07:05','2021-12-17 18:06:00'),
(34,'2021-22/00032','2021-12-17',8,50000.00,0.00,0.00,0.00,59000.00,0.00,'Rupees Fifty Nine Thousand and Zero paise only',1,0,'2021-12-17 18:07:00','2021-12-17 18:06:07'),
(35,'2021-22/00031','2021-12-17',8,117500.00,0.00,0.00,0.00,138650.00,0.00,'Rupees One Lakh Thirty Eight Thousand Six Hundred Fifty and Zero paise only',1,0,'2021-12-17 18:06:55','2021-12-17 18:06:13'),
(36,'2021-22/00030','2021-12-17',8,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2021-12-17 18:06:47','2021-12-17 18:06:19'),
(37,'','2021-12-30',22,84250.00,0.00,0.00,0.00,99415.00,0.00,'Rupees Ninety Nine Thousand Four Hundred Fifteen and Zero paise only',1,0,'2021-12-30 15:24:04','2021-12-28 11:08:23'),
(38,'2021-22/00035','2021-12-30',24,47321.00,0.00,0.00,0.00,55839.00,0.00,'Rupees Fifty Five Thousand Eight Hundred Thirty Nine and Zero paise only',1,0,'2021-12-30 16:19:54','2021-12-30 15:36:02'),
(39,'2021-22/00036','2022-01-07',25,500000.00,0.00,0.00,0.00,590000.00,0.00,'Rupees Five Lakhs Ninety Thousand and Zero paise only',1,0,'2022-01-07 18:15:39','2022-01-07 18:03:26'),
(40,'2021-22/00037','2022-01-10',25,500000.00,0.00,0.00,0.00,590000.00,0.00,'Rupees Five Lakhs Ninety Thousand and Zero paise only',1,0,'2022-01-10 09:45:11','2022-01-10 09:43:55'),
(47,'2021-22/00038','2022-01-11',26,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-01-11 16:18:39','2022-01-11 16:09:21'),
(48,'2021-22/00039','2022-01-11',26,700000.00,0.00,0.00,0.00,826000.00,0.00,'Rupees Eight Lakhs Twenty Six Thousand and Zero paise only',1,0,'2022-01-11 16:19:00','2022-01-11 16:09:24'),
(49,'2021-22/00040','2022-01-24',27,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-01-24 16:12:58','2022-01-24 14:47:09'),
(50,'2021-22/00041','2022-02-10',1,23350.00,0.00,0.00,0.00,27553.00,0.00,'Rupees Twenty Seven Thousand Five Hundred Fifty Three and Zero paise only',1,0,'2022-06-13 17:57:32','2022-02-10 16:06:21'),
(51,'2021-22/00042','2022-02-10',1,46700.00,0.00,0.00,0.00,55106.00,0.00,'Rupees Fifty Five Thousand One Hundred Six and Zero paise only',1,0,'2022-06-13 17:57:32','2022-02-10 16:06:31'),
(52,'2021-22/00043','2022-02-10',1,28020.00,0.00,0.00,0.00,33064.00,0.00,'Rupees Thirty Three Thousand Sixty Four and Zero paise only',1,0,'2022-06-13 17:57:32','2022-02-10 16:06:42'),
(53,'2021-22/00044','2022-03-01',17,20000.00,0.00,0.00,0.00,23600.00,0.00,'Rupees Twenty Three Thousand Six Hundred and Zero paise only',1,0,'2022-03-01 14:21:06','2022-03-01 13:23:19'),
(54,'2021-22/00045','2022-03-01',17,7133.00,0.00,0.00,0.00,8417.00,0.00,'Rupees Eight Thousand Four Hundred Seventeen and Zero paise only',1,0,'2022-03-01 13:27:10','2022-03-01 13:23:38'),
(55,'2021-22/00048','2022-03-01',17,14266.00,0.00,0.00,0.00,16834.00,0.00,'Rupees Sixteen Thousand Eight Hundred Thirty Four and Zero paise only',1,0,'2022-03-01 13:27:14','2022-03-01 13:23:48'),
(56,'2021-22/00047','2022-03-01',17,14266.00,0.00,0.00,0.00,16834.00,0.00,'Rupees Sixteen Thousand Eight Hundred Thirty Four and Zero paise only',1,0,'2022-03-01 13:27:13','2022-03-01 13:23:56'),
(57,'2021-22/00046','2022-03-01',17,14266.00,0.00,0.00,0.00,16834.00,0.00,'Rupees Sixteen Thousand Eight Hundred Thirty Four and Zero paise only',1,0,'2022-03-01 13:27:12','2022-03-01 13:24:03'),
(58,'2021-22/00049','2022-03-01',17,25000.00,0.00,0.00,0.00,29500.00,0.00,'Rupees Twenty Nine Thousand Five Hundred and Zero paise only',1,0,'2022-03-01 13:27:14','2022-03-01 13:24:11'),
(59,'2021-22/00050','2022-03-17',2,18046.88,0.00,0.00,0.00,21295.00,0.00,'Rupees Twenty One Thousand Two Hundred Ninety Five and Zero paise only',1,0,'2022-06-13 17:19:40','2022-03-17 12:11:28'),
(60,'2021-22/00051','2022-03-17',2,18046.88,0.00,0.00,0.00,21295.00,0.00,'Rupees Twenty One Thousand Two Hundred Ninety Five and Zero paise only',1,0,'2022-06-13 17:19:41','2022-03-17 12:11:33'),
(61,'2021-22/00052','2022-03-17',2,50531.25,0.00,0.00,0.00,59627.00,0.00,'Rupees Fifty Nine Thousand Six Hundred Twenty Seven and Zero paise only',1,0,'2022-06-13 17:19:41','2022-03-17 12:11:34'),
(62,'2021-22/00053','2022-03-17',2,36093.75,0.00,0.00,0.00,42591.00,0.00,'Rupees Forty Two Thousand Five Hundred Ninety One and Zero paise only',1,0,'2022-06-13 17:19:41','2022-03-17 12:11:42'),
(63,'2021-22/00054','2022-03-17',2,36093.75,0.00,0.00,0.00,42591.00,0.00,'Rupees Forty Two Thousand Five Hundred Ninety One and Zero paise only',1,0,'2022-06-13 17:19:41','2022-03-17 12:11:59'),
(64,NULL,'2022-03-21',15,0.00,0.00,0.00,0.00,0.00,0.00,'Zero',1,0,'2022-03-21 12:10:09','2022-03-21 12:10:09'),
(65,'2021-22/00055','2022-03-21',15,142500.00,12825.00,12825.00,0.00,168150.00,0.00,'Rupees One Lakh Sixty Eight Thousand One Hundred Fifty and Zero paise only',1,0,'2022-03-22 11:44:54','2022-03-21 12:10:21'),
(66,'2022-23/00001','2022-04-05',49,100000.00,9000.00,9000.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-04-06 13:02:02','2022-04-05 15:12:52'),
(67,'2022-23/00002','2022-04-05',42,100000.00,9000.00,9000.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-04-13 11:05:21','2022-04-05 17:52:54'),
(68,'2022-23/00004','2022-04-13',45,480000.00,0.00,0.00,0.00,566400.00,0.00,'Rupees Five Lakhs Sixty Six Thousand Four Hundred and Zero paise only',1,0,'2022-04-18 17:54:04','2022-04-13 10:43:42'),
(69,'2022-23/00005','2022-04-13',45,288000.00,0.00,0.00,0.00,339840.00,0.00,'Rupees Three Lakhs Thirty Nine Thousand Eight Hundred Forty and Zero paise only',1,0,'2022-04-18 17:54:14','2022-04-13 10:43:44'),
(70,'2022-23/00006','2022-04-13',45,288000.00,0.00,0.00,0.00,339840.00,0.00,'Rupees Three Lakhs Thirty Nine Thousand Eight Hundred Forty and Zero paise only',1,0,'2022-04-18 17:54:22','2022-04-13 10:43:55'),
(71,'2022-23/00007','2022-04-13',45,288000.00,0.00,0.00,0.00,339840.00,0.00,'Rupees Three Lakhs Thirty Nine Thousand Eight Hundred Forty and Zero paise only',1,0,'2022-04-18 17:54:27','2022-04-13 10:44:05'),
(72,'2022-23/00008','2022-04-13',45,288000.00,0.00,0.00,0.00,339840.00,0.00,'Rupees Three Lakhs Thirty Nine Thousand Eight Hundred Forty and Zero paise only',1,0,'2022-04-18 17:54:33','2022-04-13 10:44:14'),
(73,'2022-23/00003','2022-04-14',36,60000.00,0.00,0.00,0.00,70800.00,0.00,'Rupees Seventy Thousand Eight Hundred and Zero paise only',1,0,'2022-04-16 14:46:33','2022-04-14 15:53:42'),
(74,'2022-23/00009','2022-04-25',46,150000.00,0.00,0.00,0.00,177000.00,0.00,'Rupees One Lakh Seventy Seven Thousand and Zero paise only',1,0,'2022-04-25 13:27:06','2022-04-25 13:24:32'),
(75,'2022-23/00010','2022-04-29',37,500000.00,45000.00,45000.00,0.00,590000.00,0.00,'Rupees Five Lakhs Ninety Thousand and Zero paise only',1,0,'2022-04-29 18:04:34','2022-04-29 14:41:52'),
(76,'2022-23/00011','2022-05-16',36,100000.00,0.00,0.00,0.00,118000.00,0.00,'Rupees One Lakh Eighteen Thousand and Zero paise only',1,0,'2022-05-16 17:24:37','2022-05-16 15:56:30'),
(77,'2022-23/00012','2022-05-16',36,75000.00,0.00,0.00,0.00,88500.00,0.00,'Rupees Eighty Eight Thousand Five Hundred and Zero paise only',1,0,'2022-05-16 17:24:42','2022-05-16 15:56:34'),
(78,'2022-23/00013','2022-05-16',36,65000.00,0.00,0.00,0.00,76700.00,0.00,'Rupees Seventy Six Thousand Seven Hundred and Zero paise only',1,0,'2022-05-16 17:24:47','2022-05-16 15:56:36'),
(79,'2022-23/00014','2022-05-31',3,247500.00,22275.00,22275.00,0.00,292050.00,204000.00,'Rupees Three Lakhs Twenty Four Thousand Five Hundred and Zero paise only',0,0,'2022-07-20 12:41:04','2022-05-31 11:51:27'),
(80,NULL,'2022-06-04',27,50000.00,0.00,0.00,0.00,59000.00,0.00,'Rupees Fifty Nine Thousand and Zero paise only',1,0,'2022-06-04 15:16:48','2022-06-04 15:16:48'),
(82,NULL,'2022-06-07',20,20000.00,0.00,0.00,0.00,23600.00,0.00,'Rupees Twenty Three Thousand Six Hundred and Zero paise only',1,0,'2022-06-07 11:34:01','2022-06-07 11:34:01'),
(83,NULL,'2022-06-07',20,50000.00,0.00,0.00,0.00,59000.00,0.00,'Rupees Fifty Nine Thousand and Zero paise only',1,0,'2022-06-07 11:46:11','2022-06-07 11:46:11'),
(84,NULL,'2022-06-07',20,25000.00,0.00,0.00,0.00,29500.00,0.00,'Rupees Twenty Nine Thousand Five Hundred and Zero paise only',1,0,'2022-06-07 11:46:16','2022-06-07 11:46:16'),
(85,NULL,'2022-06-07',20,25000.00,0.00,0.00,0.00,29500.00,0.00,'Rupees Twenty Nine Thousand Five Hundred and Zero paise only',1,0,'2022-06-07 11:46:20','2022-06-07 11:46:20'),
(86,NULL,'2022-06-07',13,60000.00,0.00,0.00,0.00,70800.00,0.00,'Rupees Seventy Thousand Eight Hundred and Zero paise only',1,0,'2022-06-07 11:51:11','2022-06-07 11:51:11'),
(87,'2022-23/00015','2022-06-07',32,5000.00,450.00,450.00,0.00,5900.00,0.00,'Rupees Five Lakhs Ninety Thousand and Zero paise only',1,0,'2022-07-07 11:16:17','2022-06-07 11:56:48');

/*Table structure for table `inward_outwards` */

DROP TABLE IF EXISTS `inward_outwards`;

CREATE TABLE `inward_outwards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `register_no` varchar(10) DEFAULT NULL,
  `subject` varchar(500) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `handler_id` varchar(255) DEFAULT NULL,
  `file_copy` varchar(255) DEFAULT NULL,
  `file_copy_dir` varchar(255) DEFAULT NULL,
  `courier_slip` varchar(255) DEFAULT NULL,
  `courier_slip_dir` varchar(255) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assinged_on` datetime DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inward_outwards` */

/*Table structure for table `lead_types` */

DROP TABLE IF EXISTS `lead_types`;

CREATE TABLE `lead_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_type` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `lead_types` */

insert  into `lead_types`(`id`,`lead_type`,`modified_by`,`modified`,`created_by`,`created`) values 
(7,'Reference from Existing Clients',1,'2021-12-03 11:43:19',1,'2021-12-03 10:50:54'),
(8,'Tender Screening',1,'2021-12-03 10:51:08',1,'2021-12-03 10:51:08'),
(9,'Social Organization',1,'2021-12-03 11:42:25',1,'2021-12-03 11:42:25'),
(10,'Marketing Database',1,'2021-12-03 11:43:46',1,'2021-12-03 11:43:46'),
(11,'Government Officers',1,'2021-12-03 11:44:21',1,'2021-12-03 11:44:21'),
(12,'LAC Meetings of MIDC',1,'2021-12-03 11:45:19',1,'2021-12-03 11:45:19');

/*Table structure for table `levels` */

DROP TABLE IF EXISTS `levels`;

CREATE TABLE `levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `levels` */

insert  into `levels`(`id`,`project_id`,`level`) values 
(1,1,'1200ft'),
(2,2,'12 ft'),
(3,1,'2400ft');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `locations` */

insert  into `locations`(`id`,`location`,`description`) values 
(3,'Cabinet No.2','Admin. Department ');

/*Table structure for table `meeting_report_images` */

DROP TABLE IF EXISTS `meeting_report_images`;

CREATE TABLE `meeting_report_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meeting_report_id` int(11) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_dir` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `meeting_report_images` */

insert  into `meeting_report_images`(`id`,`meeting_report_id`,`remarks`,`image`,`image_dir`) values 
(1,1,'kolp','Red-Apple-vector-PNG.png','122e72d6-ef9e-44c6-8e2b-9e693fecd4ed'),
(2,2,'kolp','Red-Apple-vector-PNG.png','5c71999a-c17e-4253-857f-8b99fb331e63'),
(3,3,'kolp','Red-Apple-vector-PNG.png','131418bc-0e3f-4f0e-b6b0-f34551adbac4');

/*Table structure for table `meeting_report_stakeholders` */

DROP TABLE IF EXISTS `meeting_report_stakeholders`;

CREATE TABLE `meeting_report_stakeholders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_report_id` int(11) DEFAULT NULL,
  `stakeholder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `meeting_report_stakeholders` */

insert  into `meeting_report_stakeholders`(`id`,`meeting_report_id`,`stakeholder_id`) values 
(1,1,60),
(3,3,60),
(5,2,60),
(6,2,61);

/*Table structure for table `meeting_report_visitors` */

DROP TABLE IF EXISTS `meeting_report_visitors`;

CREATE TABLE `meeting_report_visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_report_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `meeting_report_visitors` */

insert  into `meeting_report_visitors`(`id`,`meeting_report_id`,`name`,`email`) values 
(1,1,'vinod','vs10301030@gmail.cpm'),
(2,2,'vinod','vs10301030@gmail.cpm'),
(4,3,'vinod','vs10301030@gmail.cpm'),
(6,3,'fikol','vs10301030@gmail.cpm'),
(7,2,'fikolkolp','vs10301030@gmail.cpm'),
(8,2,'ghi','vs10301030@gmail.cpm'),
(9,3,'ghi','vs10301030@gmail.cpm');

/*Table structure for table `meeting_reports` */

DROP TABLE IF EXISTS `meeting_reports`;

CREATE TABLE `meeting_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meeting_no` varchar(20) DEFAULT NULL,
  `meeting_date` date DEFAULT NULL,
  `meeting_time` time DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `report` text DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `meeting_reports` */

insert  into `meeting_reports`(`id`,`meeting_no`,`meeting_date`,`meeting_time`,`employee_id`,`project_id`,`report`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,'P00057/M0001','2022-07-23','08:30:00',38,53,'',NULL,'2022-07-23 07:03:53',NULL,'2022-07-23 07:02:43'),
(2,'2021-22/00002/M0001','2022-07-23','08:15:00',36,53,'dhgdfjks',NULL,'2022-07-23 09:10:17',NULL,'2022-07-23 07:04:46'),
(3,'P00057/M0002','2022-07-23','08:30:00',38,53,'',NULL,'2022-07-23 09:08:01',NULL,'2022-07-23 07:05:22');

/*Table structure for table `project_stakeholders` */

DROP TABLE IF EXISTS `project_stakeholders`;

CREATE TABLE `project_stakeholders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `stakeholder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `project_stakeholders` */

insert  into `project_stakeholders`(`id`,`project_id`,`stakeholder_id`) values 
(1,53,60),
(2,53,61),
(4,51,60),
(5,50,61),
(7,49,61),
(8,34,60);

/*Table structure for table `project_tasks` */

DROP TABLE IF EXISTS `project_tasks`;

CREATE TABLE `project_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `fees_type` varchar(255) DEFAULT NULL,
  `days_require` int(20) DEFAULT NULL,
  `list_order` varchar(2) DEFAULT NULL,
  `percentage` decimal(10,2) DEFAULT 0.00,
  `fees` decimal(14,2) DEFAULT NULL,
  `service` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `assigned_by` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assigned_datetime` datetime DEFAULT NULL,
  `done_by` int(11) DEFAULT NULL,
  `done_datetime` datetime DEFAULT NULL,
  `target_date` date DEFAULT NULL,
  `responsible_1` int(11) DEFAULT NULL,
  `responsible_2` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=535 DEFAULT CHARSET=latin1;

/*Data for the table `project_tasks` */

insert  into `project_tasks`(`id`,`project_id`,`task_id`,`fees_type`,`days_require`,`list_order`,`percentage`,`fees`,`service`,`description`,`assigned_by`,`assigned_to`,`assigned_datetime`,`done_by`,`done_datetime`,`target_date`,`responsible_1`,`responsible_2`,`modified_by`,`modified`,`created_by`,`created`) values 
(1,1,5,'Lumpsum Retainer',7,'01',0.00,100000.00,'On Signing of the Agreement','Retainer/advance will be converted to our fees after signing of agreement.',1,20,'2022-06-08 17:41:13',1,'2021-10-30 14:45:23','2022-06-30',36,38,1,'2022-06-08 17:41:13',1,'2021-09-18 13:10:26'),
(2,1,16,'Percentage',7,'02',5.00,23350.00,'','',1,20,'2021-10-19 17:35:17',1,'2021-10-30 12:29:50',NULL,NULL,NULL,1,'2021-10-30 12:29:50',1,'2021-09-18 13:10:26'),
(3,1,17,'Percentage',7,'03',10.00,46700.00,'On Submitting Conceptual Design and rough cost of estimate','Ascertain Client',1,20,'2021-10-19 17:35:51',1,'2021-10-30 12:34:03',NULL,NULL,NULL,1,'2021-10-30 12:34:03',1,'2021-09-18 13:10:26'),
(4,1,18,'Percentage',7,'04',10.00,46700.00,'On incorporating clients suggestion and finalising the design after brief 3d Visualisation','Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client',1,20,'2021-10-19 17:36:29',1,'2021-10-30 12:38:11',NULL,NULL,NULL,1,'2021-10-30 12:38:11',1,'2021-09-18 13:10:26'),
(5,1,19,'Percentage',60,'05',10.00,46700.00,'Submitting the final Design to statutory bodies approval like MIDC CIDCO KDMC BMC ','Prepare drawings necessary for Client',1,20,'2021-10-19 17:37:11',1,'2021-10-30 12:46:13',NULL,NULL,NULL,1,'2021-10-30 12:46:13',1,'2021-09-18 13:10:26'),
(6,1,20,'Percentage',45,'06',10.00,46700.00,'Final Working drawings Specifications and BOQ','Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method of payments, quality control procedures on materia',1,20,'2021-10-19 17:37:42',1,'2021-10-30 12:48:12',NULL,53,38,1,'2022-04-20 14:27:46',1,'2021-09-18 13:10:26'),
(7,1,21,'Percentage',15,'07',10.00,46700.00,'On Inviting receiving and analysing tenders and advising clients on appointment of contractor and Vendors','Invite, receive and analyze tenders; advise Client on appointment of contractors.',1,20,'2021-10-28 14:51:24',1,'2021-10-30 12:48:49','2021-10-30',57,38,1,'2022-04-20 14:28:41',1,'2021-09-18 13:10:26'),
(8,1,22,'Percentage',0,'09',10.00,46700.00,'Completion of Footings and Plinths Beams','',1,36,'2022-02-10 16:03:48',36,'2022-02-10 16:06:31','2022-02-10',57,38,1,'2022-04-20 14:29:34',1,'2021-09-18 13:10:26'),
(9,1,23,'Percentage',0,'10',6.00,28020.00,'','',1,36,'2022-02-10 16:04:20',36,'2022-02-10 16:06:42','2022-02-10',57,38,1,'2022-04-20 14:29:51',1,'2021-09-18 13:10:26'),
(10,1,24,'Percentage',0,'11',6.00,28020.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:30:14',1,'2021-09-18 13:10:26'),
(11,1,25,'Percentage',0,'12',6.00,28020.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:30:31',1,'2021-09-18 13:10:26'),
(12,1,26,'Percentage',0,'13',6.00,28020.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:30:58',1,'2021-09-18 13:10:26'),
(13,1,27,'Percentage',15,'14',6.00,28020.00,'On Submitting completion report and drawings for issuance of completion certificate / occupancy certificate by statutory approval whereever required and on issue as built drawwings','Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ Occupancy Certificate\" from statutory authorities, wherever required.Issue two sets of as built drawings including services and ',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:31:17',1,'2021-09-18 13:10:26'),
(14,1,36,'Percentage',10,'08',5.00,23350.00,'Submitting of working drawings and details required for commencement of work at site','Prepare and issue working drawings and details for proper execution of works during construction.Approve samples of various elements and components.Check and approve shop drawings submitted by the contractor/ vendors.Visit the site of work, at intervals m',1,36,'2022-02-10 16:03:12',36,'2022-02-10 16:06:21','2022-02-10',57,38,1,'2022-04-20 14:29:01',1,'2021-09-18 13:10:26'),
(15,2,5,'Lumpsum Retainer',7,'1',0.00,120312.49,'','',1,20,'2021-09-21 17:43:45',20,'2021-09-29 11:11:56','2021-09-01',36,36,1,'2022-04-20 14:38:10',20,'2021-09-21 17:22:32'),
(16,2,16,'Percentage',7,'2',5.00,18046.88,'','',1,36,'2022-03-09 13:10:01',36,'2022-03-17 12:11:24','2022-03-09',38,55,1,'2022-04-20 14:39:08',20,'2021-09-21 17:22:32'),
(17,2,17,'Percentage',7,'3',5.00,18046.88,'','',1,36,'2022-03-09 13:10:42',36,'2022-03-17 12:11:33','2022-03-09',38,55,1,'2022-04-20 14:39:25',20,'2021-09-21 17:22:32'),
(18,2,18,'Percentage',7,'4',14.00,50531.25,'','',1,36,'2022-03-09 13:11:16',36,'2022-03-17 12:11:34','2022-03-09',38,55,1,'2022-04-20 14:39:51',20,'2021-09-21 17:22:32'),
(19,2,19,'Percentage',15,'5',10.00,36093.75,'','',1,36,'2022-03-09 13:56:13',36,'2022-03-17 12:11:42','2022-03-09',58,38,1,'2022-04-20 14:40:07',20,'2021-09-21 17:22:32'),
(20,2,20,'Percentage',10,'6',10.00,36093.75,'','',1,36,'2022-03-09 13:56:52',36,'2022-03-17 12:11:59','2022-03-09',58,38,1,'2022-04-20 14:40:19',20,'2021-09-21 17:22:32'),
(21,2,21,'Percentage',0,'7',10.00,36093.75,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:40:32',20,'2021-09-21 17:22:32'),
(22,2,36,'Percentage',0,'8',10.00,36093.75,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:40:54',20,'2021-09-21 17:22:32'),
(23,2,22,'Percentage',0,'9',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:41:06',20,'2021-09-21 17:22:32'),
(24,2,23,'Percentage',0,'10',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:41:40',20,'2021-09-21 17:22:32'),
(25,2,24,'Percentage',0,'11',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:41:51',20,'2021-09-21 17:22:32'),
(26,2,25,'Percentage',0,'12',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:42:03',20,'2021-09-21 17:22:32'),
(27,2,26,'Percentage',0,'13',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:42:16',20,'2021-09-21 17:22:32'),
(28,2,27,'Percentage',0,'14',6.00,21656.25,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 14:42:28',20,'2021-09-21 17:22:32'),
(29,3,5,'Lumpsum Retainer',7,'1',0.00,302500.00,'','',1,20,'2021-10-25 12:56:38',20,'2021-10-25 12:56:58','2021-10-25',20,36,1,'2022-04-20 14:46:03',20,'2021-09-24 13:08:46'),
(30,3,37,'Lumpsum',7,'15',0.00,275000.00,'','',1,36,'2022-05-31 11:50:57',1,'2022-05-31 11:51:27','2022-05-31',58,38,1,'2022-05-31 11:51:27',20,'2021-09-24 13:08:46'),
(31,4,38,'Lumpsum',15,'1',0.00,195000.00,'','',1,38,'2022-06-07 11:58:39',NULL,NULL,'2022-06-07',NULL,NULL,1,'2022-06-07 11:58:39',20,'2021-09-24 15:17:06'),
(32,4,15,'Lumpsum',10,'2',0.00,65000.00,'Conversion of Documented Auto Cad Drawing into PRE-DCR Drawing','The Entire Drawing Format will be Converted into expected format of MIDC online Approval System i.e PRE -DCR and simultaneously create a AUTO-DCR report for the same and upload the same',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-09-24 15:17:06',20,'2021-09-24 15:17:06'),
(33,4,29,'Lumpsum',10,'3',0.00,65000.00,'The Building Plan Approval will be received.','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-09-24 15:17:06',20,'2021-09-24 15:17:06'),
(34,4,31,'Lumpsum',7,'4',0.00,65000.00,'The Provisional Fire NOC will be Received form Fire Department','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-09-24 15:17:06',20,'2021-09-24 15:17:06'),
(35,4,14,'Lumpsum',7,'5',0.00,32500.00,'On receipt of Occupancy Certificate','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-09-24 15:17:06',20,'2021-09-24 15:17:06'),
(51,6,32,'Lumpsum',5,'1',0.00,50000.00,'','',1,20,'2021-10-04 15:20:35',1,'2021-10-04 15:39:50','2021-10-01',NULL,NULL,1,'2021-10-04 15:39:50',20,'2021-09-24 16:27:42'),
(52,6,27,'Lumpsum',10,'2',0.00,7000.00,'','',1,20,'2021-10-04 15:20:53',1,'2021-10-04 15:39:53','2021-10-01',NULL,NULL,1,'2021-10-04 15:39:53',20,'2021-09-24 16:27:42'),
(53,2,15,'Lumpsum',0,'15',0.00,25000.00,'','',NULL,NULL,NULL,1,'2021-09-28 19:21:56',NULL,58,38,1,'2022-04-20 14:43:05',NULL,NULL),
(54,7,5,'Lumpsum Retainer',7,'01',0.00,150000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(55,7,16,'Percentage',7,'02',5.00,22704.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(56,7,17,'Percentage',7,'03',10.00,45408.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(57,7,18,'Percentage',7,'04',15.00,68112.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(58,7,19,'Percentage',60,'05',10.00,45408.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(59,7,20,'Percentage',45,'06',10.00,45408.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(60,7,21,'Percentage',15,'07',10.00,45408.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(61,7,22,'Percentage',0,'09',10.00,45408.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(62,7,23,'Percentage',0,'10',6.00,27244.80,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(63,7,24,'Percentage',0,'11',6.00,27244.80,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(64,7,25,'Percentage',0,'12',6.00,27244.80,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(65,7,26,'Percentage',15,'13',6.00,27244.80,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(66,7,27,'Percentage',30,'14',6.00,27244.80,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-11 16:33:01',1,'2021-10-11 16:33:01'),
(67,8,5,'Lumpsum Retainer',7,'1',0.00,100000.00,'','',1,36,'2021-12-17 18:04:34',36,'2021-12-17 18:06:19','2021-12-17',NULL,NULL,36,'2021-12-17 18:06:19',1,'2021-10-11 18:36:54'),
(68,8,11,'Lumpsum',7,'2',10.00,117500.00,'','',1,36,'2021-12-17 18:04:12',36,'2021-12-17 18:06:13','2021-12-17',NULL,NULL,36,'2021-12-17 18:06:13',1,'2021-10-11 18:36:54'),
(69,8,31,'Lumpsum',10,'3',10.00,50000.00,'','',1,36,'2021-12-17 18:03:50',36,'2021-12-17 18:06:07','2021-12-17',NULL,NULL,36,'2021-12-17 18:06:07',1,'2021-10-11 18:36:54'),
(70,8,32,'Lumpsum',5,'4',20.00,100000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-10-18 11:05:46',1,'2021-10-11 18:36:54'),
(89,8,15,'Lumpsum',10,'5',0.00,55125.00,'','',1,36,'2021-12-17 18:03:28',36,'2021-12-17 18:06:00','2021-12-17',NULL,NULL,36,'2021-12-17 18:06:00',20,'2021-10-13 11:48:46'),
(90,8,39,'Lumpsum',7,'6',0.00,100000.00,'','',1,36,'2021-12-17 17:55:53',36,'2021-12-17 18:05:49','2021-12-17',NULL,NULL,36,'2021-12-17 18:05:49',20,'2021-10-13 11:48:46'),
(93,10,41,'Lumpsum',7,'1',0.00,120000.00,'On Receipt of Architect & Engineer Certificate (Advance)','',NULL,NULL,NULL,NULL,NULL,NULL,36,38,1,'2022-04-20 14:54:16',20,'2021-10-23 17:11:25'),
(94,10,11,'Lumpsum',7,'2',0.00,240000.00,'Follow up for  to be provided for building plan approval','Follow up for  to be provided for building plan approval',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 14:54:30',20,'2021-10-23 17:11:25'),
(95,10,31,'Lumpsum',10,'3',0.00,240000.00,'Follow up to be provided for Provisional Fire Noc ','Follow up to be provided for Provisional Fire Noc ',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 14:54:50',20,'2021-10-23 17:11:25'),
(96,10,30,'Lumpsum',30,'4',0.00,240000.00,'Follow up for Pre dcr and auto dcr process of the submitted architectural drawing','Follow up for Pre dcr and auto dcr process of the submitted architectural drawing',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 14:55:03',20,'2021-10-23 17:11:25'),
(97,10,32,'Lumpsum',10,'5',0.00,120000.00,'Follow up for  to be Provided for Building Completion Certificate','Follow up for  to be Provided for Building Completion Certificate',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 14:55:18',20,'2021-10-23 17:11:25'),
(98,10,12,'Lumpsum',30,'6',0.00,120000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 14:55:29',20,'2021-10-23 17:11:25'),
(99,11,24,'Lumpsum',0,'1',0.00,29250.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:10:29',20,'2021-10-25 09:52:41'),
(100,11,25,'Lumpsum',0,'2',0.00,29250.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:10:44',20,'2021-10-25 09:52:41'),
(101,11,26,'Lumpsum',0,'3',0.00,29250.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:10:57',20,'2021-10-25 09:52:41'),
(102,11,27,'Lumpsum',30,'4',0.00,24375.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:11:12',20,'2021-10-25 09:52:41'),
(103,12,32,'Lumpsum',10,'5',0.00,204000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,'2021-10-25 10:03:39',20,'2021-10-25 10:03:39'),
(104,13,36,'Lumpsum',10,'1',0.00,100000.00,'','',NULL,NULL,NULL,1,'2021-10-29 10:39:13',NULL,NULL,NULL,1,'2021-10-29 10:39:13',1,'2021-10-29 10:27:16'),
(105,13,22,'Lumpsum',0,'2',0.00,60000.00,'','',NULL,NULL,NULL,1,'2021-10-29 10:39:23',NULL,NULL,NULL,1,'2021-10-29 10:39:23',1,'2021-10-29 10:27:16'),
(106,13,23,'Lumpsum',0,'3',0.00,60000.00,'','',NULL,NULL,NULL,1,'2021-10-29 10:39:31',NULL,NULL,NULL,1,'2021-10-29 10:39:31',1,'2021-10-29 10:27:16'),
(107,13,24,'Lumpsum',0,'4',0.00,60000.00,'','',1,36,'2022-06-04 15:32:17',1,'2022-06-07 11:51:11','2022-06-04',NULL,NULL,1,'2022-06-07 11:51:11',1,'2021-10-29 10:27:16'),
(108,13,25,'Lumpsum',0,'5',0.00,60000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 10:27:16',1,'2021-10-29 10:27:16'),
(109,13,26,'Lumpsum',0,'6',0.00,60000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 10:27:16',1,'2021-10-29 10:27:16'),
(110,13,27,'Lumpsum',30,'7',0.00,50000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 10:27:16',1,'2021-10-29 10:27:16'),
(111,13,11,'Lumpsum',10,'8',0.00,175000.00,'','',NULL,NULL,NULL,1,'2021-10-29 10:39:46',NULL,NULL,NULL,1,'2021-10-29 10:39:46',1,'2021-10-29 10:27:16'),
(112,13,15,'Lumpsum',15,'9',0.00,75000.00,'','',NULL,NULL,NULL,1,'2021-10-29 10:39:56',NULL,NULL,NULL,1,'2021-10-29 10:39:56',1,'2021-10-29 10:27:16'),
(113,13,13,'Lumpsum',15,'10',0.00,175000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 10:27:16',1,'2021-10-29 10:27:16'),
(114,13,12,'Lumpsum',10,'11',0.00,50000.00,'','',1,36,'2022-06-04 15:31:36',1,'2021-10-29 10:40:05','2022-06-04',NULL,NULL,1,'2022-06-04 15:31:36',1,'2021-10-29 10:27:16'),
(115,13,32,'Lumpsum',30,'12',0.00,64500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 10:27:16',1,'2021-10-29 10:27:16'),
(116,14,5,'Lumpsum Retainer',7,'1',0.00,100000.00,'','',NULL,NULL,NULL,1,'2021-10-29 15:29:18',NULL,NULL,NULL,1,'2021-10-29 15:29:18',1,'2021-10-29 13:32:53'),
(117,14,42,'Lumpsum',7,'2',0.00,280000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 13:32:53',1,'2021-10-29 13:32:53'),
(118,14,43,'Lumpsum',10,'3',0.00,280000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 13:32:53',1,'2021-10-29 13:32:53'),
(119,14,44,'Lumpsum',30,'4',0.00,340000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-10-29 13:32:53',1,'2021-10-29 13:32:53'),
(120,1,15,'Lumpsum',30,'09',0.00,50000.00,'','',1,20,'2021-11-09 11:21:57',NULL,NULL,'2021-11-12',NULL,NULL,1,'2021-11-09 11:21:57',1,'2021-10-30 12:05:23'),
(126,15,5,'Lumpsum Retainer',7,'1',0.00,95000.00,'','',1,36,'2021-12-17 16:54:11',36,'2021-12-17 17:22:47','2021-12-17',36,38,1,'2022-04-20 15:25:59',1,'2021-11-10 08:36:51'),
(127,15,16,'Lumpsum',7,'2',0.00,0.00,'','',1,36,'2022-03-21 12:00:43',36,'2022-03-21 12:10:09','2022-03-21',38,55,1,'2022-04-20 15:27:06',1,'2021-11-10 08:36:51'),
(128,15,17,'Lumpsum',7,'3',0.00,0.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,38,55,1,'2022-04-20 15:27:25',1,'2021-11-10 08:36:51'),
(129,15,18,'Lumpsum',7,'4',0.00,95000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,38,55,1,'2022-04-20 15:27:50',1,'2021-11-10 08:36:51'),
(130,15,19,'Lumpsum',60,'5',0.00,0.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,58,38,1,'2022-04-20 15:28:06',1,'2021-11-10 08:36:51'),
(131,15,20,'Lumpsum',45,'6',0.00,95000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 15:28:23',1,'2021-11-10 08:36:51'),
(132,15,21,'Lumpsum',15,'7',0.00,142500.00,'','',1,36,'2022-03-21 12:02:35',36,'2022-03-21 12:10:21','2022-03-21',57,38,1,'2022-04-20 15:28:39',1,'2021-11-10 08:36:51'),
(133,15,36,'Lumpsum',10,'8',0.00,0.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:28:53',1,'2021-11-10 08:36:51'),
(134,15,22,'Lumpsum',0,'9',0.00,0.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 15:29:14',1,'2021-11-10 08:36:51'),
(135,15,23,'Lumpsum',0,'10',0.00,95000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 16:36:10',1,'2021-11-10 08:36:51'),
(136,15,24,'Lumpsum',0,'11',0.00,95000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 16:36:23',1,'2021-11-10 08:36:51'),
(137,15,25,'Lumpsum',0,'12',0.00,95000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 16:36:38',1,'2021-11-10 08:36:51'),
(138,15,26,'Lumpsum',0,'13',0.00,142500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 16:36:49',1,'2021-11-10 08:36:51'),
(139,15,27,'Lumpsum',30,'14',0.00,42500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,57,38,1,'2022-04-20 16:39:29',1,'2021-11-10 08:36:51'),
(140,15,15,'Lumpsum',15,'15',0.00,35000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 16:39:56',1,'2021-11-10 08:36:51'),
(146,17,5,'Lumpsum Retainer',7,'1',0.00,20000.00,'','',1,36,'2022-03-01 13:19:13',36,'2022-03-01 13:23:19','2022-03-01',NULL,NULL,36,'2022-03-01 13:23:19',1,'2021-12-03 17:02:13'),
(147,17,16,'Percentage',7,'2',5.00,7133.00,'','',1,36,'2022-03-01 13:19:57',36,'2022-03-01 13:23:38','2022-03-01',NULL,NULL,36,'2022-03-01 13:23:38',1,'2021-12-03 17:02:13'),
(148,17,17,'Percentage',7,'3',10.00,14266.00,'','',1,36,'2022-03-01 13:20:19',36,'2022-03-01 13:23:48','2022-03-01',NULL,NULL,36,'2022-03-01 13:23:48',1,'2021-12-03 17:02:13'),
(149,17,18,'Percentage',7,'4',10.00,14266.00,'','',1,36,'2022-03-01 13:20:44',36,'2022-03-01 13:23:56','2022-03-01',NULL,NULL,36,'2022-03-01 13:23:56',1,'2021-12-03 17:02:13'),
(150,17,19,'Percentage',60,'5',10.00,14266.00,'','',1,36,'2022-03-01 13:21:08',36,'2022-03-01 13:24:03','2022-03-01',NULL,NULL,36,'2022-03-01 13:24:03',1,'2021-12-03 17:02:13'),
(151,17,20,'Percentage',45,'6',10.00,14266.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(152,17,21,'Percentage',15,'7',10.00,14266.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(153,17,36,'Percentage',10,'8',10.00,14266.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(154,17,22,'Percentage',0,'9',6.00,8559.60,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(155,17,23,'Percentage',0,'10',6.00,8559.60,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(156,17,24,'Percentage',0,'11',6.00,8559.60,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(157,17,25,'Percentage',0,'12',6.00,8559.60,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(158,17,26,'Percentage',0,'13',6.00,8559.60,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(159,17,27,'Percentage',30,'14',5.00,7133.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:37:23',1,'2021-12-03 17:02:13'),
(161,17,15,'Lumpsum',0,'15',0.00,25000.00,'','',1,36,'2022-03-01 13:21:36',36,'2022-03-01 13:24:11','2022-03-01',NULL,NULL,36,'2022-03-01 13:24:11',1,'2021-12-04 16:08:36'),
(162,18,5,'Lumpsum Retainer',7,'1',0.00,100000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-08 11:01:33',1,'2021-12-08 10:39:27'),
(163,18,11,'Lumpsum',7,'2',0.00,150000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-09 11:25:58',1,'2021-12-08 10:39:27'),
(164,18,31,'Lumpsum',10,'3',0.00,50000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-08 11:01:33',1,'2021-12-08 10:39:27'),
(165,18,30,'Lumpsum',30,'4',0.00,25000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-08 11:01:33',1,'2021-12-08 10:39:27'),
(166,18,32,'Lumpsum',10,'5',0.00,100000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-08 11:22:11',1,'2021-12-08 10:39:27'),
(167,18,13,'Lumpsum',10,'6',0.00,50000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-08 10:42:15',1,'2021-12-08 10:39:27'),
(168,19,18,'Lumpsum',7,'1',0.00,100000.00,'Service','Description',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-13 19:37:16',1,'2021-12-08 12:36:57'),
(169,20,5,'Lumpsum Retainer',7,'1',0.00,20000.00,'','',1,36,'2022-06-04 15:25:01',36,'2022-06-07 11:34:01','2022-06-04',36,38,36,'2022-06-07 11:34:01',1,'2021-12-14 17:31:08'),
(170,20,11,'Lumpsum',7,'2',0.00,100000.00,'','',36,NULL,'2022-01-11 17:51:15',NULL,NULL,NULL,53,38,1,'2022-04-20 16:48:14',1,'2021-12-14 17:31:08'),
(171,20,31,'Lumpsum',10,'3',0.00,50000.00,'','',36,36,'2022-06-04 15:28:48',36,'2022-06-07 11:46:11','2022-06-04',53,38,36,'2022-06-07 11:46:11',1,'2021-12-14 17:31:08'),
(172,20,30,'Lumpsum',30,'4',0.00,25000.00,'','',1,36,'2022-06-04 15:25:16',36,'2022-06-07 11:46:16','2022-06-04',53,38,36,'2022-06-07 11:46:16',1,'2021-12-14 17:31:08'),
(173,20,32,'Lumpsum',10,'5',0.00,80000.00,'','',36,NULL,'2022-01-11 17:50:46',NULL,NULL,NULL,53,38,1,'2022-04-20 16:48:51',1,'2021-12-14 17:31:08'),
(174,20,41,'Lumpsum',10,'7',0.00,25000.00,'','',1,36,'2022-06-04 15:25:26',36,'2022-06-07 11:46:20','2022-06-04',36,38,36,'2022-06-07 11:46:20',1,'2021-12-14 17:31:08'),
(175,21,5,'Percentage',7,'1',10.00,105000.00,'Advance Payment ','',36,NULL,'2022-01-11 17:50:16',36,'2021-12-17 17:14:58',NULL,NULL,NULL,36,'2022-01-11 17:50:16',1,'2021-12-17 12:47:57'),
(176,21,16,'Percentage',7,'2',5.00,52500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(177,21,17,'Percentage',7,'3',5.00,52500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(178,21,18,'Percentage',7,'4',10.00,105000.00,'Service','Description',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(179,21,19,'Percentage',15,'5',5.00,52500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(180,21,20,'Percentage',10,'6',10.00,105000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(181,21,21,'Percentage',0,'7',10.00,105000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(182,21,36,'Percentage',0,'8',10.00,105000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(183,21,22,'Percentage',0,'9',6.00,63000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(184,21,23,'Percentage',0,'10',6.00,63000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(185,21,24,'Percentage',0,'11',6.00,63000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(186,21,25,'Percentage',0,'12',6.00,63000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(187,21,26,'Percentage',0,'13',6.00,63000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(188,21,27,'Percentage',0,'14',5.00,52500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2021-12-17 12:47:57',1,'2021-12-17 12:47:57'),
(212,22,5,'Lumpsum Retainer',7,'1',0.00,15000.00,'','Retainer is an advance payment from our total fees , and should be released after signing of agreement',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(213,22,16,'Percentage',7,'2',5.00,84250.00,'','Ascertain Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(214,22,17,'Percentage',7,'3',10.00,168500.00,'','Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(215,22,18,'Percentage',7,'4',10.00,168500.00,'Service','Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(216,22,19,'Percentage',60,'5',10.00,168500.00,'','Prepare drawings necessary for Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(217,22,20,'Percentage',45,'6',10.00,168500.00,'','Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(218,22,21,'Percentage',15,'7',10.00,168500.00,'','Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(219,22,36,'Percentage',10,'8',10.00,168500.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(220,22,22,'Percentage',0,'9',6.00,101100.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(221,22,23,'Percentage',0,'10',6.00,101100.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(222,22,24,'Percentage',0,'11',6.00,101100.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(223,22,25,'Percentage',0,'12',6.00,101100.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2021-12-18 15:49:11',36,'2021-12-18 15:49:11'),
(224,22,26,'Percentage',0,'13',6.00,101100.00,'','',1,38,'2021-12-28 11:27:36',NULL,NULL,'2021-12-31',NULL,NULL,1,'2021-12-28 11:27:36',36,'2021-12-18 15:49:11'),
(225,22,27,'Percentage',30,'14',5.00,84250.00,'','After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,1,'2021-12-28 11:08:23',NULL,NULL,NULL,1,'2021-12-28 11:08:23',36,'2021-12-18 15:49:11'),
(231,24,9,'Lumpsum',10,'1',0.00,47321.00,'','DG 1250 Civil Consultancy Charges.',1,36,'2021-12-30 15:35:16',36,'2021-12-30 15:36:02','2021-12-30',NULL,NULL,36,'2021-12-30 15:36:02',36,'2021-12-30 15:14:15'),
(237,25,55,'Lumpsum',10,'1',0.00,500000.00,'','',1,36,'2022-01-07 18:01:51',36,'2022-01-07 18:03:26','2022-01-07',NULL,NULL,36,'2022-01-07 18:03:26',36,'2022-01-07 17:58:19'),
(238,25,56,'Lumpsum',10,'2',0.00,500000.00,'','',1,36,'2022-01-11 11:21:30',36,'2022-01-10 09:43:55','2022-01-15',NULL,NULL,1,'2022-01-11 11:21:30',36,'2022-01-07 17:58:19'),
(239,25,57,'Lumpsum',10,'3',0.00,500000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-07 17:58:19',36,'2022-01-07 17:58:19'),
(240,26,5,'Lumpsum Retainer',7,'1',0.00,100000.00,'','Retainer is an advance payment from our total fees , and should be released after signing of agreement',1,36,'2022-01-11 16:06:59',36,'2022-01-11 16:09:21','2022-01-11',NULL,NULL,36,'2022-01-11 16:09:21',36,'2022-01-11 15:48:16'),
(241,26,38,'Lumpsum',7,'2',0.00,700000.00,'','',1,36,'2022-01-11 16:07:20',36,'2022-01-11 16:09:24','2022-01-11',NULL,NULL,36,'2022-01-11 16:09:24',36,'2022-01-11 15:48:16'),
(252,29,54,'Lumpsum',10,'1',0.00,165000.00,'Completion of Stage 2','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-19 15:33:56',36,'2022-01-18 18:04:42'),
(253,29,48,'Lumpsum',10,'2',0.00,165000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-19 15:33:56',36,'2022-01-18 18:04:42'),
(254,29,32,'Lumpsum',10,'3',0.00,170000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-19 15:33:56',36,'2022-01-18 18:04:42'),
(261,31,58,'Lumpsum',10,'1',0.00,40000.00,'Fees for EC Purpose Drawing.','Fees for EC Purpose Drawing.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2022-01-20 10:57:11',36,'2022-01-19 14:22:41'),
(263,27,5,'Lumpsum',10,'1',0.00,100000.00,'','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-01-24 14:43:56',36,'2022-01-24 14:47:09','2022-01-24',36,38,1,'2022-04-20 16:41:28',36,'2022-01-22 15:30:31'),
(264,27,16,'Lumpsum',10,'2',0.00,50000.00,'','Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',1,36,'2022-06-04 11:25:33',36,'2022-06-04 15:16:52','2022-06-04',53,38,36,'2022-06-04 15:16:52',36,'2022-01-22 15:30:31'),
(265,27,29,'Lumpsum',10,'3',0.00,350000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 16:41:52',36,'2022-01-22 15:30:31'),
(266,27,31,'Lumpsum',10,'4',0.00,50000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 16:42:00',36,'2022-01-22 15:30:31'),
(267,27,30,'Lumpsum',10,'5',0.00,50000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 16:42:21',36,'2022-01-22 15:30:31'),
(268,27,32,'Lumpsum',30,'6',0.00,100000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,53,38,1,'2022-04-20 16:42:36',36,'2022-01-22 15:30:31'),
(269,32,5,'Lumpsum Retainer',7,'1',0.00,500000.00,'','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-06-04 15:36:33',36,'2022-06-07 11:56:48','2022-06-04',NULL,NULL,36,'2022-06-07 11:56:48',36,'2022-01-24 17:51:50'),
(270,32,11,'Lumpsum',7,'4',0.00,375000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-27 13:21:32',36,'2022-01-24 17:51:50'),
(271,32,31,'Lumpsum',10,'5',0.00,150000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2022-01-28 10:47:33',36,'2022-01-24 17:51:50'),
(272,32,30,'Lumpsum',30,'3',0.00,175000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-27 13:21:32',36,'2022-01-24 17:51:50'),
(273,32,32,'Lumpsum',10,'6',0.00,300000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2022-01-28 10:47:33',36,'2022-01-24 17:51:50'),
(274,32,38,'Lumpsum',10,'2',0.00,1000000.00,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-27 13:21:32',36,'2022-01-24 17:51:50'),
(307,33,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(308,33,16,'Percentage',7,'02',5.00,150000.00,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(309,33,17,'Percentage',7,'03',5.00,150000.00,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(310,33,18,'Percentage',7,'04',14.00,420000.00,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(311,33,19,'Percentage',15,'05',10.00,300000.00,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(312,33,20,'Percentage',10,'06',10.00,300000.00,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(313,33,21,'Percentage',0,'07',10.00,300000.00,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(314,33,36,'Percentage',0,'08',10.00,300000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(315,33,22,'Percentage',0,'09',6.00,180000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 17:14:20',36,'2022-01-28 15:49:09'),
(316,33,23,'Percentage',0,'10',6.00,180000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 15:49:09',36,'2022-01-28 15:49:09'),
(317,33,24,'Percentage',0,'11',6.00,180000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 15:49:09',36,'2022-01-28 15:49:09'),
(318,33,25,'Percentage',0,'12',6.00,180000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 15:49:09',36,'2022-01-28 15:49:09'),
(319,33,26,'Percentage',0,'13',6.00,180000.00,NULL,'',1,38,'2022-02-01 11:23:18',NULL,NULL,'2022-02-19',NULL,NULL,1,'2022-02-01 11:23:18',36,'2022-01-28 15:49:09'),
(320,33,27,'Percentage',0,'14',6.00,180000.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-01-28 15:49:09',36,'2022-01-28 15:49:09'),
(321,16,5,'Lumpsum Retainer',10,'1',0.00,355000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,36,38,1,'2022-04-20 15:24:20',36,'2022-02-03 16:48:12'),
(322,16,55,'Lumpsum',10,'2',0.00,215000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,58,38,1,'2022-04-20 15:24:30',36,'2022-02-03 16:48:12'),
(323,16,56,'Lumpsum',10,'3',0.00,215000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,58,38,1,'2022-04-20 15:24:40',36,'2022-02-03 16:48:12'),
(324,16,57,'Lumpsum',10,'4',0.00,215000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,58,38,1,'2022-04-20 15:25:04',36,'2022-02-03 16:48:12'),
(331,34,5,'Lumpsum Retainer',7,'01',0.00,30000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-30 15:03:40',36,'2022-02-07 15:19:39'),
(332,34,9,'Lumpsum',30,'02',20.00,40000.00,NULL,'Consultancy charges for Civil Work of Tray dryer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-17 16:22:25',36,'2022-02-07 15:19:39'),
(346,36,5,'Lumpsum',10,'1',0.00,60000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-04-14 15:49:47',36,'2022-04-14 15:53:42','2022-04-14',NULL,NULL,36,'2022-04-14 15:53:42',36,'2022-02-09 14:38:34'),
(347,36,55,'Lumpsum',10,'2',0.00,100000.00,NULL,'',1,36,'2022-05-16 15:55:22',36,'2022-05-16 15:56:30','2022-05-16',NULL,NULL,36,'2022-05-16 15:56:30',36,'2022-02-09 14:38:34'),
(348,36,56,'Lumpsum',10,'3',0.00,75000.00,NULL,'',1,36,'2022-05-16 15:55:32',36,'2022-05-16 15:56:34','2022-05-16',NULL,NULL,36,'2022-05-16 15:56:34',36,'2022-02-09 14:38:34'),
(349,36,57,'Lumpsum',10,'4',0.00,65000.00,NULL,'',1,36,'2022-05-16 15:55:42',36,'2022-05-16 15:56:36','2022-05-16',NULL,NULL,36,'2022-05-16 15:56:36',36,'2022-02-09 14:38:34'),
(350,37,5,'Lumpsum Retainer',7,'01',0.00,500000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-04-29 14:37:39',36,'2022-04-29 14:41:48','2022-04-29',NULL,NULL,36,'2022-04-29 14:41:48',36,'2022-02-09 16:23:05'),
(351,37,16,'Percentage',7,'02',5.00,335000.00,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(352,37,17,'Percentage',7,'03',10.00,670000.00,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(353,37,18,'Percentage',7,'04',10.00,670000.00,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(354,37,19,'Percentage',60,'05',10.00,670000.00,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(355,37,20,'Percentage',45,'06',10.00,670000.00,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(356,37,21,'Percentage',15,'07',10.00,670000.00,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(357,37,36,'Percentage',10,'08',10.00,670000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 17:16:37',36,'2022-02-09 16:23:05'),
(358,37,22,'Percentage',30,'09',6.00,402000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-11 10:26:13',36,'2022-02-09 16:23:05'),
(359,37,23,'Percentage',30,'10',6.00,402000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-11 10:26:13',36,'2022-02-09 16:23:05'),
(360,37,24,'Percentage',30,'11',6.00,402000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-11 10:26:13',36,'2022-02-09 16:23:05'),
(361,37,25,'Percentage',30,'12',6.00,402000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-11 10:26:13',36,'2022-02-09 16:23:05'),
(362,37,26,'Percentage',30,'13',6.00,402000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-11 10:26:13',36,'2022-02-09 16:23:05'),
(363,37,27,'Percentage',30,'14',5.00,335000.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-09 16:23:05',36,'2022-02-09 16:23:05'),
(364,38,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:09:27',36,'2022-02-10 17:07:03'),
(365,38,16,'Percentage',7,'02',5.00,173748.99,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(366,38,17,'Percentage',7,'03',5.00,173748.99,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(367,38,18,'Percentage',7,'04',14.00,486497.16,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(368,38,19,'Percentage',15,'05',10.00,347497.97,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(369,38,20,'Percentage',10,'06',10.00,347497.97,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(370,38,21,'Percentage',0,'07',10.00,347497.97,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(371,38,36,'Percentage',0,'08',10.00,347497.97,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(372,38,22,'Percentage',0,'09',6.00,208498.78,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(373,38,23,'Percentage',0,'10',6.00,208498.78,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(374,38,24,'Percentage',0,'11',6.00,208498.78,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(375,38,25,'Percentage',0,'12',6.00,208498.78,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(376,38,26,'Percentage',0,'13',6.00,208498.78,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(377,38,27,'Percentage',0,'14',6.00,208498.78,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:07:03'),
(378,38,30,'Lumpsum',0,'15',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:18:27',36,'2022-02-10 17:09:27'),
(379,39,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(380,39,16,'Percentage',7,'02',5.00,126381.25,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(381,39,17,'Percentage',7,'03',5.00,126381.25,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(382,39,18,'Percentage',7,'04',14.00,353867.50,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(383,39,19,'Percentage',15,'05',10.00,252762.50,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(384,39,20,'Percentage',10,'06',10.00,252762.50,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(385,39,21,'Percentage',0,'07',10.00,252762.50,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(386,39,36,'Percentage',0,'08',10.00,252762.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(387,39,22,'Percentage',0,'09',6.00,151657.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(388,39,23,'Percentage',0,'10',6.00,151657.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(389,39,24,'Percentage',0,'11',6.00,151657.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(390,39,25,'Percentage',0,'12',6.00,151657.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(391,39,26,'Percentage',0,'13',6.00,151657.50,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(392,39,27,'Percentage',0,'14',6.00,151657.50,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(393,39,30,'Lumpsum',0,'15',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-10 17:39:44',36,'2022-02-10 17:39:44'),
(394,40,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(395,40,16,'Percentage',7,'02',5.00,548807.80,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(396,40,17,'Percentage',7,'03',5.00,548807.80,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(397,40,18,'Percentage',7,'04',14.00,1536661.84,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(398,40,19,'Percentage',15,'05',10.00,1097615.60,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(399,40,20,'Percentage',10,'06',10.00,1097615.60,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(400,40,21,'Percentage',0,'07',10.00,1097615.60,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(401,40,36,'Percentage',0,'08',10.00,1097615.60,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(402,40,22,'Percentage',0,'09',6.00,658569.36,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:58:40',36,'2022-02-11 10:49:04'),
(403,40,23,'Percentage',0,'10',6.00,658569.36,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:49:04',36,'2022-02-11 10:49:04'),
(404,40,24,'Percentage',0,'11',6.00,658569.36,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:49:04',36,'2022-02-11 10:49:04'),
(405,40,25,'Percentage',0,'12',6.00,658569.36,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:49:04',36,'2022-02-11 10:49:04'),
(406,40,26,'Percentage',0,'13',6.00,658569.36,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:49:04',36,'2022-02-11 10:49:04'),
(407,40,27,'Percentage',0,'14',6.00,658569.36,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-11 10:49:04',36,'2022-02-11 10:49:04'),
(408,41,5,'Lumpsum Retainer',7,'1',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(409,41,16,'Percentage',7,'2',5.00,394000.00,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(410,41,17,'Percentage',7,'3',5.00,394000.00,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(411,41,18,'Percentage',7,'4',14.00,1103200.00,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(412,41,19,'Percentage',15,'5',10.00,788000.00,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(413,41,20,'Percentage',10,'6',10.00,788000.00,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(414,41,21,'Percentage',0,'7',10.00,788000.00,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(415,41,36,'Percentage',0,'8',10.00,788000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(416,41,22,'Percentage',0,'9',6.00,472800.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(417,41,23,'Percentage',0,'10',6.00,472800.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(418,41,24,'Percentage',0,'11',6.00,472800.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(419,41,25,'Percentage',0,'12',6.00,472800.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(420,41,26,'Percentage',0,'13',6.00,472800.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(421,41,27,'Percentage',0,'14',6.00,472800.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-21 18:24:08',36,'2022-02-21 18:24:08'),
(422,42,5,'Lumpsum Retainer',7,'1',0.00,100000.00,'','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-04-05 17:51:22',36,'2022-04-05 17:52:54','2022-04-05',NULL,NULL,36,'2022-04-05 17:53:11',36,'2022-02-21 18:30:17'),
(423,42,11,'Lumpsum',7,'2',0.00,300000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-17 10:42:26',36,'2022-02-21 18:30:17'),
(424,42,31,'Lumpsum',10,'3',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-17 10:42:26',36,'2022-02-21 18:30:17'),
(425,42,30,'Lumpsum',30,'4',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-17 10:42:26',36,'2022-02-21 18:30:17'),
(426,42,32,'Lumpsum',10,'5',0.00,100000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-17 10:42:26',36,'2022-02-21 18:30:17'),
(427,43,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-23 18:03:06',36,'2022-02-23 17:54:41'),
(428,43,11,'Lumpsum',30,'03',0.00,300000.00,NULL,'For first two plots.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 16:03:32',36,'2022-02-23 17:54:41'),
(429,43,31,'Lumpsum',15,'05',0.00,50000.00,NULL,'For first two plots.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 16:03:32',36,'2022-02-23 17:54:41'),
(430,43,30,'Lumpsum',15,'07',0.00,35000.00,NULL,'For first two plots ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 15:42:35',36,'2022-02-23 17:54:41'),
(432,43,48,'Lumpsum',30,'02',0.00,300000.00,NULL,'For three Plots i.e. H 48/1B, M93/2 & H-53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-23 17:54:41',36,'2022-02-23 17:54:41'),
(433,43,32,'Lumpsum',10,'09',0.00,425000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 15:42:35',36,'2022-02-24 15:42:35'),
(434,43,30,'Lumpsum',10,'08',0.00,35000.00,NULL,'For third plot. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 16:03:32',36,'2022-02-24 15:42:35'),
(435,43,11,'Lumpsum',30,'04',0.00,300000.00,NULL,'For third plot.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 16:03:32',36,'2022-02-24 15:42:35'),
(436,43,31,'Lumpsum',15,'06',0.00,50000.00,NULL,'For third plot.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-02-24 16:03:32',36,'2022-02-24 15:42:35'),
(447,44,25,'Lumpsum',60,'01',6.00,55188.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-19 11:17:58',36,'2022-03-15 16:11:51'),
(448,44,26,'Lumpsum',60,'02',6.00,55188.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-19 11:17:59',36,'2022-03-15 16:11:51'),
(449,44,27,'Lumpsum',10,'03',5.00,45990.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-19 11:17:59',36,'2022-03-15 16:11:51'),
(450,45,36,'Lumpsum',10,'01',0.00,480000.00,NULL,'',1,36,'2022-04-13 10:28:32',36,'2022-04-13 10:43:42','2022-04-13',NULL,NULL,36,'2022-04-13 10:43:42',36,'2022-03-15 17:50:44'),
(451,45,22,'Lumpsum',10,'02',0.00,288000.00,NULL,'',1,36,'2022-04-13 10:34:28',36,'2022-04-13 10:43:44','2022-04-13',NULL,NULL,36,'2022-04-13 10:43:44',36,'2022-03-15 17:50:44'),
(452,45,23,'Lumpsum',10,'03',0.00,288000.00,NULL,'',1,36,'2022-04-13 10:34:59',36,'2022-04-13 10:43:55','2022-04-13',NULL,NULL,36,'2022-04-13 10:43:55',36,'2022-03-15 17:50:44'),
(453,45,24,'Lumpsum',10,'04',0.00,288000.00,NULL,'',1,36,'2022-04-13 10:35:18',36,'2022-04-13 10:44:05','2022-04-13',NULL,NULL,36,'2022-04-13 10:44:05',36,'2022-03-15 17:50:44'),
(454,45,25,'Lumpsum',10,'05',0.00,288000.00,NULL,'',1,36,'2022-04-13 10:35:31',36,'2022-04-13 10:44:14','2022-04-13',NULL,NULL,36,'2022-04-13 10:44:14',36,'2022-03-15 17:50:44'),
(455,45,26,'Lumpsum',10,'06',0.00,288000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-15 17:50:44',36,'2022-03-15 17:50:44'),
(456,45,27,'Lumpsum',30,'07',0.00,240000.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-15 17:50:44',36,'2022-03-15 17:50:44'),
(472,47,58,'Lumpsum',10,'01',0.00,75000.00,NULL,'Fees for EC Purpose Drawing. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-24 12:20:16',36,'2022-03-24 12:20:16'),
(473,48,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-24 17:30:03',36,'2022-03-24 17:20:54'),
(476,48,18,'Percentage',21,'02',24.00,46560.00,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(478,48,20,'Percentage',15,'03',15.00,29100.00,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(479,48,21,'Percentage',10,'04',15.00,29100.00,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(480,48,36,'Percentage',30,'05',10.00,19400.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(482,48,23,'Percentage',60,'06',12.00,23280.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(484,48,25,'Percentage',60,'07',12.00,23280.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(486,48,27,'Percentage',60,'08',12.00,23280.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 15:15:46',36,'2022-03-24 17:20:54'),
(487,49,5,'Lumpsum Retainer',7,'01',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees , and should be released after signing of agreement',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(488,49,16,'Percentage',7,'02',5.00,61150.00,NULL,'Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. Prepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development o',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(489,49,17,'Percentage',7,'03',10.00,122300.00,NULL,'Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(490,49,18,'Percentage',7,'04',10.00,122300.00,NULL,'Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(491,49,19,'Percentage',60,'05',10.00,122300.00,NULL,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(492,49,20,'Percentage',45,'06',10.00,122300.00,NULL,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on material',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(493,49,21,'Percentage',15,'07',10.00,122300.00,NULL,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(494,49,36,'Percentage',10,'08',10.00,122300.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(495,49,22,'Percentage',30,'09',6.00,73380.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:25:12',36,'2022-03-25 16:14:52'),
(496,49,23,'Percentage',30,'10',6.00,73380.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:25:12',36,'2022-03-25 16:14:52'),
(497,49,24,'Percentage',30,'11',6.00,73380.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:25:12',36,'2022-03-25 16:14:52'),
(498,49,25,'Percentage',30,'12',6.00,73380.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:25:12',36,'2022-03-25 16:14:52'),
(499,49,26,'Percentage',30,'13',6.00,73380.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:25:12',36,'2022-03-25 16:14:52'),
(500,49,27,'Percentage',30,'14',5.00,61150.00,NULL,'After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ OccupancyCertificate\" from statutory authorities, wh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-25 16:14:52',36,'2022-03-25 16:14:52'),
(501,50,5,'Lumpsum Retainer',7,'01',0.00,80000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-30 15:37:30',36,'2022-03-30 14:59:00'),
(519,51,5,'Lumpsum Retainer',7,'1',0.00,100000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-03-30 17:52:01',36,'2022-03-30 17:52:01'),
(520,51,11,'Lumpsum',7,'2',0.00,150000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-13 15:51:39',36,'2022-03-30 17:52:01'),
(521,51,31,'Lumpsum',10,'3',0.00,100000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-13 15:51:39',36,'2022-03-30 17:52:01'),
(522,51,30,'Lumpsum',30,'4',0.00,100000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-13 15:51:39',36,'2022-03-30 17:52:01'),
(525,46,5,'Lumpsum Retainer',0,'01',0.00,150000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',1,36,'2022-04-25 13:19:53',36,'2022-04-25 13:24:32','2022-04-25',36,38,36,'2022-04-25 13:25:17',36,'2022-04-12 14:29:46'),
(526,46,53,'Lumpsum',60,'02',0.00,200000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-13 11:40:37',36,'2022-04-12 14:29:46'),
(527,52,5,'Lumpsum Retainer',0,'01',0.00,150000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-12 15:28:00',36,'2022-04-12 15:28:00'),
(528,52,53,'Lumpsum',60,'02',0.00,200000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-13 12:35:03',36,'2022-04-12 15:28:00'),
(529,46,54,'Lumpsum',60,'03',0.00,190000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-13 11:40:37',36,'2022-04-13 11:40:37'),
(530,52,54,'Lumpsum',60,'03',0.00,190000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-04-13 12:35:03',36,'2022-04-13 12:35:03'),
(531,53,5,'Lumpsum Retainer',0,'1',0.00,50000.00,NULL,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-23 15:32:17',36,'2022-05-23 15:25:38'),
(532,53,11,'Lumpsum',30,'2',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-23 15:32:17',36,'2022-05-23 15:25:38'),
(533,53,31,'Lumpsum',30,'3',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-23 15:32:17',36,'2022-05-23 15:25:38'),
(534,53,30,'Lumpsum',30,'4',0.00,25000.00,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,'2022-05-23 15:25:38',36,'2022-05-23 15:25:38');

/*Table structure for table `project_type_details` */

DROP TABLE IF EXISTS `project_type_details`;

CREATE TABLE `project_type_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_type_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `fees_type` varchar(255) DEFAULT NULL,
  `days_require` int(20) DEFAULT NULL,
  `list_order` varchar(2) DEFAULT NULL,
  `percentage` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=latin1;

/*Data for the table `project_type_details` */

insert  into `project_type_details`(`id`,`project_type_id`,`task_id`,`fees_type`,`days_require`,`list_order`,`percentage`) values 
(55,2,5,'Lumpsum Retainer',7,'1',0.00),
(56,2,16,'Percentage',7,'2',5.00),
(57,2,17,'Percentage',7,'3',5.00),
(58,2,18,'Percentage',7,'4',14.00),
(59,2,19,'Percentage',15,'5',10.00),
(60,2,20,'Percentage',10,'6',10.00),
(61,2,21,'Percentage',0,'7',10.00),
(62,2,36,'Percentage',0,'8',10.00),
(63,2,22,'Percentage',0,'9',6.00),
(64,2,23,'Percentage',0,'10',6.00),
(65,2,24,'Percentage',0,'11',6.00),
(66,2,25,'Percentage',0,'12',6.00),
(67,2,26,'Percentage',0,'13',6.00),
(68,2,27,'Percentage',0,'14',6.00),
(235,3,5,'Lumpsum Retainer',7,'1',0.00),
(236,3,11,'Lumpsum',7,'2',0.00),
(237,3,31,'Lumpsum',10,'3',0.00),
(238,3,30,'Lumpsum',30,'4',0.00),
(239,3,32,'Lumpsum',10,'5',0.00),
(240,5,5,'Lumpsum Retainer',NULL,'',NULL),
(241,5,45,'Lumpsum',NULL,'',NULL),
(242,5,46,'',NULL,'',NULL),
(243,5,47,'',NULL,'',NULL),
(244,5,37,'',NULL,'',NULL),
(245,5,48,'',NULL,'',NULL),
(246,1,5,'Lumpsum Retainer',7,'1',0.00),
(247,1,16,'Percentage',7,'2',5.00),
(248,1,17,'Percentage',7,'3',10.00),
(249,1,18,'Percentage',7,'4',10.00),
(250,1,19,'Percentage',60,'5',10.00),
(251,1,20,'Percentage',45,'6',10.00),
(252,1,21,'Percentage',15,'7',10.00),
(253,1,36,'Percentage',10,'8',10.00),
(254,1,22,'Percentage',0,'9',6.00),
(255,1,23,'Percentage',0,'10',6.00),
(256,1,24,'Percentage',0,'11',6.00),
(257,1,25,'Percentage',0,'12',6.00),
(258,1,26,'Percentage',0,'13',6.00),
(259,1,27,'Percentage',30,'14',5.00),
(260,4,5,'Lumpsum Retainer',7,'1',0.00),
(261,4,42,'Lumpsum',7,'2',25.00),
(262,4,43,'Lumpsum',10,'3',25.00),
(263,4,44,'Lumpsum',30,'4',50.00);

/*Table structure for table `project_types` */

DROP TABLE IF EXISTS `project_types`;

CREATE TABLE `project_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_type` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `project_types` */

insert  into `project_types`(`id`,`project_type`,`modified`,`created`) values 
(1,'Factory Buildings','2021-12-03 17:17:40','2021-09-18 12:38:39'),
(2,'Commercial Project','2021-09-21 17:06:30','2021-09-21 17:06:30'),
(3,'Statutory Work','2021-12-03 11:49:49','2021-09-21 18:15:16'),
(4,'GAIL Work','2021-12-03 17:18:14','2021-10-29 13:26:13'),
(5,'Land Allotment','2021-12-03 12:02:08','2021-12-03 12:02:08');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_no` varchar(30) DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `project_no` varchar(30) DEFAULT NULL,
  `project_date` date DEFAULT NULL,
  `lead_type_id` int(11) DEFAULT NULL,
  `project_type_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `client_team_id` int(11) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_address_line_1` varchar(255) DEFAULT NULL,
  `site_address_line_2` varchar(255) DEFAULT NULL,
  `sity_city` varchar(50) DEFAULT NULL,
  `site_pincode` varchar(6) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `landline` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fees_type` varchar(255) DEFAULT NULL,
  `rate` decimal(15,2) DEFAULT NULL,
  `cost_of_construction` decimal(15,2) DEFAULT NULL,
  `total_construction_area` decimal(15,2) DEFAULT NULL,
  `total_cost_of_construction` decimal(15,2) DEFAULT NULL,
  `architectural_fees` decimal(15,2) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `free_visits` int(11) DEFAULT NULL,
  `visit_charge` decimal(10,2) DEFAULT 0.00,
  `documentation_communication_charges` decimal(10,2) DEFAULT 0.00,
  `dc_unit` varchar(20) DEFAULT NULL,
  `travelling_charges` decimal(10,2) DEFAULT 0.00,
  `t_unit` varchar(20) DEFAULT NULL,
  `lodging_boarding_charges` decimal(10,2) DEFAULT 0.00,
  `lb_unit` varchar(20) DEFAULT NULL,
  `models_charges` decimal(10,2) DEFAULT 0.00,
  `m_unit` varchar(20) DEFAULT NULL,
  `computer_simulation_charges` decimal(10,2) DEFAULT 0.00,
  `cs_unit` varchar(20) DEFAULT NULL,
  `drawing_charges` decimal(10,2) DEFAULT 0.00,
  `d_unit` varchar(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `cgst` decimal(10,2) DEFAULT NULL,
  `cgst_amount` decimal(10,2) DEFAULT NULL,
  `sgst` decimal(10,2) DEFAULT NULL,
  `sgst_amount` decimal(10,2) DEFAULT NULL,
  `igst` decimal(10,2) DEFAULT NULL,
  `igst_amount` decimal(10,2) DEFAULT NULL,
  `final_amount` decimal(10,2) DEFAULT NULL,
  `inclusion` text DEFAULT NULL,
  `exclusion` text DEFAULT NULL,
  `next_followup_date` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `notes_after_discussion` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `revise_description` text DEFAULT NULL,
  `purchase_order_no` varchar(255) DEFAULT NULL,
  `purchase_order_img` varchar(255) DEFAULT NULL,
  `purchase_order_img_dir` varchar(255) DEFAULT NULL,
  `status` enum('Open','Converted','Closed','Archived') DEFAULT 'Open',
  `closed_by` int(11) DEFAULT NULL,
  `closed_on` datetime DEFAULT NULL,
  `close_reason` varchar(255) DEFAULT NULL,
  `archived_by` int(11) DEFAULT NULL,
  `archive_status` varchar(255) DEFAULT NULL,
  `archive_on` datetime DEFAULT NULL,
  `revision` int(11) DEFAULT 0,
  `prepared_by` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_datetime` datetime DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;

/*Data for the table `projects` */

insert  into `projects`(`id`,`quotation_no`,`quotation_date`,`project_no`,`project_date`,`lead_type_id`,`project_type_id`,`client_id`,`client_team_id`,`contact_person`,`site_name`,`site_address_line_1`,`site_address_line_2`,`sity_city`,`site_pincode`,`mobile`,`landline`,`email`,`fees_type`,`rate`,`cost_of_construction`,`total_construction_area`,`total_cost_of_construction`,`architectural_fees`,`fees`,`free_visits`,`visit_charge`,`documentation_communication_charges`,`dc_unit`,`travelling_charges`,`t_unit`,`lodging_boarding_charges`,`lb_unit`,`models_charges`,`m_unit`,`computer_simulation_charges`,`cs_unit`,`drawing_charges`,`d_unit`,`amount`,`cgst`,`cgst_amount`,`sgst`,`sgst_amount`,`igst`,`igst_amount`,`final_amount`,`inclusion`,`exclusion`,`next_followup_date`,`remarks`,`notes_after_discussion`,`description`,`revise_description`,`purchase_order_no`,`purchase_order_img`,`purchase_order_img_dir`,`status`,`closed_by`,`closed_on`,`close_reason`,`archived_by`,`archive_status`,`archive_on`,`revision`,`prepared_by`,`approved_by`,`approved_datetime`,`modified`,`modified_by`,`created`,`created_by`) values 
(1,'2021-22/00001','2021-07-01','2021-22/00001','2022-02-07',1,1,1,19,'Shrinivas D, Shembekar','AOPL Unit 4','Plot No. C 8/6 & 8/7, MIDC Ambernath ','Near AMP Gate, Ambernath','Thane','421506','9820219823',NULL,'shembekar@ambernathorganic.com','Percentage Basis',NULL,2100.00,10000.00,21000000.00,2.70,567000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,617000.00,9.00,55530.00,9.00,55530.00,0.00,0.00,728060.00,'<ol style=\"padding: 0px 40px; color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;\"><li>Architect &amp; Structural Engineer Scope.</li><li>Design and Site Development.</li><li>Structural design.</li><li>Taking Client\'s instructions and preparation of design brief.</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li>Follow up work for Building Plan Approval.</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li>Follow up work for Building Completion&nbsp;Certificate.</li></ol>','<ol style=\"padding: 0px 40px; color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;\"><li>Landscape Architecture &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li>Interior Architecture</li><li>Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc</li></ol>',NULL,'As per our quotations fees charged was 3.00% and work order received for 2.70% ','','1) Please Note Out of Pocket Expenses will be extra.\r\n2) Advance Payment According Stages.\r\n3) GST @ 18% will be charged by us in our bill.\r\n4) From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions. \r\n5) Site Visit Expenses will be charged extra by us in our bill.\r\n6) The Point No 3-5, have been included in Point number 1 & 2 for one-time approval. This Fee in Point 3-5 will be applicable only if we take Approvals more than one time and would quote at that juncture.\r\n   * Follow up work for Fire NOC\r\n   * Follow up work for MIDC Building Plan Approval\r\n   * Follow up work for MIDC BCC\r\n7) Reworking for the second time approvals the fees will be charge Or We will give new quotation for 2nd time approval\r\n8) Follow up work for PRE DCR and Auto DCR Plan Approval\r\n','','AOPL/ARS/U4/2021/21','Array','445f414e-08ed-42de-8854-8d666dab1ddc','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2021-10-30 12:23:47','2021-10-30',1,'2021-09-18 13:10:26',1),
(2,'2021-22/00002','2021-08-31','2021-22/00002','2021-08-31',1,2,2,21,'Ganesh Kinikar','Plot No. PAP 52 & 53','Next to MIDC Office','Anand Nagar, Add. MIDC ','Ambernath','421501','9892479614',NULL,'kinikar701@outlook.com','Percentage Basis',NULL,2500.00,5500.00,13750000.00,3.50,481250.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,506250.00,9.00,45562.50,9.00,45562.50,0.00,0.00,597375.00,'<p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Architect\r\nDesign Scope.</span><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">2)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Design\r\nand Site Development.</span><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">3)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Taking\r\nClient\'s instructions and preparation of design brief. <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">4)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; &nbsp;</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Site\r\nevaluation, analysis and impact of existing and / or proposed development on\r\nits immediate environs. <o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">5)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Follow\r\nup work for Building Plan Approval.</span><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">6)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Follow\r\nup work for Provisional Fire NOC.</span><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">7)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Follow\r\nup work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</span><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-align: justify; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;\">8)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Follow up work for\r\nBuilding Completion&nbsp;Certificate.</span><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-align: justify; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">9)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Follow up work for\r\nHardship Approval for Structures in Marginal Open Spaces.</span><span lang=\"EN-US\" style=\"font-weight: bold; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p>','<p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1;tab-stops:365.4pt\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Landscape\r\nArchitecture &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1;tab-stops:365.4pt\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\">2)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Interior\r\nArchitecture <o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:\r\n&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">3)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">Structural\r\ndesign.</span><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1;tab-stops:365.4pt\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\">4)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;\">Structural Engineer Scope</span><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">5)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Architectural\r\nConservation <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">6)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Retrofitting\r\nof Buildings <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">7)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Graphic\r\nDesign and Signage<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">8)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Lease\r\nAgreements.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">9)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Work\r\npertaining to Thane RO office.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">10)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Final Fire NOC.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">11)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Sanitary, plumbing,\r\ndrainage, water supply and sewerage design. <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">12)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Periodic inspection and\r\nevaluation of Construction works.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">13)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Electrical, electronic,\r\ncommunication systems and design. <o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">14)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Heating, ventilation and\r\nair conditioning design (HVAC) and other mechanical systems <o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">15)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Elevators, escalators,\r\netc. Fire detection, Fire protection and Security systems etc.&nbsp;<o:p></o:p></span></p>','2021-09-03','Included Follow up work for Hardship Approval for Marginal Space.','','1) Please Note Out of pocket expenses will be extra.\r\n2) Advance Payment According Stages.\r\n3) GST @ 18% will be charged by us in our bill.\r\n4) Site Visit Expenses will be charged extra by us in our bill.\r\n5) One-time approval charges for Point no 3,4, & 5, have been included in Point number 1 & 2. these fees will be applicable only if we take Approvals more than one time and would quote at that juncture.	\r\n','','WRKO/KCPL/2021-22/03','Array','d4441b05-bf13-4418-a1fd-e08b5c955a39','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2021-10-12 11:14:39','2021-10-12',1,'2021-09-21 17:22:32',20),
(3,'2021-22/00003','2021-08-10','2021-22/00003','2021-08-10',2,3,3,22,'Pradeep Choudhary','Plot No. OS-16','D-53,55,57,60','MIDC. Phase II, Kalyan-Shill Road','Dombivli','421204','7276463301',NULL,'pradip.chaudhari@aarti-industries.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,NULL,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,550000.00,9.00,49500.00,9.00,49500.00,0.00,0.00,649000.00,'','','2021-08-14','','','','','4580436541','KASHELKAR PO. D-56.pdf','41cb67cb-39d0-4b36-a459-14d2d99e1d05','Archived',NULL,NULL,NULL,1,'Project Completed','2022-06-04 11:34:52',0,NULL,20,'2022-06-04 11:34:52','2022-06-04',1,'2021-09-24 13:08:46',20),
(4,'2021-22/00004','2021-08-10','2021-22/00004','2021-08-10',2,3,3,23,'Pradeep Choudhary','Plot No. D 53 & 54','MIDC, Phase II','Kalyan-Shill Road','Dombivli','421204','',NULL,'pradip.chaudhari1@aarti-industries.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,422500.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,422500.00,9.00,38025.00,9.00,38025.00,0.00,0.00,498550.00,'<ol style=\"padding: 0px 40px; color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;\"><li>Follow up work for Building Plan Approval &amp; Building Completion Certificate.</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval.</li><li>Follow up work for Regularization of Structural Plan Approval</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems-._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Landscape Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li><li style=\"box-sizing: border-box;\">Lease Agreements.</li><li style=\"box-sizing: border-box;\">Work pertaining to Thane RO office.</li><li style=\"box-sizing: border-box;\">MIDC Transfers of Flats.</li><li style=\"box-sizing: border-box;\">All existing Transfers with MIDC.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design</li><li style=\"box-sizing: border-box;\">Final Fire NOC</li></ol>','2021-08-23','','','Changes required will be done by company. All as drawing will be handed over by company. Any compounding charges will be bourne by company. Estimate for all Statutory fees and compounding charges are subject to change. We will not responsible and liable for any changes in statutory & compounding fees.','','4580436539','KASHELKAR PO. D-53.pdf','7410d9b4-6fc2-4e2c-be7a-90067f19f3cc','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,20,'2021-09-24 16:02:39','2021-09-24',20,'2021-09-24 15:17:06',20),
(6,'2021-22/00006','2021-08-23','2021-22/00006','2021-08-23',2,3,134,24,'Sunil Joshi','Plot No. W-100','MIDC, Phase II','MIDC.','Dombivli','421204','9820979016',NULL,'chemstarindia@gmail.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,57000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,57000.00,9.00,5130.00,9.00,5130.00,0.00,0.00,67260.00,'<ol><li style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN-US\" style=\"text-indent: -18pt; text-align: justify; font-size: 11.5pt;\"><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span>Follow up work for Building Completion&nbsp;Certificate.</li><li style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp; Follow up work for Plumber License Certificate</li><li style=\"margin-left: 36pt; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;Follow up work for Documentation of MIDC</li></ol>','','2021-08-26','','','','',NULL,'Revised Work Order for Architectural Services & follow up with MIDC.pdf','9634a01a-d5de-4962-84c2-106381d93231','Archived',NULL,NULL,NULL,1,'Project Completed','2022-05-12 17:15:18',0,NULL,20,'2022-05-12 17:15:18','2022-05-12',1,'2021-09-24 16:27:42',20),
(7,'P2021-22/00029','2021-10-11',NULL,NULL,NULL,1,135,25,'Mr. Bhaskar','Plot No. W-34 ','MIDC, Phase-II,','Dombivli Industrial Area','Dombivli East , Thane','421203','7738000360',NULL,'admin@unilabchem.com','Percentage Basis',NULL,2000.00,10068.00,20136000.00,3.00,604080.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',604080.00,9.00,54367.20,9.00,54367.20,NULL,0.00,712814.40,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works</li><li style=\"box-sizing: border-box;\">Predcr and Auto Dcr for MIDC approval</li><li style=\"box-sizing: border-box;\">Empanelled rights of Accredited Architect&nbsp;</li><li style=\"box-sizing: border-box;\">Bill checking of Contractors</li></ol>','<ol><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li>Fire detection, Fire protection and Security systems &amp; design</li><li>Interior Architecture, Architectural Conservation, Retrofitting of Buildings, Graphic Design and Signage</li></ol>',NULL,'','','<ol><li>Please Note Out of pocket expenses will be extra.\r\n</li><li>Advance Payment immediately with Work Order. </li><li>GST @ 18% will be charged by us in our bill. \r\n</li></ol>','','Verbal Communication on 01.03.2022','','','Open',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,36,'2022-03-01 11:57:20','2022-03-01',36,'2021-10-11 16:33:01',1),
(8,'2021-22/00008','2021-08-10','2021-22/00008','2021-10-08',2,3,136,27,'Balabhadra samantara','Plot No.3/1','Taloja MIDC, Dist- Raigad','','Taloja','410208','9136323494',NULL,'balabhadra.s@apcotex.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,522625.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,522625.00,9.00,47036.25,9.00,47036.25,0.00,0.00,616697.50,'<p>1) Follow up work for Building Plan Approval.</p><p>2) Follow up work for Provisional Fire NOC.</p><p>3) Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</p><p>4) Follow up work for Building Completion Certificate.</p><p>5) Empaneled Architect services</p>','<p>1) Architect Engineer Scope.</p><p>2) Design and Site Development.</p><p>3) Taking Client\'s instructions and preparation of design brief.</p><p>4) Site evaluation, analysis and impact of existing and / or proposed development on its immediate</p><p>environs.</p><p>5) Landscape Architecture</p><p>6) Structural Engineer Scope.</p><p>7) Structural design.</p><p>8) Interior Architecture</p><p>9) Architectural Conservation</p><p>10) Retrofitting of Buildings</p><p>11) Graphic Design and Signage</p><p>12) Lease Agreements.</p><p>13) Work pertaining to Thane RO office.</p><p>14) Final Fire NOC.</p><p>15) Sanitary, plumbing, drainage, water supply and sewerage design.</p><p>16) Periodic inspection and evaluation of Construction works.</p><p>17) Electrical, electronic, communication systems and design.</p><p>18) Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</p><p>19) Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</p>','2021-10-11','','','1) Please note out of pocket expenses will be extra.\r\n2) Advance payment according stages.\r\n3) GST @ 18% will be charged by us in our bill.\r\n4) Charges for 10 Site Visit Included in Point No. 1 – 3, further from 11th Site Visit Charges @ 1200/- (Per Visit) will be applicable.','','2900000542','2900000542.PDF','afaf241e-4439-4c68-bc9f-f6090d9af214','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,20,'2021-10-18 13:20:32','2021-10-18',20,'2021-10-11 18:36:54',1),
(10,'2021-22/00010','2021-10-23','2021-22/00010','2021-10-23',2,3,102,29,'Vinayak Patil','Plot No. B-27/29','Phase 1, MIDC','MIDC Industrial Area','Dombivli','421203','9167525471',NULL,'vpatil@gharda.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,1080000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,1080000.00,9.00,97200.00,9.00,97200.00,0.00,0.00,1274400.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Follow up&nbsp;work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Pre Dcr and Auto Dcr MIDC Plan Approvals.</li><li style=\"box-sizing: border-box;\">Follow up&nbsp; work for Provisional Fire NOC.</li><li style=\"box-sizing: border-box;\">Follow up work for final fire Noc. Form a will be provided by your appointed fire agency.</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">MPCB consent to&nbsp;operate.</li><li style=\"box-sizing: border-box;\">MPCB consent to establish.</li><li style=\"box-sizing: border-box;\">Work pertaining to Thane RO office.</li></ol>',NULL,'','','Payment Terms:- \r\n1) GST will be charged by us in our Bill. @18% \r\n2) Advance along with Work Order\r\n3) Please Note Out of Pocket Expenses Will Be Extra.','','5600000053/CVL','GHARDA CHEMICALS MAIN PLOT- 5600000053-CVL.pdf','a0af32ce-c1e2-4578-9af7-85451db23015','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,20,'2021-10-23 17:13:10','2021-10-23',20,'2021-10-23 17:11:25',20),
(11,'2021-22/00011','2021-10-23','2021-22/00011','2021-10-23',1,1,135,NULL,'Mr. Bhaskar','Plot No. C-96 & C-97 ','MIDC Industrial Zone','MAHAD MIDC','MAHAD ','','9820030780',NULL,'admin1@unilabchem.com','Lumpsum',NULL,1500.00,2300.00,3450000.00,3.25,112125.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,112125.00,9.00,10091.25,9.00,10091.25,0.00,0.00,132307.50,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Liasioning work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Liasioning work for Provisional Fire NOC and Final Fire NOC.</li><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works</li><li style=\"box-sizing: border-box;\">Predcr and Auto Dcr for MIDC approval</li><li style=\"box-sizing: border-box;\">Empanelled rights of Accredited Architect&nbsp;</li><li style=\"box-sizing: border-box;\">Bill checking of Contractors</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li style=\"box-sizing: border-box;\">Fire detection, Fire protection and Security systems &amp; design</li><li style=\"box-sizing: border-box;\">Interior Architecture,&nbsp;Architectural Conservation,Retrofitting of Buildings,Graphic Design and Signage.</li></ol>',NULL,'','','Liasioning Fire Noc fee Rs 50,000/- Liasioning Building Plan Approval- Rs 75,000/- Liasioning Building Completion Certificate- Rs 75,000/- Predcr and Auto Dcr- Rs 155,000/- Empaneled Architect- Rs 155,000/-','','PO_7','UNILAB CHEMICALS - MAHAD - P.O. 007.pdf','2077d3e8-e313-4b15-b53c-7c0b2ae154ab','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,20,'2021-10-25 09:56:13','2021-10-25',20,'2021-10-25 09:52:41',20),
(12,'2021-22/00012','2021-10-23','2021-22/00012','2021-10-23',2,3,135,25,'Mr. Bhaskar','Plot No. W-34 ','MIDC, Phase-II,','Dombivli Industrial Area','Dombivli East , Thane','421203','7738000360',NULL,'admin@unilabchem.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,204000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,204000.00,9.00,18360.00,9.00,18360.00,0.00,0.00,240720.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Pre Dcr and Auto Dcr for MIDC Approval</li><li style=\"box-sizing: border-box;\">Follow up work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Follow up work for Provisional Fire NOC</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">MPCB consent to&nbsp;operate.</li><li style=\"box-sizing: border-box;\">MPCB consent to establish.</li><li style=\"box-sizing: border-box;\">Work pertaining to Thane RO office.</li><li style=\"box-sizing: border-box;\">Final Fire Noc. This will be in scope for Fire Person who will be executing the works prescribed by the fire officer.</li><li style=\"box-sizing: border-box;\">Empaneled Architect services</li></ol>',NULL,'Previous Purchase Order - Confirmed on email','','1) GST @ 18% will be charged by us in our Bill \r\n2) You will be deducting 10% as TDS from our Bill towards Statutory Deductions. \r\n3) Subject','','UN-1/SER/2122/52','UNILAB CHEMICALS - W-34 - P.O. 0052.pdf','954373b4-4e8a-4301-a897-bcd9a4b0be7e','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,20,'2021-10-25 10:08:38','2021-10-25',20,'2021-10-25 10:03:39',20),
(13,'2021-22/00013','2021-10-28','2021-22/00013','2021-10-28',1,1,137,30,'Ganpat Patil','Plot No. 126/1','Chikhaloli, MIDC','MIDC Industrial Area','Ambernath','421505','9322282270',NULL,'patilaic@gmail.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,989500.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,989500.00,9.00,89055.00,9.00,89055.00,NULL,0.00,1167610.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Structural Engineer Scope.</li><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Structural design.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works.</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li><li style=\"box-sizing: border-box;\">Follow up work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Follow up work for Provisional Fire NOC and Final Fire NOC</li><li style=\"box-sizing: border-box;\">Follow up work for Pre-Dcr and Auto-Dcr Plan Approval.</li><li style=\"box-sizing: border-box;\">Follow up work for Additional FSI.</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Landscape Architecture &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li><li style=\"box-sizing: border-box;\">Interior Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li><li style=\"box-sizing: border-box;\">Lease Agreements.</li><li style=\"box-sizing: border-box;\">Work pertaining to Thane RO office.</li><li style=\"box-sizing: border-box;\">Final Fire NOC.</li><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems.</li></ol>',NULL,'Revised Balance Work Order is pending from Suma Pharmaceuticals','','','','WO-LetterHead','suma pharma plan work order.pdf','3518f21a-0d52-4a42-817b-cd1aa2300d6c','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2021-10-29 10:32:42','2021-10-29',1,'2021-10-29 10:27:16',1),
(14,'2021-22/00014','2021-10-01','2021-22/00014','2021-10-01',5,4,138,31,'Sachetan Gharat','Plot Infra Narhen','Narhen Village Industrial ','Logistic Park, Palava','Dombivli','','022-61334213',NULL,'roshni.jaiswal@lodhagroup.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,100000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,1000000.00,9.00,90000.00,9.00,90000.00,0.00,0.00,1180000.00,'<p class=\"MsoListParagraphCxSpFirst\" style=\"margin-left:18.0pt;mso-add-space:\r\nauto;text-align:justify;text-indent:-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><b><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nline-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;</span></span></b><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nline-height:107%;font-family:&quot;Times New Roman&quot;,serif\">Follow up work for <b>RFP for road crossing over GAIL Pipeline\r\n(24’’ dia.) </b>along with necessarily follow up with concerned authorities<b>.<o:p></o:p></b></span></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"margin-left:18.0pt;mso-add-space:\r\nauto;text-align:justify;text-indent:-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;\">2)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><b><span lang=\"EN-US\" style=\"font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif\">Architect</span></b><span lang=\"EN-US\" style=\"font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif\">\r\n&amp; <b>Structural </b>Engineer Scope.<o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n</p><p class=\"MsoListParagraphCxSpLast\" style=\"margin-left:18.0pt;mso-add-space:auto;\r\ntext-align:justify;text-indent:-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><b><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nline-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;\">3)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></b><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nline-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-bidi-font-weight:bold\">Vetting\r\nof Drawing from </span><b><span lang=\"EN-US\" style=\"font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif\">VJTI.<o:p></o:p></span></b></p>','<p class=\"MsoListParagraph\" style=\"margin-left:14.2pt;mso-add-space:auto;\r\ntext-align:justify;text-indent:-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-weight:bold\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nline-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-bidi-font-weight:bold\">Approval\r\nother than GAIL, Belapur Office.<o:p></o:p></span></p>','2021-10-19','','','Terms & Conditions: -\r\n1)  Advance Payment with work order of Rs. 1.00 Lakh + GST 18%.\r\n2) GST @ 18% will be charged by us in our bill.\r\n3) Site Visit Expenses will be charged extra by us in our bill.\r\n4) Stamp Duty will be extra.\r\n','','7000161165','7000161165.pdf','7b95ed0c-69f8-4979-a871-5520f0978234','Archived',NULL,NULL,NULL,1,'Project Cancelled & Closed','2022-05-12 17:17:21',0,NULL,1,'2022-05-12 17:17:21','2022-05-12',1,'2021-10-29 13:32:53',1),
(15,'2021-22/00015','2021-11-09','2021-22/00015','2021-11-09',1,1,139,32,'Mr. Prasoon Kumar Shah','Plot No. W17','MIDC Badlapur','','Thane','','9890085085',NULL,'chemsynthlabs2021@gmail.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,950000.00,NULL,3500.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,985000.00,9.00,88650.00,9.00,88650.00,NULL,0.00,1162300.00,'<p></p><p></p><ol><li>Architect &amp; Structural Engineer Scope.</li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Design and Site Development.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Structural design.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Taking Client\'s instructions and preparation of design brief.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Site evaluation, analysis and impact of existing and / or proposed development on its&nbsp;</span><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">immediate environs.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Follow up work for Building Plan Approval.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Follow up work for Provisional Fire NOC.</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</span></li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Follow up work for Building Completion Certificate.</span></li></ol>','<ol><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Landscape Architecture</span><br></li><li>Interior Architecture</li><li>Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li><span style=\"font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;\">Work pertaining to Thane RO office.</span></li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li></ol>','2021-11-12','Received mail from Mr. Amit Pagare','The Site Visits mentioned in inclusion 10 visits is converted to 15 visits inclusive in the Lumpsum Package after 16th Visit Rs 3500/- Per visit will be chargabable.','','','WO-LetterHead','Quotation-2021-22-00015_Chemtek (1).pdf','ac56c3c1-dc39-4497-a95d-4fcd1edbbf5b','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,'2021-11-12 18:07:47','2021-11-12',1,'2021-11-09 15:47:44',1),
(16,'Q2021-22/00016','2022-02-07','P2021-22/00028','2022-02-23',7,5,135,26,'Mr. Bhaskar','Plot No. C-96 & C-97 ','Plot No. C-96 & C-97 ','MIDC Industrial Zone, MAHAD MIDC','MAHAD ','','9820030780',NULL,'admin1@unilabchem.com','Lumpsum',NULL,-0.01,NULL,0.00,NULL,1000000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',1000000.00,9.00,90000.00,9.00,90000.00,0.00,0.00,1180000.00,'<ol><li>Working on procedure of allotment of land &amp; follow up work &amp; amalgamation as same.</li><li>Work pertaining to – RO Office.</li><li>Follow up work for MIDC Land Allotment Approval.</li><li>Allotment of land for 172.56 Sq. Mts.</li><li>Amalgamation of land for 172.56 Sq. Mts. with Plot No. C-96 &amp; C-97.</li><li>Agreement of Plot No.C-96 &amp; C-97 of 172.56 sq. mts.</li></ol>','<div><b style=\"\">ALL THE SERVICES STRICKED BELOW ARE NOT APPLICABLE IN THIS SCOPE.</b></div><ol><li><strike>Landscape Architecture <span style=\"white-space:pre\">	</span></strike></li><li><strike>Interior Architecture&nbsp;</strike></li><li><strike>Architectural Conservation&nbsp;</strike></li><li><strike>Retrofitting of Buildings&nbsp;</strike></li><li><strike>Graphic Design and Signage</strike></li><li><strike>Lease Agreements.</strike></li><li><strike>Final fire NOC</strike></li><li><strike>Approval of BCC</strike></li><li><strike>Structural Engineer Scope.</strike></li><li><strike>Taking Client\'s instructions and preparation of design brief.&nbsp;</strike></li><li><strike>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</strike></li><li><strike>Design and site development.&nbsp;</strike></li><li><strike>Structural design.&nbsp;</strike></li><li><strike>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</strike></li><li><strike>Periodic inspection and evaluation of Construction works.</strike></li><li><strike>Electrical, electronic, communication systems and design.&nbsp;</strike></li><li><strike>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</strike></li><li><strike>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</strike></li></ol>','2022-02-04','','','<ol><li><span style=\"font-size: 16px;\">Advance Payment According Stages.</span></li><li><span style=\"font-size: 16px;\">GST @ 18% will be charged by us in our bill.</span></li><li><span style=\"font-size: 16px;\">Site Visit Expenses will be charged extra by us in our bill.</span></li><li><span style=\"font-size: 16px;\">Stamp duty &amp; Registrations Charges will be paid at actuals and is not in scope of this work</span></li></ol>','','UN-1/SER/2122/69','KASHEKAR.PO.PDF 0069.pdf','9a9026fe-d4a5-42ae-88cd-dae9e5c3a62f','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-02-23 16:58:57','2022-02-23',36,'2021-11-23 14:55:39',1),
(17,'Q2021-22/00017','2021-12-09','P2022-23/00034','2021-12-04',NULL,1,140,33,'Mr Sanjeev Kumar Mandholia','B-42, ','TTC Industrial area, , Village Elthan, Kalwa Block, Digha, ','MIDC Pipe Line Road , Airoli ,','Navi Mumbai','0','9833997465',NULL,'MEDIAIRHEALTHCARE@GMAIL.COM','Lumpsum',NULL,NULL,NULL,0.00,NULL,162660.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',187660.00,9.00,16889.40,9.00,16889.40,0.00,0.00,221438.80,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Liasioning work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Liasioning work for Provisional Fire NOC and Final Fire NOC.</li><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works</li><li style=\"box-sizing: border-box;\">Predcr and Auto Dcr for MIDC approval</li><li style=\"box-sizing: border-box;\">Empanelled rights of Accredited Architect&nbsp;</li><li style=\"box-sizing: border-box;\">Bill checking of Contractors</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li style=\"box-sizing: border-box;\">Fire detection, Fire protection and Security systems &amp; design</li><li style=\"box-sizing: border-box;\">Interior Architecture,&nbsp;Architectural Conservation,Retrofitting of Buildings,Graphic Design and Signage</li></ol>','2021-12-30','','','<ol><li><span style=\"font-size: 1rem;\">Advance Payment According to Stages.</span><br></li><li>&nbsp;Please note out of pocket expenses will be extra. </li><li>&nbsp;GST 18% will be charged by us in our bill. </li><li>&nbsp;Site Visit expenses will be charged extra by us in our bill. </li><li>&nbsp;Stamp Duty &amp; Registration Charges will be paid at actuals and is not in our scope of this work. </li></ol>','','Verbal Communication ','Array','','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-04-14 17:16:54','2022-04-14',36,'2021-12-03 17:02:13',1),
(18,'2021-22/00018','2021-12-08',NULL,NULL,NULL,3,102,34,'Mr. Vinayak A Patil','A-84, MIDC','MIDC Phase I','Dombivli East ','Thane ','421203','',NULL,'nabhalerao@gharda.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,425000.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,475000.00,9.00,42750.00,9.00,42750.00,0.00,0.00,560500.00,'<ol><li style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; Follow up work for Building Plan Approval.</li><li style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; Follow up work for Provisional Fire NOC.</li><li style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; Follow up work for Building Completion Certificate</li><li style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; Follow up work for Additional FSI (If any).&nbsp;</li></ol><p style=\"margin-left: 23px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN-US\" style=\"font-size: 11.5pt;\"></span></p><p style=\"margin-left: 23px; text-align: justify; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN-US\" style=\"font-size:11.5pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\"><o:p></o:p></span></p>','<ol><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Architect &amp; Structural Engineer Scope.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Design and Site Development.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Structural design.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Site evaluation, analysis and impact of existing and / or proposed development on its&nbsp; immediate environs.&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Landscape Architecture <span style=\"white-space:pre\">	</span></li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Interior Architecture&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Architectural Conservation&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Retrofitting of Buildings&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Graphic Design and Signage</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Lease Agreements.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Work pertaining to Thane RO office.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Final Fire NOC.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Periodic inspection and evaluation of Construction works.</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Electrical, electronic, communication systems and design.&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li style=\"margin-left: 27px; text-indent: -18pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp; &nbsp; &nbsp; &nbsp;Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li></ol>','2021-12-30',NULL,'',' 	1) Please note out of pocket expenses will be extra.\r\n 	2) Advance payment according stages. \r\n 	3) GST @ 18% will be charged by us in our bill. \r\n 	4) Site Visit Charges will be charged extra by us in our bill.\r\n','',NULL,NULL,NULL,'Open',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'2021-12-09',1,'2021-12-08 10:39:27',1),
(19,'2021-22/00019','2021-12-08',NULL,NULL,NULL,1,81,NULL,'Hariharan','Plot No T2 (Factory Building)','Plot No. T2, Taloja Industrial Area ','MIDC, Taloja , Dist. Raigad. ','Taloja','410208','9892908402',NULL,'shariharan@igpetro.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,100000.00,0,3000.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',100000.00,9.00,9000.00,9.00,9000.00,NULL,0.00,118000.00,'<p>1, Final Designing</p>','<ol><li>Architect &amp; Structural Engineer Scope.</li><li>Design and Site Development.</li><li>Structural design.</li><li>Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</li><li>Follow up work for Building Plan Approval.</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li>Follow up work for Building Completion Certificate.</li><li>Follow up work for Hardship Approval for Structures in Marginal Open Spaces.</li><li>Landscape Architecture <span style=\"white-space:pre\">	</span></li><li>Interior Architecture&nbsp;</li><li>Architectural Conservation&nbsp;</li><li>Retrofitting of Buildings&nbsp;</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.&nbsp;</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li></ol>','2021-12-30',NULL,'','<ul><li>Please note out of pocket expenses will be extra.\r\n</li><li>Advance payment according to stages. </li><li>GST @ <b>18% extra</b> will be charged by us in our bill. </li><li>Site Visit Charges will be charged extra by us in our bill.</li></ul>','',NULL,NULL,NULL,'Closed',1,'2022-05-12 17:18:47','Quotation Closed ',NULL,NULL,NULL,0,20,NULL,'2022-05-12 17:18:47','2022-05-12',1,'2021-12-08 12:36:57',1),
(20,'Q2021-22/00020','2021-12-14','2021-22/00020','2021-12-14',NULL,3,81,28,'Hariharan','Plot No. T2','Plot No. T2, Taloja Industrial Area ','MIDC, Taloja , Dist. Raigad. ','Taloja','410208','9892908402',NULL,'shariharan@igpetro.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,300000.00,10,3500.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',300000.00,9.00,27000.00,9.00,27000.00,NULL,0.00,354000.00,'<ol><li>Follow up work for Building Plan Approval.</li><li><span style=\"font-size: 1rem;\">Follow up work for Provisional Fire NOC.</span></li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li>Follow up work for Building Completion Certificate</li><li>Follow up work for Certification</li></ol>','<ol><li>Architect &amp; Structural Engineer Scope.</li><li>Design and Site Development.</li><li>Structural design.</li><li><span style=\"font-size: 1rem;\">Taking Client\'s instructions and preparation of design brief.</span></li><li><span style=\"font-size: 1rem;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate</span></li><li>environs.</li><li>Landscape Architecture</li><li><span style=\"font-size: 1rem;\">Interior Architecture</span></li><li>&nbsp;Architectural Conservation</li><li>&nbsp;Retrofitting of Buildings</li><li>&nbsp;Graphic Design and Signage</li><li>&nbsp;Lease Agreements.</li><li>&nbsp;Work pertaining to Thane RO office.</li><li>&nbsp;Final Fire NOC.</li><li>&nbsp;Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>&nbsp;Periodic inspection and evaluation of Construction works.</li><li>&nbsp;Electrical, electronic, communication systems and design.</li><li>&nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>&nbsp;Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li></ol>','2021-12-31','','','<ol><li><span style=\"font-size: 1rem;\">Please note out of pocket expenses will be extra.</span><br></li><li><span style=\"font-size: 1rem;\">Advance payment according stages.</span><br></li><li>GST @ 18% will be charged by us in our bill. </li><li>Site Visit Charges will be charged extra by us in our bill.</li></ol>',NULL,'Letter of Intend','Letter of Intent - 10.01.2022 for Qtn. 00020.pdf','e42dbde7-00af-4437-8baa-45c4c117a167','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-01-11 15:26:39','2022-01-11',36,'2021-12-14 17:31:08',1),
(21,'2021-22/00021','2021-12-18',NULL,NULL,NULL,2,141,35,'Mr. Niranjan Athalye','Ganadish Co-operative Housing Society Ltd., ','Ganadish Co-operative Housing Society Ltd., Plot No.: R/19','Dombivli (East). ','Sudershan Nagar, MIDC Residential Zone , ','421203','9920713679',NULL,'niranjan.athalye@gmail.com','Percentage Basis',NULL,2100.00,10000.00,21000000.00,5.00,1050000.00,10,1200.00,1000.00,NULL,1000.00,NULL,1000.00,NULL,1000.00,NULL,1000.00,NULL,1000.00,NULL,1050000.00,9.00,94500.00,9.00,94500.00,NULL,0.00,1239000.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works.</li><li style=\"box-sizing: border-box;\">Structural Engineer Scope.</li><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems.</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc. and Fire detection, Fire protection and Security systems etc.</li><li style=\"box-sizing: border-box;\">Follow-up work for Building Plan Approval and Provisional Fire NOC &amp; Building Completion Certificate.&nbsp;</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Landscape Architecture</li><li style=\"box-sizing: border-box;\">Interior Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li><li style=\"box-sizing: border-box;\">Lease Agreements</li><li style=\"box-sizing: border-box;\">Work Pertaining toThane RO Office</li><li style=\"box-sizing: border-box;\">Final Fire NOC</li><li style=\"box-sizing: border-box;\">Project Management Consultancy</li></ol>','2021-12-30',NULL,'','<ol><li>GST @ 18% will be charged by us in our Bill.\r\n</li><li>You will be deducting 10% as TDS from our Bill towards statutory deductions.\r\n</li><li>&nbsp;Advance Payment According Stages.</li><li> Please Note Out of Pocket Expenses Will Be Extra.</li><li>**Cost of Construction Rs.2100/-** is for calculations purpose and is subject to change as per total cost of construction. Please refer Annexure-C for the complete clarity.**</li></ol>',NULL,NULL,NULL,NULL,'Open',NULL,NULL,NULL,NULL,NULL,NULL,0,36,NULL,NULL,'2021-12-24',36,'2021-12-17 12:47:57',1),
(22,'2021-22/00022','2021-12-18',NULL,NULL,NULL,1,142,37,'Kamlesh Kumar Lader','Plot No. - G15, G-16 & G-17, ','Plot No. - G15, G-16 & G-17, Taloja MIDC Industrial Area, ','At Village - Padge, ','Panvel ','410208','7718831931',NULL,'lader.kk@balmerlawrie.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,1700000.00,0,1200.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,1700000.00,9.00,153000.00,9.00,153000.00,NULL,0.00,2006000.00,'<ol><li>Liasioning work for Building Plan Approval and Building Completion Certificate</li><li>&nbsp;Liasioning work for Provisional Fire NOC and Final Fire NOC.</li><li>Taking Client\'s instructions and preparation of design brief.</li><li>&nbsp;Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li>Design and site development.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>&nbsp;Periodic inspection and evaluation of Construction works</li><li><span style=\"font-size: 1rem;\">&nbsp;Empaneled rights of Accredited Architect</span><br></li><li>&nbsp;Bill checking of Contractors</li><li>Follow up work for Building Plan Approval.</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li> Follow up work for Building Completion Certificate</li><li>Follow up work for Certification</li></ol>','<ol><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li>Fire detection, Fire protection and Security systems &amp; design</li><li>Interior Architecture, Architectural Conservation, Retrofitting of Buildings, Graphic Design and</li><li>Signage</li></ol>','2021-12-30',NULL,'','<ol><li>&nbsp;Advance Payment According to Stages. </li><li>&nbsp;Please note out of pocket expenses will be&nbsp;<span style=\"font-size: 1rem;\">extra.</span></li><li>GST 18% will be charged by us in our bill. </li><li>Site Visit expenses will be charged extra Rs. 1200/-&nbsp; per visit , by&nbsp;<span style=\"font-size: 1rem;\">us in our bill.</span></li></ol>',NULL,NULL,NULL,NULL,'Open',NULL,NULL,NULL,NULL,NULL,NULL,0,36,NULL,NULL,'2021-12-28',1,'2021-12-18 11:46:45',36),
(24,'Q2021-22/00024','2021-11-23','2021-22/00024','2021-11-23',NULL,3,97,40,'Ankush Ajagekar','Plot No. 3/1','Plot No. 3/1, ','Maharashtra','Raidag','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,0.00,NULL,0.00,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,0.00,NULL,47321.00,9.00,4258.89,9.00,4258.89,NULL,0.00,55838.78,'','',NULL,'','','',NULL,'2900000570 ','PO no. 2900000570 - 24.11.2021 - Apcotex.pdf','972636be-6ac1-4f24-8eed-f13ba355fc35','Archived',NULL,NULL,NULL,36,'Project Completed','2022-05-17 15:12:23',0,NULL,36,'2022-05-17 15:12:23','2022-05-17',36,'2021-12-30 15:14:15',36),
(25,'Q2021-22/00025','2021-12-30','2021-22/00025','2021-12-30',NULL,5,92,41,'Mr. Maruti Darur','Plot No.: 74, Ambernath MIDC','Plot No.: 74, MIDC Chickaloli','MIDC Ambernath','Thane ','','0',NULL,'marutid@centaurlab.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,1500000.00,0,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',1500000.00,9.00,135000.00,9.00,135000.00,NULL,0.00,1770000.00,'<p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Working on procedure of allotment of\r\nland &amp; follow up work &amp; amalgamation as same.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">2)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Work pertaining to Thane RO - II office.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">3)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Follow up work for MIDC Land Allotment\r\nApproval.<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">4)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Allotment of land strip 11 MT X 9.50 MT<o:p></o:p></span></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">5)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Amalgamation of land strip 11 MT X 9.50\r\nMT with Plot No 74,75,76,76/1<o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\nmso-bidi-font-weight:bold\">6)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;mso-bidi-font-weight:bold\">Agreement of Plot No. 74 of 11 MT X 9.50\r\nMT<o:p></o:p></span></p>','<p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1;tab-stops:365.4pt\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin\">1)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Landscape Architecture <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1;tab-stops:365.4pt\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin\">2)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Interior Architecture <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">3)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Architectural Conservation <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">4)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Retrofitting of Buildings <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">5)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Graphic Design and Signage<o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">6)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; &nbsp;&nbsp;</span></span></s><s style=\"text-align: start; text-indent: 0px;\"><span lang=\"EN-US\" style=\"font-size: 12pt;\">Lease Agreements.</span></s><!--[endif]--><v:roundrect id=\"Rectangle_x003a__x0020_Rounded_x0020_Corners_x0020_6\" o:spid=\"_x0000_s1026\" alt=\"ll\" style=\"position:absolute;left:0;text-align:left;\r\n margin-left:423pt;margin-top:3.7pt;width:98.4pt;height:82.2pt;text-indent:0;\r\n z-index:251660288;visibility:visible;mso-wrap-style:square;\r\n mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;\r\n mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;\r\n mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;\r\n mso-position-horizontal-relative:text;mso-position-vertical:absolute;\r\n mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;\r\n mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:middle\" arcsize=\"10923f\" o:gfxdata=\"UEsDBBQABgAIAAAAIQC75UiUBQEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRvU7DMBSF\r\ndyTewfKKEqcMCKEmHfgZgaE8wMW+SSwc27JvS/v23KTJgkoXFsu+P+c7Ol5vDoMTe0zZBl/LVVlJ\r\ngV4HY31Xy4/tS3EvRSbwBlzwWMsjZrlprq/W22PELHjb51r2RPFBqax7HCCXIaLnThvSAMTP1KkI\r\n+gs6VLdVdad08ISeCho1ZLN+whZ2jsTzgcsnJwldluLxNDiyagkxOquB2Knae/OLUsyEkjenmdzb\r\nmG/YhlRnCWPnb8C898bRJGtQvEOiVxjYhtLOxs8AySiT4JuDystlVV4WPeM6tK3VaILeDZxIOSsu\r\nti/jidNGNZ3/J08yC1dNv9v8AAAA//8DAFBLAwQUAAYACAAAACEArTA/8cEAAAAyAQAACwAAAF9y\r\nZWxzLy5yZWxzhI/NCsIwEITvgu8Q9m7TehCRpr2I4FX0AdZk2wbbJGTj39ubi6AgeJtl2G9m6vYx\r\njeJGka13CqqiBEFOe2Ndr+B03C3WIDihMzh6RwqexNA281l9oBFTfuLBBhaZ4ljBkFLYSMl6oAm5\r\n8IFcdjofJ0z5jL0MqC/Yk1yW5UrGTwY0X0yxNwri3lQgjs+Qk/+zfddZTVuvrxO59CNCmoj3vCwj\r\nMfaUFOjRhrPHaN4Wv0VV5OYgm1p+LW1eAAAA//8DAFBLAwQUAAYACAAAACEAhGoDH5EDAAB4CQAA\r\nHwAAAGNsaXBib2FyZC9kcmF3aW5ncy9kcmF3aW5nMS54bWzUVl1v2zYUfR+w/0DwvbEkO05iVCk8\r\nbykKZGkQt+gzTVEWUYrkSNqx++t3SH3E8TYU2PZSGJBJ3sPDew/vvdLbd4dWkb1wXhpd0vwio0Ro\r\nbiqptyX9/OnuzTUlPjBdMWW0KOlRePru9uef3rLF1jHbSE7AoP2ClbQJwS4mE88b0TJ/YazQsNXG\r\ntSxg6raTyrFnMLdqUmTZfNIyqentC9WvLDCyc/JfUCnDv4pqxfSeeVAqvjhd6X1U/L8zs4Xev3d2\r\nbR9d9Jw/7B8dkVVJoZxmLSSik97QwzCdnO3avhAcatdGvKlrcijpvLieX2bgOpZ0lt8UxVXW8YlD\r\nIByAvJjdzK8B4EDk2Wx6M+sRvPn4HQ7e/PYdFjjaOYTBiZPeRhf1/q9Rz4eonwRHmmyVWJAns9OV\r\nqMjKOI3MIsBUwnNXUqVGcSLVoMxA63tR/y9NxmjYwjof3gvTkjgoqYsuRpdT+rH9vQ+dMwMu3dng\r\nTziskwDh8IupjlGKDf5x786AC3fhLb+TIL5nPjwyh1rAIqoqfMSjVua5pFxJS0lj3LfztYhDXsJC\r\nyTNqqqT+jx1zghL1QfuS3uQzXDEJaTK7vCowcaeWzalF79qVUUiN5FUaRnxQw7B2pv1iXLWMp8LE\r\nNMfZcDC4YbIKmMOE0uViuUxjblrLwr1eWxRRnmSLWn06fGHO9qoGJOmDWTfMir/TtcOmTDLLXTC1\r\n7EXv1IwG5cM6HJVIJZQ0R0oQprZoTtHBhIk+xAFUf+SB7FkMN7vKUDbdJaoTxLIOAzb4Dns9AsGQ\r\n7EiUPvVcBDucqZDMJRX6zec1lPyGE9CyKNkMwYu6Rvp0ecMWgpFwtKJmHA1gxZTcOEmJZdp4LGRF\r\ndpdd4hl/s2wan7DKwJs71kqFSp5igTfMeZFuJcXP/Qnp7/CHqVecM3BOE+80cRZnnDl692tOxInY\r\nYojhdqkUCY0gXri95MKjzcvYQwlSjzyYQJbWKsnZRgkiNaDSkzVHU4/dDJpGriTWPyuWQ7Fo/eG1\r\nuiBnQQtdxTp/Ok2UDw99ovzoYb8El24ZZRhvfOx9Oy/WNvbOrtiG5uhj3cbbVvpJ1Hgl4mVVpOtP\r\nHwRipVxXfoxzocN8rFWg47ZaKjVu7BrM2UYV8n5Tj33JrXFjl3BnG1+f2GXjcKrRYdzcSm26HnNG\r\nUH0dT+7w3euhixjixPfj5OybI0H6b6T4YXM6v/0TAAD//wMAUEsDBBQABgAIAAAAIQC2OwQiVAYA\r\nAAsaAAAaAAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWzsWUtvGzcQvhfof1jsvbHeio3Iga1H\r\n3MZOgkhJkSOlpXYZc5cLkrKjW5EcCxQomhY9NEBvPRRtAyRAL+mvcZuiTYH8hQ65D5ESVTtGChhB\r\nLMDYnf1mOJyZ/YbkXrn6IKbeEeaCsKTjVy9VfA8nExaQJOz4d0aDjy77npAoCRBlCe74cyz8q9sf\r\nfnAFbU0oSccM8WAU4Rh7YCgRW6jjR1KmWxsbYgJiJC6xFCfwbMp4jCTc8nAj4OgYBojpRq1SaW3E\r\niCT+NliUylCfwr9ECiWYUD5UZrCXoBhGvzmdkgnW2OCwqhBiLrqUe0eIdnywGbDjEX4gfY8iIeFB\r\nx6/oP39j+8oG2sqVqFyja+gN9F+ulysEhzU9Jg/H5aCNRrPR2intawCVq7h+u9/qt0p7GoAmE5hp\r\n5otps7m7udtr5lgDlF06bPfavXrVwhv26ys+7zTVz8JrUGa/sYIfDLoQRQuvQRm+uYJvNNq1bsPC\r\na1CGb63g25WdXqNt4TUooiQ5XEFXmq16t5htCZkyuueEbzYbg3YtN75AQTWU1aWGmLJErqu1GN1n\r\nfAAABaRIksST8xRP0QRqsosoGXPi7ZMwgsJLUcIEiCu1yqBSh//q19BXOiJoCyNDW/kFnogVkfLH\r\nExNOUtnxPwGrvgF5/eKn1y+eeScPn588/PXk0aOTh79khiytPZSEptarH77858ln3t/Pvn/1+Gs3\r\nXpj4P37+/PffvnIDYaaLELz85umfz5++/PaLv3587IDvcDQ24SMSY+HdwMfebRbDxHQIbM/xmL+Z\r\nxihCxNTYSUKBEqRGcdjvy8hC35gjihy4XWxH8C4HinEBr83uWw4PIz6TxGHxehRbwAPG6C7jzihc\r\nV2MZYR7NktA9OJ+ZuNsIHbnG7qLEym9/lgK3EpfJboQtN29RlEgU4gRLTz1jhxg7ZnePECuuB2TC\r\nmWBT6d0j3i4izpCMyNiqpoXSHokhL3OXg5BvKzYHd71dRl2z7uEjGwlvBaIO50eYWmG8hmYSxS6T\r\nIxRTM+D7SEYuJ4dzPjFxfSEh0yGmzOsHWAiXzk0O8zWSfh3oxZ32AzqPbSSX5NBlcx8xZiJ77LAb\r\noTh1YYckiUzsx+IQShR5t5h0wQ+Y/Yaoe8gDStam+y7BVrpPZ4M7wKymS4sCUU9m3JHLa5hZ9Tuc\r\n0ynCmmqA+C0+j0lyKrkv0Xrz/6V1INKX3z1xzOqiEvoOJ843am+Jxtfhlsm7y3hALj5399AsuYXh\r\ndVltYO+p+z11++88da97n98+YS84GuhbLRWzpbpeuMdr1+1TQulQzineF3rpLqAzBQMQKj29P8Xl\r\nPi6N4FK9yTCAhQs50joeZ/JTIqNhhFJY31d9ZSQUuelQeCkTsOzXYqdthaez+IAF2Xa1WlVb04w8\r\nBJILeaVZymGrITN0q73YgpXmtbeh3ioXDijdN3HCGMx2ou5wol0IVZD0xhyC5nBCz+yteLHp8OKy\r\nMl+kasULcK3MCiydPFhwdfxmA1RACXZUiOJA5SlLdZFdncy3mel1wbQqANYRRQUsMr2pfF07PTW7\r\nrNTOkGnLCaPcbCd0ZHQPExEKcF6dSnoWN94015uLlFruqVDo8aC0Fm60L/+XF+fNNegtcwNNTKag\r\niXfc8Vv1JpTMBKUdfwrbfriMU6gdoZa8iIZwYDaRPHvhz8MsKReyh0SUBVyTTsYGMZGYe5TEHV9N\r\nv0wDTTSHaN+qNSCEC+vcJtDKRXMOkm4nGU+neCLNtBsSFensFhg+4wrnU61+frDSZDNI9zAKjr0x\r\nnfHbCEqs2a6qAAZEwOlPNYtmQOA4sySyRf0tNaacds3zRF1DmRzRNEJ5RzHJPINrKi/d0XdlDIy7\r\nfM4QUCMkeSMch6rBmkG1umnZNTIf1nbd05VU5AzSXPRMi1VU13SzmDVC0QaWYnm+Jm94VYQYOM3s\r\n8Bl1L1PuZsF1S+uEsktAwMv4ObruGRqC4dpiMMs15fEqDSvOzqV27ygmeIprZ2kSBuu3CrNLcSt7\r\nhHM4EJ6r84PectWCaFqsK3WkXZ8mDlDqjcNqx4fPA3A+8QCu4AODD7KaktWUDK7gqwG0i+yov+Pn\r\nF4UEnmeSElMvJPUC0ygkjULSLCTNQtIqJC3f02fi8B1GHYf7XnHkDT0sPyLP1xb295vtfwEAAP//\r\nAwBQSwMEFAAGAAgAAAAhAJxmRkG7AAAAJAEAACoAAABjbGlwYm9hcmQvZHJhd2luZ3MvX3JlbHMv\r\nZHJhd2luZzEueG1sLnJlbHOEj80KwjAQhO+C7xD2btJ6EJEmvYjQq9QHCMk2LTY/JFHs2xvoRUHw\r\nsjCz7DezTfuyM3liTJN3HGpaAUGnvJ6c4XDrL7sjkJSl03L2DjksmKAV201zxVnmcpTGKSRSKC5x\r\nGHMOJ8aSGtHKRH1AVzaDj1bmIqNhQaq7NMj2VXVg8ZMB4otJOs0hdroG0i+hJP9n+2GYFJ69elh0\r\n+UcEy6UXFqCMBjMHSldnnTUtXYGJhn39Jt4AAAD//wMAUEsBAi0AFAAGAAgAAAAhALvlSJQFAQAA\r\nHgIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEA\r\nrTA/8cEAAAAyAQAACwAAAAAAAAAAAAAAAAA2AQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEA\r\nhGoDH5EDAAB4CQAAHwAAAAAAAAAAAAAAAAAgAgAAY2xpcGJvYXJkL2RyYXdpbmdzL2RyYXdpbmcx\r\nLnhtbFBLAQItABQABgAIAAAAIQC2OwQiVAYAAAsaAAAaAAAAAAAAAAAAAAAAAO4FAABjbGlwYm9h\r\ncmQvdGhlbWUvdGhlbWUxLnhtbFBLAQItABQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAAAAAAAAA\r\nAAAAAHoMAABjbGlwYm9hcmQvZHJhd2luZ3MvX3JlbHMvZHJhd2luZzEueG1sLnJlbHNQSwUGAAAA\r\nAAUABQBnAQAAfQ0AAAAA\r\n\" fillcolor=\"white [3201]\" strokecolor=\"#70ad47 [3209]\" strokeweight=\"1pt\">\r\n <v:stroke joinstyle=\"miter\">\r\n <v:textbox>\r\n  <!--[if !mso]-->\r\n  </v:textbox></v:stroke></v:roundrect></p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s style=\"text-align: justify; text-indent: -18pt; font-size: 1rem;\"><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><s style=\"text-align: justify; text-indent: -18pt; font-size: 1rem;\"><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Final fire NOC</span></s></p><p></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">8)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Approval of BCC<o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">9)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin\">Structural Engineer Scope.<o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">10)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Taking Client\'s\r\ninstructions and preparation of design brief. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">11)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Site evaluation,\r\nanalysis and impact of existing and /<o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify\"><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">&nbsp;or proposed development on its immediate\r\nenvirons. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">12)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Design and site\r\ndevelopment. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">13)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Structural\r\ndesign. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">14)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Sanitary,\r\nplumbing, drainage, water supply and sewerage design. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">15)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Periodic\r\ninspection and evaluation of Construction works.<o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">16)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Electrical,\r\nelectronic, communication systems and design. <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">17)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Heating,\r\nventilation and air conditioning design (HVAC) and <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify\"><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">other mechanical\r\nsystems <o:p></o:p></span></s></p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify;text-indent:\r\n-18.0pt;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">18)<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp; </span></span></s><!--[endif]--><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Elevators,\r\nescalators, etc. Fire detection, Fire protection and Security<o:p></o:p></span></s></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNoSpacing\" style=\"margin-left:36.0pt;text-align:justify\"><s><span lang=\"EN-US\" style=\"font-size:12.0pt;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">&nbsp;systems etc.&nbsp;<o:p></o:p></span></s></p>','2022-01-31','','','<p style=\"text-align: left; margin-bottom: 0cm; text-indent: -18pt; line-height: normal; margin-left: 25px;\">Advance Payment According Stages.</p><p style=\"text-align: left; margin-bottom: 0cm; text-indent: -18pt; line-height: normal; margin-left: 25px;\">GST @ 18% will be charged by us in our bill.</p><p style=\"text-align: left; margin-bottom: 0cm; text-indent: -18pt; line-height: normal; margin-left: 25px;\">Site Visit Expenses – N.A.</p><p style=\"text-align: left; margin-bottom: 0cm; text-indent: -18pt; line-height: normal; margin-left: 25px;\">Stamp duty &amp; Registrations Charges will be paid at actuals and will not be scope of this work</p>',NULL,'','Array','','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-01-11 15:31:12','2022-01-11',36,'2022-01-07 17:10:03',36),
(26,'Q2021-22/00026','2021-10-18','2021-22/00026','2021-10-18',NULL,3,81,28,'Hariharan','Plot No. T2','Plot No. T2, Taloja Industrial Area ','MIDC, Taloja , Dist. Raigad. ','Taloja','410208','9892908402',NULL,'shariharan@igpetro.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,800000.00,10,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',800000.00,9.00,72000.00,9.00,72000.00,NULL,0.00,944000.00,'<ol><li>Follow up work for Regularization of Ancillary Structures and Structures in Marginal Open Spaces for Hardship Approval.&nbsp;</li></ol>','<ol><li>Architect &amp; Structural Engineer Scope.</li><li>Design and Site Development.</li><li>Structural design.</li><li>Taking Client\'s instructions and preparation of design brief.</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li>Landscape Architecture&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li><li>Interior Architecture</li><li>Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li><li>Follow up work for Building Plan Approval &amp; Building Completion Certificate.</li><li>Follow up work for Provisional &amp; Final Fire NOC.</li></ol>','2021-12-31','','','<ol><li>Please note out of pocket expenses will be extra</li><li>Advance payment according to stages : Rs.1,00,000/- in advance with Work order &amp; Rs.7,00,000/- balance against receipt of regularization of structure.&nbsp;</li><li>GST @18% extra will be charged by us in our bill.&nbsp;</li><li>Site visit charges will be charged extra by us in our bill.&nbsp;</li></ol>',NULL,'Letter of Intend','Letter of Intent - 10.01.2022 for Qtn. 00016.pdf','3f4dac7e-9bb9-4bf1-af48-5d495ce4bba8','Archived',NULL,NULL,NULL,36,'Project Completed','2022-05-17 15:14:27',0,36,36,'2022-05-17 15:14:27','2022-05-17',36,'2022-01-11 15:48:16',36),
(27,'Q2021-22/00027','2022-01-20','2021-22/00027','2022-01-20',7,3,143,45,'Mr. Ardhendu.Purkayastha','W-40/41/42, MIDC Ambernath West','W-40/41/42, MIDC Industrial Area, ','Morivali Village, Ambernath West, ','Thane. ','','00000',NULL,'ganapati.s@acharyagroup.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,700000.00,0,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',700000.00,9.00,63000.00,9.00,63000.00,NULL,0.00,826000.00,'<ol><li><span style=\"font-size: 1rem;\">Conceptual design layout as per MIDC norms.</span></li><li><span style=\"font-size: 1rem;\">Follow up work for Building Plan Approval.</span><br></li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li>Follow up work for Building Completion Certificate.</li></ol>','<ol><li>Architect Design Scope.</li><li>Design and Site Development.</li><li>Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</li><li>Landscape Architecture <span style=\"white-space:pre\">	</span></li><li>Interior Architecture&nbsp;</li><li>Structural design.</li><li>Structural Engineer Scope</li><li>Architectural Conservation&nbsp;</li><li>Retrofitting of Buildings&nbsp;</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.&nbsp;</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc</li><li>Follow up work for Hardship Approval for Structures in Marginal Open Spaces.</li></ol>','2022-01-22','','','<ol><li>Please note out of pocket expenses will be extra.</li><li>Advance payment according stages.&nbsp;</li><li>GST @ 18% will be charged by us in our bill.&nbsp;</li><li>From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</li><li><span style=\"font-size: 1rem;\">Annexure - C is Not Applicable.&nbsp;</span><br></li></ol>',NULL,'21-22/0868 - 21.01.2022','PO no.0868 - 21.01.2021 - Acharya Chemicals.pdf','1080275b-3e21-47f8-ba2f-1e592d735a6e','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-01-24 14:40:14','2022-01-24',36,'2022-01-12 15:02:00',36),
(29,'2021-22/00028','2022-02-23',NULL,NULL,NULL,5,108,NULL,'Mr. Shripati','W-256, W-257  & W-258','W-256, W-257  & W-258','MIDC Dombivli East','Thane ','421204','0000000',NULL,'purchase@acharyagroup.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,500000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',500000.00,9.00,45000.00,9.00,45000.00,0.00,0.00,590000.00,'<ol><li>Working on procedure of allotment of land &amp; follow up work &amp; amalgamation as same.</li><li>Work pertaining to Thane RO office.</li><li>Follow up work for MIDC approval</li><li>Allotment of land strip 2 MT X 50 MT</li><li>Amalgamation of land strip 2 MT X 50 MT with W-256, W-257, &amp; W-258.</li><li>Complete BCC of W-256, W-257 &amp; W-258 along with strip of 2 MT X 50 MT</li></ol>','<ol><li>Landscape Architecture <span style=\"white-space:pre\">	</span></li><li>Interior Architecture&nbsp;</li><li>Architectural Conservation&nbsp;</li><li>Retrofitting of Buildings&nbsp;</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Final fire NOC</li><li>Approval of BCC</li><li>Structural Engineer Scope.</li><li>Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</li><li>Design and site development.&nbsp;</li><li>Structural design.&nbsp;</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.&nbsp;</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li></ol>',NULL,NULL,'','<ol><li>Please Note Out of pocket expenses will be extra.</li><li>Advance Payment According Stages.</li><li>GST @ 18% will be charged by us in our bill.</li><li>You will be deducting 7.5% as TDS from our bill towards statutory deductions till March 2021.&nbsp;</li><li>From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</li><li>Site Visit Expenses will be charged extra by us in our bill.</li></ol>',NULL,NULL,NULL,NULL,'Open',NULL,NULL,NULL,NULL,NULL,NULL,0,36,NULL,NULL,'2022-01-19',36,'2022-01-18 18:04:42',36),
(31,'2021-22/00029','2022-01-19',NULL,NULL,NULL,3,97,40,'Ankush Ajagekar','Plot No. 3/1','Plot No. 3/1, Taloja, ','Maharashtra','Raidag','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,40000.00,0,3500.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',40000.00,9.00,3600.00,9.00,3600.00,NULL,0.00,47200.00,'<p>Consulting for EC Purpose Drawing.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>','<p>No other services except Consulting for EC Purpose Drawing.&nbsp;</p>','2022-01-30',NULL,'','<ol><li>Please note out of pocket expenses will be extra.</li><li>Advance payment according stages.&nbsp;</li><li>GST @ 18% will be charged by us in our bill.&nbsp;</li><li>From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</li><li>&nbsp;Site Visit Charges will be Rs.3500/- extra per visit.&nbsp;</li></ol>',NULL,NULL,NULL,NULL,'Open',NULL,NULL,NULL,NULL,NULL,NULL,0,36,NULL,NULL,'2022-01-20',36,'2022-01-19 14:22:41',36),
(32,'2021-22/00030','2022-01-31','P2022-23/00040','2022-05-17',7,3,3,46,'Pradeep Choudhary','Plot No.: D-55 - 60,','Plot No. D-55 - 60, ','MIDC Phase II, Kalyan Shill Road, Dombivli','Thane ','421204','00000',NULL,'pradip.chaudhari@aarti-industries.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,2500000.00,0,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',2500000.00,9.00,225000.00,9.00,225000.00,NULL,0.00,2950000.00,'<ol style=\"padding: 0px 40px; color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;\"><li>Follow up work for Regularization of Structural Plan Approval</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval.</li><li>Follow up work for Building Plan Approval .</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Building Completion Certificate.&nbsp;</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems-._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc._ Additional Scope can be added</li><li style=\"box-sizing: border-box;\">Landscape Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li><li style=\"box-sizing: border-box;\">Lease Agreements.</li><li style=\"box-sizing: border-box;\">Work pertaining to Thane RO office.</li><li style=\"box-sizing: border-box;\">MIDC Transfers of Flats.</li><li style=\"box-sizing: border-box;\">All existing Transfers with MIDC.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design</li><li style=\"box-sizing: border-box;\">Final Fire NOC</li></ol>','2022-01-31','','<p><br></p>','<ol><li><span style=\"font-size: 1rem;\">&nbsp;Please note out of pocket expenses will be extra.\r\n</span></li><li><span style=\"font-size: 1rem;\">&nbsp;Advance payment according stages.\r\n</span></li><li><span style=\"font-size: 1rem;\">&nbsp;GST @ 18% will be charged by us in our bill.\r\n</span></li><li><span style=\"font-size: 1rem;\">&nbsp;From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions</span></li></ol><p><font color=\"#373a3c\">Note : Changes required will be done by company. All as drawing will be handed over by company. Any compounding charges will be borne&nbsp;by company. Estimate for all Statutory fees and compounding charges are subject to change. We will not responsible and liable for any changes in statutory &amp; compounding fees.</font><br></p>',NULL,'4580003029 dt.: 21.04.2022','PO no. 4580003029 - Aarti Industrial Limited..pdf','0bd697c5-d4d3-4ae6-9226-3816a5077704','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-05-17 16:09:54','2022-05-17',36,'2022-01-24 17:51:50',36),
(33,'2021-22/00031','2022-01-31','P00042','2022-06-04',7,2,144,47,'Mr.Kaushik ','Hotel Project ','Hotel Project, Dombivli ','','Thane ','','97692 51479',NULL,'modernmonarchjv@gmail.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,3100000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',3100000.00,9.00,279000.00,9.00,279000.00,NULL,0.00,3658000.00,'<ol><li>Architect Design Scope.</li><li>Design and Site Development.</li><li>Structural design.</li><li>Structural Engineer Scope</li><li>Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</li><li>Follow up work for Building Plan Approval. (One Time Approval)</li><li>Follow up work for Provisional Fire NOC. (One Time Approval)</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval (One Time Approval)</li><li>Follow up work for Building Completion Certificate (One Time Approval).&nbsp;</li></ol><p>Please note point no. 7 to 10 is included only for ONE time approval. If we go for second approval it would be charged and billed separately.&nbsp;</p>','<ol><li>Landscape Architecture <span style=\"white-space:pre\">	</span></li><li>Interior Architecture&nbsp;</li><li><span style=\"font-size: 1rem;\">Architectural Conservation&nbsp;</span><br></li><li>Retrofitting of Buildings&nbsp;</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.&nbsp;</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li><li>Follow up work for Hardship Approval for Structures in Marginal Open Spaces.</li></ol>',NULL,'','','<ol><li>Please Note Out of pocket expenses will be extra.</li><li>Advance Payment According Stages.</li><li>GST @ 18% will be charged by us in our bill.</li><li>From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.&nbsp;</li><li>Site Visit Expenses will be charged extra by us in our bill.</li><li>Please note point no. 7 to 10 from Inclusion, is included only for ONE time approval. If we go for second approval it would be charged and billed separately.&nbsp;<br></li></ol>',NULL,'Verbal Communication - Will submit mail later','Array','','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-06-04 11:43:30','2022-06-04',1,'2022-01-25 17:09:54',36),
(34,'Q2021-22/00032','2022-03-30','P2022-23/00041','2022-07-22',7,1,97,40,'Ankush Ajagekar','Plot No. 3/1, Proposed Shed Tray Drayer','Plot No. 3/1, Proposed Shed Tray Drayer','Maharashtra','Raigad','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Lumpsum',NULL,2100.00,1138.51,2390871.00,3.00,70000.00,10,1200.00,0.00,'At Actuals',0.00,'At Actuals',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'At Actuals',70000.00,9.00,6300.00,9.00,6300.00,0.00,0.00,82600.00,'<ol><li>Architect Engineer Scope.</li><li>Design and Site Development.</li><li>Taking Client\'s instructions and preparation of design brief.&nbsp;</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.&nbsp;</li></ol>','<ol><li>Landscape Architecture&nbsp;</li><li>Structural Engineer Scope.</li><li>Structural design.</li><li>Interior Architecture&nbsp;</li><li>Architectural Conservation&nbsp;</li><li>Retrofitting of Buildings&nbsp;</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.&nbsp;</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.&nbsp;</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems&nbsp;</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li></ol><div><br></div>','2022-03-30','hi',NULL,'<ol><li>Please note out of pocket expenses will be extra.</li><li>Advance payment according to stages.&nbsp;</li><li>GST @ 18% will be charged by us in our bill.&nbsp;</li><li><span style=\"font-size: 1rem;\">Site Visit Charges @ 1200/- (Per Visit).&nbsp;</span><br></li></ol>',NULL,'2900000703 Dt.: 02.052022','PO no. 2900000703 - Apcotex Industries.pdf','a0c73deb-668a-4cf4-91c8-67874d32013c','Converted',NULL,NULL,NULL,NULL,NULL,NULL,2,36,1,'2022-07-22 11:39:38','2022-07-22',1,'2022-02-03 18:08:21',36),
(36,'Q2021-22/00033','2022-02-09','P2022-23/00033','2022-02-09',7,5,97,40,'Ankush Ajagekar','Plot No. 3/1','Plot No. 3/1, ','Maharashtra','Raidag','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Lumpsum',NULL,500000.00,NULL,0.00,NULL,300000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',300000.00,9.00,27000.00,9.00,27000.00,NULL,0.00,354000.00,'<ol><li style=\"margin-left: 25px;\">Working on procedure of allotment of land.&nbsp;</li><li style=\"margin-left: 25px;\">Work pertaining to SPA Office,&nbsp;</li><li style=\"margin-left: 25px;\">Follow up work for MIDC Land Allotment Approval.\r\n</li><li style=\"margin-left: 25px;\">Allotment of land .</li></ol>','<p>ALL THE SERVICES STRICKED BELOW ARE NOT APPLICABLE IN THIS SCOPE.\r\n</p><ol><li><strike>&nbsp;Landscape Architecture\r\n</strike></li><li><strike>Interior Architecture\r\n</strike></li><li><strike>Architectural Conservation\r\n</strike></li><li><strike>Retrofitting of Buildings\r\n</strike></li><li><strike>Graphic Design and Signage</strike></li><li><strike>Lease Agreements.\r\n</strike></li><li><strike>Final fire NOC\r\n</strike></li><li><strike>Approval of BCC\r\n</strike></li><li><strike>Structural Engineer Scope.\r\n</strike></li><li><strike><span style=\"font-size: 1rem;\">Taking Client\'s instructions and preparation of design brief.\r\n</span></strike></li><li><span style=\"font-size: 1rem;\"><strike>Site evaluation, analysis and impact of existing and / or proposed development on its immediate\r\nenvirons.\r\n</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Design and site development.\r\n</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Structural design.\r\n</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Sanitary, plumbing, drainage, water supply and sewerage design.\r\n</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Periodic inspection and evaluation of Construction works.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Electrical, electronic, communication systems and design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</strike></span></li></ol>','2022-02-16','',NULL,'<ol><li>Working on procedure of allotment of land &amp; follow up work &amp; amalgamation as same.\r\n</li><li>Work pertaining to – RO Office.\r\n</li><li>Follow up work for MIDC Land Allotment Approval.</li><li>Allotment of land .\r\n</li><li>Amalgamation of land\r\n6. Agreement of Plot .<br></li></ol>',NULL,'2900000687','WO_290687_Kashelkar Design Pvt Ltd (2).pdf','d4ce9640-ddc3-4501-88ec-44468b851e3d','Converted',NULL,NULL,NULL,NULL,NULL,NULL,1,36,36,'2022-04-14 15:26:19','2022-04-14',36,'2022-02-09 14:38:34',36),
(37,'Q2021-22/00034','2022-02-09','P2022-23/00038','2022-04-29',7,1,145,NULL,'Mr. Madhav Singh','Plot No. 16/2, 17/4, 17/5, 20/4, 21/1 & 21/2','Plot No. 16/2, 17/4, 17/5, 20/4, 21/1 & 21/2,','Village Borle, Neral,  Karjat','Raigad','','',NULL,'','Lumpsum',NULL,NULL,NULL,0.00,NULL,7200000.00,10,2100.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',7200000.00,9.00,648000.00,9.00,648000.00,NULL,0.00,8496000.00,'<ol><li><span style=\"font-size: 1rem;\">Taking Client\'s instructions and preparation of design brief.</span><br></li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li>Design and site development.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works</li><li><span style=\"font-size: 1rem;\">Bill checking of Contractors</span></li></ol>','<ol><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li>Fire detection, Fire protection and Security systems &amp; design</li><li>Interior Architecture, Architectural Conservation, Retrofitting of Buildings, Graphic Design and Signage</li><li><span style=\"font-size: 1rem;\">Follow-up for Provisional Fire NOC and Final Fire NOC.</span><br></li><li>Follow up work for Building Plan Approval.</li><li><span style=\"font-size: 1rem;\">Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</span><br></li><li>Follow up work for Building Completion Certificate</li><li>Follow up work for Certification</li></ol>','2022-02-16','',NULL,'<ol><li>Advance Payment According to Stages.</li><li>Please note out of pocket expenses will be extra.</li><li>GST 18% will be charged by us in our bill.</li><li>Site Visit expenses will be charged extra Rs. 2100/- per visit , by us in our bill</li></ol>',NULL,'Letter dated 28.04.2022','PO of Globe Constructin.jpg','428e2025-643c-4eaa-9488-36b5c0720033','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-04-29 13:33:33','2022-04-29',36,'2022-02-09 16:23:05',36),
(38,'Q2021-22/00035','2022-02-10','P00056','2022-07-21',7,2,2,21,'Ganesh Kinikar','Plot No..P27,  Hotel Project','Plot No..P27,  Hotel Project','Next to MIDC Office, Anand Nagar, Add. MIDC ','Ambernath','421501','9892479614',NULL,'kinikar701@outlook.com','Percentage Basis',NULL,2100.00,48639.18,102142278.00,3.50,3574979.73,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',3599979.70,9.00,323998.17,9.00,323998.17,NULL,0.00,4247976.04,'<ol><li>Architect Design Scope. </li><li>Design and Site Development.</li><li>Taking Client\'s instructions and preparation of design brief.</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs. </li><li>Follow up work for Building Plan Approval. </li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval </li><li>Follow up work for Building Completion Certificate. </li><li>Follow up work for Hardship Approval for Structures in Marginal Open Spaces.</li></ol><p>One-time approval charges for Point no 5,6, &amp; 8 of Inclusion, have been included in our charges,&nbsp;<span style=\"font-size: 1rem;\">these fees will be applicable only if we take Approvals more than one time and would quote at&nbsp;</span><span style=\"font-size: 1rem;\">that juncture</span><br></p>','<ol><li>Landscape Architecture</li><li>Interior Architecture </li><li>Structural design.</li><li>Structural Engineer Scope</li><li>Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li></ol>','2022-02-17','hi',NULL,'<p>Please Note Out of pocket expenses will be extra.</p><p>➢ Advance Payment According Stages.</p><p>➢ GST @ 18% will be charged by us in our bill.</p><p>➢ From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</p><p>➢ Site Visit Expenses will be charged extra by us in our bill.</p><p>➢ One-time approval charges for Point no 5,6, &amp; 8 of Inclusion, have been included in our charges,&nbsp;<span style=\"font-size: 1rem;\">these fees will be applicable only if we take Approvals more than one time and would quote at&nbsp;</span><span style=\"font-size: 1rem;\">that juncture</span></p>',NULL,'','Red-Apple-vector-PNG.png','f0a9b721-55a0-430d-afab-ed07b9fe0252','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 08:08:20','2022-07-21',1,'2022-02-10 17:07:03',36),
(39,'Q2021-22/00036','2022-02-10','P00055','2022-07-21',7,2,2,NULL,'Ganesh Kinikar','Plot No. P 27/1 - Hospital Project','Plot No. P 27/1 - Hospital Project','Next to MIDC Office, Anand Nagar, Add. MIDC ','Ambernath','421501','9892479614',NULL,'kinikar701@outlook.com','Percentage Basis',NULL,2100.00,35750.00,75075000.00,3.50,2627625.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',2652625.00,9.00,238736.25,9.00,238736.25,NULL,0.00,3130097.50,'<ol><li>Architect Design Scope. </li><li>Design and Site Development.</li><li> Taking Client\'s instructions and preparation of design brief.</li><li> Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li> Follow up work for Building Plan Approval.</li><li>Follow up work for Provisional Fire NOC.</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li>Follow up work for Building Completion Certificate. </li><li>Follow up work for Hardship Approval for Structures in Marginal Open Spaces</li></ol><p>One-time approval charges for Point no 5 ,6 &amp; 8 have been included in our charges,&nbsp;<span style=\"font-size: 1rem;\">these fees will be applicable only if we take Approvals more than one time and would quote at&nbsp;</span><span style=\"font-size: 1rem;\">that juncture.</span></p>','<ol><li>Landscape Architecture</li><li>Interior Architecture </li><li>Structural design.</li><li>Structural Engineer Scope</li><li>Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li>Lease Agreements.</li><li>Work pertaining to Thane RO office.</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.</li></ol>','2022-02-17','nothing',NULL,'<p>Please Note Out of pocket expenses will be extra.</p><p>➢ Advance Payment According Stages.</p><p>➢ GST @ 18% will be charged by us in our bill.</p><p>➢ From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</p><p>➢ Site Visit Expenses will be charged extra by us in our bill.</p><p>➢&nbsp;<span style=\"font-size: 1rem;\">One-time approval charges for Point no&nbsp;</span>5 ,6 &amp; 8<span style=\"font-size: 1rem;\">&nbsp;of Inclusion, have been included in our charges,&nbsp;</span><span style=\"font-size: 1rem;\">these fees will be applicable only if we take Approvals more than one time and would quote at&nbsp;</span><span style=\"font-size: 1rem;\">that juncture.</span></p>',NULL,'','project management.png','bcd38031-3226-4fd9-af4a-c51da5fe0fc9','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 08:07:22','2022-07-21',1,'2022-02-10 17:39:44',36),
(40,'Q2021-22/00037','2022-02-11',NULL,NULL,8,2,146,NULL,'President ','Vidya Niketan School','Vidya Niketan School, Vidya Niketan Marg, ','Manpada Village, Dombivli East, ','Thane ','421204','7045792502',NULL,'vidya.niketan.school@gmail.com','Percentage Basis',NULL,2100.00,150696.00,316461600.00,3.50,11076156.00,-1,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',11076156.00,9.00,996854.04,9.00,996854.04,NULL,0.00,13069864.08,'<ol><li>Architect Design Scope.</li><li><span style=\"font-size: 1rem;\">Design and Site Development.</span></li><li>Taking Client\'s instructions and preparation of design brief.\r\n</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate\r\nenvirons.</li><li>Follow up work for Building Plan Approval.</li><li><span style=\"font-size: 1rem;\">Follow up work for Provisional Fire NOC.</span></li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval</li><li><span style=\"font-size: 1rem;\">Follow up work for Building Completion Certificate.</span></li><li><span style=\"font-size: 1rem;\">Follow up work for Hardship Approval for Structures in Marginal Open Spaces</span></li></ol><p>One-time approval charges for Point no 5 ,6 &amp; 8 have been included in our charges, these fees will be\r\napplicable only if we take Approvals more than one time and would quote at that juncture.&nbsp;<span style=\"font-size: 1rem;\"><br></span></p>','<ol><li>Landscape Architecture\r\n</li><li>Interior Architecture\r\n</li><li>Structural design.\r\n</li><li>Structural Engineer Scope</li><li>&nbsp;Architectural Conservation\r\n</li><li>Retrofitting of Buildings\r\n</li><li>Graphic Design and Signage</li><li>Lease Agreements.\r\n</li><li>Work pertaining to Thane RO office.\r\n</li><li>Final Fire NOC.\r\n</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.\r\n</li><li>Periodic inspection and evaluation of Construction works.\r\n</li><li>Electrical, electronic, communication systems and design.\r\n</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems\r\n</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc<br></li></ol>',NULL,NULL,NULL,'<p>Please Note Out of pocket expenses will be extra.\r\n</p><p>➢ Advance Payment According Stages.\r\n</p><p>➢ GST @ 18% will be charged by us in our bill.\r\n</p><p>➢ From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.</p><p>\r\n➢ Site Visit Expenses will be charged extra by us in our bill.\r\n</p><p>➢ One-time approval charges for Point no 5 ,6 &amp; 8 of Inclusion, have been included in our\r\ncharges, these fees will be applicable only if we take Approvals more than one time and would quote\r\nat that juncture.<br></p>',NULL,NULL,NULL,NULL,'Closed',1,'2022-06-04 11:45:29','Tender not approved ',NULL,NULL,NULL,0,36,NULL,'2022-06-04 11:45:29','2022-06-04',1,'2022-02-11 10:49:04',36),
(41,'Q2021-22/00038','2022-02-19',NULL,NULL,NULL,2,148,NULL,'','','','','','','',NULL,'','Percentage Basis',NULL,21000.00,19000.00,399000000.00,2.00,7980000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',7980000.00,9.00,718200.00,9.00,718200.00,NULL,0.00,9416400.00,'','',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'Open',1,'2022-06-04 11:46:06','Quotation Closed ',NULL,NULL,NULL,0,NULL,NULL,'2022-06-04 11:46:06','2022-06-04',1,'2022-02-21 18:24:08',36),
(42,'Q2021-22/00039','2022-02-21','P2022-23/00030','2022-02-21',7,3,147,NULL,'Mr. Kapil Girotra','','','','','','',NULL,'kapil.girotra@henichemicals.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,649000.00,10,3500.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',550000.00,9.00,49500.00,9.00,49500.00,NULL,0.00,649000.00,'<p>1.&nbsp;<span style=\"font-size: 1rem;\">Follow up work for Building Plan Approval .</span></p><p>2. Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval.</p><p>3.&nbsp;<span style=\"font-size: 1rem;\">Follow up work for Provisional Fire NOC.</span></p><p>4. Follow up work for Building Completion Certificate.&nbsp;&nbsp;</p>','<p>1. Electrical, electronic, communication systems and design._ Additional Scope can be added\r\n</p><p>2. Heating, ventilation and air conditioning design (HVAC) and other mechanical systems-._ Additional\r\nScope can be added\r\n</p><p>3. Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc._ Additional Scope\r\ncan be added</p><p>\r\n4. Landscape Architecture\r\n</p><p>5. Architectural Conservation\r\n6. Retrofitting of Buildings\r\n</p><p>7. Graphic Design and Signage\r\n</p><p>8. Lease Agreements.</p><p>9. Work pertaining to Thane RO office.\r\n</p><p>10. MIDC Transfers of Flats.\r\n</p><p>11. All existing Transfers with MIDC.\r\n</p><p>12. Sanitary, plumbing, drainage, water supply and sewerage design\r\n</p><p>13. Final Fire NOC<br></p>','2022-02-28','',NULL,'<p>1. Please note out of pocket expenses will be extra.\r\n</p><p>2. Advance payment according stages.\r\n</p><p>3. GST @ 18% will be charged by us in our bill.\r\n</p><p>4. From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory\r\ndeductions</p>',NULL,'HCI/004/2021-22 DT.: 29.03.2022','P.O.KASHELKAR DESIGNS PRIVATE LIMITED..pdf','892703c6-a490-4b8e-85f2-7387753c7847','Converted',NULL,NULL,NULL,NULL,NULL,NULL,2,36,36,'2022-04-05 09:49:19','2022-04-05',36,'2022-02-21 18:30:17',36),
(43,'Q2021-22/00040','2022-02-25','P00053','2022-07-21',7,3,149,NULL,'Mr. Jayesh Awati','Alpha Pharma Health Care India Pvt. Ltd., Plot No. H 48/1B, M93/2 & H-53','Alpha Pharma Health Care Pvt. Ltd., Plot No. H 48/1B, M93/2 & H-53','Addl. Murbad Industrial Area, Tal. Murbad ','Thane ','','7738251521',NULL,'ja@alpha-pharma.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,1595000.00,10,7500.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',1595000.00,9.00,143550.00,9.00,143550.00,NULL,0.00,1882100.00,'<ol><li>Follow up work for Amalgamation of all three plots.&nbsp;</li><li><span style=\"font-size: 1rem;\">Follow up work for Building Plan Approval .</span></li><li>Follow up work for Provisional Fire NOC.</li><li><span style=\"font-size: 1rem;\">Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval.</span></li><li><span style=\"font-size: 1rem;\">Follow up work for Building Completion Certificate.&nbsp;</span></li></ol>','<ol><li>Electrical, electronic, communication systems and design._ Additional Scope can be added\r\n</li><li>&nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems-._ Additional\r\nScope can be added\r\n</li><li>&nbsp;Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc._ Additional Scope\r\ncan be added\r\n</li><li>&nbsp;Landscape Architecture\r\n</li><li>Architectural Conservation\r\n6. Retrofitting of Buildings</li><li>Graphic Design and Signage\r\n</li><li>Lease Agreements.\r\n</li><li>Work pertaining to Thane RO office.\r\n</li><li>MIDC Transfers of Flats.\r\n</li><li>All existing Transfers with MIDC.\r\n</li><li>Sanitary, plumbing, drainage, water supply and sewerage design\r\n</li><li>Final Fire NOC</li><li>Hardship Approval.&nbsp;</li></ol>','2022-02-28','fdsf',NULL,'<p>1. Please note out of pocket expenses will be extra.\r\n</p><p>2. Advance payment according stages.\r\n</p><p>3. GST @ 18% will be charged by us in our bill.\r\n</p><p>4. From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory\r\ndeductions&nbsp;</p><p>5. Please note in Invoice Summary from Sr. no.03 to Sr. no.08 is charged twice, as all relevant approvals has to be taken twice in this scope of project.&nbsp;<br></p>',NULL,'','project management.png','1d2b1399-d2aa-4604-9761-c051f105f591','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 07:54:47','2022-07-21',1,'2022-02-23 17:54:41',36),
(44,'Q2021-22/00041','2022-03-15','P00052','2022-07-21',7,1,150,NULL,'Mr. Nandan Khopkar','W-261, MIDC,','W-261, MIDC, Phase II, ','Dombivli','Thane','421204','+91 8605011765',NULL,'nandan_khopkar@fineorganics.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,184512.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',156366.00,9.00,14072.94,9.00,14072.94,NULL,0.00,184511.88,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Liasioning work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Liasioning work for Provisional Fire NOC and Final Fire NOC.</li><li style=\"box-sizing: border-box;\">Structural Engineer Scope.</li><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Structural design.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works</li><li style=\"box-sizing: border-box;\">PreDCR</li><li style=\"box-sizing: border-box;\">AutoDCR</li><li style=\"box-sizing: border-box;\">Powers of Empanelled Architect</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc.Fire detection, Fire protection and Security systems etc.</li><li style=\"box-sizing: border-box;\">Landscape Architecture</li><li style=\"box-sizing: border-box;\">Interior Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li></ol>',NULL,'nothing',NULL,'<ol><li>REFER YOUR PURCHASE ORDER NO.: 4915 DATED 29.07.2017 REV.NO./DATE: 1 / 02-08-2017</li><li>&nbsp;3% of total&nbsp;<span style=\"font-size: 1rem;\">actual construction cost to be </span>considered<span style=\"font-size: 1rem;\">&nbsp;for billing, as pe old Purchase Order no.: 4915 dt.: 02.08.2017.&nbsp;</span></li><li><span style=\"font-size: 1rem;\">Payment terms :&nbsp;</span><span style=\"font-size: 1rem;\">&nbsp;Payment against running bills.</span></li><li><span style=\"font-size: 1rem;\">a) DOCUMENTATION AND COMMUNICATION CHARGES : Apart from the professional fee, the Client shall pay to the Architect Documentation and Communication charges, @ 10% of the professional fee payable to the Architect at all stages. The Above stands Cancelled. b) REIMBURSABLE EXPENSES : Up till 10 visits of site no charges of site visit, after that 11th site visit is chargeable and charges is 1500/- per visit. The above point stands cancelled and site visit fee are included in the architects fee structure. c)Any revision in the drawings, tenders and documents, once approved, required to be made by the Client shall be compensated as additional services rendered by the Architect and paid for @ 50% of the fee prescribed for the relevant stage(s). The above point stands cancelled d) But the outstation site visit and 3d views and model if required to be executed my the architect will be charged as per actuals.<br></span><br></li></ol>',NULL,'78829 / 04.05.2022','project management.png','70a13979-2047-4f90-af2e-03ffc893bf8b','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 07:50:12','2022-07-21',1,'2022-03-15 16:11:51',36),
(45,'Q2021-22/00042','2022-03-15','P2022-23/00032','2022-03-15',7,1,150,NULL,'Mr. Nandan Khopkar','E-73, Additional Patalganga MIDC Industrial Area','E-73, Additional Patalganga MIDC Industrial Area','Raigad, Panvel','Thane','410220','8605011765',NULL,'nandan_khopkar@fineorganics.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,2548800.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',2160000.00,9.00,194400.00,9.00,194400.00,NULL,0.00,2548800.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Follow up work for Building Plan Approval and Building Completion Certificate</li><li style=\"box-sizing: border-box;\">Follow up work for Provisional Fire NOC and Final Fire NOC.</li><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works</li><li style=\"box-sizing: border-box;\">Predcr and Auto Dcr for MIDC approval</li><li style=\"box-sizing: border-box;\">Empanelled rights of Accredited Architect&nbsp;</li><li style=\"box-sizing: border-box;\">Bill checking of Contractors</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems &amp; design</li><li style=\"box-sizing: border-box;\">Fire detection, Fire protection and Security systems &amp; design</li><li style=\"box-sizing: border-box;\">Interior Architecture,&nbsp;Architectural Conservation, Retrofitting of Buildings, Graphic Design and Signage</li></ol>',NULL,'Converted Purchase Order from M.R.Kashelkar & Co. ',NULL,'<ul><li>REFER YOUR PURCHASE ORDER NO.: 57242 DATED 28.08.2019 REV.NO./DATE: 1 / 03.09.2019</li><li>&nbsp;3% of total&nbsp;<span style=\"font-size: 1rem;\">actual construction cost to be&nbsp;</span>considered<span style=\"font-size: 1rem;\">&nbsp;for billing.</span></li><li><span style=\"font-size: 1rem;\">Total construction area : 80,000 Sq.ft , as per PO No.: 57242 dt.: 03092019.&nbsp;</span></li><li><span style=\"font-size: 1rem;\">Payment terms :&nbsp;</span><span style=\"font-size: 1rem;\">&nbsp;Payment against running bills.</span></li><li><span style=\"font-size: 1rem;\">The company shall go for one approval and BCC hence above charges will not be Applicable. Site Visit will be Done by Architect and Engineer. All travel to and fro will be arranged by Fine Organics Ltd. The Fees are revised from 3.5 % to 3.0%<br></span></li></ul>',NULL,'77909 Dt. 05.04.2022','PO no. 77909_0001_0001 - Fine Organic.pdf','7556a46e-e8a6-4b10-8bc7-9f10540a4c54','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,36,'2022-04-13 10:25:38','2022-04-13',36,'2022-03-15 17:50:44',36),
(46,'Q2021-22/00043','2022-04-12','P00050','2022-07-21',7,5,152,NULL,'Mr. Makarand Joshi','W/71, Badlapur MIDC','W/71, Badlapur MIDC Industrial Area','Badlapur,','Thane ','421503','+91 9819884251',NULL,'makarand.joshi@anuvi.in','Lumpsum',NULL,NULL,NULL,0.00,NULL,540000.00,0,0.00,0.00,'At Actuals',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',540000.00,9.00,48600.00,9.00,48600.00,NULL,0.00,637200.00,'<ol><li style=\"margin-left: 25px;\">Working on procedure of allotment of land.&nbsp;</li><li style=\"margin-left: 25px;\"><span style=\"font-size: 1rem;\">Allotment of land .</span><br></li></ol>','<p>ALL THE SERVICES STRICKED BELOW ARE NOT APPLICABLE IN THIS SCOPE.</p><ol><li><strike>&nbsp;Landscape Architecture</strike></li><li><strike>Interior Architecture</strike></li><li><strike>Architectural Conservation</strike></li><li><strike>Retrofitting of Buildings</strike></li><li><strike>Graphic Design and Signage</strike></li><li><strike>Lease Agreements.</strike></li><li><strike>Final fire NOC</strike></li><li><strike>Approval of BCC</strike></li><li><strike>Structural Engineer Scope.</strike></li><li><strike><span style=\"font-size: 1rem;\">Taking Client\'s instructions and preparation of design brief.</span></strike></li><li><span style=\"font-size: 1rem;\"><strike>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Design and site development.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Structural design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Sanitary, plumbing, drainage, water supply and sewerage design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Periodic inspection and evaluation of Construction works.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Electrical, electronic, communication systems and design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</strike></span></li></ol>','2022-04-07','nothing',NULL,'<ol><li><span style=\"font-size: 1rem;\">Advance Payment According Stages.</span><br></li><li>GST @ 18% will be charged by us in our bill.</li></ol>',NULL,'ACL/G/007/2022-23','Red-Apple-vector-PNG.png','86e40a41-0c37-43e1-962e-d7ff1e06e405','Converted',NULL,NULL,NULL,NULL,NULL,NULL,1,36,1,'2022-07-21 07:21:42','2022-07-21',1,'2022-03-17 15:22:52',36),
(47,'Q2021-22/00044','2022-03-24','P00048','2022-07-21',7,2,81,NULL,'Mr. Hariharan','','','','','','',NULL,'','Lumpsum',NULL,NULL,NULL,0.00,NULL,75000.00,0,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',75000.00,9.00,6750.00,9.00,6750.00,NULL,0.00,88500.00,'<p>Consulting for EC Purpose Drawing.&nbsp;<br></p>','<p>No other services except Consulting for EC Purpose Drawing.&nbsp;<br></p>','2022-03-31','noyh',NULL,'<ol><li>Please note out of pocket expenses will be extra.</li><li> Advance payment according stages.</li><li> GST @ 18% will be charged by us in our bill.</li><li>From 1st April 2021 you will be deducting 10% as TDS from our bill towards statutory deductions.\r\n</li><li>Site Visit Charges will be Rs.3500/- extra per visit.<br></li></ol>',NULL,'ACL/G/007/2022-23','Array','fd4a026a-d3dc-4a45-ac78-1b92a847f256','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 07:16:03','2022-07-21',1,'2022-03-24 12:08:29',36),
(48,'Q2021-22/00045','2022-03-24','P00043','2022-06-04',7,2,153,NULL,'Mr. G.G. Mhatre','Plot No.: 4 - Survey No.: 21/1B','Plot No.: 4 - Survey No.: 21/1B','','Panvel ','','7506418602',NULL,'mhatregg@gmail.com','Percentage Basis',NULL,2100.00,4000.00,8400000.00,3.50,294000.00,NULL,7500.00,0.00,'At Actuals',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',294000.00,9.00,26460.00,9.00,26460.00,NULL,0.00,346920.00,'<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Taking Client\'s instructions and preparation of design brief.</li><li style=\"box-sizing: border-box;\">Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li style=\"box-sizing: border-box;\">Design and site development.</li><li style=\"box-sizing: border-box;\">Sanitary, plumbing, drainage, water supply and sewerage design.</li><li style=\"box-sizing: border-box;\">Periodic inspection and evaluation of Construction works.</li><li style=\"box-sizing: border-box;\">Structural Engineer Scope.</li><li style=\"box-sizing: border-box;\">Electrical, electronic, communication systems and design.</li><li style=\"box-sizing: border-box;\">Heating, ventilation and air conditioning design (HVAC) and other mechanical systems.</li><li style=\"box-sizing: border-box;\">Elevators, escalators, etc. and Fire detection, Fire protection and Security systems etc.</li></ol>','<ol style=\"box-sizing: border-box; margin-bottom: 10px; color: rgb(51, 51, 51); font-size: 14px;\"><li style=\"box-sizing: border-box;\">Landscape Architecture</li><li style=\"box-sizing: border-box;\">Interior Architecture</li><li style=\"box-sizing: border-box;\">Architectural Conservation</li><li style=\"box-sizing: border-box;\">Retrofitting of Buildings</li><li style=\"box-sizing: border-box;\">Graphic Design and Signage</li><li style=\"box-sizing: border-box;\">Lease Agreements</li><li style=\"box-sizing: border-box;\">Work Pertaining thane RO Office</li><li style=\"box-sizing: border-box;\">Final Fire NOC</li><li style=\"box-sizing: border-box;\">Project Management Consultancy</li><li style=\"box-sizing: border-box;\">Follow-up work for Building Plan Approval and Provisional Fire NOC &amp; Building Completion Certificate.&nbsp;</li></ol>','2022-03-30','',NULL,'<ol><li>GST @ 18% will be charged by us in our Bill.</li><li><span style=\"font-size: 1rem;\">&nbsp;Advance Payment According Stages.</span><br></li><li>Please Note Out of Pocket Expenses Will Be Extra.</li><li>**Cost of Construction Rs.2100/-** is for calculations purpose and is subject to change as per total cost of construction.&nbsp;</li><li>Visualizing Fees, per view Rs.15000/-.&nbsp;</li></ol>',NULL,'Verbal Communication ','Array','','Open',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-06-04 11:37:59','2022-06-04',1,'2022-03-24 17:20:54',36),
(49,'Q2021-22/00046','2022-03-25','P00066','2022-07-21',7,1,154,54,'Mr. M.M. Bhate','PAL 202 Project','PAL 202 Project, ','47, MIDC Area, Dhatav, Tal. Roha,','Raigad','402116','9820615622',NULL,'bhate@sncl.com','Percentage Basis',NULL,2100.00,21000.00,44100000.00,3.00,1323000.00,10,2500.00,0.00,'At Actuals',0.00,'At Actuals',0.00,'Lumsum',0.00,'Lumsum',0.00,'At Actuals',0.00,'Lumsum',1323000.00,9.00,119070.00,9.00,119070.00,NULL,0.00,1561140.00,'<ol><li>Taking Client\'s instructions and preparation of design brief.</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</li><li>Design and site development.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.</li><li>Periodic inspection and evaluation of Construction works.</li><li>Structural Engineer Scope.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems.</li><li>Elevators, escalators, etc. and Fire detection, Fire protection and Security systems etc.</li></ol>','<ol><li>Landscape Architecture</li><li> Interior Architecture</li><li> Architectural Conservation</li><li>Retrofitting of Buildings</li><li>Graphic Design and Signage</li><li> Lease Agreements</li><li> Work Pertaining RO Office</li><li><span style=\"font-size: 1rem;\">&nbsp;Final Fire NOC</span><br></li><li>Project Management Consultancy</li><li>Follow-up work for Building Plan Approval and Provisional Fire NOC &amp; Building Completion Certificate.</li></ol>','2022-03-30','nothing',NULL,'<ol><li>GST @ 18% will be charged by us in our Bill.</li><li>Advance Payment According Stages.</li><li>Please Note Out of Pocket Expenses Will Be Extra.</li><li>**Cost of Construction Rs.2100/-** is for calculations purpose and is subject to change as per total cost of&nbsp;<span style=\"font-size: 1rem;\">construction.</span></li></ol>',NULL,'004/SNC/PAL-202/CON-C/2022-23  Dt.: 01.04.2022','project management.png','4f6c0ccd-321b-4a8b-b608-29caeb540e80','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 13:33:24','2022-07-21',1,'2022-03-25 16:14:52',36),
(50,'Q2021-22/00047','2022-03-30','P00064','2022-07-21',7,1,97,40,'Ankush Ajagekar','Plot No. 3/1, Clock Room','Plot No. 3/1, Clock Room ','Maharashtra','Raigad','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Percentage Basis',NULL,2100.00,2679.70,5627370.00,3.00,168821.10,10,1200.00,0.00,'At Actuals',0.00,'At Actuals',0.00,'At Actuals',0.00,'At Actuals',0.00,'At Actuals',0.00,'At Actuals',80000.00,9.00,8638.90,9.00,8638.90,NULL,0.00,97277.80,'<ol><li>Architect Engineer Scope.\r\n</li><li>Design and Site Development.\r\n</li><li>Taking Client\'s instructions and preparation of design brief.\r\n</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate\r\nenvirons.</li></ol>','<ol><li>Landscape Architecture\r\n</li><li>&nbsp;Structural Engineer Scope.\r\n</li><li>Structural design.\r\n</li><li>&nbsp;Interior Architecture\r\n</li><li>Architectural Conservation\r\n</li><li>Retrofitting of Buildings\r\n</li><li>Graphic Design and Signage\r\n</li><li>Lease Agreements.\r\n</li><li>Work pertaining to Thane RO office.\r\n</li><li>Final Fire NOC.\r\n</li><li>Sanitary, plumbing, drainage, water supply and sewerage design.\r\n</li><li>Periodic inspection and evaluation of Construction works.</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.<br></li></ol>','2022-03-30','nothing',NULL,'<ol><li>Please note out of pocket expenses will be extra.</li><li>Advance payment according to the stages.\r\n</li><li>GST @ 18% will be charged by us in our bill.</li><li><span style=\"font-size: 1rem;\">Site Visit Charges @ 1200/- (Per Visit).&nbsp;</span><br></li></ol>',NULL,'12','Red-Apple-vector-PNG.png','caa41b0c-11f7-4fe8-b8d4-7e2d9506bee6','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 11:56:53','2022-07-21',1,'2022-03-30 14:59:00',36),
(51,'Q2021-22/00048','2022-03-30','P00063','2022-07-21',7,3,97,40,'Ankush Ajagekar','Plot No. 3/1','Plot No. 3/1, ','Maharashtra','Raidag','410208','9594710175',NULL,'ankush.ajagekar@apcotex.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,450000.00,NULL,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',450000.00,9.00,40500.00,9.00,40500.00,NULL,0.00,531000.00,'<p>1. Follow up work for Building Plan Approval.&nbsp;</p><p>2. Follow up work for Provisional Fire NOC.&nbsp;</p><p>3. Follow up work for Pre DCR &amp; Auto DCR for MIDC Plan Approval.&nbsp;</p><p><br></p>','<p>&nbsp;1. Follow up work for Building Completion Certificate.&nbsp;<br></p>','2022-03-30','fgghhjh',NULL,'<p>1. Please note out of pocket expenses will be extra.\r\n</p><p>2. Advance payment according to the stages.\r\n</p><p>3. GST @ 18% will be charged by us in our bill.\r\n</p><p>4. Site Visit Charges @ 1200/- (Per Visit).&nbsp;<br></p>',NULL,'2900000715 dt.: 28.05.2022','Array','bc9006ac-8820-491b-a987-5b8c1463b16d','Converted',NULL,NULL,NULL,NULL,NULL,NULL,2,36,1,'2022-07-21 11:33:13','2022-07-21',1,'2022-03-30 17:52:01',36),
(52,'Q2022-23/00049','2022-04-12','P00058','2022-07-21',7,5,155,NULL,'Mr.Dharmendra Desai','W/72, Badlapur MIDC ','W/72, Badlapur MIDC Industrial Area, ','Badlapur','Thane ','421503','+91 9323394010',NULL,'harkabchemicalsw56c@gmail.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,540000.00,0,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',540000.00,9.00,48600.00,9.00,48600.00,NULL,0.00,637200.00,'<p>1. Working on procedure of allotment of land.</p><p><span style=\"font-size: 1rem;\">2. Allotment of land .</span></p>','<p>ALL THE SERVICES STRICKED BELOW ARE NOT APPLICABLE IN THIS SCOPE.</p><ol><li><strike>&nbsp;Landscape Architecture</strike></li><li><strike>Interior Architecture</strike></li><li><strike>Architectural Conservation</strike></li><li><strike>Retrofitting of Buildings</strike></li><li><strike>Graphic Design and Signage</strike></li><li><strike>Lease Agreements.</strike></li><li><strike>Final fire NOC</strike></li><li><strike>Approval of BCC</strike></li><li><strike>Structural Engineer Scope.</strike></li><li><strike><span style=\"font-size: 1rem;\">Taking Client\'s instructions and preparation of design brief.</span></strike></li><li><span style=\"font-size: 1rem;\"><strike>Site evaluation, analysis and impact of existing and / or proposed development on its immediate environs.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Design and site development.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Structural design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Sanitary, plumbing, drainage, water supply and sewerage design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Periodic inspection and evaluation of Construction works.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Electrical, electronic, communication systems and design.</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>&nbsp;Heating, ventilation and air conditioning design (HVAC) and other mechanical systems</strike></span></li><li><span style=\"font-size: 1rem;\"><strike>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</strike></span></li></ol>',NULL,'sfsdf',NULL,'<p>1.<span style=\"font-size: 1rem;\">&nbsp;Advance Payment According Stages.</span></p><p>2. GST @ 18% will be charged by us in our bill.\r\n</p><p><br></p>',NULL,'234234','Red-Apple-vector-PNG.png','808353d3-550c-43a4-8c8b-22e6999d3220','Converted',NULL,NULL,NULL,NULL,NULL,NULL,0,36,1,'2022-07-21 11:00:54','2022-07-21',1,'2022-04-12 15:28:00',36),
(53,'Q2022-23/00050','2022-05-23','P00057','2022-07-21',7,3,1,19,'Shrinivas D, Shembekar','AOPL Unit 4','Plot No. C 8/6 & 8/7, MIDC Ambernath ','Near AMP Gate, Ambernath','Thane','421506','9820219823',NULL,'shembekar@ambernathorganic.com','Lumpsum',NULL,NULL,NULL,0.00,NULL,125000.00,-1,0.00,0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',0.00,'Lumsum',125000.00,9.00,11250.00,9.00,11250.00,NULL,0.00,147500.00,'<ol><li>Follow up work for Building Plan Approval.\r\n</li><li>Follow up work for Provisional Fire NOC.\r\n</li><li>Follow up work for Pre Dcr &amp; Auto Dcr for MIDC Plan Approval&nbsp;<br></li></ol>','<ol><li>Architect &amp; Structural Engineer Scope.\r\n</li><li>Design and Site Development.\r\n</li><li>Structural design.\r\n</li><li>Taking Client\'s instructions and preparation of design brief.</li><li>Site evaluation, analysis and impact of existing and / or proposed development on its immediate\r\nenvirons.</li><li>Landscape Architecture\r\n</li><li>Interior Architecture\r\n8. Architectural Conservation\r\n</li><li>Retrofitting of Buildings\r\n10. Graphic Design and Signage</li><li>Lease Agreements.\r\n</li><li>Work pertaining to Thane RO office.\r\n</li><li>Final Fire NOC.</li><li>Sanitary, plumbing, drainage, water supply and sewerage design. </li><li>Periodic inspection and evaluation of Construction works.\r\n</li><li>Electrical, electronic, communication systems and design.</li><li>Heating, ventilation and air conditioning design (HVAC) and other mechanical systems\r\n</li><li>Elevators, escalators, etc. Fire detection, Fire protection and Security systems etc.&nbsp;</li><li>Follow up work for Building Completion Certificate\r\n</li><li>Follow up work for Additional FSI (If any).<br></li></ol>',NULL,'noyh',NULL,'<ol><li>Please note out of pocket expenses will be extra. </li><li>Advance payment according stages.</li><li>GST @ 18% will be charged by us in our bill. </li><li>Site Visit Charges will be charged extra by us in\r\nour bill.<br></li></ol>',NULL,'','project management.png','cf0f0eb3-fba9-4248-a20b-b61ba8edd320','Converted',36,'2022-05-25 16:31:25','Quotation no required ',NULL,NULL,NULL,0,36,1,'2022-07-21 08:15:27','2022-07-21',1,'2022-05-23 15:25:38',36);

/*Table structure for table `revisions` */

DROP TABLE IF EXISTS `revisions`;

CREATE TABLE `revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `revision_date` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `revisions` */

insert  into `revisions`(`id`,`project_id`,`revision_date`,`remarks`) values 
(1,42,'2022-03-17','After discussion, quoted rates are revised.&nbsp;'),
(2,42,'2022-03-17','<p>After discussion, quoted rates are revised.&nbsp;</p>'),
(3,34,'2022-03-30','<p>Quotation for only Proposed Shed (Tray Drayer Area)&nbsp;</p>'),
(4,36,'2022-04-11','<p>Revision in Quoted Rates.</p>'),
(5,46,'2022-04-12','<p>Revised due to change in Service&nbsp; to Land Allotment of Open Space.</p>'),
(6,51,'2022-05-05','<p>Revision in rates&nbsp;</p>'),
(7,51,'2022-05-13','<p>Revision in Rate</p>'),
(8,34,'2022-05-17','<p>Revised in Stages and price</p>');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `alias` varchar(64) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`alias`,`created`,`modified`) values 
(1,'Root','root',NULL,NULL),
(2,'Admin','admin',NULL,NULL),
(3,'Business Development Executive','business_development_executive',NULL,'2021-05-31 16:27:28'),
(4,'Business Development Head','business_development_head',NULL,'2021-05-31 16:27:42'),
(5,'Chief Of Marketing','chief_of_marketing',NULL,'2021-03-25 11:48:12'),
(6,'Client Representative','client_representative',NULL,'2021-03-25 11:47:17'),
(7,'Contractor Representative','contractor_representative',NULL,'2021-03-25 11:46:28'),
(8,'HR Manager','hr_manager','2021-03-25 11:42:44','2021-03-25 11:42:44'),
(9,'Project Manager','project_manager','2021-03-25 11:44:55','2021-03-25 11:44:55'),
(10,'Project Engineer','project_engineer','2021-03-25 11:45:40','2021-03-25 11:45:40'),
(11,'Managing Director','managing_director','2021-03-25 11:49:16','2021-03-25 11:49:16'),
(12,'Design and Development','design_development','2022-03-11 10:37:35','2022-03-11 10:37:35');

/*Table structure for table `roles_users` */

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

/*Data for the table `roles_users` */

insert  into `roles_users`(`id`,`user_id`,`role_id`) values 
(1,1,1),
(21,19,6),
(22,20,1),
(25,21,6),
(26,22,6),
(27,23,6),
(28,24,6),
(29,25,6),
(30,26,6),
(31,27,6),
(32,28,6),
(33,29,6),
(34,30,6),
(35,31,6),
(36,32,6),
(37,33,6),
(38,34,6),
(39,35,6),
(40,36,1),
(41,37,6),
(43,39,6),
(44,40,6),
(45,36,2),
(46,36,8),
(47,41,6),
(48,42,6),
(49,43,6),
(50,44,6),
(51,45,6),
(52,46,6),
(53,47,6),
(54,48,6),
(67,54,6),
(68,55,1),
(69,55,11),
(70,38,1),
(71,56,6),
(72,55,2),
(73,55,3),
(74,55,4),
(75,55,5),
(76,55,6),
(77,55,7),
(78,55,8),
(79,55,9),
(80,55,10),
(81,55,12),
(82,53,12),
(83,38,2),
(84,38,3),
(85,38,4),
(86,38,5),
(87,38,6),
(88,38,7),
(89,38,8),
(90,38,9),
(91,38,10),
(92,38,11),
(93,38,12),
(94,57,10),
(95,58,12),
(96,59,6),
(97,60,7),
(98,61,7);

/*Table structure for table `service_invoice_details` */

DROP TABLE IF EXISTS `service_invoice_details`;

CREATE TABLE `service_invoice_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `dis_amt` decimal(10,2) DEFAULT NULL,
  `cgst_rate` decimal(10,2) DEFAULT NULL,
  `cgst_amount` decimal(10,2) DEFAULT NULL,
  `sgst_rate` decimal(10,2) DEFAULT NULL,
  `sgst_amount` decimal(10,2) DEFAULT NULL,
  `igst_rate` decimal(10,2) DEFAULT NULL,
  `igst_amount` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `service_invoice_details` */

insert  into `service_invoice_details`(`id`,`invoice_id`,`task_id`,`description`,`fees`,`discount`,`dis_amt`,`cgst_rate`,`cgst_amount`,`sgst_rate`,`sgst_amount`,`igst_rate`,`igst_amount`,`amount`) values 
(1,1,48,'sdfsdf',500.00,10.00,50.00,10.00,45.00,10.00,45.00,0.00,0.00,540.00),
(2,2,4,'sdfsdf',500.00,10.00,50.00,9.00,40.50,9.00,40.50,0.00,0.00,531.00),
(3,2,5,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',80000.00,10.00,8000.00,9.00,6480.00,9.00,6480.00,8.00,5760.00,90720.00),
(4,3,20,'Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method ofpayments, quality control procedures on materials & works and other conditions of contract.',50000.00,9.00,4500.00,9.00,4095.00,8.00,3640.00,10.00,4550.00,57785.00),
(5,4,5,'Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ',100.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,100.00),
(6,5,19,'Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ',40000.00,10.00,4000.00,9.00,3240.00,9.00,3240.00,9.00,3240.00,45720.00),
(7,6,21,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',60000.00,10.00,6000.00,9.00,4860.00,8.00,4320.00,8.00,4320.00,67500.00),
(8,7,21,'Invite, receive and analyze tenders; advise Client on appointment of contractors.',5000.00,10.00,500.00,10.00,450.00,10.00,450.00,10.00,450.00,5850.00);

/*Table structure for table `service_invoices` */

DROP TABLE IF EXISTS `service_invoices`;

CREATE TABLE `service_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(50) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `amount_before_tax` decimal(10,2) DEFAULT 0.00,
  `cgst` decimal(10,2) DEFAULT 0.00,
  `sgst` decimal(10,2) DEFAULT 0.00,
  `igst` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `received_amount` decimal(10,2) DEFAULT 0.00,
  `amount_in_words` varchar(255) DEFAULT NULL,
  `is_cancel` tinyint(1) DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `service_invoices` */

insert  into `service_invoices`(`id`,`client_id`,`project_id`,`invoice_no`,`invoice_date`,`fees`,`discount`,`amount_before_tax`,`cgst`,`sgst`,`igst`,`total_amount`,`received_amount`,`amount_in_words`,`is_cancel`,`modified`,`created`) values 
(1,2,NULL,'2022-23/00001','2022-06-08',NULL,NULL,450.00,45.00,45.00,0.00,540.00,0.00,'Rupees Five Hundred Forty and Zero paise only',1,'2022-06-13 10:11:38','2022-06-08 12:03:27'),
(2,1,NULL,'2022-23/S00002','2022-06-10',80500.00,8050.00,72450.00,6520.50,6520.50,5760.00,91251.00,0.00,'Rupees Ninety One Thousand Two Hundred Fifty One and Zero paise only',0,'2022-06-22 18:01:36','2022-06-08 13:08:01'),
(3,1,1,'2022-23/S00003','2022-06-06',50000.00,4500.00,45500.00,4095.00,3640.00,4550.00,57785.00,0.00,'Rupees Fifty Seven Thousand Seven Hundred Eighty Five and Zero paise only',0,'2022-06-11 17:29:43','2022-06-08 17:32:12'),
(4,1,1,'2022-23/S00004','2022-06-09',100.00,0.00,100.00,0.00,0.00,0.00,100.00,0.00,'Rupees One Hundred and Zero paise only',0,'2022-06-11 16:06:56','2022-06-09 10:06:33'),
(5,72,2,'2022-23/S00005','2022-06-13',40000.00,4000.00,36000.00,3240.00,3240.00,3240.00,45720.00,15200.00,'Rupees Forty Five Thousand Seven Hundred Twenty and Zero paise only',0,'2022-07-20 14:10:58','2022-06-10 15:35:36'),
(6,73,14,'2022-23/S00006','2022-06-07',60000.00,6000.00,54000.00,4860.00,4320.00,4320.00,67500.00,0.00,'Rupees Sixty Seven Thousand Five Hundred and Zero paise only',0,'2022-06-22 18:01:51','2022-06-10 15:36:31'),
(7,1,1,'2022-23/S00007','2022-06-06',5000.00,500.00,4500.00,450.00,450.00,450.00,5850.00,100.00,'Rupees Five Thousand Eight Hundred Fifty and Zero paise only',0,'2022-07-07 11:17:02','2022-06-11 14:55:55');

/*Table structure for table `service_receipt_invoices` */

DROP TABLE IF EXISTS `service_receipt_invoices`;

CREATE TABLE `service_receipt_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_receipt_id` int(11) DEFAULT NULL,
  `service_invoice_id` int(11) DEFAULT NULL,
  `received_amount` decimal(10,2) DEFAULT 0.00,
  `tds` decimal(10,2) DEFAULT 0.00,
  `total_received_amount` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `service_receipt_invoices` */

insert  into `service_receipt_invoices`(`id`,`service_receipt_id`,`service_invoice_id`,`received_amount`,`tds`,`total_received_amount`) values 
(25,25,7,50.00,50.00,100.00),
(28,28,5,4000.00,2000.00,6000.00),
(29,29,5,9000.00,200.00,9200.00);

/*Table structure for table `service_receipts` */

DROP TABLE IF EXISTS `service_receipts`;

CREATE TABLE `service_receipts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(20) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `payment_mode` enum('Cash','Bank','Bank Transfer') DEFAULT NULL,
  `transaction_no` varchar(50) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `cheque_no` varchar(6) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `amount_in_words` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `service_receipts` */

insert  into `service_receipts`(`id`,`receipt_no`,`receipt_date`,`client_id`,`payment_mode`,`transaction_no`,`transaction_date`,`cheque_no`,`cheque_date`,`bank_name`,`amount`,`amount_in_words`,`modified_by`,`modified`,`created_by`,`created`) values 
(25,'2021-22/S00001','2022-03-16',1,'Cash','',NULL,'',NULL,'',100.00,'Rupees One Hundred and Zero paise only',1,'2022-06-22 18:10:34',1,'2022-06-22 18:10:34'),
(28,'2022-23/S00001','2022-06-14',72,'Cash','',NULL,'',NULL,'',6000.00,'Rupees Six Thousand and Zero paise only',1,'2022-06-24 05:37:58',1,'2022-06-24 05:37:58'),
(29,'2022-23/S00002','2022-07-20',72,'Cash','',NULL,'',NULL,'',9200.00,'Rupees Nine Thousand Two Hundred and Zero paise only',1,'2022-07-20 14:10:58',1,'2022-07-20 14:10:58');

/*Table structure for table `site_visit_details` */

DROP TABLE IF EXISTS `site_visit_details`;

CREATE TABLE `site_visit_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_visit_id` int(11) DEFAULT NULL,
  `check_list_section_id` int(11) DEFAULT NULL,
  `check_list_item_id` int(11) DEFAULT NULL,
  `as_per_drawing` tinyint(1) DEFAULT 0,
  `remarks` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `site_visit_details` */

insert  into `site_visit_details`(`id`,`site_visit_id`,`check_list_section_id`,`check_list_item_id`,`as_per_drawing`,`remarks`) values 
(3,3,4,30,1,'hjhj'),
(4,4,2,17,1,'hjhj'),
(5,5,2,18,1,'hjhj'),
(6,6,3,23,1,'hjhj'),
(7,8,3,25,1,'hjhj'),
(8,10,2,18,1,'hjhj'),
(9,11,1,1,1,''),
(12,12,2,17,1,'hjhj'),
(36,37,3,24,1,'hjhj'),
(37,38,3,24,1,'hjhj'),
(38,39,4,29,1,'hjhj');

/*Table structure for table `site_visit_images` */

DROP TABLE IF EXISTS `site_visit_images`;

CREATE TABLE `site_visit_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_visit_id` int(11) DEFAULT NULL,
  `caption` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_dir` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Data for the table `site_visit_images` */

insert  into `site_visit_images`(`id`,`site_visit_id`,`caption`,`remarks`,`image`,`image_dir`) values 
(2,3,'hiiii','hiiiiii','digital-marketing-blog-1.jpg','4416c466-e6e2-4695-993d-b40ac9ee5a30'),
(3,4,'hiiii','hiiiiii','download.jpg','5479039c-7f20-4fed-8ee6-24d722d1b77d'),
(4,5,'hiiii','hiiiiii','download.jpg','a26f6423-f72f-4976-92b7-beb45cfa36e6'),
(5,6,'hiiii','hiiiiii','Red-Apple-vector-PNG.png','3818d44d-b877-45ac-a2cf-ded66c7e169c'),
(7,8,'sf','sdf','Red-Apple-vector-PNG.png','1474c8f7-e40e-4a15-ba2d-9f7c1d567e64'),
(24,10,'hiii','jiko','Red-Apple-vector-PNG.png','72a69b28-fb23-4fee-bda6-37acc4d2bc16'),
(26,11,'hiii','jiko','Red-Apple-vector-PNG.png','e0956ab0-bcf1-40f5-b4a4-895e46c5ac4a'),
(27,12,'hiiii','hiiiiii','project management.png','3386d768-d8a0-4ec8-94c7-f9c53f645c45'),
(38,28,NULL,'hiiiiii',NULL,NULL),
(48,37,'hiiii','hiiiiii','Red-Apple-vector-PNG.png','aa35852a-b1c4-4c32-a4c4-437b51f2634d'),
(49,37,'hiii','jiko','project management.png','2c819216-120b-482f-8051-b67938701ed9'),
(50,38,'hiiii','hiiiiii','project management.png','c7422966-9fb0-4cf0-96e1-a63a427a7c94');

/*Table structure for table `site_visit_stakeholders` */

DROP TABLE IF EXISTS `site_visit_stakeholders`;

CREATE TABLE `site_visit_stakeholders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_visit_id` int(11) DEFAULT NULL,
  `stakeholder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

/*Data for the table `site_visit_stakeholders` */

insert  into `site_visit_stakeholders`(`id`,`site_visit_id`,`stakeholder_id`) values 
(30,37,60),
(32,39,60),
(33,39,61);

/*Table structure for table `site_visit_visitors` */

DROP TABLE IF EXISTS `site_visit_visitors`;

CREATE TABLE `site_visit_visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_visit_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

/*Data for the table `site_visit_visitors` */

insert  into `site_visit_visitors`(`id`,`site_visit_id`,`name`,`email`) values 
(25,37,'sagar','vs10301030@gmail.cpm'),
(26,37,'vinod','vs10301030@gmail.cpm'),
(27,37,'dipak','vs10301030@gmail.cpm'),
(28,38,'sagar','vs10201020@gmail.cpm'),
(29,38,'vinod','vs10201020@gmail.cpm'),
(30,39,'sagar','vs10301030@gmail.cpm'),
(31,39,'vinod','vs10301030@gmail.cpm'),
(35,38,'dipak','vs10301030@gmail.cpm');

/*Table structure for table `site_visits` */

DROP TABLE IF EXISTS `site_visits`;

CREATE TABLE `site_visits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visit_no` varchar(20) DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `visit_time` time DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `report` text DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `site_visits` */

insert  into `site_visits`(`id`,`visit_no`,`visit_date`,`visit_time`,`employee_id`,`project_id`,`report`,`modified_by`,`modified`,`created_by`,`created`) values 
(3,'2021-22/S00001','2022-03-09','08:15:00',20,2,'tggdt',1,'2022-06-23 09:37:16',1,'2022-06-23 09:37:16'),
(4,'2022-23/S00003','2022-06-23','08:30:00',36,2,'fggfgh',1,'2022-06-23 09:37:52',1,'2022-06-23 09:37:52'),
(5,'2022-23/S00004','2022-06-24','20:00:00',36,1,'hgjjgh',1,'2022-06-23 11:42:13',1,'2022-06-23 11:42:13'),
(6,'2022-23/S00005','2022-06-23','00:00:23',36,1,'ghgf',1,'2022-06-23 12:06:31',1,'2022-06-23 12:01:07'),
(7,'2021-22/S00002','2022-02-02','00:00:00',20,1,'',1,'2022-07-07 12:58:49',1,'2022-06-23 12:38:01'),
(8,'2022-23/S00006','2022-06-24','08:15:00',20,1,'',1,'2022-07-07 12:19:15',1,'2022-06-24 07:55:49'),
(10,'2022-23/S00007','2022-06-24','08:30:00',53,1,'',1,'2022-07-07 12:59:36',1,'2022-06-24 08:55:31'),
(11,'2022-23/S00008','2022-06-24','08:15:00',53,1,'',1,'2022-07-07 13:13:04',1,'2022-06-24 09:42:50'),
(12,'2022-23/S00009','2022-07-07','08:45:00',36,2,'',1,'2022-07-07 13:17:02',1,'2022-07-07 13:17:02'),
(28,'2022-23/S00010','2022-07-22','08:30:00',36,1,'',1,'2022-07-22 08:45:44',1,'2022-07-22 08:45:44'),
(37,'2022-23/S00011','2022-07-22','08:15:00',36,53,'azsx',1,'2022-07-22 11:42:32',1,'2022-07-22 09:24:26'),
(38,'2021-22/00002/S0010','2022-07-22','08:30:00',36,2,'hiii',1,'2022-07-23 08:56:08',1,'2022-07-22 13:05:33'),
(39,'P00057/S0012','2022-07-22','09:00:00',38,53,'kol',1,'2022-07-22 14:14:15',1,'2022-07-22 13:13:52');

/*Table structure for table `stakeholders` */

DROP TABLE IF EXISTS `stakeholders`;

CREATE TABLE `stakeholders` (
  `id` int(11) unsigned NOT NULL,
  `contractor` varchar(255) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `pan_no` varchar(30) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  `office_address_line_1` varchar(255) DEFAULT NULL,
  `office_address_line_2` varchar(255) DEFAULT NULL,
  `office_pincode` int(6) DEFAULT NULL,
  `office_city` varchar(150) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `mobile_1` varchar(15) DEFAULT NULL,
  `mobile_2` varchar(15) DEFAULT NULL,
  `landline` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alternate_email` varchar(150) DEFAULT NULL,
  `created_by` datetime DEFAULT NULL,
  `modified_by` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stakeholders` */

insert  into `stakeholders`(`id`,`contractor`,`gst_no`,`pan_no`,`address_line_1`,`address_line_2`,`city`,`pincode`,`office_address_line_1`,`office_address_line_2`,`office_pincode`,`office_city`,`contact_person`,`mobile_1`,`mobile_2`,`landline`,`email`,`alternate_email`,`created_by`,`modified_by`) values 
(60,'sumit','458697','457896','RAHUL NAGAR NO 2, PIPELINE, OPP PRIYADARSHINI','','MUMBAI',400022,'RAHUL NAGAR NO 2, PIPELINE, OPP PRIYADARSHINI','RAHUL NAGAR NO 2, PIPELINE, OPP PRIYADARSHINI',400022,'MUMBAI',NULL,'8976931445','8596741523','','vs10201020@gmail.cpm',NULL,'1970-01-01 00:00:01','1970-01-01 00:00:01'),
(61,'sumit gaikwad','458697','457896','RAHUL NAGAR NO 2, PIPELINE, OPP PRIYADARSHINI','RAHUL NAGAR NO 2, PIPELINE,','MUMBAI',400022,'RAHUL NAGAR NO 2, PIPELINE, OPP PRIYADARSHINI','RAHUL NAGAR NO 2, PIPELINE,',400022,'MUMBAI',NULL,'8976931431','','','admin@mbf.com',NULL,'1970-01-01 00:00:01','1970-01-01 00:00:01');

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task` varchar(255) DEFAULT NULL,
  `fees_type` enum('Rate','Fixed') DEFAULT NULL,
  `service` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

/*Data for the table `tasks` */

insert  into `tasks`(`id`,`task`,`fees_type`,`service`,`description`,`modified`,`created`) values 
(3,'Designing','Rate',NULL,NULL,'2021-03-24 11:55:11','2021-03-24 11:55:11'),
(4,'Landscape Designing','Fixed',NULL,NULL,'2021-03-24 11:55:35','2021-03-24 11:55:35'),
(5,'Retainer','Rate','','Retainer is an advance payment from our total fees, and should be released after appointment / signing of agreement & acceptance of offer. ','2022-01-18 17:52:31','2021-04-02 11:28:00'),
(6,'Rough Working Drawing','Rate',NULL,NULL,'2021-04-02 11:28:09','2021-04-02 11:28:09'),
(7,'Physical Work Completed','Rate',NULL,NULL,'2021-04-02 11:28:18','2021-04-02 11:28:18'),
(8,'Final Report','Rate',NULL,NULL,'2021-04-02 11:28:27','2021-04-02 11:28:27'),
(9,'Architectural Consulting','Rate',NULL,NULL,'2021-05-05 16:23:36','2021-05-05 16:23:36'),
(10,'Structural Consulting','Rate',NULL,NULL,'2021-05-05 16:24:19','2021-05-05 16:24:19'),
(11,'Follow up work for MIDC Building Plan Approval','Fixed',NULL,NULL,'2021-05-05 16:24:39','2021-05-05 16:24:39'),
(12,'Follow up work for Fire NOC','Fixed',NULL,NULL,'2021-05-05 16:24:53','2021-05-05 16:24:53'),
(13,'Follow up work for Additional FSI','Fixed',NULL,NULL,'2021-05-05 16:25:04','2021-05-05 16:25:04'),
(14,'Follow up work for OC','Fixed','','','2022-01-12 14:49:27','2021-05-05 16:25:14'),
(15,'Pre-DCR & Auto DCR Plan Approval','Fixed',NULL,NULL,'2021-05-05 16:25:27','2021-05-05 16:25:27'),
(16,'Conceptual Design ( CD ) ','','','Ascertain clients requirement, examine site constraints & potential, and prepare a design brief for Clients approval. \r\nPrepare report on site evaluation, state of existing buildings, if any, and analysis and impact of existing and or proposed development on its immediate environs. \r\nPrepare drawings and documents to enable the client to get done the detailed survey and soil investigation at the site of the project. \r\nFinished report on measures required to be taken to mitigate the adverse impact, if any, of the existing and /or proposed development on its immediate environs.\r\nPrepare conceptual design with reference to requirements given and prepare rough estimate of cost on area basis. ','2022-01-14 13:35:03','2021-05-07 19:57:45'),
(17,'Preliminary Design ( PD )','','','Modify the conceptual designs incorporating required changes and prepare the preliminary drawings, sketches, study model, etc., for the Client\'s approval along with preliminary estimate of cost on area basis. \r\n','2022-01-14 13:36:10','2021-05-07 19:58:05'),
(18,'Final Design ','','Service','Modify the Preliminary designs incorporating required changes and prepare the Final drawings, sketches, study model, etc., for the Client','2021-12-17 15:01:20','2021-05-07 19:58:27'),
(19,'Statutory Design ','','','Prepare drawings necessary for Client\'s/ statutory approvals and ensure compliance with codes, standards and legislation, as applicable and assist the Client in obtaining the statutory approvals thereof, if required. ','2022-01-14 13:37:57','2021-05-07 19:58:49'),
(20,'Working Drawings','','','Prepare working drawings, specifications and schedule of quantities sufficient to prepare estimate of cost and tender documents including code of practice covering aspects like mode of measurement, method of\r\npayments, quality control procedures on materials & works and other conditions of contract.\r\n','2021-12-17 14:59:15','2021-05-07 19:59:08'),
(21,'Appointment of Contractors ','','','Invite, receive and analyze tenders; advise Client on appointment of contractors.','2021-12-17 14:58:35','2021-05-07 19:59:41'),
(22,'Construction Stage - Completion of 20 % of work','',NULL,NULL,'2021-05-07 20:00:21','2021-05-07 20:00:21'),
(23,'Construction Stage - Completion of 40 % of work','',NULL,NULL,'2021-05-07 20:00:40','2021-05-07 20:00:40'),
(24,'Construction Stage - Completion of 60 % of work','',NULL,NULL,'2021-05-07 20:00:54','2021-05-07 20:00:54'),
(25,'Construction Stage - Completion of 80 % of work','',NULL,NULL,'2021-05-07 20:01:09','2021-05-07 20:01:09'),
(26,'Construction Stage - Completion of 100 % of work','',NULL,NULL,'2021-05-07 20:01:39','2021-05-07 20:01:39'),
(27,'Completion Stage','','','After BCC issued. All as built of drawings will be issued to the developer. Prepare and submit completion reports and drawings for the project as required and assist the Client in obtaining \"Completion/ Occupancy\r\nCertificate\" from statutory authorities, wherever required. Issue two sets of as built drawings including services and structures','2021-12-17 14:57:33','2021-05-07 20:02:02'),
(28,'Follow up for Demolition Plan','',NULL,NULL,'2021-05-07 21:12:26','2021-05-07 21:12:26'),
(29,'Follow up for Building Plan Approval','',NULL,NULL,'2021-05-07 21:12:56','2021-05-07 21:12:56'),
(30,'Follow up for Pre-DCR and Auto-DCR process','',NULL,NULL,'2021-05-07 21:13:45','2021-05-07 21:13:45'),
(31,'Follow up for Provisional Fire NOC','',NULL,NULL,'2021-05-07 21:14:10','2021-05-07 21:14:10'),
(32,'Follow for Building Completion Certificate','',NULL,NULL,'2021-05-07 21:14:32','2021-05-07 21:14:32'),
(33,'Total Station Survey - Entire Plot','',NULL,NULL,'2021-05-07 21:24:47','2021-05-07 21:24:47'),
(34,'Internal Dimensioning of Each flat / Shop','',NULL,NULL,'2021-05-07 21:25:03','2021-05-07 21:25:03'),
(35,'Area Certificate','',NULL,NULL,'2021-05-07 21:25:23','2021-05-07 21:25:23'),
(36,'Construction Stage',NULL,NULL,NULL,'2021-09-18 12:40:32','2021-09-18 12:40:32'),
(37,'Follow up work for Lease Agreement Approval ',NULL,NULL,NULL,'2021-09-21 18:29:02','2021-09-21 18:29:02'),
(38,'On Receipt of Regularization of Structure	',NULL,NULL,NULL,'2021-09-24 13:30:54','2021-09-24 13:30:54'),
(39,'Follow up work for Empaneled Services of the Architect',NULL,NULL,NULL,'2021-10-13 10:56:54','2021-10-13 10:56:54'),
(40,'Follow up work for Documention',NULL,NULL,NULL,'2021-10-23 17:00:48','2021-10-23 17:00:48'),
(41,'Certification Fees ',NULL,NULL,NULL,'2021-10-23 17:02:28','2021-10-23 17:02:28'),
(42,'On Completion of 1st Crossing',NULL,NULL,NULL,'2021-10-29 13:23:01','2021-10-29 13:23:01'),
(43,'On Completion of 2nd Crossing',NULL,NULL,NULL,'2021-10-29 13:23:19','2021-10-29 13:23:19'),
(44,'On Completion of 3rd Crossing',NULL,NULL,NULL,'2021-10-29 13:23:44','2021-10-29 13:23:44'),
(45,'Survey',NULL,NULL,NULL,'2021-12-03 11:52:46','2021-12-03 11:52:46'),
(46,'Land Allotment',NULL,'','','2022-04-13 11:37:04','2021-12-03 11:56:35'),
(47,'Registration ',NULL,NULL,NULL,'2021-12-03 11:56:56','2021-12-03 11:56:56'),
(48,'Amalgamation',NULL,NULL,NULL,'2021-12-03 11:58:06','2021-12-03 11:58:06'),
(49,'Project Management Charges at Stage 1',NULL,'','','2021-12-18 13:10:56','2021-12-18 13:10:56'),
(50,'Project Management Charges at Stage 2',NULL,'','','2021-12-18 13:11:11','2021-12-18 13:11:11'),
(51,'Project Management Charges at Stage 3',NULL,'','','2021-12-18 13:11:24','2021-12-18 13:11:24'),
(52,'Project Management Charges at Stage 4',NULL,'','','2021-12-18 13:11:39','2021-12-18 13:11:39'),
(53,'Land Allotment - Stage 1',NULL,'Completion of Stage 1','','2022-04-13 11:37:32','2022-01-07 17:46:23'),
(54,'Land Allotment - Stage 2',NULL,'Completion of Stage 2','','2022-04-13 11:37:22','2022-01-07 17:50:07'),
(55,'Completion of Stage - 1',NULL,'','','2022-01-07 17:55:08','2022-01-07 17:55:08'),
(56,'Completion of Stage - 2',NULL,'','','2022-01-07 17:55:46','2022-01-07 17:55:46'),
(57,'Completion of Stage - 3',NULL,'','','2022-01-07 17:56:16','2022-01-07 17:56:16'),
(58,'Consulting Fees for EC Purpose Drawings. ',NULL,'','Fees for EC Purpose Drawing. ','2022-01-19 15:54:49','2022-01-19 13:39:27');

/*Table structure for table `tiny_auth_acl_rules` */

DROP TABLE IF EXISTS `tiny_auth_acl_rules`;

CREATE TABLE `tiny_auth_acl_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `role` varchar(50) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`,`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tiny_auth_acl_rules` */

insert  into `tiny_auth_acl_rules`(`id`,`type`,`role`,`path`,`created`,`modified`) values 
(1,1,'root','TinyAuthBackend.Auth::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(2,1,'root','TinyAuthBackend.Allow::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(3,1,'root','TinyAuthBackend.Acl::*','2021-02-26 11:05:37','2021-02-26 11:05:37'),
(4,1,'*','Admin/Dashboard::*','2021-02-26 11:09:35','2021-02-27 17:19:15');

/*Table structure for table `tiny_auth_allow_rules` */

DROP TABLE IF EXISTS `tiny_auth_allow_rules`;

CREATE TABLE `tiny_auth_allow_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tiny_auth_allow_rules` */

insert  into `tiny_auth_allow_rules`(`id`,`type`,`path`,`created`,`modified`) values 
(1,1,'Applications::apply','2021-02-26 11:21:43','2021-02-26 11:21:43');

/*Table structure for table `tiny_auth_backend_phinxlog` */

DROP TABLE IF EXISTS `tiny_auth_backend_phinxlog`;

CREATE TABLE `tiny_auth_backend_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tiny_auth_backend_phinxlog` */

insert  into `tiny_auth_backend_phinxlog`(`version`,`migration_name`,`start_time`,`end_time`,`breakpoint`) values 
(20191027092406,'TinyAuthBackendDefault','2021-02-26 11:05:36','2021-02-26 11:05:36',0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `api_key_plain` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`mobile`,`password`,`activation_key`,`api_key_plain`,`api_key`,`active`,`created`,`modified`) values 
(1,'root','root@dominion.com','9820592630','$2y$10$YzWKCsKp7Fim/rYaZsKkV.Vdb8b6DKq8NFlrYgkgsT8HAbjgwcYzy',NULL,NULL,NULL,1,'2018-08-21 09:48:02','2021-05-26 16:28:53'),
(19,'Shrinivas D, Shembekar','shembekar@ambernathorganic.com','9820219823','$2y$10$psMojxLExHnFLicR9/TvE.nEdFb5RHyiEoqZCVvDBlBRmG.gCIOf.',NULL,NULL,NULL,1,'2021-09-18 11:32:15','2021-09-18 11:38:22'),
(20,'Madhurani Joshi','accounts@archkdpl.com','9920951531','$2y$10$TJOP1p3JQWH1taBV6jGh7OGai6.SAUwCyKFTOKVQXSmVMvpynM6.u',NULL,NULL,NULL,1,'2021-09-21 16:30:17','2022-04-20 11:03:30'),
(21,'Ganesh Kinikar','kinikar701@outlook.com','9892479614','$2y$10$yrLEkfTITJc0hDcc516hGO6lkDYv3vEweNvZvMSNv5S8719rokuVW',NULL,NULL,NULL,1,'2021-09-21 16:43:37','2021-09-21 16:43:37'),
(22,'Pradeep Choudhary','pradip.chaudhari@aarti-industries.com','7276463301','$2y$10$eihYcXTVC/zVpk/DafJS.ui87sQkdDOZ66lXsUr3M4we89aRFvY.u',NULL,NULL,NULL,1,'2021-09-21 18:07:25','2021-09-21 18:07:25'),
(23,'Pradeep Choudhary','pradip.chaudhari1@aarti-industries.com','','$2y$10$WmP3/83GIte3WU7r4ARg5e7i/O4dLi1DX/PgNUDnp6mHRy.p4akF2',NULL,NULL,NULL,1,'2021-09-24 13:19:03','2022-01-24 17:25:01'),
(24,'Sunil Joshi','chemstarindia@gmail.com','9820979016','$2y$10$R.Phvd7EnrWgbVWBvXiryuAAapHfqPGLIV./UYB48xScmfQJRZ/Z.',NULL,NULL,NULL,1,'2021-09-24 16:14:14','2021-09-24 16:14:14'),
(25,'Mr. Bhaskar','admin@unilabchem.com','7738000360','$2y$10$v53uD3AeugUMLVTZbn4kh.hyE6ZG8Bug0RekU9ViMG2Oj5IPCq9DC',NULL,NULL,NULL,1,'2021-09-27 17:41:00','2021-09-27 17:41:00'),
(26,'Mr. Bhaskar','admin1@unilabchem.com','9820030780','$2y$10$IQcfk0JRyhMU.w5nTo.7QugnQSTFe1z4iEFBq1ThHc8dvNA9IoHu6',NULL,NULL,NULL,1,'2021-09-27 17:45:08','2021-11-23 14:47:39'),
(27,'Balabhadra samantara','balabhadra.s@apcotex.com','9136323494','$2y$10$YTyK9Q7XxhhK/9B3/UYcKeWYQq.8v.MZm0L/sMDzncUroL.zX54DW',NULL,NULL,NULL,1,'2021-10-11 18:26:29','2021-10-11 18:26:29'),
(28,'Hariharan','shariharan@igpetro.com','9892908402','$2y$10$6amd3.b3DLPu5Wi7jh7GROzhPRQLjg/V9VHYZjWZkhxbz5Erd/jte',NULL,NULL,NULL,1,'2021-10-18 13:32:43','2021-12-14 10:58:27'),
(29,'Vinayak Patil','vpatil@gharda.com','9167525471','$2y$10$0ElpRico8aUpN/783OOpJ.zTA785XdNPLOziL4EOy1HlY.yzEhK9.',NULL,NULL,NULL,1,'2021-10-23 16:40:42','2021-10-23 16:40:42'),
(30,'Ganpat Patil','patilaic@gmail.com','9322282270','$2y$10$HuGARcKo4XFYWXrqGQmOGeo.63VjM9OXjjQsdTgaAJCLEJr4IaFtW',NULL,NULL,NULL,1,'2021-10-29 09:44:43','2021-10-29 09:44:43'),
(31,'Sachetan Gharat','roshni.jaiswal@lodhagroup.com','022-61334213','$2y$10$tbcqSewucRf.U7ZMV5vJSeaIhH3Y.UJvH6GTrJ0YJ/LkPRjNnJMpO',NULL,NULL,NULL,1,'2021-10-29 13:19:02','2021-10-29 13:19:02'),
(32,'Mr. Prasoon Kumar Shah','chemsynthlabs2021@gmail.com','9890085085','$2y$10$KHkcIufItCDz4.ZwAB7m3edddZ1CbzW.PEi120Nll9AwAFqPXws7e',NULL,NULL,NULL,1,'2021-11-12 17:59:13','2021-11-12 17:59:13'),
(33,'Mr Sanjeev Kumar Mandholia','MEDIAIRHEALTHCARE@GMAIL.COM','9833997465','$2y$10$Nbf039Oem1yiki2n0plXM.8JYslfe/sBRAArsI/AUeETb160g239K',NULL,NULL,NULL,1,'2021-12-03 16:10:10','2021-12-03 16:14:15'),
(34,'Mr. Vinayak A Patil','nabhalerao@gharda.com','0000','$2y$10$DqT7fyOzY1pjxbXy5MIFpObzjdPLeDnmOnIx3CR5WhCtQJHF5nQMm',NULL,NULL,NULL,1,'2021-12-08 10:22:02','2021-12-08 10:22:02'),
(35,'Mr. Niranjan Athalye','niranjan.athalye@gmail.com','9920713679','$2y$10$tQ4xWzUPp3PML7gIot3K2uGvOrNOdsi9nui.y.e7/INk8YW5p.4fS',NULL,NULL,NULL,1,'2021-12-17 12:35:51','2021-12-17 12:36:06'),
(36,'Darpan Powale','info@archkdpl.com','9920951588','$2y$10$sZetiFSuax2kUr0PA.CafuMkQUIHQwF7rh.Jm4O0dwwnW3stJQ/B6',NULL,NULL,NULL,1,'2021-12-17 16:48:08','2022-04-20 11:01:35'),
(37,'Kamlesh Kumar Lader','lader.kk@balmerlawrie.com','7718831931','$2y$10$F8XW3AL4NdZrS.CbHicPseKEaALeGv43z8GVhxmlmS0K6b8PZWcMW',NULL,NULL,NULL,1,'2021-12-18 11:20:31','2021-12-18 11:20:31'),
(38,'Kaustubh Madhav Kashelkar','kaustubh@archkdpl.com','9920951511','$2y$10$Y0NaZWdTCW7U8e4lp0aTNuRCLQ5quLmn/CSD1CKonHQLRee5FL9AC',NULL,NULL,NULL,1,'2021-12-25 14:42:45','2022-04-20 11:00:33'),
(39,'Rohit Soni','rsoni@igpetro.com','9892999047','$2y$10$A/0XYu8N7vZB/NAeeY6kW.IL1jdDc7Qj1rq/j6Gt3J/LOKyT/Tm1i',NULL,NULL,NULL,1,'2021-12-30 08:19:41','2021-12-30 08:19:41'),
(40,'Ankush Ajagekar','ankush.ajagekar@apcotex.com','9594710175','$2y$10$wbhnToiNce7JJalK5DxKq.6XclhRGwXNjrkWunhKyAH9a9EPABkAu',NULL,NULL,NULL,1,'2021-12-30 15:08:12','2021-12-30 15:08:12'),
(41,'Mr. Maruti Darur','marutid@centaurlab.com','0','$2y$10$14GQH.tC7LApvdIRNdJVWu9zNdbNkIGdjBM0n0pefAt3XkzyyPtoO',NULL,NULL,NULL,1,'2022-01-07 16:43:16','2022-01-07 16:51:22'),
(42,'Mr. Maruti Darur','marutid1@centaurlab.com','0000000000','$2y$10$3pIzkZnUrGLSaaEU2hZ6dedUkI1Zd4jbooiEwy1smH25TGXhxtNoK',NULL,NULL,NULL,1,'2022-01-07 16:50:34','2022-01-07 16:51:40'),
(43,'Mr. Ardhendu Purkayastha','ardhendu.purkayastha@acharyagroup.com','00000000000000','$2y$10$xY6LGvDhmmBH9vYSn07Jlu4ZqDp/22N2i0SmfdEBZHEy49VaQuxdy',NULL,NULL,NULL,1,'2022-01-12 14:42:17','2022-01-12 14:42:17'),
(44,'Mr. Shripati','purchase@acharyagroup.com','0000000','$2y$10$XEtZLZs9gLc.o7I07FBW3Oqf25Rgnp9HEzl.QLXxjAgAhYPo4j./C',NULL,NULL,NULL,1,'2022-01-18 17:47:18','2022-01-18 17:47:18'),
(45,'Mr. Ardhendu.Purkayastha','ganapati.s@acharyagroup.com','00000','$2y$10$0KBccyca4mbBM4Ybjd.FpuduhAGvn.EkuVPpKqgvJmZtsyODEQZR6',NULL,NULL,NULL,1,'2022-01-22 14:43:39','2022-01-22 14:43:39'),
(46,'Pradeep Choudhary','ajitkumar.more@aarti-industries.com','000000','$2y$10$oLB48yvklsSvgxHyVRJocuiGCr4QgtRDAgK5KN/PTKms5Oor/srFK',NULL,NULL,NULL,1,'2022-01-24 17:31:42','2022-01-24 17:31:42'),
(47,'Mr.Kaushik ','modernmonarchjv@gmail.com','97692 51479','$2y$10$ui52ThIdRKPmE7MSsVejbe.mxzTe6gfMca8OhwwfeZYGwU6Jsvu3e',NULL,NULL,NULL,1,'2022-01-25 16:53:07','2022-01-25 16:53:07'),
(48,'Mr. Jayesh Awati','ja@alpha-pharma.com','7738251521','$2y$10$bi5kN1/51Nx7wTVUvjmTieluWYZA01YHHLoe8H103vs6c4gaNjNWm',NULL,NULL,NULL,1,'2022-02-23 17:34:51','2022-02-23 17:34:51'),
(53,'Pramod  Prabhakar Sawant','ppsawant@archkdpl.com',NULL,'$2y$10$bkngUieAitOiZXE.8RWhOuWe1eMNL.nt.GtSNV/.M9BKH6.Lnf4BW',NULL,NULL,NULL,1,'2022-03-22 17:40:50','2022-04-20 10:57:49'),
(54,'Mr. M.M. Bhate','bhate@sncl.com','9820615622','$2y$10$QwBmxOCh1R/2UjX.xDXPLu0PPWQZLjKHLZNltX8QA2/WD0Y5/z4g2',NULL,NULL,NULL,1,'2022-03-25 16:07:17','2022-04-05 17:13:32'),
(55,'Madhav Rajaram Kashelkar','madhav@archkdpl.com',NULL,'$2y$10$ZWRuf71S.sxTmj6FaSNzZuZ9Oxb.5YGasPZ9Ds4nruVAOCtEUS0/q',NULL,NULL,NULL,1,'2022-04-06 18:27:01','2022-04-20 10:54:28'),
(56,'Mr.Makarand Joshi','makarand.joshi@anuvi.in','+91 9819884251','$2y$10$xvDMbKb16iEmu91pkTGXG.VJLW9vS/QgpKzor8ABGxjA403lODbKa',NULL,NULL,NULL,1,'2022-04-12 14:19:16','2022-04-12 14:19:16'),
(57,'Amey Atul Kulkarni ','projects@archkdpl.com',NULL,'$2y$10$l.9YvKG74oq.MjomtqbLl.q6El.3fDWqn/3rOWpMLMF5PDcsf.caO',NULL,NULL,NULL,1,'2022-04-20 11:08:56','2022-04-20 14:35:06'),
(58,'Prasad Date','padate@archkdpl.com',NULL,'$2y$10$BzqnKNyKcbwoUeTMcgpFJeE0oYsvNvg1lThutWv4lXxojxsx5/mZ2',NULL,NULL,NULL,1,'2022-04-20 12:09:36','2022-04-20 12:09:36'),
(59,'Mr. Madhav Singh','globe.madhavsingh@gmail.com','+91 9819807070','$2y$10$6ebi9Osqq5dTJ/1qewOCqOhnyMvZ35IvxQRkM6Oc2gubDwvkLqk7G',NULL,NULL,NULL,1,'2022-04-29 13:09:26','2022-04-29 13:12:32'),
(60,'sumit gaikwad','sumit@kom.com','8976931445','$2y$10$qvEH/.n15adlEZ4A.cBg6eMW3sl.9V/CuzaM.iIGwX8tBLKRo1VFC',NULL,NULL,NULL,1,'2022-07-21 06:31:53','2022-07-21 06:31:53'),
(61,NULL,'sumit@komal.com','8976931431','$2y$10$xHUrZbZwRaiWS9AzZLlRAOnOd94YHfVPDoU1nX6ZZwiGbUTaLjW.6',NULL,NULL,NULL,1,'2022-07-21 06:50:58','2022-07-21 06:56:05');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
